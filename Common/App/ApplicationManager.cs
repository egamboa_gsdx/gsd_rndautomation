﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

using TestStack.White.Factory;

using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Configuration;
using TestStack.White.UIItems.WindowItems;
using GSD_RnDAutomation.Common.Methods;

using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Appium.Service;
using GSD_RnDAutomation.Lib.TORRENT;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Android;
using GSD_RnDAutomation.Common.Exceptions;

namespace GSD_RnDAutomation

{
    /// <summary>
    /// ApplicationManager manages the current instrument manager window 
    /// and allows the application to open, close, or switch instrument managers
    /// </summary>
    public class ApplicationManager
    {
        private static string AIXInstallPath = @"C:\Program Files\Gold Standard Diagnostics\AIX1000 Suite\AIX1000InstrumentManager.exe";
        private static string StormInstallPath = @"C:\Program Files\Gold Standard Diagnostics\Storm Suite\StormInstrumentManager.exe";
        private static string TorrentInstallPath = @"C:\Program Files\Gold Standard Diagnostics\Torrent Test Designer\Torrent.TestDesigner.UI.exe";

        private static string TempestFakeInstrumentPath = @"C:\Program Files\Gold Standard Diagnostics\Tempest Fake Instrument\Tempest.FakeInstrument.exe";


        private static TestStack.White.Application application;
        private static AppiumDriver driver;
        private static WebDriverWait waitDriver;
        //private static AppiumLocalService localAppiumService = new AppiumServiceBuilder().UsingPort(4723).Build();
        private static Window applicationWindow;
        private static InstrumentManager instrumentManager;
        private static RackManager rackManager;

        /// <summary>
        /// The TestStack.White Application for the current instrument manager
        /// </summary>
        public static TestStack.White.Application Application
        {
            get { return application; }
        }

        /// <summary>
        /// The TestStack.White Window for the current instrument manager
        /// </summary>
        public static Window ApplicationWindow
        {
            get { return applicationWindow; }
        }

        /// <summary>
        /// The current software version (e.g. Storm/AIX) for the current instrument manager
        /// </summary>
        public static InstrumentManager InstrumentManager
        {
            get { return instrumentManager; }
        }

        /// <summary>
        /// The current rack set up (classic/SIR) for the current instrument manager
        /// </summary>
        public static RackManager RackManager
        {
            get { return rackManager; }
        }

        /// <summary>
        /// Driver to connect to Android Device via Appium
        /// </summary>
        public static AppiumDriver Driver { get { return driver; } }

        /// <summary>
        /// Wait tool to allow android app to catch up to automation
        /// </summary>
        public static WebDriverWait WaitDriver { get { return waitDriver; } }

        /// <summary>
        /// Appium HTTP server connection service. Must be started to begin testing
        /// </summary>
       // public static AppiumLocalService LocalAppiumService { get { return localAppiumService; } }


        /// <summary>
         /// Launches a new instrument manager and logs in.
         /// </summary>
         /// <param name="instrumentManager">The instrument manager to open.</param>
         /// <returns>The Process ID of the new instrument manager.</returns>
         public static int LaunchAndLogin(InstrumentManager instrumentManager, RackManager rackManager = RackManager.classic)
         {
             ApplicationManager.instrumentManager = instrumentManager;
             ApplicationManager.rackManager = rackManager;

             if (instrumentManager == InstrumentManager.Storm)
             {
                 StatusManager.AddStatus("Launching new Storm Instrument Manager");
                 ProcessStartInfo storm = new ProcessStartInfo(StormInstallPath, DataProperty.Default.InstrumentArgument);
                 if (rackManager == RackManager.SIR)
                 {
                     storm = new ProcessStartInfo(StormInstallPath, DataProperty.Default.sirMicroArrayInstrumentArgument);
                 }
                 else if (rackManager == RackManager.bolt)
                 {
                     storm = new ProcessStartInfo(StormInstallPath, DataProperty.Default.boltInstrumentArgument);
                 }
                 application = TestStack.White.Application.Launch(storm);
                LoginMethods.LoginToManager();
            }
            else if (instrumentManager == InstrumentManager.AIX)
            {
                StatusManager.AddStatus("Launching new AIX1000 Instrument Manager");
                ProcessStartInfo aix = new ProcessStartInfo(AIXInstallPath, DataProperty.Default.InstrumentArgument);
                if (rackManager == RackManager.SIR)
                {
                    aix = new ProcessStartInfo(AIXInstallPath, DataProperty.Default.sirInstrumentArgument);
                }
                application = TestStack.White.Application.Launch(aix);
                LoginMethods.LoginToManager();
            }
            else if (instrumentManager == InstrumentManager.Torrent)
            {
                StatusManager.AddStatus("Launching Android Emulator & Tempest Instrument Manager");
                if (Process.GetProcessesByName("Tempest.FakeInstrument").Length == 0)
                {
                    StatusManager.AddStatus("No Instrument connected...");
                    StatusManager.AddStatus("Starting a Fake Tempest Instrument...");
                    ProcessStartInfo fakeInstrument = new ProcessStartInfo(TempestFakeInstrumentPath);
                    application = TestStack.White.Application.Launch(fakeInstrument);
                }
                else
                {
                    application = TestStack.White.Application.Attach("Tempest.FakeInstrument");
                    SetApplicationWindow();
                }

                
                try
                {
                    // Start appium server...hopefully
                   /* if (!localAppiumService.IsRunning)
                    {
                        StatusManager.AddStatus("Starting Appium Server...");
                        localAppiumService.Start();
                    }
                    */

                    // Get Capabilities
                    if (driver == null)
                    {
                        StatusManager.AddStatus("Connecting to  Android Device...");
                        StatusManager.AddStatus("  Platform Name....." + AndroidDeviceProperty.PlatformName);
                        StatusManager.AddStatus("  Platform Version.." + AndroidDeviceProperty.PlatformVersion);
                        StatusManager.AddStatus("  Device Name......." + AndroidDeviceProperty.DeviceName);
                        StatusManager.AddStatus("  UdId.............." + AndroidDeviceProperty.UdId);

                        AppiumOptions cap = new AppiumOptions();

                        cap.AddAdditionalAppiumOption("PlatformName", AndroidDeviceProperty.PlatformName);
                        cap.AddAdditionalAppiumOption("PlatformVersion", AndroidDeviceProperty.PlatformVersion);
                        cap.AddAdditionalAppiumOption("DeviceName", AndroidDeviceProperty.DeviceName);
                        cap.AddAdditionalAppiumOption("udid", AndroidDeviceProperty.UdId);
                        cap.AddAdditionalAppiumOption(MobileCapabilityType.NewCommandTimeout, 60);

                        cap.AddAdditionalAppiumOption("appPackage", "com.gsd.tempest.ui");
                        cap.AddAdditionalAppiumOption("appActivity", "crc647dba07fdb0af043d.MainActivity");

                        Uri url = new Uri("http://127.0.0.1:4723/wd/hub");

                        StatusManager.AddStatus("Starting Tempest App...");

                       // driver = new AndroidDriver(localAppiumService, cap);
                        waitDriver = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
                        //Driver.Close();
                    }
                    else
                    {
                        StatusManager.AddStatus("Already connected to Android device...");
                        StatusManager.AddStatus("Starting Tempest App...");
                        driver.LaunchApp();
                    }

                }
                catch
                {
                    throw new WorkflowException("Could not start Tempest Instrument Manager");
                }

                LoginMethods.LoginToManager();
            }
            else 
            {
                StatusManager.AddStatus("Launching Torrent Test Desinger");
                ProcessStartInfo testdesinger = new ProcessStartInfo(TorrentInstallPath);
                application = TestStack.White.Application.Launch(testdesinger);

            }

            //LoginMethods.LoginToManager();
            SetApplicationWindow();
            return application.Process.Id;
         }

        /// <summary>
        /// Switches to an existing instrument manager.
        /// </summary>
        /// <param name="instrumentManager">The instrument manager to use.</param>
        /// <returns>The Process ID of the new instrument manager.</returns>
        public static int SwitchTo(int processID, InstrumentManager instrumentManager)
        {
            StatusManager.AddStatus("Switching Instrument Managers");

            ApplicationManager.instrumentManager = instrumentManager;
            application = TestStack.White.Application.Attach(processID);
            SetApplicationWindow();
            return application.Process.Id;
        }

        /// <summary>
        /// Switches to an existing instrument manager. Throws argumentEe no managers are found
        /// </summary>
        /// <param name="instrumentManager">The instrument manager to use.</param>
        /// <returns>The Process ID of the new instrument manager.</returns>
        public static int AttemptSwitchTo(InstrumentManager instrumentManager)
        {
            //StatusManager.AddStatus("Attempting to switch Instrument Managers");

            ApplicationManager.instrumentManager = instrumentManager;
            string executableName;
            switch (instrumentManager)
            {

                case InstrumentManager.Storm:
                    StatusManager.AddStatus("Attempting to switch Instrument Managers");
                    executableName = DataProperty.Default.StormExecutableName;
                    break;
                case InstrumentManager.AIX:
                    StatusManager.AddStatus("Attempting to switch Instrument Managers");
                    executableName = DataProperty.Default.AIXExecutableName;
                    break;
                case InstrumentManager.Torrent:
                    executableName = "Tempest.FakeInstrument";
                    break;
                case InstrumentManager.TorrentTestDesigner:
                    StatusManager.AddStatus("Attempting to switch to Torrent Test Designer");
                    executableName = DataProperty.Default.TorrentExecutableName;
                    break;
                default:
                    throw new ArgumentException("Invalid instrument manager selected");
            }
            try
            {
                
                application = TestStack.White.Application.Attach(executableName);
                SetApplicationWindow();
                return application.Process.Id;
            }
            catch
            {
                throw new ArgumentException("No instrument manager found");
            }
        }

        /// <summary>
        /// Switches to an existing instrument manager or launches a new one if none exists and logs in.
        /// </summary>
        /// <param name="instrumentManager">The instrument manager to use.</param>
        /// <returns>The Process ID of the new instrument manager.</returns>
        public static int SwitchOrLaunchAndLogin(InstrumentManager instrumentManager, RackManager rackManager = RackManager.classic)
        {
                //check to see if any instrument managers were open before starting program
                //causes an exception that is not handled currently
            if (ApplicationManager.RackManager == RackManager.undefined || ApplicationManager.InstrumentManager == InstrumentManager.undefined)
            {
                if (Process.GetProcessesByName(DataProperty.Default.StormExecutableName).Length != 0)
                {
                    StatusManager.AddStatus("Previous Storm application left open. Closing and relaunching...");
                    ApplicationManager.AttemptSwitchTo(InstrumentManager.Storm);
                    GUIMethods.Exit();
                }
                if (Process.GetProcessesByName(DataProperty.Default.AIXExecutableName).Length != 0)
                {
                    StatusManager.AddStatus("Previous AIX1000 application left open. Closing and relaunching...");
                    ApplicationManager.AttemptSwitchTo(InstrumentManager.AIX);
                    GUIMethods.Exit();
                }
                if (Process.GetProcessesByName(DataProperty.Default.TorrentExecutableName).Length != 0)
                {
                        StatusManager.AddStatus("Previous Torrent application left open. Closing and relaunching...");
                        ApplicationManager.AttemptSwitchTo(InstrumentManager.TorrentTestDesigner);
                        GUIMethods.Close();
                 }
                
                 return LaunchAndLogin(instrumentManager, rackManager);
            }
            else if(ApplicationManager.InstrumentManager == instrumentManager)
            {
                 //check to see if the correct rack manager is being used for the proper test.
                 //if not the correct rack display (classic/slide in) close and launch a new application
                    StatusManager.AddStatus("Checking instrument manager is setup for correct rack type to test...");
                    if (ApplicationManager.RackManager != rackManager)
                    {
                        StatusManager.AddStatus("Rack being tested does not match Rack Manager...");
                        GUIMethods.Exit();

                        return LaunchAndLogin(instrumentManager, rackManager);
                    }
                    StatusManager.AddStatus("Everything looks good! Just like you! Proceeding...");
            }


            if (instrumentManager == InstrumentManager.Storm)
            {
                Process[] OpenStormManagers = Process.GetProcessesByName(DataProperty.Default.StormExecutableName);

                if (OpenStormManagers.Length == 0)
                {
                    return LaunchAndLogin(instrumentManager, rackManager);
                }

                StatusManager.AddStatus("Switching to open Instrument Manager");

                ApplicationManager.instrumentManager = instrumentManager;
                application = TestStack.White.Application.Attach(OpenStormManagers[0]);
            }
            else if (instrumentManager == InstrumentManager.AIX)
            {
                Process[] OpenAIXManagers = Process.GetProcessesByName(DataProperty.Default.AIXExecutableName);

                if (OpenAIXManagers.Length == 0)
                {
                    return LaunchAndLogin(instrumentManager, rackManager);
                }

                StatusManager.AddStatus("Switching to open Instrument Manager");

                ApplicationManager.instrumentManager = instrumentManager;
                application = TestStack.White.Application.Attach(OpenAIXManagers[0]);
            }
            else 
            {
                Process[] OpenTorrent = Process.GetProcessesByName(DataProperty.Default.TorrentExecutableName);
                if (OpenTorrent.Length == 0)
                {
                    return LaunchAndLogin(instrumentManager);
                }

                StatusManager.AddStatus("Switching to open Torrent Test Designer");

                ApplicationManager.instrumentManager = instrumentManager;
                application = TestStack.White.Application.Attach(OpenTorrent[0]);
            }

            SetApplicationWindow();
            return application.Process.Id;
        } 

        /// <summary>
        /// Gets the main application window.
        /// </summary>
        private static void SetApplicationWindow()
        {
            if (instrumentManager == InstrumentManager.Storm)
            {
                applicationWindow = application.GetWindow(WindowName.Default.StormWindow, InitializeOption.NoCache);
                applicationWindow.DisplayState = DisplayState.Maximized;
                applicationWindow.Focus();
            }
            else if (instrumentManager == InstrumentManager.AIX)
            {
                applicationWindow = application.GetWindow(WindowName.Default.AIXWindow, InitializeOption.NoCache);
                applicationWindow.DisplayState = DisplayState.Maximized;
                applicationWindow.Focus();
            }
            else if (instrumentManager == InstrumentManager.Torrent)
            {
                applicationWindow = application.GetWindow("Fake Instrument", InitializeOption.NoCache);
                applicationWindow.DisplayState = DisplayState.Maximized;
                applicationWindow.Focus();
            }
            else if (instrumentManager == InstrumentManager.TorrentTestDesigner)
            {
                Process p = Process.GetProcessesByName(DataProperty.Default.TorrentExecutableName)[0];

                Task.WaitAll();
                List<Window> windows = application.GetWindows();
                applicationWindow = application.GetWindow(p.MainWindowTitle, InitializeOption.NoCache);
                applicationWindow.Focus();
            }
        }

        /// <summary>
        /// Gets the current version number for the current instrument manager.
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentSoftwareVersion()
        {
            string path;
            switch (instrumentManager)
            {
                case InstrumentManager.Storm:
                    path = StormInstallPath;
                    break;
                case InstrumentManager.AIX:
                    path = AIXInstallPath;
                    break;
                case InstrumentManager.TorrentTestDesigner:
                    path = TorrentInstallPath;
                    break;
                default:
                    return "0.0.0";
            }
            return FileVersionInfo.GetVersionInfo(path).FileVersion;
        }

        /// <summary>
        /// Gets the current version number from the .exe file
        /// </summary>
        /// <returns></returns>
        public static string GetFormattedSoftwareVersionString(InstrumentManager software)
        {
            if (software == InstrumentManager.Storm)
            {
                var StormExePath = StormInstallPath;
                var version = FileVersionInfo.GetVersionInfo(StormExePath);
                return "Storm_" + version.FileVersion;
            }
            else if (software == InstrumentManager.AIX)
            {
                var AIXExePath = AIXInstallPath;
                var version = FileVersionInfo.GetVersionInfo(AIXExePath);
                return "AIX1000_" + version.FileVersion;
            }
            else if (software == InstrumentManager.TorrentTestDesigner)
            {
                var TorrentExePath = TorrentInstallPath;
                var version = FileVersionInfo.GetVersionInfo(TorrentExePath);
                return "TorrentTestDesinger_" + version.FileVersion;
            }

            return "Unknown_Version";
        }

        /// <summary>
        /// Returns the status of the data server on this computer.
        /// Throws ArgumentException when the server is not installed on this computer.
        /// </summary>
        /// <param name="software"></param>
        /// <returns>ServiceControllerStatus of the service</returns>
        public static ServiceControllerStatus GetServerStatus(InstrumentManager software)
        {
            ServiceController service;
            switch (software)
            {
                case InstrumentManager.Storm:
                    service = new ServiceController(DataProperty.Default.StormServerProcessName);
                    break;
                case InstrumentManager.AIX:
                    service = new ServiceController(DataProperty.Default.AIXServerProcessName);
                    break;
                default:
                    throw new ArgumentException("Unknown Software");
            }
            return service.Status;
        }

        /// <summary>
        /// Returns ApplicationManager.Application.GetWindows() or times out after a specified amount of time, throwing an Exception.
        /// Note, this function behaves differently with no parameters.
        /// </summary>
        /// <param name="timeout">The maximum wait time in seconds.</param>
        public static List<Window> TryGetWindows(int timeout)
        {
            // See link for details: https://stackoverflow.com/questions/13513650/how-to-set-timeout-for-a-line-of-c-sharp-code

            Task<List<Window>> task = Task.Run(() => ApplicationManager.Application.GetWindows());
            if (task.Wait(TimeSpan.FromSeconds(timeout)))
            {
                return task.Result;
            }
            else
            {
                throw new Exception("Failed to get windows in " + timeout + " seconds");
            }
        }

        /// <summary>
        /// Returns ApplicationManager.Application.GetWindows(). If the function times out after a set number of attempts, an empty list will be returned instead.
        /// <para>The timeout and number of retries can be configured through AutomationConfiguration.</para>
        /// </summary>
        public static List<Window> TryGetWindows()
        {
            // See link for details: https://stackoverflow.com/questions/13513650/how-to-set-timeout-for-a-line-of-c-sharp-code

            if (ApplicationManager.instrumentManager != InstrumentManager.Torrent)
            {
                int retries = AutomationConfiguration.GetWindowsRetries;
                while (retries > 0)
                {
                    Task<List<Window>> task = Task.Run(() => ApplicationManager.Application.GetWindows());
                    if (task.Wait(TimeSpan.FromMilliseconds(AutomationConfiguration.GetWindowsTimeout)))
                    {
                        return task.Result;
                    }
                    else
                    {
                        retries--;
                    }
                }
            }
            
            return new List<Window>();
        }
    }
}
