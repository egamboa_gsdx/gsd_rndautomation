﻿namespace GSD_RnDAutomation.Configuration
{
    /// <summary>
    /// Defines time constants for the entire GSDAutomationFramework
    /// 
    /// These can be configured in code to increase or decrease the default delays.
    /// </summary>
    public class AutomationConfiguration
    {
        private static int timeDelay = DataProperty.Default.TimeBetweenActions;
        private static int setupTimeDelay = DataProperty.Default.TimeBetweenSetupActions;

        private static bool isSetup = false;

        private static int getWindowsTimeout = DataProperty.Default.GetWindowsTimeout;
        private static int getWindowsRetries = DataProperty.Default.GetWindowsRetries;

        /// <summary>
        /// The time to wait between normal actions in milliseconds
        /// </summary>
        public static int TimeDelay
        {
            get { return timeDelay; }
            set
            {
                if (value >= 0)
                {
                    timeDelay = value;
                }
            }
        }

        /// <summary>
        /// The time to wait between setup actions in milliseconds
        /// </summary>
        public static int SetupTimeDelay
        {
            get { return setupTimeDelay; }
            set
            {
                if (value >= 0)
                {
                    setupTimeDelay = value;
                }
            }
        }

        /// <summary>
        /// When true, wait times will use the SetupTimeDelay instead of TimeDelay.
        /// </summary>
        public static bool IsSetup
        {
            get { return isSetup; }
            set { isSetup = value; }
        }

        /// <summary>
        /// The amount of time to wait (in milliseconds) before timing out the GetWindow call in the ApplicationManager.TryGetWindows() function.
        /// </summary>
        public static int GetWindowsTimeout
        {
            get { return getWindowsTimeout; }
            set { getWindowsTimeout = value; }
        }

        /// <summary>
        /// The number of times to retry the GetWindow call in the ApplicationManager.TryGetWindows() function.
        /// </summary>
        public static int GetWindowsRetries
        {
            get { return getWindowsRetries; }
            set { getWindowsRetries = value; }
        }
    }
}
