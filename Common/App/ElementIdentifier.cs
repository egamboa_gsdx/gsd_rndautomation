﻿using TestStack.White.UIItems.Finders;
using System.Windows.Automation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.ServiceProcess;
using TestStack.White.Factory;
using TestStack.White.UIItems.WindowItems;

namespace GSD_RnDAutomation.Common
{
    public class ElementIdentifier
    {
        private string textValue = null;
        private string className = null;
        private string automationID = null;
        private int index = -1;
        private ControlType controlType = null;

        public ElementIdentifier(string textValue = null, string className = null, string automationID = null, int index = -1, ControlType controlType = null)
        {
            TextValue = textValue;
            ClassName = className;
            AutomationID = automationID;
            Index = index;
            ControlType = controlType;
        }

        // allows implicit conversion to SearchCriteria
        public static implicit operator SearchCriteria(ElementIdentifier e) => e.ToSearchCriteria();

        public SearchCriteria ToSearchCriteria()
        {
            SearchCriteria searchCriteria = null;

            if (textValue != null)
            {
                if (searchCriteria == null)
                {
                    searchCriteria = SearchCriteria.ByText(textValue);
                }
                else
                {
                    searchCriteria.AndByText(textValue);
                }
            }

            if (className != null)
            {
                if (searchCriteria == null)
                {
                    searchCriteria = SearchCriteria.ByClassName(className);
                }
                else
                {
                    searchCriteria.AndByClassName(className);
                }
            }

            if (automationID != null)
            {
                if (searchCriteria == null)
                {
                    searchCriteria = SearchCriteria.ByAutomationId(automationID);
                }
                else
                {
                    searchCriteria.AndAutomationId(automationID);
                }
            }

            // Searching by index does not work as intended
            // Index should be handled using GetMultiple(...)[index] instead

            //if (index != -1)
            //{
            //    if (searchCriteria == null)
            //    {
            //        searchCriteria = SearchCriteria.Indexed(index);
            //    }
            //    else
            //    {
            //        searchCriteria.AndIndex(index);
            //    }
            //}

            if (controlType != null)
            {
                if (searchCriteria == null)
                {
                    searchCriteria = SearchCriteria.ByControlType(controlType);
                }
                else
                {
                    searchCriteria.AndControlType(controlType);
                }
            }

            if (searchCriteria == null)
            {
                return SearchCriteria.All;
            }

            return searchCriteria;
        }

        // Getters and Setters

        public string TextValue
        {
            get { return textValue; }
            set
            {
                if (textValue == null)
                    textValue = value;
            }
        }

        public string ClassName
        {
            get { return className; }
            set
            {
                if (className == null)
                    className = value;
            }
        }

        public string AutomationID
        {
            get { return automationID; }
            set
            {
                if (automationID == null)
                    automationID = value;
            }
        }

        public int Index
        {
            get { return index; }
            set
            {
                if (index == -1)
                    if (value >= 0)
                        index = value;
            }
        }

        public ControlType ControlType
        {
            get { return controlType; }
            set
            {
                if (controlType == null)
                    controlType = value;
            }
        }
    }
}
