﻿namespace GSD_RnDAutomation.Status
{
    /// <summary>
    /// Interface allowing a class to listen to status messages as they are sent.
    /// 
    /// Each instance of IStatusHandler must be registered using StatusManager.RegisterHandler(...)
    /// </summary>
    public interface IStatusHandler
    {
        void OnStatusAdd(string status);
    }
}
