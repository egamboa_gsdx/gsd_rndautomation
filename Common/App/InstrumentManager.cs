﻿namespace GSD_RnDAutomation
{
    /// <summary>
    /// Enum for all software versions supported by the framework
    /// </summary>
    public enum InstrumentManager
    {
        undefined,
        Storm,
        AIX,
        Torrent,
        TorrentTestDesigner,
        ServerStressTest
    }

    public enum RackManager
    {
        undefined,
        classic,
        SIR,
        bolt
    }
}