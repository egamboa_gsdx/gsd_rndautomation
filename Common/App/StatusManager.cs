﻿using System.Collections.Generic;

namespace GSD_RnDAutomation.Status
{
    /// <summary>
    /// StatusManager allows applications to receive updates about the library's functions.
    /// </summary>
    public class StatusManager
    {
        private static List<IStatusHandler> handlers = new List<IStatusHandler>();

        /// <summary>
        /// Lets a IStatusHandler have its OnStatusAdd() called when a new status is added.
        /// </summary>
        /// <param name="statusHandler">The IStatusHandler to register.</param>
        public static void RegisterHandler(IStatusHandler statusHandler)
        {
            handlers.Add(statusHandler);
        }

        /// <summary>
        /// Adds a new status message and calls OnStatusAdd() for each registered IStatusHandler.
        /// </summary>
        /// <param name="status">The status message</param>
        public static void AddStatus(string status)
        {
            foreach (IStatusHandler handler in handlers)
            {
                try
                {
                    handler.OnStatusAdd(status);
                }
                catch
                {
                }
            }
        }
    }
}
