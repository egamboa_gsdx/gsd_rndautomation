﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Win32.TaskScheduler;

namespace GSD_RnDAutomation.TaskScheduler
{
    class TaskScheduler
    {
        private static string sharePointTaskName = "Test Automation - SharePoint Downloader - Both";
        private static string smokeTestTaskName = "Test Automation - Smoke Test";

        public static bool AutomaticRunEnabled
        {
            get
            {
                // Note, this only ensures there are two tasks with the correct names, it does not verify their triggers or actions
                using (TaskService ts = new TaskService())
                {
                    var t = ts.RootFolder.GetTasks(new Regex(sharePointTaskName));
                    if (ts.RootFolder.GetTasks(new Regex(sharePointTaskName)).Count == 0)
                        return false;
                    if (ts.RootFolder.GetTasks(new Regex(smokeTestTaskName)).Count == 0)
                        return false;
                }
                return true;
            }
            set
            {
                // In order for changes to scheduled tasks to apply, the user must disable and re-enable automatic runs on the GUI
                if (value == true) // install
                {
                    using (TaskService ts = new TaskService())
                    {
                        // Create a new task definition and assign properties
                        TaskDefinition sharePointDefinition = ts.NewTask();
                        sharePointDefinition.RegistrationInfo.Description = "Automatically launches the SharePoint Downloader at noon every weekday";

                        // Create a trigger that will fire the task at 12:00pm every day
                        DaysOfTheWeek days = DaysOfTheWeek.Monday | DaysOfTheWeek.Tuesday | DaysOfTheWeek.Wednesday | DaysOfTheWeek.Thursday | DaysOfTheWeek.Friday | DaysOfTheWeek.Saturday | DaysOfTheWeek.Sunday;
                        WeeklyTrigger sharePointTrigger = new WeeklyTrigger(days);
                        sharePointTrigger.StartBoundary = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 12, 0, 0);
                        sharePointDefinition.Triggers.Add(sharePointTrigger);

                        // Create an action that will launch the SharePoint Downloader whenever the trigger fires
                        //sharePointDefinition.Actions.Add(new ExecAction("\"C:\\Test Automation\\SharePoint Downloader.exe\""));
                        sharePointDefinition.Actions.Add(new ExecAction(@"C:\Users\egamboa\source\repos\gsd_rndautomation\bin\Debug\GSD_RnDAutomation.exe", "download"));

                        // Set the process to run at the highest level
                        sharePointDefinition.Principal.RunLevel = TaskRunLevel.Highest;

                        // Set the process to run for the current user
                        sharePointDefinition.Principal.UserId = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                        // Register the task in the root folder
                        ts.RootFolder.RegisterTaskDefinition(sharePointTaskName, sharePointDefinition);


                        // Create a new task definition and assign properties
                        TaskDefinition smokeTestDefinition = ts.NewTask();
                        smokeTestDefinition.RegistrationInfo.Description = "Automatically launches the Smoke Test after login";

                        // Create a trigger that will fire the task at Login
                        smokeTestDefinition.Triggers.Add(new LogonTrigger());

                        // Create an action that will launch the Smoke Test whenever the trigger fires
                        //smokeTestDefinition.Actions.Add(new ExecAction("\"C:\\Test Automation\\SmokeTest.exe\"", "RegKey"));
                        smokeTestDefinition.Actions.Add(new ExecAction(@"C:\Users\egamboa\source\repos\gsd_rndautomation\bin\Debug\GSD_RnDAutomation.exe", "RegKey"));

                        // Set the process to run at the highest level
                        smokeTestDefinition.Principal.RunLevel = TaskRunLevel.Highest;

                        // Set the process to run for the current user
                        smokeTestDefinition.Principal.UserId = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                        // Register the task in the root folder
                        ts.RootFolder.RegisterTaskDefinition(smokeTestTaskName, smokeTestDefinition);
                    }
                }
                else
                {
                    using (TaskService ts = new TaskService())
                    {
                        try
                        {
                            ts.RootFolder.DeleteTask(sharePointTaskName);
                        }
                        catch
                        {
                            // The task was not found
                        }

                        try
                        {
                            ts.RootFolder.DeleteTask(smokeTestTaskName);
                        }
                        catch
                        {
                            // The task was not found
                        }
                    }
                }
            }
        }
    }
}
