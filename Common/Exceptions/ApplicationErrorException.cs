﻿using System;

namespace GSD_RnDAutomation.Common.Exceptions
{
    public class ApplicationErrorException : Exception
    {
        public ApplicationErrorException(string message) : base(message) { }

        public ApplicationErrorException(string message, Exception innerException) : base(message, innerException) { }
    }
}
