﻿using System;

namespace GSD_RnDAutomation.Common.Exceptions
{
    public class MissingNameException : Exception
    {
        public MissingNameException(string message) : base(message) { }

        public MissingNameException(string message, Exception innerException) : base(message, innerException) { }

    }
}
