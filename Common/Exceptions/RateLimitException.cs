﻿/**
 * TestRail API binding for .NET (API v2, available since TestRail 3.0)
 *
 * Learn more:
 *
 * http://docs.gurock.com/testrail-api2/start
 * http://docs.gurock.com/testrail-api2/accessing
 *
 * Copyright Gurock Software GmbH. See license.md for details.
 */


using System;
using System.Runtime.Serialization;

namespace GSD_RnDAutomation.Common.Exceptions
{
    [Serializable]
    public class RateLimitException : APIException
    {
        public readonly int waitTime;
        public RateLimitException(int waitTime)
        {
            this.waitTime = waitTime;
        }
        public RateLimitException(string message, int waitTime) : base(message)
        {
            this.waitTime = waitTime;
        }

        public RateLimitException(string message, Exception innerException, int waitTime) : base(message, innerException) 
        {
            this.waitTime = waitTime;
        }

        protected RateLimitException(SerializationInfo info,
            StreamingContext context, int waitTime) : base(info, context)
        {
            this.waitTime = waitTime;
        }
    }
}

