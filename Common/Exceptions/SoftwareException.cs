﻿using System;

namespace GSD_RnDAutomation.Common.Exceptions
{
    public class SoftwareException : Exception
    {
        public SoftwareException(string message) : base(message) { }

        public SoftwareException(string message, Exception innerException) : base(message, innerException) { }
    }
}
