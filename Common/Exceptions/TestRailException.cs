﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSD_RnDAutomation.Common.Exceptions
{
    public class TestRailException : Exception
    {
        public TestRailException(string message) : base(message) { }

        public TestRailException(string message, Exception innerException) : base(message, innerException) { }

    }
}
