﻿using System;

namespace GSD_RnDAutomation.Common.Exceptions
{
    public class TimeoutException : TestRailException
    {
        public TimeoutException(string message) : base(message) { }

        public TimeoutException(string message, Exception innerException) : base(message, innerException) { }
    }
}
