﻿using GSD_RnDAutomation.Status;




namespace GSD_RnDAutomation.Common.Methods
{
    public class GUIMethods
    {
        /// <summary>
        /// Exits the Instrument Manager. Does nothing if the current application is already closed.
        /// </summary>
        public static void Exit()
        {
            if (!ApplicationManager.Application.HasExited)
            {
                StatusManager.AddStatus("Exiting instrument manager");
                HelperMethods.Click(GeneralProperty.Default.ExitButton);
                HelperMethods.Click(CommonProperty.Default.YesButton_textValue);
            }
        }
        public static void Close() 
        {
            StatusManager.AddStatus("Exiting Torrent Test Designer");
            HelperMethods.Click(GeneralProperty.Default.CloseWindow);
        }
    }
}
