﻿using System;
using System.Threading;

using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

using GSD_RnDAutomation.Common;
using GSD_RnDAutomation.Configuration;

namespace GSD_RnDAutomation.Common.Methods
{
    public class HelperMethods
    {
        /// <summary>
        /// Waits for a small amount of time.
        /// </summary>
        public static void Wait()
        {
            if (AutomationConfiguration.IsSetup)
            {
                Thread.Sleep(AutomationConfiguration.SetupTimeDelay);
            }
            else
            {
                Thread.Sleep(AutomationConfiguration.TimeDelay);
            }
        }

        public static void WaitForSetupAction()
        {
            Thread.Sleep(AutomationConfiguration.SetupTimeDelay);
        }

        /// <summary>
        /// Clicks on an element based on an ElementIdentifier.
        /// </summary>
        /// <param name="elementIdentifier">The ElementIdentifier to search for.</param>
        public static void Click(ElementIdentifier elementIdentifier)
        {
            Click(elementIdentifier, ApplicationManager.ApplicationWindow);
        }

        /// <summary>
        /// Clicks on an element based on an ElementIdentifier and Window.
        /// </summary>
        /// <param name="elementIdentifier">The ElementIdentifier to search for.</param>
        /// <param name="window">The Window to search in.</param>
        public static void Click(ElementIdentifier elementIdentifier, Window window)
        {
            Refocus(window);

            Click(window.Get(elementIdentifier));
        }

        /// <summary>
        /// Clicks on an IUIItem. This also ensures the correct instrument manager is selected and adds wait after clicking.
        /// </summary>
        /// <param name="item">The IUIItem to click.</param>
        public static void Click(IUIItem item)
        {
            item.Click();

            Wait();
        }

        /// <summary>
        /// Hovers over an element based on an ElementIdentifier and Window.
        /// </summary>
        /// <param name="elementIdentifier">The ElementIdentifier to search for.</param>
        public static void Hover(ElementIdentifier elementIdentifier)
        {
            Hover(elementIdentifier, ApplicationManager.ApplicationWindow);
        }

        /// <summary>
        /// Hovers over an element based on an ElementIdentifier and Window.
        /// </summary>
        /// <param name="elementIdentifier">The ElementIdentifier to search for.</param>
        /// <param name="window">The Window to search in.</param>
        public static void Hover(ElementIdentifier elementIdentifier, Window window)
        {
            var mouse = ApplicationManager.ApplicationWindow.Mouse;
            mouse.Move(window.Get(elementIdentifier).ClickablePoint);
            Wait();
        }

        /// <summary>
        /// Hovers over an element based on an IUIItem.
        /// </summary>
        /// <param name="item">The IUIItem to click.</param>
        public static void Hover(IUIItem item)
        {
            var mouse = ApplicationManager.ApplicationWindow.Mouse;
            mouse.Move(item.ClickablePoint);
            Wait();
        }

        /// <summary>
        /// Ensures the window is focused and in the front, fixes other instrument managers popups from appearing on top like the worklist finish dialog.
        /// </summary>
        public static void Refocus(Window window)
        {
            window.Focus();
        }

        /// <summary>
        /// Returns the tooltip when hovering over an element.
        /// </summary>
        /// <param name="elementIdentifier">The element to hover over.</param>
        /// <returns>The current tooltip</returns>
        public static ToolTip GetTooltip(ElementIdentifier elementIdentifier)
        {
            return GetTooltip(elementIdentifier, ApplicationManager.ApplicationWindow);
        }

        /// <summary>
        /// Returns the tooltip when hovering over an element.
        /// </summary>
        /// <param name="elementIdentifier">The element to hover over.</param>
        /// <param name="window">The window of the element.</param>
        /// <returns>The current tooltip</returns>
        public static ToolTip GetTooltip(ElementIdentifier elementIdentifier, Window window)
        {
            return GetTooltip(window.Get(elementIdentifier), window);
        }

        /// <summary>
        /// Returns the tooltip when hovering over an element.
        /// </summary>
        /// <param name="item">The element to hover over.</param>
        /// <returns>The current tooltip</returns>
        public static ToolTip GetTooltip(IUIItem item)
        {
            return GetTooltip(item, ApplicationManager.ApplicationWindow);
        }

        /// <summary>
        /// Returns the tooltip when hovering over an element.
        /// </summary>
        /// <param name="item">The element to hover over.</param>
        /// <param name="window">The window of the element.</param>
        /// <returns>The current tooltip</returns>
        public static ToolTip GetTooltip(IUIItem item, Window window)
        {
            AttachedMouse mouse = window.Mouse;
            mouse.Location = item.ClickablePoint;
            Thread.Sleep(1500);
            return ApplicationManager.ApplicationWindow.ToolTip;
        }
    }
}
