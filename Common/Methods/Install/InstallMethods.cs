﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Collections.Generic;

using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.InputDevices;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.WindowsAPI;

using GSD_RnDAutomation.Common;

namespace GSD_RnDAutomation.Common.Methods
{
    class InstallMethods
    {
        /// <summary>
        /// Launches and automates the installer for the selected software.
        /// </summary>
        /// <param name="software">The software to install. "Storm" for Storm or "AIX" for AIX1000 or TorrentTestDesigner </param>
        /// <param name="restartOnFinish">When true, will restart the computer to complete the install.</param>
        private static string StormInstallerPath = @"C:\Builds\Storm";
        private static string AIXInstallerPath = @"C:\Builds\AIX";
        public static void Install(string software, bool restartOnFinish = true)
        {
           
            switch (software)
            {
                case "Storm":
                    Install(InstrumentManager.Storm, restartOnFinish);
                    return;
                case "AIX":
                    Install(InstrumentManager.AIX, restartOnFinish);
                    return;
                default:
                    throw new ArgumentException("Software was not set to \"Storm\" or \"AIX\"");
            }
        }

        /// <summary>
        /// Launches and automates the installer for the selected software.
        /// </summary>
        /// <param name="software">The software to install.</param>
        /// <param name="restartOnFinish">When true, will restart the computer to complete the install.</param>
        public static void Install(InstrumentManager software, bool restartOnFinish = true)
        {
            string buildsPath = "";
            string windowName = "";
            ElementIdentifier autoStartIdentifier;

            switch (software)
            {
                case InstrumentManager.Storm:
                    buildsPath = StormInstallerPath;
                    windowName = DataProperty.Default.StormInstallerWindowName;
                    autoStartIdentifier = InstallProperty.Default.StormAutoStart;
                    break;
                case InstrumentManager.AIX:
                    buildsPath = AIXInstallerPath;
                    windowName = DataProperty.Default.AIXInstallerWindowName;
                    autoStartIdentifier = InstallProperty.Default.AIXAutoStart;
                    break;
                default:
                    throw new ArgumentException("Invalid software version");
            }


            // Launch the installer
            var BuildsDirectory = new DirectoryInfo(buildsPath);
            FileInfo[] Files = BuildsDirectory.GetFiles("*.exe");
            Application application = Application.Launch(Files[0].FullName);
            application.WaitWhileBusy();

            // Record the application name, but change the extension to .tmp
            string processName = Files[0].Name;
            processName = processName.Substring(0, processName.Length - 4);
            processName += ".tmp";

            Thread.Sleep(15000);

            // Find the correct application, found on a .tmp process instead of .exe
            Process[] processlist = Process.GetProcesses();
            foreach (Process process in processlist)
            {
                if (process.ProcessName == processName)
                {
                    application = Application.Attach(process.Id);
                }
            }

            // Automate the install GUI
            Window window = application.GetWindow(windowName, InitializeOption.NoCache);
            window.Focus();

            AttachedKeyboard keyboard = window.Keyboard;

            // Welcome
            keyboard.HoldKey(KeyboardInput.SpecialKeys.LEFT_ALT);
            keyboard.Enter("n"); // alt+n = next
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.LEFT_ALT);

            // Select Destination 
            keyboard.HoldKey(KeyboardInput.SpecialKeys.LEFT_ALT);
            keyboard.Enter("n"); // alt+n = next
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.LEFT_ALT);

            // Select Components
            keyboard.HoldKey(KeyboardInput.SpecialKeys.LEFT_ALT);
            keyboard.Enter("n"); // alt+n = next
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.LEFT_ALT);

            // Select Additional Tasks
            // Uncheck auto start at system boot
            TestStack.White.UIItems.CheckBox chckbox = window.Get<TestStack.White.UIItems.CheckBox>();
            if (chckbox.Checked)
            {
                chckbox.Click(); // Disable
            }

            keyboard.HoldKey(KeyboardInput.SpecialKeys.LEFT_ALT);
            keyboard.Enter("n"); // alt+n = next
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.LEFT_ALT);

            // Ready
            keyboard.HoldKey(KeyboardInput.SpecialKeys.LEFT_ALT);
            keyboard.Enter("i"); // alt+i = install
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.LEFT_ALT);

            window.WaitWhileBusy();

            // Check for finish for 1 hour
            for (int LoopCount = 0; LoopCount < 360; LoopCount++)
            {
                Thread.Sleep(10000); // 10 sec * 6 * 60
                try
                {
                    // Look for an Ximea prompt
                    List<Window> allWindows = Desktop.Instance.Windows();
                    foreach (Window w in allWindows)
                    {
                        // Check the window name
                        if (w.Name.StartsWith(WindowName.Default.XimeaSetupStart))
                        {
                            w.Focus();
                            AttachedKeyboard ximeaKeyboard = w.Keyboard;
                            ximeaKeyboard.HoldKey(KeyboardInput.SpecialKeys.LEFT_ALT);
                            ximeaKeyboard.Enter("y"); // alt+y = yes  (reinstall)
                            ximeaKeyboard.LeaveKey(KeyboardInput.SpecialKeys.LEFT_ALT);
                        }
                    }

                    // If the installer is finished
                    if (!restartOnFinish)
                    {
                        // select 'no, restart later'
                        window.Focus();
                        keyboard.Enter("n");
                    }

                    window.Focus();
                    keyboard.HoldKey(KeyboardInput.SpecialKeys.LEFT_ALT);
                    keyboard.Enter("f"); // alt+f = finish
                    keyboard.LeaveKey(KeyboardInput.SpecialKeys.LEFT_ALT);

                    // If the installer asks to shut down the current server, press next
                    window.Focus();
                    keyboard.HoldKey(KeyboardInput.SpecialKeys.LEFT_ALT);
                    keyboard.Enter("n"); // alt+n = next
                    keyboard.LeaveKey(KeyboardInput.SpecialKeys.LEFT_ALT);
                }
                catch
                {
                    // The button was not found
                }

                if (application.HasExited)
                {
                    break;
                }
            }
        }
    }
}
