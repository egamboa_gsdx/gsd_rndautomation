﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TestStack.White.Factory;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Service;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.TORRENT;
using GSD_RnDAutomation.TORRENT.Elements;


namespace GSD_RnDAutomation.Common.Methods
{
    public class LoginMethods
    {
        /// <summary>
        /// Log in using the Tech Service account.
        /// </summary>
        public static void LoginToManager()
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm || ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                LoginToManager(DataProperty.Default.Username, DataProperty.Default.Password);
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.Torrent)
            {
                LoginToTorrentManager("TechService", "123techservice321");
            }
        }

        /// <summary>
        /// Log in using to a specific user.
        /// </summary>
        /// <param name="username">The user's username.</param>
        /// <param name="password">The user's password.</param>
        public static void LoginToManager(string username, string password)
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm || ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                StatusManager.AddStatus("Logging in to Instrument Manager");

                Window loginWindow = null;
                for (int i = 0; i < 4; i++)
                {
                    try
                    {
                        loginWindow = ApplicationManager.Application.GetWindow(WindowName.Default.SelectUser, InitializeOption.NoCache);
                    }
                    catch
                    {
                        if (i == 3)
                        {
                            throw new WorkflowException("Failed to find login dialog after waiting 2 minutes (4 attempts at 30 seconds)");
                        }
                        else
                        {
                            StatusManager.AddStatus("Failed to find login dialog in 30 seconds, trying again");
                        }
                    }
                }

                // TODO Update this to find the text from the list Element instead
                // Select the user
                IUIItem usernameItem = loginWindow.Get(SearchCriteria.ByText(username));
                usernameItem.Click();
                HelperMethods.Wait();

                // Log on
                //HelperMethods.Click(LoginElements.LogOnButton, loginWindow);
                HelperMethods.Click(LoginProperty.Default.LogOnBtn, loginWindow);

                // Enter the password
                TextBox PasswordBox = loginWindow.Get<TextBox>(LoginProperty.Default.PasswordBox);
                PasswordBox.Enter(password);
                HelperMethods.Wait();

                // Ok
                HelperMethods.Click(CommonProperty.Default.OkButton_textValue, loginWindow);

                // Wait for the instrument manager to open or the "Load Autosaved Worklist" popup
                try
                {
                    // If the window is found, continue
                    if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                    {
                        ApplicationManager.Application.GetWindow(WindowName.Default.StormWindow, InitializeOption.NoCache);
                    }
                    else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                    {
                        ApplicationManager.Application.GetWindow(WindowName.Default.AIXWindow, InitializeOption.NoCache);
                    }
                    return;
                }
                catch // If the window is not found in 30 seconds, try to close the "Load Autosaved Worklist" popup
                {
                    Window loadAutosavedWorklistWindow = ApplicationManager.Application.GetWindow(LoginProperty.Default.LoadAutosavedWorklistWindowName, InitializeOption.NoCache);
                    HelperMethods.Click(LoginProperty.Default.NoBtn, loadAutosavedWorklistWindow);
                }
            }
        }

        /// <summary>
        /// Log in Torrent
        /// </summary>
        /// <param name="username">user name to log in</param>
        /// <param name="password">user password to log in</param>
        public static void LoginToTorrentManager(string username, string password)
        {
            StatusManager.AddStatus("Waiting for login screen...");
            ApplicationManager.WaitDriver.Until(ExpectedConditions.ElementIsVisible(TempestLoginFindBy.PasswordEntryBy));

            AppiumElement passwordTxt = ApplicationManager.Driver.FindElement(TempestLoginFindBy.PasswordEntryBy);
            AppiumElement enterBtn = ApplicationManager.Driver.FindElement(TempestLoginFindBy.enterPasswordBtnBy);
            StatusManager.AddStatus("Logging into Tempest..");
            passwordTxt.SendKeys(password);
            enterBtn.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(TempestGeneralFindBy.HeaderTitleBy));
        }
    }
}
