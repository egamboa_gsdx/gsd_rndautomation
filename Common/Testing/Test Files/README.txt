This directory is for files used by the Test Automation Framework.

Notes:
Mercurial does not allow easy "include" statements, so most files here are ignored by default.
However Mercurial does allow you to commit ignored files, so if you are using TortoiseHg:
 1. Go to the Commit tab
 2. Above the file list and between the text filer and refresh button, click on the question mark dropdown
 3. Enabled "I ignored"
 4. Select the file(s) and commit
 5. Reopen the question mark dropwodn and disable "I ignore"

These files are not automatically included in the installer, they must be added by:
 1. Right-click on "TestAutomationInstaller", click "View" > "File System"
 2. On the left, navigate to "Application Folder" > "Test Data"
 3. On the right, right-click and select "Add" > "File..."
 4. Select the files from the directory of this README, then click "Open"

During development, these files should be manually copied to "c:\Test Automation\Test Data\"

Within the Automation Framework, these files are assumed to be in the "c:\Test Automation\Test Data\" directory
