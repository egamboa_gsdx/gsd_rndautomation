﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSD_RnDAutomation.Common.Testing
{
    abstract class TestCase
    {
        /// <summary>
        /// The TestRail test Case name for test being ran...i.e. HOME > Fly-out Status Log + Click Arrow to open
        /// </summary>
        public abstract string TestCaseName { get; }
        
        /// <summary>
        /// Call methods to perform test and validation
        /// </summary>
        public abstract void RunTest();
    }
}
