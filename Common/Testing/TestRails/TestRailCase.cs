﻿using GSD_RnDAutomation.Common.Exceptions;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace GSD_RnDAutomation.Common.Testing.TestRails
{
    public class TestRailCase
    {
        private int id;
        private string name;

        private TestRailCase(int id)
        {
            this.id = id;
            Refresh();
        }

        public static int[] GetListOfIDsInSection(int projectID, TestRailSection section)
        {
            // http://docs.gurock.com/testrail-api2/reference-cases#get_cases
            JArray caseListResponse = (JArray)TestRailManager.SendGet("get_cases/" + projectID + (section == null ? "" : "&section_id=" + section.ID));

            List<int> IDs = new List<int>();
            foreach (JObject caseObject in caseListResponse)
            {
                IDs.Add((int)caseObject["id"]);
            }
            return IDs.ToArray();
        }

        /// <summary>
        /// Searches for a TestRail case by case ID.
        /// </summary>
        /// <param name="id">The case ID</param>
        /// <returns></returns>
        public static TestRailCase GetByID(int id)
        {
            return new TestRailCase(id);
        }

        /// <summary>
        /// Searches for a TestRail case by name and project ID.
        /// Will throw MissingNameException when the case name is not found.
        /// or throw DuplicateNameException when the case name is found multiple times.
        /// </summary>
        /// <param name="name">The case name</param>
        /// <param name="projectID">The project ID</param>
        /// <returns></returns>
        public static TestRailCase GetByName(string name, int projectID, TestRailSection section = null)
        {
            // http://docs.gurock.com/testrail-api2/reference-cases#get_cases
            JArray caseListResponse = (JArray)TestRailManager.SendGet("get_cases/" + projectID + (section == null ? "" : "&section_id=" + section.ID));
            //JArray caseListResponse = (JArray)TestRailManager.SendGet("get_tests/" + section.ID);

            // Case name to case id list
            Dictionary<string, int> caseList = new Dictionary<string, int>();
            foreach (JObject caseObject in caseListResponse)
            {
                int caseID = (int)caseObject["id"];
                string caseName = (string)caseObject["title"];
                if (caseList.ContainsKey(caseName))
                {
                    // Error, duplicate case name found. Name will be ignored
                    caseList[caseName] = -1;
                }
                else
                {
                    caseList.Add(caseName, caseID);
                }
            }

            if (!caseList.ContainsKey(name))
            {
                throw new MissingNameException("The test case name \"" + name + "\" was not found.");
            }

            if (caseList[name] == -1)
            {
                throw new DuplicateNameException("The test case name \"" + name + "\" was found multiple times.");
            }

            return new TestRailCase(caseList[name]);
        }

        /// <summary>
        /// Creates a new case with a name inside a project.
        /// Will throw DuplicateNameException when the case name already exists.
        /// </summary>
        /// <param name="name">The case name</param>
        /// <param name="projectID">The project ID</param>
        /// <param name="section">The section where the case should be created</param>
        /// <returns></returns>
        public static TestRailCase CreateByName(string name, int projectID, TestRailSection section)
        {
            // http://docs.gurock.com/testrail-api2/reference-cases#get_cases
            JArray caseListResponse = (JArray)TestRailManager.SendGet("get_cases/" + projectID + "&section_id=" + section.ID);

            foreach (JObject caseObject in caseListResponse)
            {
                string caseName = (string)caseObject["title"];
                if (caseName == name)
                {
                    throw new DuplicateNameException("A test case with the name \"" + name + "\" already exists.");
                }
            }

            Dictionary<string, object> caseCreateRequest = new Dictionary<string, object>();
            caseCreateRequest.Add("title", name);

            JObject caseCreateResponse = (JObject)TestRailManager.SendPost("add_case/" + section.ID, caseCreateRequest);
            return new TestRailCase((int)caseCreateResponse["id"]);
        }

        /// <summary>
        /// Refreshes the API response data
        /// </summary>
        private void Refresh()
        {
            JObject response = (JObject)TestRailManager.SendGet("get_case/" + id);

            name = (string)response["title"];
        }

        // Getters and Setters

        public int ID
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
        }
    }
}
