﻿using System;
using System.Threading;

using GSD_RnDAutomation.Common.Testing.TestRails.GuRock;
using GSD_RnDAutomation.Common.Exceptions;
using TimeoutException = GSD_RnDAutomation.Common.Exceptions.TimeoutException;

namespace GSD_RnDAutomation.Common.Testing.TestRails
{
    public class TestRailManager
    {
        private static TestRailManager instance = null;
        private static int maxRateLimitRetry = 10;

        public static TestRailManager Instance
        {
            get
            {
                if (instance == null)
                {
                    throw new TestRailException("Manager not logged in, use Login() to log in.");
                }
                return instance;
            }
        }

        public APIClient Client;

        private TestRailManager(string baseURL, string username, string password)
        {
            Client = new APIClient(baseURL);
            Client.User = username;
            Client.Password = password;
        }

        public static void Login(string baseURL, string username, string password)
        {
            instance = new TestRailManager(baseURL, username, password);
        }

        public static object SendGet(string uri)
        {
            for (int i = 0; i < maxRateLimitRetry; i++)
            {
                try
                {
                    return Instance.Client.SendGet(uri);
                }
                catch (RateLimitException e)
                {
                    Thread.Sleep(1000 * e.waitTime);
                }
            }
            throw new TimeoutException(string.Format("Failed to GET uri:{0} after {1} retries", uri, maxRateLimitRetry));
        }

        public static object SendPost(string uri, object data)
        {
            for (int i = 0; i < maxRateLimitRetry; i++)
            {
                try
                {
                    return Instance.Client.SendPost(uri, data);
                }
                catch (RateLimitException e)
                {
                    Thread.Sleep(1000 * e.waitTime);
                }
            }
            throw new TimeoutException(string.Format("Failed to POST uri:{0} after {1} retries", uri, maxRateLimitRetry));
        }
    }
}
