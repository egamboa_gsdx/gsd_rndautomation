﻿using GSD_RnDAutomation.Common.Exceptions;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace GSD_RnDAutomation.Common.Testing.TestRails
{
    public class TestRailPlan
    {
        private int id;
        private string name;
        private Dictionary<string, int?> runIDsByName;  // int? is a nullable int

        private TestRailPlan(int id)
        {
            this.id = id;
            runIDsByName = new Dictionary<string, int?>();
            refresh();
        }

        /// <summary>
        /// Searches for a TestRail plan by plan ID.
        /// </summary>
        /// <param name="id">The plan ID</param>
        /// <returns></returns>
        public static TestRailPlan GetByID(int id)
        {
            return new TestRailPlan(id);
        }

        /// <summary>
        /// Searches for a TestRail plan by name and project ID.
        /// Will throw MissingNameException when the plan name is not found.
        /// or throw DuplicateNameException when the plan name is found multiple times.
        /// </summary>
        /// <param name="name">The plan name</param>
        /// <param name="projectID">The project ID</param>
        /// <returns></returns>
        public static TestRailPlan GetByName(string name, int projectID)
        {
            // http://docs.gurock.com/testrail-api2/reference-plans#get_plans
            // this response is limited to 250 entries, see link for details
            JArray planListResponse = (JArray)TestRailManager.SendGet("get_plans/" + projectID);

            // Plan name to plan id list
            Dictionary<string, int> planList = new Dictionary<string, int>();
            foreach (JObject plan in planListResponse)
            {
                int planID = (int)plan["id"];
                string planName = (string)plan["name"];
                if (planList.ContainsKey(planName))
                {
                    // Error, duplicate plan name found. Name will be ignored
                    planList[planName] = -1;
                }
                else
                {
                    planList.Add(planName, planID);
                }
            }

            if (!planList.ContainsKey(name))
            {
                throw new MissingNameException("The test plan name \"" + name + "\" was not found.");
            }

            if (planList[name] == -1)
            {
                throw new DuplicateNameException("The test plan name \"" + name + "\" was found multiple times.");
            }

            return new TestRailPlan(planList[name]);
        }

        /// <summary>
        /// Creates a new plan with a name inside a project.
        /// Will throw DuplicateNameException when the plan name already exists.
        /// </summary>
        /// <param name="name">The plan name</param>
        /// <param name="projectID">The project ID</param>
        /// <returns></returns>
        public static TestRailPlan CreateByName(string name, int projectID)
        {
            // http://docs.gurock.com/testrail-api2/reference-plans#get_plans
            // this response is limited to 250 entries, see link for details
            JArray planListResponse = (JArray)TestRailManager.SendGet("get_plans/" + projectID);

            foreach (JObject plan in planListResponse)
            {
                string planName = (string)plan["name"];
                if (planName == name)
                {
                    throw new DuplicateNameException("A test plan with the name \"" + name + "\" already exists.");
                }
            }

            Dictionary<string, object> planCreateRequest = new Dictionary<string, object>();
            planCreateRequest.Add("name", name);

            JObject planCreateResponse = (JObject)TestRailManager.SendPost("add_plan/" + projectID, planCreateRequest);
            return new TestRailPlan((int)planCreateResponse["id"]);
        }

        /// <summary>
        /// Returns an exising plan by name, or creates one if name does not exist.
        /// Will throw DuplicateNameException if multiple plans have the same name.
        /// </summary>
        /// <param name="name">The plan name</param>
        /// <param name="projectID">The project ID</param>
        /// <returns></returns>
        public static TestRailPlan GetOrCreateByName(string name, int projectID)
        {
            try
            {
                return GetByName(name, projectID);
            }
            catch (MissingNameException)
            {
                return CreateByName(name, projectID);
            }
        }

        /// <summary>
        /// Refreshes the API response data
        /// </summary>
        private void refresh()
        {
            JObject response = (JObject)TestRailManager.SendGet("get_plan/" + id);

            name = (string)response["name"];

            runIDsByName = new Dictionary<string, int?>();
            foreach (JObject entry in (JArray)response["entries"])
            {
                foreach (JObject run in (JArray)entry["runs"])
                {
                    string name = (string)run["name"];
                    int id = (int)run["id"];

                    if (runIDsByName.ContainsKey(name))
                    {
                        runIDsByName.Remove(name);
                        runIDsByName.Add(name, null);
                    }
                    else
                    {
                        runIDsByName.Add(name, id);
                    }
                }
            }
        }

        /// <summary>
        /// Gets a Test run by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public TestRailRun GetRunByName(string name)
        {
            if (!runIDsByName.ContainsKey(name))
            {
                throw new MissingNameException("The test run name \"" + name + "\" was not found.");
            }

            int? id = runIDsByName[name];
            if (id == null)
            {
                throw new DuplicateNameException("The test run name \"" + name + "\" was found multiple times.");
            }

            return TestRailRun.GetByID((int)id);
        }

        /// <summary>
        /// Gets a Test run by name, if it exists, or creates a new run
        /// </summary>
        /// <param name="name"></param>
        /// <param name="project"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public TestRailRun GetOrCreateRunByName(string name, TestRailProject project, TestRailSection section)
        {
            try
            {
                return GetRunByName(name);
            }
            catch (MissingNameException)
            {
                return CreateNewRun(name, project, section);
            }
            catch (DuplicateNameException)  // This is being handled to let keep existing plans with duplicate names functioning  
            {
                return CreateNewRun(name, project, section);
            }
        }

        /// <summary>
        /// Adds a Test run to the plan
        /// </summary>
        /// <param name="name"></param>
        public TestRailRun CreateNewRun(string name, TestRailProject project, TestRailSection section)
        {
            // http://docs.gurock.com/testrail-api2/reference-plans#add_plan_entry
            Dictionary<string, object> planAddEntryRequest = new Dictionary<string, object>();

            planAddEntryRequest.Add("suite_id", project.MasterSuite.ID); // Required
            planAddEntryRequest.Add("name", name);
            planAddEntryRequest.Add("include_all", false);
            planAddEntryRequest.Add("case_ids", project.GetListOfTestCasesInSection(section));

            JObject planAddEntryResponse = (JObject)TestRailManager.SendPost("add_plan_entry/" + id, planAddEntryRequest);

            // Refresh the list of known test runs
            refresh();

            foreach (JObject planEntry in planAddEntryResponse["runs"])
            {
                if ((string)planEntry["name"] == name)
                {
                    return TestRailRun.GetByID((int)planEntry["id"]);
                }
            }
            return TestRailRun.GetByID((int)planAddEntryResponse["runs"][((JArray)planAddEntryResponse["runs"]).Count - 1]["id"]);
        }
    }
}
