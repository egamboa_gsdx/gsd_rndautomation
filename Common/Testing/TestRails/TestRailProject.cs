﻿using System;
using Newtonsoft.Json.Linq;

namespace GSD_RnDAutomation.Common.Testing.TestRails
{
    public class TestRailProject
    {
        private int id;
        private TestRailSuite masterSuite = null;

        public TestRailProject(int id)
        {
            this.id = id;
        }

        // Getters and Setters

        public int ID
        {
            get { return id; }
        }

        // This should be the suite labeled master
        public TestRailSuite MasterSuite
        {
            get
            {
                if (masterSuite == null)
                {
                    masterSuite = TestRailSuite.GetByName("Master", id);
                }
                return masterSuite;
            }
        }

        // Test Section - a group of cases

        public TestRailSection GetSection(int sectionID)
        {
            return TestRailSection.GetByID(sectionID);
        }

        // Test Cases

        public int[] GetListOfTestCasesInSection(TestRailSection section)
        {
            return TestRailCase.GetListOfIDsInSection(id, section);
        }

        public TestRailCase GetTestCase(int caseID)
        {
            return TestRailCase.GetByID(caseID);
        }

        public TestRailCase GetTestCase(string name, TestRailSection section = null)
        {
            return TestRailCase.GetByName(name, id, section);
        }

        public TestRailCase CreateTestCase(string name, TestRailSection section)
        {
            return TestRailCase.CreateByName(name, id, section);
        }

        // Plans - A group of runs

        public TestRailPlan GetPlan(int planID)
        {
            return TestRailPlan.GetByID(planID);
        }

        public TestRailPlan GetPlan(string name)
        {
            return TestRailPlan.GetByName(name, id);
        }

        public TestRailPlan CreatePlan(string name)
        {
            return TestRailPlan.CreateByName(name, id);
        }

        public TestRailPlan GetOrCreatePlan(string name)
        {
            return TestRailPlan.GetOrCreateByName(name, id);
        }

        // Runs should be accessed from plans

        public void Debug()
        {
            JArray response = (JArray)TestRailManager.SendGet("get_suites/" + id);

            foreach (JObject suite in response)
            {
                int suiteID = (int)suite["id"];
                string suiteName = (string)suite["name"];
            }
        }
    }
}
