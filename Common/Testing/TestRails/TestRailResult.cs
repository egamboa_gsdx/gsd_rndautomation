﻿using Newtonsoft.Json.Linq;

namespace GSD_RnDAutomation.Common.Testing.TestRails
{
    public class TestRailResult
    {
        // Results can be reported a number of ways, but the method used here is add_results_for_cases
        // http://docs.gurock.com/testrail-api2/reference-results#add_results_for_cases

        private int runID;
        private int caseID;
        private string testRailResultStatus;
        private string comment;

        public TestRailResult(int runID, int caseID, string testRailResultStatus, string comment = null)
        {
            this.runID = runID;
            this.caseID = caseID;
            this.testRailResultStatus = testRailResultStatus;
            this.comment = (comment == null) ? "" : comment;
        }

        public JObject ToJson()
        {
            JObject json = new JObject();

            json.Add("case_id", caseID);
            json.Add("status_id", testRailResultStatus);

            if (comment != "")
            {
                json.Add("comment", comment);
            }

            return json;
        }
    }
}
