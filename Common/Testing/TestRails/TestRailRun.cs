﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace GSD_RnDAutomation.Common.Testing.TestRails
{
    public class TestRailRun
    {
        private int id;
        private string name;
        private string description;
        private List<TestRailResult> pendingResults;

        private TestRailRun(int id)
        {
            this.id = id;
            pendingResults = new List<TestRailResult>();
            Refresh();
        }

        /// <summary>
        /// Searches for a TestRail run by run ID.
        /// </summary>
        /// <param name="id">The run ID</param>
        /// <returns></returns>
        public static TestRailRun GetByID(int id)
        {
            return new TestRailRun(id);
        }

        public int ID
        {
            get { return id; }
        }

        /// <summary>
        /// Refreshes the API response data
        /// </summary>
        private void Refresh()
        {
            JObject response = (JObject)TestRailManager.SendGet("get_run/" + id);

            name = (string)response["name"];
            description = (string)response["description"];
        }

        /// <summary>
        /// Adds a result to the run. Must be sent later using SendResults().
        /// </summary>
        /// <param name="testCase">The test case</param>
        /// <param name="status">The test result status</param>
        /// <param name="comment">A comment about the result (optional)</param>
        public void AddResult(TestRailCase testCase, string testRailResultStatus, string comment = null)
        {
            TestRailResult result = new TestRailResult(this.id, testCase.ID, testRailResultStatus, comment);
            pendingResults.Add(result);
        }

        /// <summary>
        /// Sends the added results to TestRail. Use AddResult() to add results.
        /// </summary>
        public void SendResults()
        {
            // http://docs.gurock.com/testrail-api2/reference-results#add_results_for_cases
            JObject request = new JObject();

            JArray resultsList = new JArray();
            foreach (TestRailResult result in pendingResults)
            {
                resultsList.Add(result.ToJson());
            }
            request.Add("results", resultsList);

            TestRailManager.SendPost("add_results_for_cases/" + id, request);

            pendingResults.Clear();
        }

        // Getters and Setters
        public string Description
        {
            get { return description; }
            //set
            //{
            //    // figure out why this gives error 403 FORBIDDEN
            //    // Update through API
            //    JObject request = new JObject();
            //    request.Add("description", value);
            //    TestRailManager.Instance.Client.SendPost("update_run/" + id, request);

            //    Refresh();
            //}
        }
    }
}
