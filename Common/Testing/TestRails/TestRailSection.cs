﻿using Newtonsoft.Json.Linq;

namespace GSD_RnDAutomation.Common.Testing.TestRails
{
    public class TestRailSection
    {
        private int id;
        private string name;

        private TestRailSection(int id)
        {
            this.id = id;
            Refresh();
        }

        /// <summary>
        /// Searches for a TestRail section by section ID.
        /// </summary>
        /// <param name="id">The section ID</param>
        /// <returns></returns>
        public static TestRailSection GetByID(int id)
        {
            return new TestRailSection(id);
        }

        /// <summary>
        /// Refreshes the API response data
        /// </summary>
        private void Refresh()
        {
            JObject response = (JObject)TestRailManager.SendGet("get_section/" + id);

            name = (string)response["title"];
        }

        // Getters and Setters

        public int ID
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
        }
    }
}
