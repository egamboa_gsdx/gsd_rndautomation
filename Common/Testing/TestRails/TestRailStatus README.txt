TEST RAIL STATUS ID
----------------------------------
The following are status_ids to 
pass into AddResult() to 
upload to test rail
-----------------------------------
Passed - Blackbox   | status_id: 1
Passed - Unit       | status_id: 6
Passed - Automation | status_id: 7
Failed              | status_id: 5
Verified/Closed     | status_id: 4
Blocked             | status_id: 3
-----------------------------------
if future statuses are added to
testrail, use the following statement:
JArray testRailStatus = (JArray)TestRailManager.SendGet("get_statuses");
*There is a commented out line of code in TestRailOploader line 80