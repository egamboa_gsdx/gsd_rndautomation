﻿using GSD_RnDAutomation.Common.Exceptions;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace GSD_RnDAutomation.Common.Testing.TestRails
{
    public class TestRailSuite
    {
        private int id;
        private string name;

        public TestRailSuite(int id)
        {
            this.id = id;
            refresh();
        }

        /// <summary>
        /// Searches for a TestRail suite by suite ID.
        /// </summary>
        /// <param name="id">The suite ID</param>
        /// <returns></returns>
        public static TestRailSuite GetByID(int id)
        {
            return new TestRailSuite(id);
        }

        /// <summary>
        /// Searches for a TestRail suite by name and project ID.
        /// Will throw MissingNameException when the suite name is not found.
        /// or throw DuplicateNameException when the suite name is found multiple times.
        /// </summary>
        /// <param name="name">The suite name</param>
        /// <param name="projectID">The project ID</param>
        /// <returns></returns>
        public static TestRailSuite GetByName(string name, int projectID)
        {
            // http://docs.gurock.com/testrail-api2/reference-suites#get_suites
            // this response is limited to 250 entries, see link for details
            JArray suiteListResponse = (JArray)TestRailManager.SendGet("get_suites/" + projectID);

            // Suite name to suite id list
            Dictionary<string, int> suiteList = new Dictionary<string, int>();
            foreach (JObject suite in suiteListResponse)
            {
                int suiteID = (int)suite["id"];
                string suiteName = (string)suite["name"];
                if (suiteList.ContainsKey(suiteName))
                {
                    // Error, duplicate suite name found. Name will be ignored
                    suiteList[suiteName] = -1;
                }
                else
                {
                    suiteList.Add(suiteName, suiteID);
                }
            }

            if (!suiteList.ContainsKey(name))
            {
                throw new MissingNameException("The test suite name \"" + name + "\" was not found.");
            }

            if (suiteList[name] == -1)
            {
                throw new DuplicateNameException("The test suite name \"" + name + "\" was found multiple times.");
            }

            return new TestRailSuite(suiteList[name]);
        }

        private void refresh()
        {
            JObject response = (JObject)TestRailManager.SendGet("get_suite/" + id);

            name = (string)response["name"];
        }

        // Getters and Setters

        public int ID
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
        }
    }
}
