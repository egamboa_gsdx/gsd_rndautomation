﻿using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Common.Testing.TestRails;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace GSD_RnDAutomation.Common.Testing
{
    class TestRailUploader
    {
        private static string url = "https://gsdx.testrail.net";
        private static string username = "nsingh@gsdx.us";
        private static string password = "gsdadmin!";

        // The end date of a previous sprint, used to calcuate future sprint end dates
        private static DateTime previousSprintDate = new DateTime(year: 2019, month: 7, day: 30);

        private InstrumentManager software;
        private RackManager rack;
        private int projectID;
        private int sectionID;

        public TestRailUploader(InstrumentManager software, RackManager rack)
        {
            this.software = software;
            this.rack = rack;
            switch (software)
            {
                case InstrumentManager.Storm:
                    //projectID = 67;
                    //sectionID = 14140;
                    if(rack == RackManager.classic)
                    {
                        projectID = 77;
                        sectionID = 34892;
                    }
                    else
                    {
                        projectID = 79;
                        sectionID = 35676;
                    }
                    break;
                case InstrumentManager.AIX:
                    //projectID = 46;
                    //sectionID = 7904;
                    if (rack == RackManager.classic)
                    {
                        projectID = 76;
                        sectionID = 34543;
                    }
                    else
                    {
                        projectID = 78;
                        sectionID = 39107;
                    }
                    break;
                default:
                    throw new ArgumentException("InstrumentManager does not have a project configured for TestRail");
            }
        }

        public List<TestResults> UploadResults(List<TestResults> results, bool automaticUpload = false)
        {
            TestRailManager.Login(url, username, password);

            TestRailProject project = new TestRailProject(projectID);
            TestRailSection section = project.GetSection(sectionID);

            TestRailPlan sprintPlan = project.GetOrCreatePlan("Sprint " + GetCurrentSprintDateString());
            TestRailRun run = sprintPlan.GetOrCreateRunByName(ApplicationManager.GetFormattedSoftwareVersionString(software) + (automaticUpload ? "" : " (Manual)"), project, section);

            string testRailStatusID;

            for (int i = 0; i < results.Count; i++)
            {
                try
                {
                    //Grabs statuses if any custom statues were created; use this to check for status id to pass in results to 'run'
                    //JArray testRailStatus = (JArray)TestRailManager.SendGet("get_statuses");
                    TestRailCase testRailCase = project.GetTestCase(results[i].TestCaseName, section);
                    switch (results[i].TestRailSResultStatus)
                    {
                        case "PASSED":
                            testRailStatusID = "7";
                            break;
                        case "FAILED":
                            testRailStatusID = "5";
                            break;
                        case "RETEST":
                            testRailStatusID = "5";
                            break;
                        case "BLOCKED":
                            testRailStatusID = "2";
                            break;
                        default:
                            testRailStatusID = "2";
                            break;
                    }
                    //run.AddResult(testRailCase, results[i].TestRailSResultStatus, results[i].Comment);
                    run.AddResult(testRailCase, testRailStatusID, results[i].Comment);
                    results[i].UploadStatus = "Uploaded";
                }
                catch (MissingNameException)
                {
                    // The test case name could not be found
                    results[i].UploadStatus = "Missing Case";
                }
                catch (DuplicateNameException)
                {
                    // The test case name was found multiple times
                    results[i].UploadStatus = "Duplicate Case";
                }
            }

            run.SendResults();

            return results;
        }

        /// <summary>
        /// Returns the end date of the next sprint in the form "2019.07.30"
        /// </summary>
        /// <returns></returns>
        private string GetCurrentSprintDateString()
        {
            int daysSincePreviousSprint = (DateTime.Today - previousSprintDate).Days;
            int daysSinceLastSprint = ((daysSincePreviousSprint - 1) % 14) + 1;
            int daysToNextSprint = 14 - daysSinceLastSprint;
            return DateTime.Today.AddDays(daysToNextSprint).ToString("yyyy.MM.dd");
        }
    }
}