﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSD_RnDAutomation.Common.Testing
{
    class TestResults
    {
        private string testCaseName;
        private string testRailResultStatus;
        private string comment;
        private string uploadStatus;

        public TestResults(string testCaseName, string testRailResultStatus, string comment = "")
        {
            this.testCaseName = testCaseName;
            this.testRailResultStatus = testRailResultStatus;
            this.comment = comment;
            uploadStatus = "";
        }

        public string TestCaseName
        {
            get { return testCaseName; }
        }

        public string TestRailSResultStatus
        {
            get { return testRailResultStatus; }
        }

        public string Comment
        {
            get { return comment; }
        }

        public string UploadStatus
        {
            get { return uploadStatus; }
            set { uploadStatus = value; }
        }
    }
}
