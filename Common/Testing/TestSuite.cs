﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;

using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Methods.TORRENT.Popups;

namespace GSD_RnDAutomation.Common.Testing
{
    class TestSuite
    {
        private static Dictionary<string, TestCase> stormTestCases = GetTestCases<StormTestCase>();
        private static Dictionary<string, TestCase> stormClassicTestCases = GetTestCases<StormClassicTestCase>();
        private static Dictionary<string, TestCase> stormSIRTestCases = GetTestCases<StormSIRTestCase>();
        private static Dictionary<string, TestCase> stormBoltTestCases = GetTestCases<StormBoltTestCase>();

        private static Dictionary<string, TestCase> aixTestCases = GetTestCases<AIXTestCase>();
        private static Dictionary<string, TestCase> aixSIRTestCases = GetTestCases<AIXSIRTestCase>();
        private static Dictionary<string, TestCase> aixClassicTestCases = GetTestCases<AIXClassicTestCase>();

        private static Dictionary<string, TestCase> disabledStormTestCases = GetTestCases<DisabledStormTestCase>();
        private static Dictionary<string, TestCase> disabledAIXTestCases = GetTestCases<DisabledAIXTestCase>();

        private static Dictionary<string, TestCase> torrentTestCases = GetTestCases<TempestTestCase>();
        private static Dictionary<string, TestCase> disabledTorrentTestCases = GetTestCases<DisabledTempestTestCase>();

        private static Dictionary<string, TestCase> torrentTestDesignerTestCases = GetTestCases<TempestTestDesignerTestCase>();


        private static Dictionary<string, TestCase> GetTestCases<T>() where T : TestCase
        {
            Dictionary<string, TestCase> TestCases = new Dictionary<string, TestCase>();

            foreach (Type type in Assembly.GetCallingAssembly().GetTypes())
            {
                if (!type.IsAbstract && type.BaseType == typeof(T))
                {
                    T TestCaseInstance = (T)Activator.CreateInstance(type);
                    TestCases.Add(TestCaseInstance.TestCaseName, TestCaseInstance);
                }
            }
            return TestCases;
        }

        public static List<string> TestCaseNames(InstrumentManager software, RackManager rack = RackManager.classic)
        {
            List<string> temp = new List<string>();
            switch (software)
            {
                case InstrumentManager.Storm:
                    temp.AddRange(new List<string>(stormTestCases.Keys));
                    if (rack == RackManager.classic)
                        temp.AddRange(new List<string>(stormClassicTestCases.Keys));
                    else if (rack == RackManager.SIR)
                        temp.AddRange(new List<string>(stormSIRTestCases.Keys));
                    else if (rack ==RackManager.bolt)
                        temp.AddRange(new List<string>(stormBoltTestCases.Keys));
                    return temp;
                case InstrumentManager.AIX:
                    temp.AddRange(new List<string>(aixTestCases.Keys));
                    if (rack == RackManager.classic)
                        temp.AddRange(new List<string>(aixClassicTestCases.Keys));
                    else if (rack == RackManager.SIR)
                        temp.AddRange(new List<string>(aixSIRTestCases.Keys));
                    return temp;
                case InstrumentManager.Torrent:
                    temp.AddRange(new List<string>(torrentTestCases.Keys));
                    return temp;
                case InstrumentManager.TorrentTestDesigner:
                    temp.AddRange(new List<string>(torrentTestDesignerTestCases.Keys));
                    return temp;
                default:
                    throw new Exception("No test cases found");
            }
        }

        private List<string> selectedTestCases;
        private Dictionary<string, TestCase> testCases;
        private List<TestResults> results;
        private InstrumentManager software;
        private RackManager rack;
        private bool run;
        private bool pause;
        private bool running;
        private List<string> disabledCases;

        public TestSuite(List<string> testCaseNames, InstrumentManager software, RackManager rack, bool reportDisabledCases = false)
        {
            switch (software)
            {
                case InstrumentManager.Storm:
                    //testCases = stormTestCases;
                    if (rack == RackManager.classic)
                    {
                        testCases = stormTestCases
                            .Concat(stormClassicTestCases)
                            .GroupBy(i => i.Key)
                            .ToDictionary(group => group.Key, group => group.First().Value);
                    }
                    else if (rack == RackManager.SIR)
                    {
                        testCases = stormTestCases
                            .Concat(stormSIRTestCases)
                            .GroupBy(i => i.Key)
                            .ToDictionary(group => group.Key, group => group.First().Value);
                    }
                    else if (rack == RackManager.bolt) 
                    {
                        testCases = stormTestCases
                            .Concat(stormBoltTestCases)
                            .GroupBy(i => i.Key)
                            .ToDictionary(group => group.Key, group => group.First().Value);
                    }
                    disabledCases = reportDisabledCases ? new List<string>(disabledStormTestCases.Keys) : null;
                    break;
                case InstrumentManager.AIX:
                    if (rack == RackManager.classic)
                    {
                        testCases = aixTestCases
                            .Concat(aixClassicTestCases)
                            .GroupBy(i => i.Key)
                            .ToDictionary(group => group.Key, group => group.First().Value);

                    }
                    else if (rack == RackManager.SIR)
                    {
                        testCases = aixTestCases
                            .Concat(aixSIRTestCases)
                            .GroupBy(i => i.Key)
                            .ToDictionary(group => group.Key, group => group.First().Value);
                    }
                    disabledCases = reportDisabledCases ? new List<string>(disabledAIXTestCases.Keys) : null;
                    break;
                case InstrumentManager.Torrent:
                    testCases = torrentTestCases
                        .Concat(torrentTestCases)
                        .GroupBy(i => i.Key)
                        .ToDictionary(group => group.Key, group => group.First().Value);
                    disabledCases = reportDisabledCases ? new List<string>(disabledTorrentTestCases.Keys) : null;
                    break;
                case InstrumentManager.TorrentTestDesigner:
                    testCases = torrentTestDesignerTestCases
                        .Concat(torrentTestDesignerTestCases)
                        .GroupBy(i => i.Key)
                        .ToDictionary(group => group.Key, group => group.First().Value);
                    
                    break;
                default:
                    throw new ArgumentException("Invalid Instrument Manager Selected");
            }
            selectedTestCases = testCaseNames;
            this.software = software;
            this.rack = rack;
            run = true;
            pause = false;
            running = false;
            results = new List<TestResults>();
        }

        public bool Run
        {
            get { return run; }
            set { run = value; }
        }

        public bool Pause
        {
            get { return pause; }
            set { pause = value; }
        }
        public bool Running
        {
            get { return running; }
        }
        public InstrumentManager InstrumentManager
        {
            get { return software; }
        }

        public RackManager RackManager
        {
            get { return rack; }
        }

        public List<TestResults> Results
        {
            get { return results; }
            set { results = value; }
        }

        public void RunTests()
        {
            running = true;
            Dictionary<string, TestResults> results = new Dictionary<string, TestResults>();

            if (disabledCases != null)
            {
                foreach (string testCaseName in disabledCases)
                {
                    string message = "Automation for\"" + testCaseName + "\" is currently manually disabled.";
                    results.Add(testCaseName, new TestResults(testCaseName, TestRailResultsStatus.Default.BLOCKED, message));
                }
            }


            string StartAndLoginTestCaseName = "LOGIN > Tech Support User Permission";


            if (selectedTestCases.Contains(StartAndLoginTestCaseName))
            {
                string resultStatus = TestRailResultsStatus.Default.UNTESTED;
                string comment = "";

                if (software == InstrumentManager.Storm)
                {
                    if (Process.GetProcessesByName(DataProperty.Default.StormExecutableName).Length != 0)
                    {
                        StatusManager.AddStatus("Previous Storm application left open. Closing and relaunching...");
                        ApplicationManager.AttemptSwitchTo(InstrumentManager.Storm);
                        GUIMethods.Exit();
                    }
                }
                else if (software == InstrumentManager.AIX)
                {
                    if (Process.GetProcessesByName(DataProperty.Default.AIXExecutableName).Length != 0)
                    {
                        StatusManager.AddStatus("Previous AIX1000 application left open. Closing and relaunching...");
                        ApplicationManager.AttemptSwitchTo(InstrumentManager.AIX);
                        GUIMethods.Exit();
                    }
                }
                else if (software == InstrumentManager.Torrent)
                {
                    //if (Process.GetProcessesByName("Tempest.FakeInstrument").Length == 0)
                    //{
                    //    StatusManager.AddStatus("Starting Fake Tempest Instrument...");
                    //    ProcessStartInfo fakeInstrument = new ProcessStartInfo(@"C:\Program Files\Gold Standard Diagnostics\Tempest Fake Instrument\Tempest.FakeInstrument.exe");
                    //    TestStack.White.Application.Launch(fakeInstrument);
                    //    HelperMethods.Wait();
                    //}
                    StatusManager.AddStatus("We fucking shit up now!!!");
                }
                else 
                {
                    if (software == InstrumentManager.TorrentTestDesigner) 
                    {
                        if (Process.GetProcessesByName(DataProperty.Default.TorrentExecutableName).Length != 0)
                        {
                            StatusManager.AddStatus("Previous Torrent Test Designer application left open. Closing and relaunching...");
                            ApplicationManager.AttemptSwitchTo(InstrumentManager.TorrentTestDesigner);
                            GUIMethods.Close();
                        }
                    }
                
                } 

                StatusManager.AddStatus("Running Test Case: \"" + StartAndLoginTestCaseName + "\"");

                try
                {
                    ApplicationManager.LaunchAndLogin(software, rack);
                    //if successful login
                    resultStatus = TestRailResultsStatus.Default.PASSED;
                }
                catch (Exception e)
                {
                    resultStatus = TestRailResultsStatus.Default.FAILED;
                    comment = "Launch and Login failed, exception: \"" + e.Message + "\"";
                }

                results.Add(StartAndLoginTestCaseName, new TestResults(StartAndLoginTestCaseName, resultStatus, comment));
            }
            else
            {
                ApplicationManager.SwitchOrLaunchAndLogin(software, rack);
            }
            foreach (string testCaseName in selectedTestCases)
            {
                string resultStatus = TestRailResultsStatus.Default.UNTESTED;
                string comment = "";
                if (testCaseName != StartAndLoginTestCaseName)
                {
                    try
                    {
                        if (pause && run)
                        {
                            StatusManager.AddStatus("================\nPAUSED\n================");
                            while (pause && run)
                            {
                                System.Threading.Thread.Sleep(1000);
                            }
                        }

                        if (!run)
                            break;

                        if (!testCases.ContainsKey(testCaseName))
                            continue;

                        TestCase testCase = testCases[testCaseName];
                        if (software == InstrumentManager.Storm || software == InstrumentManager.AIX)
                        {
                            try
                            {
                                WorklistMethods.New();
                            }
                            catch
                            {

                            }
                        }
                        else if (software == InstrumentManager.Torrent)
                        {
                            try
                            {
                                TempestConfirmationMethods.ConfirmCancel();
                            }
                            catch
                            {

                            }
                        }
                        

                        try
                        {
                            StatusManager.AddStatus("Running Test Case: \"" + testCaseName + "\"");
                            testCase.RunTest();
                            resultStatus = TestRailResultsStatus.Default.PASSED;
                        }
                        catch (ValidationException e)
                        {
                            resultStatus = TestRailResultsStatus.Default.FAILED;
                            comment = "Validation failed \"" + e.Message + "\"";
                        }
                        catch (WorkflowException e)
                        {
                            resultStatus = TestRailResultsStatus.Default.FAILED;
                            comment = "Poorly formed test case \"" + e.Message + "\"";
                        }
                        catch (SoftwareException e)
                        {
                            resultStatus = TestRailResultsStatus.Default.FAILED;
                            comment = "Poorly formed test case \"" + e.Message + "\"";
                        }
                    }
                    catch (Exception e)
                    {
                        resultStatus = TestRailResultsStatus.Default.RETEST;
                        comment = "Unhandled exception \"" + e.Message + "\"";

                        if (software == InstrumentManager.Storm || software == InstrumentManager.AIX)
                        {
                            try
                            {
                                WorklistMethods.New();
                            }
                            catch
                            {
                                try
                                {
                                    ApplicationErrorMethods.RestartOnApplicationError();
                                }
                                catch (ApplicationErrorException e2)
                                {
                                    resultStatus = TestRailResultsStatus.Default.FAILED;
                                    comment = "Application erro! \"" + e2.Message + "\"";
                                }
                            }
                        }
                        else if (software == InstrumentManager.Torrent)
                        {
                            try
                            {
                                TempestConfirmationMethods.ConfirmCancel();
                            }
                            catch
                            {

                            }
                        }
                    }

                    if (!results.ContainsKey(testCaseName))
                    {
                        results.Add(testCaseName, new TestResults(testCaseName, resultStatus, comment));
                    }
                }

            }

            this.results = new List<TestResults>(results.Values);
            running = false;

        }
    }
}
