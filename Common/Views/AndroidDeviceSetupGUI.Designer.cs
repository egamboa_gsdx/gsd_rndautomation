﻿
namespace GSD_RnDAutomation.Views
{
    partial class AndroidDeviceSetupGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AndroidDeviceSetupGUI));
            this.continueBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.deviceNameLbl = new System.Windows.Forms.Label();
            this.udidLbl = new System.Windows.Forms.Label();
            this.deviceNameTxt = new System.Windows.Forms.TextBox();
            this.udidTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.platformNameLbl = new System.Windows.Forms.Label();
            this.platformVersionLbl = new System.Windows.Forms.Label();
            this.platNameTb = new System.Windows.Forms.TextBox();
            this.platVerTb = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // continueBtn
            // 
            this.continueBtn.Location = new System.Drawing.Point(268, 174);
            this.continueBtn.Name = "continueBtn";
            this.continueBtn.Size = new System.Drawing.Size(75, 23);
            this.continueBtn.TabIndex = 0;
            this.continueBtn.Text = "Continue";
            this.continueBtn.UseVisualStyleBackColor = true;
            this.continueBtn.Click += new System.EventHandler(this.continueBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(187, 174);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 1;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // deviceNameLbl
            // 
            this.deviceNameLbl.AutoSize = true;
            this.deviceNameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceNameLbl.Location = new System.Drawing.Point(17, 117);
            this.deviceNameLbl.Name = "deviceNameLbl";
            this.deviceNameLbl.Size = new System.Drawing.Size(101, 18);
            this.deviceNameLbl.TabIndex = 2;
            this.deviceNameLbl.Text = "Device Name:";
            // 
            // udidLbl
            // 
            this.udidLbl.AutoSize = true;
            this.udidLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udidLbl.Location = new System.Drawing.Point(17, 147);
            this.udidLbl.Name = "udidLbl";
            this.udidLbl.Size = new System.Drawing.Size(42, 18);
            this.udidLbl.TabIndex = 3;
            this.udidLbl.Text = "UdId:";
            // 
            // deviceNameTxt
            // 
            this.deviceNameTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceNameTxt.Location = new System.Drawing.Point(154, 111);
            this.deviceNameTxt.Name = "deviceNameTxt";
            this.deviceNameTxt.Size = new System.Drawing.Size(189, 24);
            this.deviceNameTxt.TabIndex = 4;
            // 
            // udidTxt
            // 
            this.udidTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udidTxt.Location = new System.Drawing.Point(154, 141);
            this.udidTxt.Name = "udidTxt";
            this.udidTxt.Size = new System.Drawing.Size(189, 24);
            this.udidTxt.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(269, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Please Verify Android Device is Correct.";
            // 
            // platformNameLbl
            // 
            this.platformNameLbl.AutoSize = true;
            this.platformNameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.platformNameLbl.Location = new System.Drawing.Point(17, 52);
            this.platformNameLbl.Name = "platformNameLbl";
            this.platformNameLbl.Size = new System.Drawing.Size(112, 18);
            this.platformNameLbl.TabIndex = 7;
            this.platformNameLbl.Text = "Platform Name:";
            // 
            // platformVersionLbl
            // 
            this.platformVersionLbl.AutoSize = true;
            this.platformVersionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.platformVersionLbl.Location = new System.Drawing.Point(17, 83);
            this.platformVersionLbl.Name = "platformVersionLbl";
            this.platformVersionLbl.Size = new System.Drawing.Size(122, 18);
            this.platformVersionLbl.TabIndex = 8;
            this.platformVersionLbl.Text = "Platform Version:";
            // 
            // platNameTb
            // 
            this.platNameTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.platNameTb.Location = new System.Drawing.Point(154, 49);
            this.platNameTb.Name = "platNameTb";
            this.platNameTb.Size = new System.Drawing.Size(189, 24);
            this.platNameTb.TabIndex = 9;
            // 
            // platVerTb
            // 
            this.platVerTb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.platVerTb.Location = new System.Drawing.Point(154, 80);
            this.platVerTb.Name = "platVerTb";
            this.platVerTb.Size = new System.Drawing.Size(189, 24);
            this.platVerTb.TabIndex = 10;
            // 
            // AndroidDeviceSetupGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 209);
            this.Controls.Add(this.platVerTb);
            this.Controls.Add(this.platNameTb);
            this.Controls.Add(this.platformVersionLbl);
            this.Controls.Add(this.platformNameLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.udidTxt);
            this.Controls.Add(this.deviceNameTxt);
            this.Controls.Add(this.udidLbl);
            this.Controls.Add(this.deviceNameLbl);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.continueBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AndroidDeviceSetupGUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Setup Android Device";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button continueBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Label deviceNameLbl;
        private System.Windows.Forms.Label udidLbl;
        private System.Windows.Forms.TextBox deviceNameTxt;
        private System.Windows.Forms.TextBox udidTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label platformNameLbl;
        private System.Windows.Forms.Label platformVersionLbl;
        private System.Windows.Forms.TextBox platNameTb;
        private System.Windows.Forms.TextBox platVerTb;
    }
}