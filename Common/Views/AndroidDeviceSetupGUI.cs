﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

using GSD_RnDAutomation.Lib.TORRENT;

namespace GSD_RnDAutomation.Views
{
    public partial class AndroidDeviceSetupGUI : Form
    {
        private string _regKeyLocation = @"SOFTWARE\Gold Standard Diagnostics\AutomationPlan";
        private string _regKeyDeviceName = "DeviceName";
        private string _regKeyUdId = "UdId";
        private string _regKeyPlatformName = "PlatformName";
        private string _regKeyPlatformVersion = "PlatformVersion";
        private RegistryKey RegKey;
        public AndroidDeviceSetupGUI()
        {
            InitializeComponent();
            RegKey = Registry.LocalMachine.CreateSubKey(_regKeyLocation, true);

            string platformName = (string)RegKey.GetValue(_regKeyPlatformName);
            string platformVersion = (string)RegKey.GetValue(_regKeyPlatformVersion);
            string deviceName = (string)RegKey.GetValue(_regKeyDeviceName);
            string udid = (string)RegKey.GetValue(_regKeyUdId);

            platNameTb.Text = !string.IsNullOrEmpty(platformName) ? platformName : AndroidDeviceProperty.PlatformName;

            platVerTb.Text = !string.IsNullOrEmpty(platformVersion) ? platformVersion : AndroidDeviceProperty.PlatformVersion;

            deviceNameTxt.Text = !string.IsNullOrEmpty(deviceName) ? deviceName : AndroidDeviceProperty.DeviceName;

            udidTxt.Text = !string.IsNullOrEmpty(udid) ? udid : AndroidDeviceProperty.UdId;
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void continueBtn_Click(object sender, EventArgs e)
        {
            // Save Android Device settings to registry
            RegKey.SetValue(_regKeyPlatformName, platNameTb.Text);
            RegKey.SetValue(_regKeyPlatformVersion, platVerTb.Text);
            RegKey.SetValue(_regKeyDeviceName, deviceNameTxt.Text);
            RegKey.SetValue(_regKeyUdId, udidTxt.Text);

            AndroidDeviceProperty.PlatformName = platNameTb.Text;
            AndroidDeviceProperty.PlatformVersion = platVerTb.Text;
            AndroidDeviceProperty.DeviceName = deviceNameTxt.Text;
            AndroidDeviceProperty.UdId = udidTxt.Text;

            RegKey.Close();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
