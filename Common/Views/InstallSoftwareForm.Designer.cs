﻿
namespace GSD_RnDAutomation
{
    partial class InstallSoftwareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnStorm = new System.Windows.Forms.Button();
            this.btnAIX = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Storm or AIX?";
            // 
            // btnStorm
            // 
            this.btnStorm.Location = new System.Drawing.Point(16, 56);
            this.btnStorm.Name = "btnStorm";
            this.btnStorm.Size = new System.Drawing.Size(75, 23);
            this.btnStorm.TabIndex = 1;
            this.btnStorm.Text = "Storm";
            this.btnStorm.UseVisualStyleBackColor = true;
            this.btnStorm.Click += new System.EventHandler(this.btnStorm_Click);
            // 
            // btnAIX
            // 
            this.btnAIX.Location = new System.Drawing.Point(97, 56);
            this.btnAIX.Name = "btnAIX";
            this.btnAIX.Size = new System.Drawing.Size(75, 23);
            this.btnAIX.TabIndex = 2;
            this.btnAIX.Text = "AIX1000";
            this.btnAIX.UseVisualStyleBackColor = true;
            this.btnAIX.Click += new System.EventHandler(this.btnAIX_Click);
            // 
            // InstallSoftwareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(199, 101);
            this.Controls.Add(this.btnAIX);
            this.Controls.Add(this.btnStorm);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InstallSoftwareForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Software Install";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStorm;
        private System.Windows.Forms.Button btnAIX;
    }
}