﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSD_RnDAutomation
{
    public partial class InstallSoftwareForm : Form
    {
        string software;
        public InstallSoftwareForm()
        {
            InitializeComponent();
        }

        private void btnStorm_Click(object sender, EventArgs e)
        {
            software = "Storm";
            this.Close();
        }

        private void btnAIX_Click(object sender, EventArgs e)
        {
            software = "AIX";
            this.Close();
        }
        public string Software
        {
            get { return software; }
        }

        
    }
}
