﻿
namespace GSD_RnDAutomation.Views
{
    partial class ResultsGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultsGUI));
            this.listView1 = new System.Windows.Forms.ListView();
            this.TestCaseHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ResultHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CommentHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.UploadHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RunAgainButton = new System.Windows.Forms.Button();
            this.UploadButton = new System.Windows.Forms.Button();
            this.DoneButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.TestCaseHeader,
            this.ResultHeader,
            this.CommentHeader,
            this.UploadHeader});
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(17, 17);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1129, 356);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // TestCaseHeader
            // 
            this.TestCaseHeader.Text = "Test Case";
            this.TestCaseHeader.Width = 400;
            // 
            // ResultHeader
            // 
            this.ResultHeader.Text = "Result(s)";
            // 
            // CommentHeader
            // 
            this.CommentHeader.Text = "Comment";
            this.CommentHeader.Width = 400;
            // 
            // UploadHeader
            // 
            this.UploadHeader.Text = "Upload";
            // 
            // RunAgainButton
            // 
            this.RunAgainButton.Location = new System.Drawing.Point(849, 379);
            this.RunAgainButton.Name = "RunAgainButton";
            this.RunAgainButton.Size = new System.Drawing.Size(95, 33);
            this.RunAgainButton.TabIndex = 2;
            this.RunAgainButton.Text = "Run Again";
            this.RunAgainButton.UseVisualStyleBackColor = true;
            this.RunAgainButton.Click += new System.EventHandler(this.RunAgainButton_Click);
            // 
            // UploadButton
            // 
            this.UploadButton.Location = new System.Drawing.Point(950, 379);
            this.UploadButton.Name = "UploadButton";
            this.UploadButton.Size = new System.Drawing.Size(95, 33);
            this.UploadButton.TabIndex = 3;
            this.UploadButton.Text = "Upload";
            this.UploadButton.UseVisualStyleBackColor = true;
            this.UploadButton.Click += new System.EventHandler(this.UploadButton_Click);
            // 
            // DoneButton
            // 
            this.DoneButton.Location = new System.Drawing.Point(1051, 379);
            this.DoneButton.Name = "DoneButton";
            this.DoneButton.Size = new System.Drawing.Size(95, 33);
            this.DoneButton.TabIndex = 4;
            this.DoneButton.Text = "Done";
            this.DoneButton.UseVisualStyleBackColor = true;
            this.DoneButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // ResultsGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1158, 430);
            this.Controls.Add(this.DoneButton);
            this.Controls.Add(this.UploadButton);
            this.Controls.Add(this.RunAgainButton);
            this.Controls.Add(this.listView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ResultsGUI";
            this.Text = "Automation Test Results";
            this.Load += new System.EventHandler(this.ResultsGUI_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader TestCaseHeader;
        private System.Windows.Forms.ColumnHeader ResultHeader;
        private System.Windows.Forms.ColumnHeader CommentHeader;
        private System.Windows.Forms.ColumnHeader UploadHeader;
        private System.Windows.Forms.Button RunAgainButton;
        private System.Windows.Forms.Button UploadButton;
        private System.Windows.Forms.Button DoneButton;
    }
}