﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using GSD_RnDAutomation.Common.Testing;

namespace GSD_RnDAutomation.Views
{
    public partial class ResultsGUI : Form
    {
        private TestSuite[] testSuites;
        private bool automaticUpload;
        internal ResultsGUI(TestSuite[] testSuites, bool automaticUpload = false)
        {
            this.DialogResult = DialogResult.Cancel;
            this.automaticUpload = automaticUpload;

            this.testSuites = testSuites;
            InitializeComponent();

            if (automaticUpload)
            {
                UploadButton_Click(null, null);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RunAgainButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Retry;
            this.Close();
        }

        private void ResultsGUI_Load(object sender, EventArgs e)
        {
            DisplayResults();

            this.BringToFront();

            try
            {
                SaveResultsLocally();
            }
            catch
            {

            }
        }

        /// <summary>
        /// Saves the results to a csv file on the computer in the event that the TestRail upload fails.
        /// </summary>
        private void SaveResultsLocally()
        {
            string StormResultsFolder = @"C:\Test Automation\Storm Results\";
            string AIXResultsFolder = @"C:\Test Automation\AIX Results\";
            foreach (TestSuite testSuite in testSuites)
            {
                string resultsFolder = "";
                switch (testSuite.InstrumentManager)
                {
                    case InstrumentManager.Storm:
                        if (!Directory.Exists(StormResultsFolder)) Directory.CreateDirectory(StormResultsFolder);
                        resultsFolder = StormResultsFolder;
                        break;
                    case InstrumentManager.AIX:
                        if (!Directory.Exists(AIXResultsFolder)) Directory.CreateDirectory(AIXResultsFolder);
                        resultsFolder = AIXResultsFolder;
                        break;
                }
                string softwareVersionString = ApplicationManager.GetFormattedSoftwareVersionString(testSuite.InstrumentManager);

                StreamWriter csvWriter = new StreamWriter(resultsFolder + softwareVersionString + ".csv");
                csvWriter.WriteLine("Test,Result");

                foreach (TestResults result in testSuite.Results)
                {
                    csvWriter.WriteLine(result.TestCaseName + "," + result.TestRailSResultStatus);
                }

                csvWriter.Flush();
                csvWriter.Close();
            }
        }

        /// <summary>
        /// Updates the list to show the results. Called a second time to show TestRail upload failures.
        /// </summary>
        private void DisplayResults()
        {
            listView1.Items.Clear();
            listView1.Groups.Clear();
            foreach (TestSuite testSuite in testSuites)
            {
                ListViewGroup group = new ListViewGroup(testSuite.InstrumentManager.ToString("F"));
                listView1.Groups.Add(group);

                foreach (TestResults result in testSuite.Results)
                {
                    string[] columns = { result.TestCaseName, result.TestRailSResultStatus, result.Comment, result.UploadStatus };
                    ListViewItem resultItem = new ListViewItem(columns);
                    resultItem.Group = group;
                    listView1.Items.Add(resultItem);
                }
            }
        }

        private void UploadButton_Click(object sender, EventArgs e)
        {
            if (UploadButton.Enabled)
            {
                UploadButton.Enabled = false;
                UploadButton.Text = "Uploading";

                try
                {
                    foreach (TestSuite testSuite in testSuites)
                    {
                        TestRailUploader uploader = new TestRailUploader(testSuite.InstrumentManager, testSuite.RackManager);
                        testSuite.Results = uploader.UploadResults(testSuite.Results, automaticUpload);
                        UploadButton.Text = "Uploaded";
                    }
                }
                catch
                {
                    UploadButton.Enabled = true;
                    UploadButton.Text = "Retry";
                }
                DisplayResults();
            }
        }
    }
}
