﻿
namespace GSD_RnDAutomation
{
    partial class ServerStatusGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerStatusGUI));
            this.statusBox1 = new System.Windows.Forms.RichTextBox();
            this.statusLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblByteSent = new System.Windows.Forms.Label();
            this.lblBytesReceived = new System.Windows.Forms.Label();
            this.lblBytesTotal = new System.Windows.Forms.Label();
            this.lblServerStatus = new System.Windows.Forms.Label();
            this.lblTotalMBytesSent = new System.Windows.Forms.Label();
            this.lblTotalMBytesReceived = new System.Windows.Forms.Label();
            this.lblTotalMBytes = new System.Windows.Forms.Label();
            this.lblServerCurrentStatus = new System.Windows.Forms.Label();
            this.lblTestRun = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTestRunAmt = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Automation = new System.ComponentModel.BackgroundWorker();
            this.statusLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBox1
            // 
            this.statusBox1.Location = new System.Drawing.Point(0, 0);
            this.statusBox1.Name = "statusBox1";
            this.statusBox1.ReadOnly = true;
            this.statusBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.statusBox1.Size = new System.Drawing.Size(284, 104);
            this.statusBox1.TabIndex = 0;
            this.statusBox1.Text = "Running...CTRL + Q to QUIT...";
            this.statusBox1.TextChanged += new System.EventHandler(this.statusBox1_TextChanged);
            // 
            // statusLayoutPanel1
            // 
            this.statusLayoutPanel1.Controls.Add(this.lblByteSent);
            this.statusLayoutPanel1.Controls.Add(this.lblBytesReceived);
            this.statusLayoutPanel1.Controls.Add(this.lblBytesTotal);
            this.statusLayoutPanel1.Controls.Add(this.lblServerStatus);
            this.statusLayoutPanel1.Controls.Add(this.lblTotalMBytesSent);
            this.statusLayoutPanel1.Controls.Add(this.lblTotalMBytesReceived);
            this.statusLayoutPanel1.Controls.Add(this.lblTotalMBytes);
            this.statusLayoutPanel1.Controls.Add(this.lblServerCurrentStatus);
            this.statusLayoutPanel1.Controls.Add(this.lblTestRun);
            this.statusLayoutPanel1.Controls.Add(this.label1);
            this.statusLayoutPanel1.Controls.Add(this.label4);
            this.statusLayoutPanel1.Controls.Add(this.label5);
            this.statusLayoutPanel1.Controls.Add(this.lblTestRunAmt);
            this.statusLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.statusLayoutPanel1.Location = new System.Drawing.Point(0, 40);
            this.statusLayoutPanel1.Name = "statusLayoutPanel1";
            this.statusLayoutPanel1.Size = new System.Drawing.Size(284, 82);
            this.statusLayoutPanel1.TabIndex = 1;
            // 
            // lblByteSent
            // 
            this.lblByteSent.AutoSize = true;
            this.lblByteSent.Location = new System.Drawing.Point(3, 3);
            this.lblByteSent.Margin = new System.Windows.Forms.Padding(3);
            this.lblByteSent.Name = "lblByteSent";
            this.lblByteSent.Size = new System.Drawing.Size(78, 13);
            this.lblByteSent.TabIndex = 0;
            this.lblByteSent.Text = "Total MB Sent:";
            // 
            // lblBytesReceived
            // 
            this.lblBytesReceived.AutoSize = true;
            this.lblBytesReceived.Location = new System.Drawing.Point(3, 22);
            this.lblBytesReceived.Margin = new System.Windows.Forms.Padding(3);
            this.lblBytesReceived.Name = "lblBytesReceived";
            this.lblBytesReceived.Size = new System.Drawing.Size(102, 13);
            this.lblBytesReceived.TabIndex = 1;
            this.lblBytesReceived.Text = "Total MB Received:";
            // 
            // lblBytesTotal
            // 
            this.lblBytesTotal.AutoSize = true;
            this.lblBytesTotal.Location = new System.Drawing.Point(3, 41);
            this.lblBytesTotal.Margin = new System.Windows.Forms.Padding(3);
            this.lblBytesTotal.Name = "lblBytesTotal";
            this.lblBytesTotal.Size = new System.Drawing.Size(53, 13);
            this.lblBytesTotal.TabIndex = 2;
            this.lblBytesTotal.Text = "Total MB:";
            // 
            // lblServerStatus
            // 
            this.lblServerStatus.AutoSize = true;
            this.lblServerStatus.Location = new System.Drawing.Point(3, 60);
            this.lblServerStatus.Margin = new System.Windows.Forms.Padding(3);
            this.lblServerStatus.Name = "lblServerStatus";
            this.lblServerStatus.Size = new System.Drawing.Size(71, 13);
            this.lblServerStatus.TabIndex = 3;
            this.lblServerStatus.Text = "Sever Status:";
            // 
            // lblTotalMBytesSent
            // 
            this.lblTotalMBytesSent.AutoSize = true;
            this.lblTotalMBytesSent.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblTotalMBytesSent.Location = new System.Drawing.Point(111, 3);
            this.lblTotalMBytesSent.Margin = new System.Windows.Forms.Padding(3);
            this.lblTotalMBytesSent.Name = "lblTotalMBytesSent";
            this.lblTotalMBytesSent.Size = new System.Drawing.Size(13, 13);
            this.lblTotalMBytesSent.TabIndex = 4;
            this.lblTotalMBytesSent.Text = "0";
            // 
            // lblTotalMBytesReceived
            // 
            this.lblTotalMBytesReceived.AutoSize = true;
            this.lblTotalMBytesReceived.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblTotalMBytesReceived.Location = new System.Drawing.Point(111, 22);
            this.lblTotalMBytesReceived.Margin = new System.Windows.Forms.Padding(3);
            this.lblTotalMBytesReceived.Name = "lblTotalMBytesReceived";
            this.lblTotalMBytesReceived.Size = new System.Drawing.Size(13, 13);
            this.lblTotalMBytesReceived.TabIndex = 5;
            this.lblTotalMBytesReceived.Text = "0";
            // 
            // lblTotalMBytes
            // 
            this.lblTotalMBytes.AutoSize = true;
            this.lblTotalMBytes.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblTotalMBytes.Location = new System.Drawing.Point(111, 41);
            this.lblTotalMBytes.Margin = new System.Windows.Forms.Padding(3);
            this.lblTotalMBytes.Name = "lblTotalMBytes";
            this.lblTotalMBytes.Size = new System.Drawing.Size(13, 13);
            this.lblTotalMBytes.TabIndex = 6;
            this.lblTotalMBytes.Text = "0";
            // 
            // lblServerCurrentStatus
            // 
            this.lblServerCurrentStatus.AutoSize = true;
            this.lblServerCurrentStatus.Location = new System.Drawing.Point(111, 60);
            this.lblServerCurrentStatus.Margin = new System.Windows.Forms.Padding(3);
            this.lblServerCurrentStatus.Name = "lblServerCurrentStatus";
            this.lblServerCurrentStatus.Size = new System.Drawing.Size(47, 13);
            this.lblServerCurrentStatus.TabIndex = 7;
            this.lblServerCurrentStatus.Text = "\"Status\"";
            // 
            // lblTestRun
            // 
            this.lblTestRun.AutoSize = true;
            this.lblTestRun.Location = new System.Drawing.Point(164, 3);
            this.lblTestRun.Margin = new System.Windows.Forms.Padding(3);
            this.lblTestRun.Name = "lblTestRun";
            this.lblTestRun.Size = new System.Drawing.Size(54, 13);
            this.lblTestRun.TabIndex = 2;
            this.lblTestRun.Text = "Test Run ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(164, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(164, 41);
            this.label4.Margin = new System.Windows.Forms.Padding(3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(164, 60);
            this.label5.Margin = new System.Windows.Forms.Padding(3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 9;
            // 
            // lblTestRunAmt
            // 
            this.lblTestRunAmt.AutoSize = true;
            this.lblTestRunAmt.Location = new System.Drawing.Point(224, 3);
            this.lblTestRunAmt.Margin = new System.Windows.Forms.Padding(3);
            this.lblTestRunAmt.Name = "lblTestRunAmt";
            this.lblTestRunAmt.Size = new System.Drawing.Size(13, 13);
            this.lblTestRunAmt.TabIndex = 10;
            this.lblTestRunAmt.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(164, 3);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(164, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 11;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Automation
            // 
            this.Automation.WorkerSupportsCancellation = true;
            this.Automation.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Automation_DoWork);
            this.Automation.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Automation_ProgressChanged);
            // 
            // ServerStatusGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 141);
            this.Controls.Add(this.statusLayoutPanel1);
            this.Controls.Add(this.statusBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ServerStatusGUI";
            this.Opacity = 0.5D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ServerStatus";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Status_FormClosing);
            this.Load += new System.EventHandler(this.Status_load);
            this.statusLayoutPanel1.ResumeLayout(false);
            this.statusLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox statusBox1;
        private System.Windows.Forms.FlowLayoutPanel statusLayoutPanel1;
        private System.Windows.Forms.Label lblByteSent;
        private System.Windows.Forms.Label lblBytesReceived;
        private System.Windows.Forms.Label lblBytesTotal;
        private System.Windows.Forms.Label lblServerStatus;
        private System.Windows.Forms.Label lblTotalMBytesSent;
        private System.Windows.Forms.Label lblTotalMBytesReceived;
        private System.Windows.Forms.Label lblTotalMBytes;
        private System.Windows.Forms.Label lblServerCurrentStatus;
        private System.Windows.Forms.Label lblTestRun;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTestRunAmt;
        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.BackgroundWorker Automation;
    }
}