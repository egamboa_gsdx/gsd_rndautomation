﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.ServiceProcess;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

using GSD_RnDAutomation.Properties;
using GSD_RnDAutomation.StressTest;
using GSD_RnDAutomation.Configuration;

using NHotkey.WindowsForms;
using System.Runtime.InteropServices;

namespace GSD_RnDAutomation
{
    public partial class ServerStatusGUI : Form
    {
        private StressTestType stressTest;
        private InstrumentManager software;
        private NetworkPerformanceReporter networkReporter;
        private StreamWriter csvWriter;
        private int iterator = 0;
        private const int ticksUntilReport = 8;
        private double totalBSent, totalBReceived, TotalBytes;
        private bool DontLogServerData = false;

        internal ServerStatusGUI(StressTestType stressTest, InstrumentManager software)
        {
            this.stressTest = stressTest;
            this.software = software;

            //Create and open stream to write csv log
            var fileAndVersionInfo = ApplicationManager.GetFormattedSoftwareVersionString(software);
            csvWriter = new StreamWriter(StressTestProperty.Default.ResultsFolder + @"\ServerStressTest_" + fileAndVersionInfo + ".csv");
            csvWriter.WriteLine("Time,Bytes Sent,Bytes Received,Bytes Total,Server Status,Testrun");

            //Register Key Preview Property and hotkey to quit the application
            this.KeyPreview = true;
            HotkeyManager.Current.AddOrReplace("Quit", Keys.Control | Keys.Q, Status_HotkeyDown);

            InitializeComponent();
        }

        //On Load perform this
        private void Status_load(object sender, EventArgs e)
        {
            //Set default start position to the top right corner
            var src = Screen.FromPoint(this.Location);
            this.Location = new Point(src.WorkingArea.Right - this.Width, src.WorkingArea.Bottom - this.Height);

            //Start Network Reporting
            var processString = GetProcessString();
            networkReporter = NetworkPerformanceReporter.Create(processString);
            this.statusBox1.Text += "\nNetwork Reporting Started";
            timer1.Start();
            this.statusBox1.Text += "\nTimer Started";
            this.statusBox1.Text += "\nReporting every: " + (ticksUntilReport * timer1.Interval).ToString() + "ms";
            this.statusBox1.Text += "\nRefreshing every: " + (timer1.Interval).ToString() + "ms";

            //Register and start automation thread
            Automation.WorkerReportsProgress = true;
            Automation.RunWorkerAsync();

            //Set server status string and grab server status
            var status = SetServiceStatusText();

            //If the service is running log the data. If not then just log the test runs
            DontLogServerData = (status == "Running") ? false : true;

            //if the local server is not available then log jsut the test run instead
            if (DontLogServerData)
            {
                statusBox1.Text += "\nLocal Server Not Found!\nCollecting Test Run Data only!";
            }

        }

        //get the process name
        private string GetProcessString()
        {
            if (StressTestProperty.Default.StormDownload || StressTestProperty.Default.StormUpload)
                return "StormServer";
            else if (StressTestProperty.Default.AIXDownload || StressTestProperty.Default.AIXUpload)
                return "AIX1000Server";
            else
                return "";
        }

        //while the form is closing perform these actions
        private void Status_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Write exit command in console
            this.statusBox1.Text += "\nExited: " + DateTime.Now;

            //flush csv buffer and close the streamwriter
            csvWriter.Flush();
            //csvWriter.Close();
        }

        //Timer which is defined in the WinForms App
        private void timer1_Tick(object sender, EventArgs e)
        {
            iterator += 1;

            //Timer goes off every half second. Only wirtes to the log every 8*500 ms or 4 seconds
            var testRun = StressTestProperty.Default.TestRunAmount;
            this.lblTestRunAmt.Text = testRun.ToString();

            //set server status string and grab server status
            SetServiceStatusText();

            if (DontLogServerData)
            {
                //Write info to log
                csvWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5}", DateTime.Now, "N/A", "N/A", "N/A", "Server Not Found!", testRun));
                csvWriter.Flush();
                iterator = 0;
                return;
            }

            try
            {
                //Get network data and populate GUI
                var NetworkInfo = networkReporter.GetNetworkPerformanceData();
                totalBSent += NetworkInfo.BytesSent;
                totalBReceived += NetworkInfo.BytesReceived;
                TotalBytes += (NetworkInfo.BytesReceived + NetworkInfo.BytesSent);

                //Set GUI Values
                lblTotalMBytesSent.Text = (Math.Round(totalBSent / 1048576, 2)).ToString();
                lblTotalMBytesReceived.Text = (Math.Round(totalBReceived / 1048576, 2)).ToString();
                lblTotalMBytes.Text = (Math.Round(TotalBytes / 1048576, 2)).ToString();

                if(iterator == ticksUntilReport)
                {
                    //Write info to log
                    csvWriter.WriteLine(String.Format("{0},{1},{2},{3},{4},{5}", 
                                        DateTime.Now, totalBSent, totalBReceived, 
                                        TotalBytes, lblServerCurrentStatus.Text, testRun));
                    csvWriter.Flush();
                    iterator = 0;
                }
            } catch(Exception ex)
            {
                if(!(System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"Documents\Stress Test\Log")))
                {
                    System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"Documents\Stress Test\Log");
                }
                System.IO.File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
                                            + @"Documents\Stress Test\Log\Error_" 
                                            + DateTime.Now.ToString("yyy-dd-M--HH-mm-ss")
                                            + ".txt", ex.Message + ex.StackTrace);
                DontLogServerData = true;
            }
        }
        private void Status_HotkeyDown(object sender, NHotkey.HotkeyEventArgs e)
        {
            //this.DialogResult = DialogResult.Retry;
            //this.Close();
            Application.Exit();
        }

        //Set the proper server status string based off the server service
        private string SetServiceStatusText()
        {
            try
            {
                var status = ApplicationManager.GetServerStatus(software);
                this.lblServerCurrentStatus.Text = status.ToString();
                if(status != ServiceControllerStatus.Running)
                {
                    this.lblServerCurrentStatus.ForeColor = Color.Yellow;
                }
                else
                {
                    this.lblServerCurrentStatus.ForeColor = Color.Green;
                }
                return status.ToString();
            }
            catch
            {
                this.lblServerCurrentStatus.ForeColor = Color.Gray;
                this.lblServerCurrentStatus.Text = "Not Found!";
                return "Not Found";
            }
        }

        //Background worker for the Automation methods, Runs the automation in the background asynchronously
        //while outputting data via ReportProgress events
        private void Automation_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int testNum = Convert.ToInt32(StressTestProperty.Default.TestNumber);
                if (StressTestProperty.Default.StormUpload && !StressTestProperty.Default.StormDownload)
                {
                    Upload(testNum);
                }
                else if (StressTestProperty.Default.AIXUpload && !StressTestProperty.Default.AIXDownload)
                {
                    Upload(testNum);
                }
                else if (StressTestProperty.Default.StormDownload && !StressTestProperty.Default.StormUpload)
                {
                    Download(testNum);
                }
                else if ( StressTestProperty.Default.AIXDownload && !StressTestProperty.Default.AIXUpload)
                {
                    Download(testNum);
                }
                if (StressTestProperty.Default.AIXUpload && StressTestProperty.Default.AIXDownload)
                {
                    UploadAndDownload(testNum);
                }
                if (StressTestProperty.Default.StormUpload && StressTestProperty.Default.StormDownload)
                {
                    UploadAndDownload(testNum);
                }

            }
            catch (Exception exception)
            {
                Automation.ReportProgress(1, "\n" + exception.Message + "\n" + exception.StackTrace);
                Thread.Sleep(20000);
                Thread.CurrentThread.Abort();
            }
        }

        //also part of the background worker
        //event fires and changes the status box with an update of the current automation process
        private void Automation_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            statusBox1.Text += e.UserState as string;
            if(e.ProgressPercentage == 1)
            {
                Automation.Dispose();
            }
        }

        //Main method that executes the download test
        private void Download(int numberOfClients)
        {
            var TestRun = 0;
            var Methods = new StressTestMethods();
            List<int> clients = new List<int>();

            //Setup
            for(int i = 0; i < numberOfClients; i++)
            {
                AutomationConfiguration.IsSetup = true;
                Automation.ReportProgress(0, "\nLauncing Instrument Manager and Logging in...");
                clients.Add(ApplicationManager.SwitchOrLaunchAndLogin(software));
                Automation.ReportProgress(0, "Done!");

                Automation.ReportProgress(0, "\nCreating worklist...");
                Methods.CreateWorklist();
                Automation.ReportProgress(0, "Done!");
                AutomationConfiguration.IsSetup = false;
            }

            //Test
            while (true)
            {
                foreach(int clientPID in clients)
                {
                    //Switch to the correct instrument manager
                    ApplicationManager.SwitchTo(clientPID, software);
                   
                    //attempts to start test. If Successful increment test run integer
                    if (Methods.SpecialStartReRead())
                    {
                        TestRun++;
                        StressTestProperty.Default.TestRunAmount = TestRun;
                    }

                    //Check for Errors and Report them to the automation console
                    if (Methods.ErrorFlag)
                    {
                        Automation.ReportProgress(1, "\nERROR! Application Error or Server Communication Loss!");
                        Thread.CurrentThread.Abort();
                    }
                }
            }
        }

        //Main Method that executes the Upload Test
        private void Upload(int numberOfClients)
        {
            var TestRun = 0;
            var Methods = new StressTestMethods();
            List<int> clients = new List<int>();

            //Setup
            for(int i = 0; i < numberOfClients; i++)
            {
                AutomationConfiguration.IsSetup = true;
                Automation.ReportProgress(0, "\nLauncing Instrument Manager and Logging in...");
                clients.Add(ApplicationManager.SwitchOrLaunchAndLogin(software));
                Automation.ReportProgress(0, "Done!");

                Thread.Sleep(500);
                Methods.ClickEvalButton();
                AutomationConfiguration.IsSetup = false;
            }

            //Test
            while (true)
            {
                foreach(int clientPID in clients)
                {
                    ApplicationManager.SwitchTo(clientPID, software);

                    Methods.ClickEvalButton();
                    //attemp to start test. If successful incremtn test run integer
                    if (Methods.Evaluation())
                    {
                        TestRun++;
                        StressTestProperty.Default.TestRunAmount = (int)TestRun;
                    }

                    //Check for errors and reprot them to the automation console
                    if (Methods.ErrorFlag)
                    {
                        Automation.ReportProgress(1, "\nERROR! Application Error or Server Communication Loss!");
                        Thread.CurrentThread.Abort();
                    }
                }
            }
        }


        private void UploadAndDownload(int numberOfClients)
        {

            var TestRun1 = 0;
            var TestRun2 = 0;
            var TestRun = TestRun1+TestRun2;
            var Methods = new StressTestMethods();
            List<int> clients = new List<int>();

            //Setup
            for (int i = 0; i < numberOfClients; i++)
            {
                AutomationConfiguration.IsSetup = true;
                Automation.ReportProgress(0, "\nLauncing Instrument Manager and Logging in...");
                clients.Add(ApplicationManager.SwitchOrLaunchAndLogin(software));
                Automation.ReportProgress(0, "Done!");

                Automation.ReportProgress(0, "\nCreating worklist...");
                Methods.CreateWorklist();
                Automation.ReportProgress(0, "Done!");
                
                AutomationConfiguration.IsSetup = false;

            }

            //Test
            while (true)
            {
                foreach (int clientPID in clients)
                {
                    //Switch to the correct instrument manager
                    ApplicationManager.SwitchTo(clientPID, software);

                    


                    //attempts to start test. If Successful increment test run integer
                    if (Methods.SpecialStartReRead())
                    {
                        TestRun2++;
                        StressTestProperty.Default.TestRunAmount =(int) TestRun2;
                    }

                    Methods.ClickEvalButton();
                    //attemp to start test. If successful incremtn test run integer
                    if (Methods.Evaluation())
                    {
                        TestRun1++;
                        StressTestProperty.Default.TestRunAmount = (int)TestRun1;
                    }

                    //Check for errors and reprot them to the automation console
                    if (Methods.ErrorFlag)
                    {
                        Automation.ReportProgress(1, "\nERROR! Application Error or Server Communication Loss!");
                        Thread.CurrentThread.Abort();
                    }
                    TestRun = TestRun1 + TestRun2;
                }
            }


        }

        private void statusBox1_TextChanged(object sender, EventArgs e)
        {
            //Set caret to the end position
            statusBox1.SelectionStart = statusBox1.Text.Length;
            //scroll to the bottom fo the status box feed
            statusBox1.ScrollToCaret();
        }
    }
}
