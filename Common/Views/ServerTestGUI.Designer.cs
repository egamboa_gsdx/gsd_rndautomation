﻿
namespace GSD_RnDAutomation
{
    partial class ServerTestGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerTestGUI));
            this.btnReturn = new System.Windows.Forms.Button();
            this.testComboBox = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblStormServerStatus = new System.Windows.Forms.Label();
            this.lblAIXServerStatus = new System.Windows.Forms.Label();
            this.lblTorrentServerStatus = new System.Windows.Forms.Label();
            this.lblStormServerUpdate = new System.Windows.Forms.Label();
            this.lblAIXServerUpdate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblResultsFolder = new System.Windows.Forms.Label();
            this.tbResultsFolder = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lblClientNum = new System.Windows.Forms.Label();
            this.lblGlobalTestDelay = new System.Windows.Forms.Label();
            this.lblTestSetupDelay = new System.Windows.Forms.Label();
            this.clientNumber = new System.Windows.Forms.NumericUpDown();
            this.timeDelay = new System.Windows.Forms.NumericUpDown();
            this.setupTimeDelay = new System.Windows.Forms.NumericUpDown();
            this.btnSaveAndRun = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setupTimeDelay)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(28, 255);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(75, 23);
            this.btnReturn.TabIndex = 0;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // testComboBox
            // 
            this.testComboBox.FormattingEnabled = true;
            this.testComboBox.Items.AddRange(new object[] {
            "- Select Test -",
            "Storm Download",
            "Storm Upload",
            "Storm Download & Upload",
            "AIX Download",
            "AIX Upload",
            "AIX Download & Upload",
            "Storm & AIX Download & Upload",
            "Torrent Download",
            "Torrent Upload",
            "Torrent Download & Upload"});
            this.testComboBox.Location = new System.Drawing.Point(28, 27);
            this.testComboBox.Name = "testComboBox";
            this.testComboBox.Size = new System.Drawing.Size(292, 21);
            this.testComboBox.TabIndex = 1;
            this.testComboBox.Text = "- Select Test -";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.lblStormServerStatus);
            this.flowLayoutPanel1.Controls.Add(this.lblAIXServerStatus);
            this.flowLayoutPanel1.Controls.Add(this.lblTorrentServerStatus);
            this.flowLayoutPanel1.Controls.Add(this.lblStormServerUpdate);
            this.flowLayoutPanel1.Controls.Add(this.lblAIXServerUpdate);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(28, 59);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(292, 83);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // lblStormServerStatus
            // 
            this.lblStormServerStatus.AutoSize = true;
            this.lblStormServerStatus.Location = new System.Drawing.Point(13, 13);
            this.lblStormServerStatus.Margin = new System.Windows.Forms.Padding(3);
            this.lblStormServerStatus.Name = "lblStormServerStatus";
            this.lblStormServerStatus.Size = new System.Drawing.Size(104, 13);
            this.lblStormServerStatus.TabIndex = 0;
            this.lblStormServerStatus.Text = "Storm Server Status:";
            // 
            // lblAIXServerStatus
            // 
            this.lblAIXServerStatus.AutoSize = true;
            this.lblAIXServerStatus.Location = new System.Drawing.Point(13, 32);
            this.lblAIXServerStatus.Margin = new System.Windows.Forms.Padding(3);
            this.lblAIXServerStatus.Name = "lblAIXServerStatus";
            this.lblAIXServerStatus.Size = new System.Drawing.Size(94, 13);
            this.lblAIXServerStatus.TabIndex = 1;
            this.lblAIXServerStatus.Text = "AIX Server Status:";
            // 
            // lblTorrentServerStatus
            // 
            this.lblTorrentServerStatus.AutoSize = true;
            this.lblTorrentServerStatus.Location = new System.Drawing.Point(13, 51);
            this.lblTorrentServerStatus.Margin = new System.Windows.Forms.Padding(3);
            this.lblTorrentServerStatus.Name = "lblTorrentServerStatus";
            this.lblTorrentServerStatus.Size = new System.Drawing.Size(111, 13);
            this.lblTorrentServerStatus.TabIndex = 3;
            this.lblTorrentServerStatus.Text = "Torrent Server Status:";
            // 
            // lblStormServerUpdate
            // 
            this.lblStormServerUpdate.AutoSize = true;
            this.lblStormServerUpdate.Location = new System.Drawing.Point(130, 13);
            this.lblStormServerUpdate.Margin = new System.Windows.Forms.Padding(3);
            this.lblStormServerUpdate.Name = "lblStormServerUpdate";
            this.lblStormServerUpdate.Size = new System.Drawing.Size(105, 13);
            this.lblStormServerUpdate.TabIndex = 3;
            this.lblStormServerUpdate.Text = "\"StormServerStatus\"";
            // 
            // lblAIXServerUpdate
            // 
            this.lblAIXServerUpdate.AutoSize = true;
            this.lblAIXServerUpdate.Location = new System.Drawing.Point(130, 32);
            this.lblAIXServerUpdate.Margin = new System.Windows.Forms.Padding(3);
            this.lblAIXServerUpdate.Name = "lblAIXServerUpdate";
            this.lblAIXServerUpdate.Size = new System.Drawing.Size(95, 13);
            this.lblAIXServerUpdate.TabIndex = 3;
            this.lblAIXServerUpdate.Text = "\"AIXServerStatus\"";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(130, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "\"TorrentServerStatus\"";
            // 
            // lblResultsFolder
            // 
            this.lblResultsFolder.AutoSize = true;
            this.lblResultsFolder.Location = new System.Drawing.Point(28, 149);
            this.lblResultsFolder.Name = "lblResultsFolder";
            this.lblResultsFolder.Size = new System.Drawing.Size(77, 13);
            this.lblResultsFolder.TabIndex = 3;
            this.lblResultsFolder.Text = "Results Folder:";
            // 
            // tbResultsFolder
            // 
            this.tbResultsFolder.Location = new System.Drawing.Point(28, 166);
            this.tbResultsFolder.Name = "tbResultsFolder";
            this.tbResultsFolder.Size = new System.Drawing.Size(211, 20);
            this.tbResultsFolder.TabIndex = 4;
            this.tbResultsFolder.Text = "C:\\Stress Tests";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(245, 163);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 5;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // lblClientNum
            // 
            this.lblClientNum.AutoSize = true;
            this.lblClientNum.Location = new System.Drawing.Point(28, 193);
            this.lblClientNum.Name = "lblClientNum";
            this.lblClientNum.Size = new System.Drawing.Size(43, 13);
            this.lblClientNum.TabIndex = 6;
            this.lblClientNum.Text = "Client #";
            // 
            // lblGlobalTestDelay
            // 
            this.lblGlobalTestDelay.AutoSize = true;
            this.lblGlobalTestDelay.Location = new System.Drawing.Point(92, 193);
            this.lblGlobalTestDelay.Name = "lblGlobalTestDelay";
            this.lblGlobalTestDelay.Size = new System.Drawing.Size(91, 13);
            this.lblGlobalTestDelay.TabIndex = 7;
            this.lblGlobalTestDelay.Text = "Global Test Delay";
            // 
            // lblTestSetupDelay
            // 
            this.lblTestSetupDelay.AutoSize = true;
            this.lblTestSetupDelay.Location = new System.Drawing.Point(198, 196);
            this.lblTestSetupDelay.Name = "lblTestSetupDelay";
            this.lblTestSetupDelay.Size = new System.Drawing.Size(122, 13);
            this.lblTestSetupDelay.TabIndex = 8;
            this.lblTestSetupDelay.Text = "Global Test Setup Delay";
            // 
            // clientNumber
            // 
            this.clientNumber.Location = new System.Drawing.Point(28, 212);
            this.clientNumber.Name = "clientNumber";
            this.clientNumber.Size = new System.Drawing.Size(43, 20);
            this.clientNumber.TabIndex = 9;
            this.clientNumber.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // timeDelay
            // 
            this.timeDelay.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.timeDelay.Location = new System.Drawing.Point(112, 212);
            this.timeDelay.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.timeDelay.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.timeDelay.Name = "timeDelay";
            this.timeDelay.Size = new System.Drawing.Size(49, 20);
            this.timeDelay.TabIndex = 10;
            this.timeDelay.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // setupTimeDelay
            // 
            this.setupTimeDelay.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.setupTimeDelay.Location = new System.Drawing.Point(217, 212);
            this.setupTimeDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.setupTimeDelay.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.setupTimeDelay.Name = "setupTimeDelay";
            this.setupTimeDelay.Size = new System.Drawing.Size(52, 20);
            this.setupTimeDelay.TabIndex = 11;
            this.setupTimeDelay.Value = new decimal(new int[] {
            800,
            0,
            0,
            0});
            // 
            // btnSaveAndRun
            // 
            this.btnSaveAndRun.Location = new System.Drawing.Point(232, 254);
            this.btnSaveAndRun.Name = "btnSaveAndRun";
            this.btnSaveAndRun.Size = new System.Drawing.Size(87, 23);
            this.btnSaveAndRun.TabIndex = 12;
            this.btnSaveAndRun.Text = "Save and Run";
            this.btnSaveAndRun.UseVisualStyleBackColor = true;
            this.btnSaveAndRun.Click += new System.EventHandler(this.btnSaveAndRun_Click);
            // 
            // ServerTestGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 290);
            this.Controls.Add(this.btnSaveAndRun);
            this.Controls.Add(this.setupTimeDelay);
            this.Controls.Add(this.timeDelay);
            this.Controls.Add(this.clientNumber);
            this.Controls.Add(this.lblTestSetupDelay);
            this.Controls.Add(this.lblGlobalTestDelay);
            this.Controls.Add(this.lblClientNum);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.tbResultsFolder);
            this.Controls.Add(this.lblResultsFolder);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.testComboBox);
            this.Controls.Add(this.btnReturn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ServerTestGUI";
            this.Text = "Server Stress Test";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setupTimeDelay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.ComboBox testComboBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lblStormServerStatus;
        private System.Windows.Forms.Label lblAIXServerStatus;
        private System.Windows.Forms.Label lblTorrentServerStatus;
        private System.Windows.Forms.Label lblStormServerUpdate;
        private System.Windows.Forms.Label lblAIXServerUpdate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblResultsFolder;
        private System.Windows.Forms.TextBox tbResultsFolder;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label lblClientNum;
        private System.Windows.Forms.Label lblGlobalTestDelay;
        private System.Windows.Forms.Label lblTestSetupDelay;
        private System.Windows.Forms.NumericUpDown clientNumber;
        private System.Windows.Forms.NumericUpDown timeDelay;
        private System.Windows.Forms.NumericUpDown setupTimeDelay;
        private System.Windows.Forms.Button btnSaveAndRun;
    }
}