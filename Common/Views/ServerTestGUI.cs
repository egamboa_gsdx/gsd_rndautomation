﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using GSD_RnDAutomation.Configuration;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.StressTest;
using System.ServiceProcess;
using System.Drawing;
using GSD_RnDAutomation.Properties;

namespace GSD_RnDAutomation
{
    public partial class ServerTestGUI : Form
    {
        internal ServerTestGUI()
        {
            
            InitializeComponent();
            //this.tbResultsFolder.Text = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + @"\Documents\Stress Test";
            this.tbResultsFolder.Text = @"C:\Stress Test";
        }

        //Button  Action Event to return to main page of app
        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Retry;
            this.Close();
        }

        private void btnSaveAndRun_Click(object sender, EventArgs e)
        {
            if(testComboBox.SelectedIndex == 0)
            {
                MessageBox.Show("Please select a test...", "Warning");
                return;
            }

            //Check for valid directory
            if (!(Directory.Exists(this.tbResultsFolder.Text)))
            {
                string message = "The selected folder does not exist. Woud you like to create folder?";
                string caption = "Folder Not Found";
                DialogResult result;
                result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo);
                if(result == System.Windows.Forms.DialogResult.No)
                {
                    MessageBox.Show("Please select a valid directory...", "Warning");
                    return;
                }
                else
                {
                    System.IO.Directory.CreateDirectory(this.tbResultsFolder.Text);
                    return;
                }
                
            }

            // save user settings
            switch (testComboBox.SelectedIndex)
            {
                case 1:
                    StressTestProperty.Default.StormDownload = true;
                    StressTestProperty.Default.StormUpload = false;
                    StressTestProperty.Default.AIXDownload = false;
                    StressTestProperty.Default.AIXUpload = false;
                    StressTestProperty.Default.TempestDownload = false;
                    StressTestProperty.Default.TempestUpload = false;
                    break;
                case 2:
                    StressTestProperty.Default.StormDownload = false;
                    StressTestProperty.Default.StormUpload = true;
                    StressTestProperty.Default.AIXDownload = false;
                    StressTestProperty.Default.AIXUpload = false;
                    StressTestProperty.Default.TempestDownload = false;
                    StressTestProperty.Default.TempestUpload = false;
                    break;
                case 3:
                    StressTestProperty.Default.StormDownload = true;
                    StressTestProperty.Default.StormUpload = true;
                    StressTestProperty.Default.AIXDownload = false;
                    StressTestProperty.Default.AIXUpload = false;
                    StressTestProperty.Default.TempestDownload = false;
                    StressTestProperty.Default.TempestUpload = false;
                    break;
                case 4:
                    StressTestProperty.Default.StormDownload = false;
                    StressTestProperty.Default.StormUpload = false;
                    StressTestProperty.Default.AIXDownload = true;
                    StressTestProperty.Default.AIXUpload = false;
                    StressTestProperty.Default.TempestDownload = false;
                    StressTestProperty.Default.TempestUpload = false;
                    break;
                case 5:
                    StressTestProperty.Default.StormDownload = false;
                    StressTestProperty.Default.StormUpload = false;
                    StressTestProperty.Default.AIXDownload = false;
                    StressTestProperty.Default.AIXUpload = true;
                    StressTestProperty.Default.TempestDownload = false;
                    StressTestProperty.Default.TempestUpload = false;
                    break;
                case 6:
                    StressTestProperty.Default.StormDownload = false;
                    StressTestProperty.Default.StormUpload = false;
                    StressTestProperty.Default.AIXDownload = true;
                    StressTestProperty.Default.AIXUpload = true;
                    StressTestProperty.Default.TempestDownload = false;
                    StressTestProperty.Default.TempestUpload = false;
                    break;
                case 7:
                    StressTestProperty.Default.StormDownload = true;
                    StressTestProperty.Default.StormUpload = true;
                    StressTestProperty.Default.AIXDownload = true;
                    StressTestProperty.Default.AIXUpload = true;
                    StressTestProperty.Default.TempestDownload = false;
                    StressTestProperty.Default.TempestUpload = false;
                    break;
                case 8:
                    StressTestProperty.Default.StormDownload = false;
                    StressTestProperty.Default.StormUpload = false;
                    StressTestProperty.Default.AIXDownload = false;
                    StressTestProperty.Default.AIXUpload = false;
                    StressTestProperty.Default.TempestDownload = true;
                    StressTestProperty.Default.TempestUpload = false;
                    break;
                case 9:
                    StressTestProperty.Default.StormDownload = false;
                    StressTestProperty.Default.StormUpload = false;
                    StressTestProperty.Default.AIXDownload = false;
                    StressTestProperty.Default.AIXUpload = false;
                    StressTestProperty.Default.TempestDownload = false;
                    StressTestProperty.Default.TempestUpload = true;
                    break;
                case 10:
                    StressTestProperty.Default.StormDownload = false;
                    StressTestProperty.Default.StormUpload = false;
                    StressTestProperty.Default.AIXDownload = false;
                    StressTestProperty.Default.AIXUpload = false;
                    StressTestProperty.Default.TempestDownload = true;
                    StressTestProperty.Default.TempestUpload = true;
                    break;
                default:
                    StressTestProperty.Default.StormDownload = false;
                    StressTestProperty.Default.StormUpload = false;
                    StressTestProperty.Default.AIXDownload = false;
                    StressTestProperty.Default.AIXUpload = false;
                    StressTestProperty.Default.TempestDownload = false;
                    StressTestProperty.Default.TempestUpload = false;
                    break;


            }
            //Number of Clients
            StressTestProperty.Default.TestNumber = this.clientNumber.Value;

            //Global Delay
            StressTestProperty.Default.GlobalDelay = this.timeDelay.Value;

            //Setup Delay
            StressTestProperty.Default.SetupDelay = this.setupTimeDelay.Value;

            //Results Folder
            StressTestProperty.Default.ResultsFolder = this.tbResultsFolder.Text;

            //Save the changed settings
            StressTestProperty.Default.Save();

            //Configure Automation Framework
            AutomationConfiguration.TimeDelay = (int)timeDelay.Value;
            AutomationConfiguration.SetupTimeDelay = (int)setupTimeDelay.Value;

            StressTestType test;
            InstrumentManager software;

            if(testComboBox.SelectedIndex == 2 || testComboBox.SelectedIndex == 5 || testComboBox.SelectedIndex == 9)
            {
                test = StressTestType.Upload;
            }else if(testComboBox.SelectedIndex == 1 || testComboBox.SelectedIndex == 4 || testComboBox.SelectedIndex == 8)
            {
                test = StressTestType.Download;
            }
            else
            {
                test = StressTestType.UploadAndDownload;
            }

            if(testComboBox.SelectedIndex == 1 || testComboBox.SelectedIndex == 2 || testComboBox.SelectedIndex == 3)
            {
                software = InstrumentManager.Storm;
            }else if(testComboBox.SelectedIndex == 4 || testComboBox.SelectedIndex == 5 || testComboBox.SelectedIndex == 6)
            {
                software = InstrumentManager.AIX;
            }else if(testComboBox.SelectedIndex == 7 || testComboBox.SelectedIndex == 8 || testComboBox.SelectedIndex == 9)
            {
                software = InstrumentManager.Torrent;
            }
            else
            {
                software = InstrumentManager.undefined;
            }

            this.Visible = false;
           
            var StatusWindow = new ServerStatusGUI(test, software);
            StatusWindow.ShowDialog();

            if(StatusWindow.DialogResult == DialogResult.Retry)
            {
                this.Visible = true;;
                //Application.OpenForms[this.Name].Activate();
            }

            // Once the StatusWindow finishes, close this window
            //Close();
        }

        private void UpdateServerService(ref Label label, InstrumentManager software)
        {
            //Poll server services
            try
            {
                var status = ApplicationManager.GetServerStatus(software);
                switch (status)
                {
                    case ServiceControllerStatus.ContinuePending:
                        label.Text = "Continue Pending";
                        label.ForeColor = Color.Yellow;
                        break;
                    case ServiceControllerStatus.Paused:
                        label.Text = "Paused";
                        label.ForeColor = Color.Yellow;
                        break;
                    case ServiceControllerStatus.PausePending:
                        label.Text = "Pause Pending";
                        label.ForeColor = Color.Yellow;
                        break;
                    case ServiceControllerStatus.Running:
                        label.Text = "Running";
                        label.ForeColor = Color.Green;
                        break;
                    case ServiceControllerStatus.StartPending:
                        label.Text = "Start Pending";
                        label.ForeColor = Color.Yellow;
                        break;
                    case ServiceControllerStatus.Stopped:
                        label.Text = "Stopped";
                        label.ForeColor = Color.Red;
                        break;
                    case ServiceControllerStatus.StopPending:
                        label.Text = "Stop Pending";
                        label.ForeColor = Color.Red;
                        break;
                    default:
                        label.Text = "Unknown";
                        label.ForeColor = Color.Red;
                        break;
                }
            }
            catch
            {
                label.Text = "Not Found";
                label.ForeColor = Color.Gray;
            }
        }

        //update state of StormUpload, StormDownload, AIXUpload, AIXDownload, TempestUpload, TempestDownload

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (var fldDlg = new FolderBrowserDialog())
            {
                if(fldDlg.ShowDialog() == DialogResult.OK)
                {
                    StressTestProperty.Default.ResultsFolder = fldDlg.SelectedPath;
                    this.tbResultsFolder.Text = fldDlg.SelectedPath;
                }
            }
        }
    }
}
