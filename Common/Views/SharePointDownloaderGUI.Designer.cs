﻿
namespace GSD_RnDAutomation
{
    partial class SharePointDownloaderGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SharePointDownloaderGUI));
            this.rtbDownloadStatus = new System.Windows.Forms.RichTextBox();
            this.bgwSharePointDownloader = new System.ComponentModel.BackgroundWorker();
            this.lblDownloadProgress = new System.Windows.Forms.Label();
            this.lblProgressPercent = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // rtbDownloadStatus
            // 
            this.rtbDownloadStatus.Location = new System.Drawing.Point(0, -1);
            this.rtbDownloadStatus.Name = "rtbDownloadStatus";
            this.rtbDownloadStatus.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbDownloadStatus.Size = new System.Drawing.Size(285, 159);
            this.rtbDownloadStatus.TabIndex = 0;
            this.rtbDownloadStatus.Text = "";
            this.rtbDownloadStatus.TextChanged += new System.EventHandler(this.rtbDownloadStatus_TextChanged);
            // 
            // bgwSharePointDownloader
            // 
            this.bgwSharePointDownloader.WorkerSupportsCancellation = true;
            this.bgwSharePointDownloader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwSharePointDownloader_DoWork);
            // 
            // lblDownloadProgress
            // 
            this.lblDownloadProgress.AutoSize = true;
            this.lblDownloadProgress.Location = new System.Drawing.Point(-3, 165);
            this.lblDownloadProgress.Name = "lblDownloadProgress";
            this.lblDownloadProgress.Size = new System.Drawing.Size(105, 13);
            this.lblDownloadProgress.TabIndex = 1;
            this.lblDownloadProgress.Text = "Download Progress: ";
            // 
            // lblProgressPercent
            // 
            this.lblProgressPercent.AutoSize = true;
            this.lblProgressPercent.Location = new System.Drawing.Point(202, 165);
            this.lblProgressPercent.Name = "lblProgressPercent";
            this.lblProgressPercent.Size = new System.Drawing.Size(0, 13);
            this.lblProgressPercent.TabIndex = 2;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(96, 164);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(100, 14);
            this.progressBar1.TabIndex = 3;
            // 
            // SharePointDownloaderGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 216);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblProgressPercent);
            this.Controls.Add(this.lblDownloadProgress);
            this.Controls.Add(this.rtbDownloadStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SharePointDownloaderGUI";
            this.Opacity = 0.5D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SharePoint Downloader";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Status_load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbDownloadStatus;
        private System.ComponentModel.BackgroundWorker bgwSharePointDownloader;
        private System.Windows.Forms.Label lblDownloadProgress;
        private System.Windows.Forms.Label lblProgressPercent;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}