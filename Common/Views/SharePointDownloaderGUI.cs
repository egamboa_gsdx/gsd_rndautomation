﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml.Linq;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;
using NHotkey.WindowsForms;
using GSD_RnDAutomation.SharePoint_Downloader;
using GSD_RnDAutomation.Common.Methods;
using System.Drawing;

namespace GSD_RnDAutomation
{
    public partial class SharePointDownloaderGUI : System.Windows.Forms.Form
    {
        private static bool restart = false;

        internal SharePointDownloaderGUI()
        {


            HotkeyManager.Current.AddOrReplace("Quit", Keys.Control | Keys.Q, Status_HotkeyDown);
            InitializeComponent();
        }

        private void Status_load(object sender, EventArgs e)
        {
            var src = Screen.FromPoint(this.Location);
            this.Location = new Point(0, src.WorkingArea.Bottom - this.Height);

            rtbDownloadStatus.Text += "\n================================";
            rtbDownloadStatus.Text += "\nDownload Script Started!";
            rtbDownloadStatus.Text += "\n================================";

            rtbDownloadStatus.Text += "\n================================";
            rtbDownloadStatus.Text += "\n";
            rtbDownloadStatus.Text += @"Options in C:\Builds\settings.xml";
            rtbDownloadStatus.Text += "\nBoth_Software:      true\\false";
            rtbDownloadStatus.Text += "\nAuto_Install:       true\\false";
            rtbDownloadStatus.Text += "\nSoftware:           AIX\\STORM";
            rtbDownloadStatus.Text += "\nTest_Both_Software: true\\false";
            rtbDownloadStatus.Text += "\n=================================";

            bgwSharePointDownloader.RunWorkerAsync();
        }
        private void rtbDownloadStatus_TextChanged(object sender, EventArgs e)
        {
            rtbDownloadStatus.SelectionStart = rtbDownloadStatus.Text.Length;

            rtbDownloadStatus.ScrollToCaret();
        }

        private void bgwSharePointDownloader_DoWork(object sender, DoWorkEventArgs e)
        {
            Download_Install(new string[] { });            
        }

        private void Download_Install(string[] args)
        {
            //create file structure
            var TAFFolder = @"C:\Builds\Test Automation Framework";
            var StormFolder = @"C:\Builds\Storm";
            var AIXFolder = @"C:\Builds\AIX";
            var TorrentFolder = @"C:\Builds\Torrent";

            if (!Directory.Exists(TAFFolder))
                Directory.CreateDirectory(TAFFolder);
            if (!Directory.Exists(StormFolder))
                Directory.CreateDirectory(StormFolder);
            if (!Directory.Exists(AIXFolder))
                Directory.CreateDirectory(AIXFolder);

            //get password, if the password is missing then ask for a new one from the user
            var SPHelper = new SPContext();
            var securePass = "";
            string DownloadFileString, SoftwareToInstall;

            try
            {
                securePass = SPContext.FetchPasswordString(@"C:\Builds\Test Automation Framework\creds.txt");
            }
            catch
            {
                //securePass = SPHelper.GetPassword();
                PasswordForm pform = new PasswordForm();
                if (pform.ShowDialog() != DialogResult.OK)
                {
                    pform.Close();
                }
                else
                {
                    securePass = pform.Pword();
                }


                //Encrypt password
                SPContext.SavePasswordString(securePass, @"C:\Builds\Test Automation Framework\creds.txt");
                rtbDownloadStatus.Text += "\n";
            }

            //convert passowrd to secure string
            var secure = new SecureString();
            foreach (char c in securePass)
            {
                secure.AppendChar(c);
            }

            //load Options xml (settings.xml), if missing then create one
            //These options affect which software to download and whether to install them
            if (!System.IO.File.Exists(@"C:\Builds\settings.xml"))
            {
                Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan");
                CreateOptions();
            }

            XElement settings = XElement.Load(@"C:\Builds\settings.xml", LoadOptions.SetBaseUri | LoadOptions.SetLineInfo);

            //Check main function entry point arguments for multiple arguments. This allows main method to run a second time
            //to allow both software to be downloaded
            if (args.Count() != 0)
            {
                if ("AIX" == (args[0] ?? "Not AIX"))
                {
                    settings.SetElementValue("Software", "AIX");
                }
                else if ("Storm" == (args[0] ?? "Not Storm"))
                {
                    settings.SetElementValue("Software", "Storm");
                }
                else if ("Torrent" == (args[0] ?? "Not Torrent"))
                {
                    settings.SetElementValue("Software", "Torrent");
                }
            }

            //Initialize settings dependent variables
            if (settings.Element("Software").Value == "Storm")
            {
                SoftwareToInstall = "Storm";
                SPHelper.InitClientContextQuery(SPContext.Software.Storm, secure);
                DownloadFileString = "StormLaboratoryInstaller";
            }
            else if (settings.Element("Software").Value == "AIX")
            {
                SoftwareToInstall = "AIX";
                SPHelper.InitClientContextQuery(SPContext.Software.AIX, secure);
                DownloadFileString = "AIX1000SoftwareSuiteInstaller";
            }
            else
            {
                SoftwareToInstall = "Torrent";
                SPHelper.InitClientContextQuery(SPContext.Software.Torrent, secure);
                DownloadFileString = "TempestMainUIInstaller";
            }

            //create context, create creds and connect
            if (!SPHelper.InitConnectionQuery())
            {
                //Login Failed
                rtbDownloadStatus.Text += "\nLogin Request Failed!";
                rtbDownloadStatus.Text += "\nPlease press CTRL + Q to quit...";

                //Delete password as it is incorrect
                var filePassInfo = new FileInfo(@"C:\Builds\Test Automation Framework\creds.txt");
                filePassInfo.Delete();
                securePass = null;
                return;
            }

            //Grab list of software
            var ListOfDownloads = SPHelper.Context.Web.GetFolderByServerRelativeUrl(SPHelper.DocLibName).Files;
            SPHelper.Context.Load(ListOfDownloads);
            SPHelper.Context.ExecuteQuery();

            if (ListOfDownloads[0].Name == null)
            {
                rtbDownloadStatus.Text += "\nList of files not initialized!";
                rtbDownloadStatus.Text += "\nPress CTRL + Q to quit...\n";
                return;
            }

            //iterate over list of files and find which ones are named the corresponding software string
            var fileCurrent = ListOfDownloads[0];
            foreach (var file in ListOfDownloads)
            {
                if (file.Name.Contains(DownloadFileString))
                {
                    if (file.Name.EndsWith(".zip"))
                    {
                        fileCurrent = file;
                        break;
                    }
                }
            }

            //Find which files are .zip and only grab the most current one using the version number
            foreach(var file in ListOfDownloads)
            {
                if (file.Name.Contains(DownloadFileString))
                {
                    if (file.Name.EndsWith(".zip"))
                    {
                        rtbDownloadStatus.Text += "\n" + file.Name;
                        Regex rgx = new Regex(@"[^\d]+");
                        if (int.Parse(rgx.Replace(file.Name, string.Empty)) > int.Parse(rgx.Replace(fileCurrent.Name, string.Empty)))
                        {
                            fileCurrent = file;
                        }
                    }
                }
            }

            //remove old build files
            rtbDownloadStatus.Text += "\nBuild found: " + fileCurrent.Name;
            rtbDownloadStatus.Text += "\nRemoving old Builds...";

            var di = new DirectoryInfo(SPHelper.DestinationPath);
            //if the most recent version of the software does not exist
            if (di.EnumerateFiles(fileCurrent.Name).Count() == 0)
            {
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete();
                }

                rtbDownloadStatus.Text += "\nDownloading Build...\n";
                var backgroundWorker = new BackgroundWorker();
                //backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
                backgroundWorker.WorkerReportsProgress = true;
                backgroundWorker.WorkerSupportsCancellation = true;

                //setup download build
                Microsoft.SharePoint.Client.FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(SPHelper.Context, fileCurrent.ServerRelativeUrl);
                SPHelper.Context.Load(ListOfDownloads.GetByUrl(fileCurrent.ServerRelativeUrl), file => file.Length);
                SPHelper.Context.ExecuteQuery();
                var fileName = Path.Combine(SPHelper.DestinationPath, fileCurrent.Name);

                //download build with progress update
                using (var fileStream = System.IO.File.Create(fileName))
                {
                    int data, left;
                    int previousPercent = 0;
                    long test = 0;
                    long total = ListOfDownloads.GetByUrl(fileCurrent.ServerRelativeUrl).Length;

                    lblProgressPercent.Text = "0%";
                    progressBar1.Visible = true;
                    while ((data = fileInfo.Stream.ReadByte()) != -1)
                    {
                        fileStream.WriteByte((byte)data);
                        test++;
                        if (test % 10000 == 0)
                        {
                            var percentProgress = (int)(test * 100 / total);
                            if (percentProgress != previousPercent)
                            {
                                lblProgressPercent.Text = String.Format("{0}%", percentProgress);
                            }
                        }
                    }
                    lblProgressPercent.Text = "";
                    progressBar1.Visible = false;

                }

                //unzip build
                rtbDownloadStatus.Text += "\nUnzipping Build";
                ZipFile.ExtractToDirectory(SPHelper.DestinationPath + @"\" + fileCurrent.Name, SPHelper.DestinationPath);

                if (Boolean.Parse(settings.Element("Auto_Install").Value))
                {
                    //Create Reg creation thread (this ensures that the reg key is set since the taf process
                    //was initially programmed to resest the computer which
                    //could result in this process being incomplete
                    Thread t = new Thread(CreateRegKey);
                    t.Start();

                    //Start TAF
                    try
                    {
                        if (Boolean.Parse(settings.Element("Both_Software").Value) && args.Count() == 0)
                        {
                            InstallMethods.Install(SoftwareToInstall, false);
                            restart = true;
                        }
                        else
                        {
                            InstallMethods.Install(SoftwareToInstall, true);
                        }
                    }
                    catch
                    {
                        rtbDownloadStatus.Text += "\nTest Automation Framework failed to run!";
                        rtbDownloadStatus.Text += "\nMake sure .exe is in the current location";

                        Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan");
                        Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationFlag", false);
                        return;
                    }

                    while (t.IsAlive)
                    {
                        //Do Nothing
                    }
                }
            }
            else
            {
                rtbDownloadStatus.Text += "\nNewest " + SoftwareToInstall + " Build already installed!";

                if (restart)
                {
                    Process.Start("shutdown", "/r /t 0");
                }
            }

            //re-enter main with the different arg to download build if both softwares need to be downloaded
            if (Boolean.Parse(settings.Element("Both_Software").Value) && settings.Element("Software").Value == "Storm" && args.Count() == 0)
            {
                rtbDownloadStatus.Text += "\n" + @"Both Software is Enabled! If undesired, please change Both_Software to 'false' in settings.xml";
                Download_Install(new string[] { "AIX" });
            }
            else if (Boolean.Parse(settings.Element("Both_Software").Value) && settings.Element("Software").Value == "AIX" && args.Count() == 0)
            {
                rtbDownloadStatus.Text += "\n" + @"Both Software is Enabled! If undesired, please change Both_Software to 'false' in settings.xml";
                Download_Install(new string[] { "Storm" });
            }

            if (Boolean.Parse(settings.Element("Test_Both_Software").Value))
            {
                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationApp", "Both");
            }
            else
            {
                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationApp", settings.Element("Software").Value);
            }

            //for dev purposes, remove after automation is complete
            /*if (args.Count() > 0)
            {
                rtbDownloadStatus.Text += "\nPress CTRL + Q to quit...";
                return;
            }*/
        }
        private void CreateOptions()
        {
            XElement settings = new XElement("Settings");
            string message = "Download both software?";
            string install_message = "Install Software?";
            MessageBoxButtons btn = MessageBoxButtons.YesNo;
            DialogResult installResult;
            DialogResult result = MessageBox.Show(message, "Download Software", btn);
            if(result == DialogResult.No)
            {
                settings.SetElementValue("Both_Software", false);
                
                installResult = MessageBox.Show(install_message, "Install", btn);
                if(installResult == DialogResult.No)
                {
                    settings.SetElementValue("Auto_Install", false);
                }
                else
                {
                    settings.SetElementValue("Auto_Install", true);
                }

                InstallSoftwareForm installForm = new InstallSoftwareForm();
                installForm.ShowDialog();
                settings.SetElementValue("Software", installForm.Software);
            }
            else
            {
                rtbDownloadStatus.Text += @"Both Software is Enabled! If undesired, please change Both_Software to 'false' in settings.xml";
                settings.SetElementValue("Both_Software", true);
                settings.SetElementValue("Software", "Storm");
                installResult = MessageBox.Show(install_message, "Install", btn);
                if (installResult == DialogResult.No)
                {
                    settings.SetElementValue("Auto_Install", false);
                }
                else
                {
                    settings.SetElementValue("Auto_Install", true);
                }

                DialogResult testBothResult = MessageBox.Show("Test both software?", "Test Software", btn);
                if(testBothResult == DialogResult.Yes)
                {
                    Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationApp", "Both");
                    settings.SetElementValue("Test_Both_Software", true);
                }
                else
                {
                    Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true)
                        .SetValue("AutomationApp", settings.Element("Software").Value);
                    settings.SetElementValue("Test_Both_Software", false);
                }
            }
            settings.Save(@"C:\Builds\settings.xml");
        }

        static void CreateRegKey()
        {
            try
            {
                Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan");
                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationFlag", true);
            }
            catch
            {
                //rtbDownloadStatus.Text += "\nSomething went wrong writing the registry key flag!";
                //rtbDownloadStatus.Text += "\nCheck priveleges and try again";
                //return;
            }
        }
        private void Status_HotkeyDown(object sender, NHotkey.HotkeyEventArgs e)
        {
            Application.Exit();
        }
    }
}
