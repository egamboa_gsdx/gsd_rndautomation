﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

using NHotkey.WindowsForms;

using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Testing;

namespace GSD_RnDAutomation
{
    public partial class StatusGUI : Form, IStatusHandler
    {
        private TestSuite testSuite;

        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        internal StatusGUI(ref TestSuite testSuite)
        {
            StatusManager.RegisterHandler(this);
            this.testSuite = testSuite;

            //Register Key Preview Prop and hotkey to quit the application
            this.KeyPreview = true;
            HotkeyManager.Current.AddOrReplace("Quit", Keys.Control | Keys.Q, HotkeyQuit);
            HotkeyManager.Current.AddOrReplace("Kill", Keys.Control | Keys.K, HotkeyKill);
            HotkeyManager.Current.AddOrReplace("Pause", Keys.Control | Keys.P, HotkeyPause);

            InitializeComponent();
        }

        private void StatusGUI_Load(object sender, EventArgs e)
        {
            //Set default start position to the bottom right corner
            var src = Screen.FromPoint(this.Location);
            this.Location = new Point(src.WorkingArea.Right - this.Width, src.WorkingArea.Bottom - this.Height);

            // Allow the window to be clicked through, allowing the software under the window to be controlled.
            // See link for details: https://stackoverflow.com/questions/39855720/windows-forms-pass-clicks-through-a-partially-transparent-always-on-top-window
            const int GWL_EXSTYLE = -20;
            const int WS_EX_LAYERED = 0x80000;
            const int WS_EX_TRANSPARENT = 0x20;
            var style = GetWindowLong(this.Handle, GWL_EXSTYLE);
            SetWindowLong(this.Handle, GWL_EXSTYLE, style | WS_EX_LAYERED | WS_EX_TRANSPARENT);

            Program.WorkerReportsProgress = true;
            Program.RunWorkerAsync();
        }
        
        public void Exit()
        {
            GUIMethods.Exit();
            if (testSuite.InstrumentManager != InstrumentManager.Torrent)
            {
                try
                {
                    while (true)
                    {
                        ApplicationManager.AttemptSwitchTo(testSuite.InstrumentManager);
                        GUIMethods.Exit();
                    }
                }
                catch
                {

                }
            }
            

            this.Close();
        }

        private void HotkeyQuit(object sender, NHotkey.HotkeyEventArgs e)
        {
            testSuite.Run = false;
        }

        private void HotkeyKill(object sender, NHotkey.HotkeyEventArgs e)
        {
            testSuite.Run = false;
            this.Dispose();
        }

        private void HotkeyPause(object sender, NHotkey.HotkeyEventArgs e)
        {
            testSuite.Pause = !testSuite.Pause;
            if (testSuite.Pause)
            {
                OnStatusAdd("Pausing after current function...");
            }
            else
            {
                OnStatusAdd("Resuming");
            }
        }

        public void OnStatusAdd(string status)
        {
            Program.ReportProgress(0, "\n" + status);
        }

        private void Program_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                testSuite.RunTests();
            }
            catch (Exception exception)
            {
                OnStatusAdd("Exception: " + exception.Message + "\n\n" + exception.StackTrace);
                Thread.Sleep(10000);
                throw exception;
            }
            Program.CancelAsync();
        }

        private void Program_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            statusTextBox.Text += e.UserState as string;
        }

        private void Program_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }

        /*private void statusTextBox_TextChanged(object sender, EventArgs e)
        {
            //Set caret to the end position
            statusTextBox.SelectionStart = statusTextBox.Text.Length;
            //Scroll to the bottom of the status box feed
            statusTextBox.ScrollToCaret();
        }*/

        private void statusTextBox_TextChanged_1(object sender, EventArgs e)
        {
            statusTextBox.SelectionStart = statusTextBox.Text.Length;

            statusTextBox.ScrollToCaret();
        }
    }
}
