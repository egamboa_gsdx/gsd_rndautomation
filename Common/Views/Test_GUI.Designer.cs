﻿
namespace GSD_RnDAutomation
{
    partial class Test_GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Test_GUI));
            this.InstrumentBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TestList = new System.Windows.Forms.CheckedListBox();
            this.SelectAll = new System.Windows.Forms.Button();
            this.RunButton = new System.Windows.Forms.Button();
            this.InstrumentExitButton = new System.Windows.Forms.Button();
            this.toggleAutomaticRunButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // InstrumentBox
            // 
            this.InstrumentBox.FormattingEnabled = true;
            this.InstrumentBox.Items.AddRange(new object[] {
            "- Select Instrument-",
            "AIX - CLASSIC",
            "AIX - SIR",
            "STORM - CLASSIC",
            "STORM - SIR",
            "STORM - BOLT",
            "TORRENT",
            "TEST DESIGNER",
            "SERVER STRESS TEST"});
            this.InstrumentBox.Location = new System.Drawing.Point(39, 92);
            this.InstrumentBox.Name = "InstrumentBox";
            this.InstrumentBox.Size = new System.Drawing.Size(171, 21);
            this.InstrumentBox.TabIndex = 0;
            this.InstrumentBox.Text = "- Select Instrument -";
            this.InstrumentBox.SelectedIndexChanged += new System.EventHandler(this.InstrumentBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(154, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(426, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "R+D System Automation Test";
            // 
            // TestList
            // 
            this.TestList.CheckOnClick = true;
            this.TestList.FormattingEnabled = true;
            this.TestList.Location = new System.Drawing.Point(219, 92);
            this.TestList.Name = "TestList";
            this.TestList.Size = new System.Drawing.Size(503, 199);
            this.TestList.Sorted = true;
            this.TestList.TabIndex = 2;
            this.TestList.SelectedIndexChanged += new System.EventHandler(this.TestList_SelectedIndexChanged);
            // 
            // SelectAll
            // 
            this.SelectAll.BackColor = System.Drawing.SystemColors.Control;
            this.SelectAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SelectAll.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.SelectAll.FlatAppearance.BorderSize = 0;
            this.SelectAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectAll.Location = new System.Drawing.Point(219, 297);
            this.SelectAll.Name = "SelectAll";
            this.SelectAll.Size = new System.Drawing.Size(108, 23);
            this.SelectAll.TabIndex = 3;
            this.SelectAll.UseVisualStyleBackColor = false;
            this.SelectAll.Click += new System.EventHandler(this.SelectAll_Click);
            // 
            // RunButton
            // 
            this.RunButton.BackColor = System.Drawing.SystemColors.Control;
            this.RunButton.Location = new System.Drawing.Point(39, 154);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(171, 108);
            this.RunButton.TabIndex = 4;
            this.RunButton.Text = "Run!";
            this.RunButton.UseVisualStyleBackColor = true;
            this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
            // 
            // InstrumentExitButton
            // 
            this.InstrumentExitButton.Location = new System.Drawing.Point(39, 268);
            this.InstrumentExitButton.Name = "InstrumentExitButton";
            this.InstrumentExitButton.Size = new System.Drawing.Size(171, 23);
            this.InstrumentExitButton.TabIndex = 5;
            this.InstrumentExitButton.Text = "Exit Open Instrument(s)";
            this.InstrumentExitButton.UseVisualStyleBackColor = true;
            this.InstrumentExitButton.Click += new System.EventHandler(this.InstrumentExitButton_Click);
            // 
            // toggleAutomaticRunButton
            // 
            this.toggleAutomaticRunButton.Location = new System.Drawing.Point(39, 126);
            this.toggleAutomaticRunButton.Name = "toggleAutomaticRunButton";
            this.toggleAutomaticRunButton.Size = new System.Drawing.Size(171, 22);
            this.toggleAutomaticRunButton.TabIndex = 6;
            this.toggleAutomaticRunButton.Text = "Automatic Run Currenty Disabled";
            this.toggleAutomaticRunButton.UseVisualStyleBackColor = true;
            this.toggleAutomaticRunButton.Click += new System.EventHandler(this.toggleAutomaticRunButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 7;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // Test_GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(753, 323);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.toggleAutomaticRunButton);
            this.Controls.Add(this.InstrumentExitButton);
            this.Controls.Add(this.RunButton);
            this.Controls.Add(this.SelectAll);
            this.Controls.Add(this.TestList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.InstrumentBox);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Test_GUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GSD R&D Instrumentation and Software";
            this.Load += new System.EventHandler(this.Test_GUI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox InstrumentBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox TestList;
        private System.Windows.Forms.Button SelectAll;
        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.Button InstrumentExitButton;
        private System.Windows.Forms.Button toggleAutomaticRunButton;
        private System.Windows.Forms.Label label2;
    }
}