﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Views;

namespace GSD_RnDAutomation
{
    public partial class Test_GUI : Form
    {
        public List<string> testCases;
        public static StatusGUI statusGUI;
        public static ResultsGUI resultsGUI;
        public static ServerTestGUI serverTestGUI;
        public static ServerStatusGUI serverStatusGUI;
        public static AndroidDeviceSetupGUI androidDeviceSetupGUI;
        private bool softwareSelected;
        private InstrumentManager software;
        private RackManager rackType;
        private string softwareString;
        private bool manualRun;
        private bool bothSoftware;
        private bool allCheckState;
        /// <summary>
        /// Launches the main GUI with an empty list
        /// </summary>
        public Test_GUI()
        {
            testCases = new List<string>();
            softwareSelected = false;
            manualRun = true;
            InitializeComponent();
        }

        public Test_GUI(string automaticSoftwareRun)
        {
            testCases = new List<string>();
            switch (automaticSoftwareRun)
            {
                case "Storm":
                    software = InstrumentManager.Storm;
                    softwareSelected = true;
                    break;
                case "AIX":
                    software = InstrumentManager.AIX;
                    softwareSelected = true;
                    break;
                case "Torrent":
                    software = InstrumentManager.Torrent;
                    softwareSelected = true;
                    break;
                case "Both":
                    bothSoftware = true;
                    softwareSelected = false;
                    break;
                default:
                    throw new ArgumentException("Invalid software version, must be \"Storm\" or \"AIX\"");
            }
            manualRun = false;
            InitializeComponent();
        }

        private void Test_GUI_Load(object sender, EventArgs e)
        {
            UpdateAutomaticRunStatus();

            //If Framework is being run automatically
            if (!manualRun)
            {
                UpdateTestList();
                RunButton_Click(null, null);
                return;
            }

            UpdateTestList();
        }

        private void UpdateAutomaticRunStatus()
        {
            if (TaskScheduler.TaskScheduler.AutomaticRunEnabled)
            {
                //atomaticRunLabel.Text = "Automatic runs are currently enabled";
                toggleAutomaticRunButton.Text = "Automatic Run Enabled";
            }
            else
            {
                //automaticRunLabel.Text = "Automatic Runs are currently disabled";
                toggleAutomaticRunButton.Text = "Automatic Run Disabled";            }
        }

        private void UpdateTestList()
        {
            TestList.Items.Clear();
            if (softwareSelected)
            {
                foreach (string testCase in TestSuite.TestCaseNames(software, rackType))
                {
                    int index = TestList.Items.Add(testCase);
                    TestList.SetItemChecked(index, true);
                    SelectAll.Text = "Select/DeSelect All";
                    allCheckState = true; ;
                }
            }
            
        }
        private void SelectAll_Click(object sender, EventArgs e)
        {
            if (allCheckState == false)
            {
                allCheckState = true;
                SelectDeselectAll(true);
                SelectAll.Text = "Select/DeSelect All";
            } else if(allCheckState == true)
            {
                allCheckState = false;
                SelectDeselectAll(false);
                SelectAll.Text = "Select/DeSelect All";
            }
                
        }

        private void SelectDeselectAll(bool state)
        {
            for(int i = 0; i < TestList.Items.Count; i++)
            {
                TestList.SetItemChecked(i, state);
            }
        }
        private void InstrumentBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (InstrumentBox.SelectedIndex)
            {
                case 1:
                    softwareSelected = true;
                    software = InstrumentManager.AIX;
                    rackType = RackManager.classic;
                    softwareString = "AIX1000 Instrument Manager";
                    UpdateTestList();
                    break;
                case 2:
                    softwareSelected = true;
                    software = InstrumentManager.AIX;
                    rackType = RackManager.SIR;
                    softwareString = "AIX1000 Instrument Manager";
                    UpdateTestList();
                    break;
                case 3:
                    softwareSelected = true;
                    software = InstrumentManager.Storm;
                    rackType = RackManager.classic;
                    softwareString = "Storm Instrument Manager";
                    UpdateTestList();
                    break;
                case 4:
                    softwareSelected = true;
                    software = InstrumentManager.Storm;
                    rackType = RackManager.SIR;
                    softwareString = "Storm Instrument Manager";
                    UpdateTestList();
                    break;
                case 5:
                    softwareSelected = true;
                    software = InstrumentManager.Storm;
                    rackType = RackManager.bolt;
                    softwareString = "Storm Instrument Manager";
                    UpdateTestList();
                    break;
                case 6:
                    softwareSelected = true;
                    software = InstrumentManager.Torrent;
                    softwareString = "Torrent";
                    UpdateTestList();
                    break;
                case 7:
                    softwareSelected = true;
                    software = InstrumentManager.TorrentTestDesigner;
                    softwareString = "Test Designer";
                    UpdateTestList();
                    break;
                case 8:
                    softwareSelected = true;
                    software = InstrumentManager.ServerStressTest;
                    softwareString = "Server Stress Test";
                    break;
                default:
                    softwareSelected = false;
                    SelectAll.Text = "";
                    UpdateTestList();
                    break;

            }
        }

        private void TestList_SelectedIndexChanged(object sender, EventArgs e)
        {
            allCheckState = false;
        }
        private void RunButton_Click(object sender, EventArgs e)
        {
            if (!bothSoftware)
            {
                if (software == InstrumentManager.ServerStressTest)
                {
                    serverTestGUI = new ServerTestGUI();
                    this.Hide();
                    serverTestGUI.ShowDialog();

                    if (serverTestGUI.DialogResult == DialogResult.Retry)
                    {
                        manualRun = true;
                        this.Show();
                        Application.OpenForms[this.Name].Activate();
                    }
                    return;
                }

                if (software == InstrumentManager.Torrent)
                {
                    androidDeviceSetupGUI = new AndroidDeviceSetupGUI();
                    androidDeviceSetupGUI.ShowDialog();
                    if (androidDeviceSetupGUI.DialogResult == DialogResult.Cancel)
                    {
                        return;
                    }
                }

                if (!softwareSelected)
                {
                    MessageBox.Show("Please select an Instrument...", "Warning!");
                    return;
                }

                if (TestList.CheckedItems.Count <= 0)
                {
                    MessageBox.Show("Please select a test for " + softwareString, "Warning!");
                    return;
                }

                testCases = new List<string>(TestList.CheckedItems.OfType<string>().ToList());

                TestSuite testSuite = new TestSuite(testCases, software, rackType, false);

                statusGUI = new StatusGUI(ref testSuite);
                this.Hide();
                statusGUI.ShowDialog();

                resultsGUI = new ResultsGUI(new TestSuite[] { testSuite }, !manualRun);
                resultsGUI.ShowDialog();

                if (resultsGUI.DialogResult == DialogResult.Retry)
                {
                    manualRun = true;
                    this.Show();
                    Application.OpenForms[this.Name].Activate();
                }
                else
                {
                    this.Close();
                }
            }
            else if (bothSoftware && !manualRun) // !manualRun for verification
            {
                List<string> StormTestCaseNames = TestSuite.TestCaseNames(InstrumentManager.Storm);
                StormTestCaseNames.Sort();
                TestSuite StormTestSuite = new TestSuite(StormTestCaseNames, InstrumentManager.Storm, rackType, true);

                // Hide the GUI and start the statusGUI (which will run the tests)
                statusGUI = new StatusGUI(ref StormTestSuite);
                this.Hide();
                statusGUI.ShowDialog(); // Runs the tests and waits for until the window closes

                List<string> AIXTestCaseNames = TestSuite.TestCaseNames(InstrumentManager.AIX);
                AIXTestCaseNames.Sort();
                TestSuite AIXTestSuite = new TestSuite(AIXTestCaseNames, InstrumentManager.AIX, rackType, true);

                // Hide the GUI and start the statusGUI (which will run the tests)
                statusGUI = new StatusGUI(ref AIXTestSuite);
                this.Hide();
                statusGUI.ShowDialog(); // Runs the tests and waits for until the window closes
                
                List<string> TestDesignerTestCaseNames = TestSuite.TestCaseNames(InstrumentManager.TorrentTestDesigner);
                TestDesignerTestCaseNames.Sort();
                TestSuite TestDesingerSuite = new TestSuite(TestDesignerTestCaseNames, InstrumentManager.TorrentTestDesigner, RackManager.undefined, true);

                // Hide the GUI and start the statusGUI (which will run the tests)
                statusGUI = new StatusGUI(ref TestDesingerSuite);
                this.Hide();
                statusGUI.ShowDialog(); // Runs the tests and waits for until the window closes

                // Show the results
                resultsGUI = new ResultsGUI(new TestSuite[] { StormTestSuite, AIXTestSuite, TestDesingerSuite }, !manualRun);
                resultsGUI.ShowDialog();

                // If the user closed the resultsGUI with the "Run Again" button, show the GUI again.
                if (resultsGUI.DialogResult == DialogResult.Retry)
                {
                    // The user is manually running now
                    manualRun = true;
                    bothSoftware = false;
                    this.Show();
                    Application.OpenForms[this.Name].Activate();
                }
                // Otherwise close the application
                else
                {
                    this.Close();
                }
            }
        }

        private void toggleAutomaticRunButton_Click(object sender, EventArgs e)
        {
            toggleAutomaticRunButton.Enabled = false;
            TaskScheduler.TaskScheduler.AutomaticRunEnabled = !TaskScheduler.TaskScheduler.AutomaticRunEnabled;
            UpdateAutomaticRunStatus();
            toggleAutomaticRunButton.Enabled = true;
        }

        private void InstrumentExitButton_Click(object sender, EventArgs e)
        {
            InstrumentExitButton.Text = "Closing any open instrument(s)...";
            InstrumentExitButton.Enabled = false;
            if (statusGUI != null)
                statusGUI.Exit();
            if (resultsGUI != null)
                resultsGUI.Close();
            
            if (Process.GetProcessesByName(DataProperty.Default.StormExecutableName).Length != 0)
            {
                ApplicationManager.AttemptSwitchTo(InstrumentManager.Storm);
                GUIMethods.Exit();
            }
            if (Process.GetProcessesByName(DataProperty.Default.AIXExecutableName).Length != 0)
            {
               // ApplicationManager.AttemptSwitchTo(InstrumentManager.AIX);
                GUIMethods.Exit();
            }
            if (Process.GetProcessesByName(DataProperty.Default.TorrentExecutableName).Length != 0) 
            {
                ApplicationManager.AttemptSwitchTo(InstrumentManager.TorrentTestDesigner);
                GUIMethods.Close();
            }
            DialogResult result = MessageBox.Show("All Open Instruments are closed...");
            if(result == DialogResult.OK)
            {
                InstrumentExitButton.Text = "Exit Open Instrument(s)";
                InstrumentExitButton.Enabled = true;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
