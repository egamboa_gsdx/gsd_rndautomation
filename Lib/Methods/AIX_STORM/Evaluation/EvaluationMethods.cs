﻿using System.Linq;
using System.Threading;

using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WPFUIItems;

using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    public class EvaluationMethods
    {
        /// <summary>
        /// Checks the worklist reviewed checkbox
        /// </summary>
        /// <param name="check"> Indicates whether checkbox should be checked or not </param>
        public static void CheckWorklistReview(bool check)
        {
            StatusManager.AddStatus("Checking Worklist Review Checkbox");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            CheckBox chckbox = ApplicationManager.ApplicationWindow.Get<CheckBox>(EvaluationProperty.Default.WorklistReviewedCheckbox);
            if ((!chckbox.Checked && check) || (chckbox.Checked && !check))
            {
                HelperMethods.Click(chckbox);
            }
        }

        /// <summary>
        /// Opens the worklist dropdown list
        /// </summary>
        public static void DropDownReportList()
        {
            StatusManager.AddStatus("Opening Drop Down List");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            HelperMethods.Click(EvaluationProperty.Default.WorklistPulldown);
        }

       /* public static void PageArrows(string name = null)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);
            Button button;

            switch (name)
            {
                case "First Page":
                    /*StatusManager.AddStatus("Navigating to the First Page " + EvaluationProperty.Default.CurrentPage.TextValue);
                    button = ApplicationManager.ApplicationWindow.Get<Button>(EvaluationProperty.Default.FirstPageButton);
                    string currentPage = ApplicationManager.ApplicationWindow.Get<string>(EvaluationProperty.Default.CurrentPage);

                    if (EvaluationProperty.Default.CurrentPage.TextValue != "1")
                    {
                        HelperMethods.Click(button);
                    }*//*
                    break;
                case "Last Page":
                    break;
                case "Next Page":
                    break;
                case "Previous Page":
                    break;
                default:
                    throw new WorkflowException(name + " button is not available!");
            }
        } */

        /// <summary>
        /// Navigates to the first page via the first page arrow
        /// </summary>
        public static void FirstPageArrow(bool errorOnFail = true)
        {
            StatusManager.AddStatus("Navigating to the First Page");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            Button FirstPageButton = ApplicationManager.ApplicationWindow.Get<Button>(EvaluationProperty.Default.FirstPageButton);
            if (FirstPageButton.Enabled)
            {
                HelperMethods.Click(FirstPageButton);
            }
            else if (errorOnFail)
            {
                throw new WorkflowException("First Page button is not enabled");
            }
        }

        /// <summary>
        /// Navigates to the last page via the last page arrow
        /// </summary>
        public static void LastPageArrow(bool errorOnFail = true)
        {
            StatusManager.AddStatus("Navigating to the Last Page");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);
            Button LastPageButton = ApplicationManager.ApplicationWindow.Get<Button>(EvaluationProperty.Default.LastPageButton);
            if (LastPageButton.Enabled)
            {
                HelperMethods.Click(LastPageButton);
            }
            else if (errorOnFail)
            {
                throw new WorkflowException("First Page button is not enabled");
            }
        }

        /// <summary>
        /// Navigates to the last page via the next page arrow
        /// </summary>
        public static void NextPageArrow()
        {
            StatusManager.AddStatus("Navigating to the last page via Next Page");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            Button NextPageButton = ApplicationManager.ApplicationWindow.Get<Button>(EvaluationProperty.Default.NextPageButton);
            while (NextPageButton.Enabled)
            {
                HelperMethods.Click(NextPageButton);
            }
        }

        /// <summary>
        /// Opens a worklist report
        /// </summary>
        public static void OpenWorklistReport()
        {
            StatusManager.AddStatus("Opening Worklist Report");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            HelperMethods.Click(EvaluationProperty.Default.WorklistPulldown);
            ComboBox pulldown = ApplicationManager.ApplicationWindow.Get<ComboBox>(EvaluationProperty.Default.WorklistPulldown);

            if (pulldown.Items.Count == 0)
            {
                // Consider throwing a workflowException instead of running a worklist.
                // This would cause issues for some test cases, maybe make a second method
                // that runs a worklist when a WorkflowException is found.
                // Using two methods would allow for better control and error handling.
                if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
                    WorklistMethods.AddSamples(1);
                    // Assign all to first test because it is very short
                    WorklistMethods.AssignAllSamplesToTest();
                    HomeMethods.Start();
                    Thread.Sleep(3000);
                    WorklistMethods.New();
                }
                else if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                {
                    MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
                    WorklistMethods.AddSamples(1);
                    WorklistMethods.AddTest();
                    HomeMethods.SpecialStartSpecificPosition("Last");
                    StatusManager.AddStatus("Running test for 10 more seconds");
                    Thread.Sleep(10000);
                    HelperMethods.Click(CommonProperty.Default.OkButton);
                    WorklistMethods.New();
                }

                MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);
                HelperMethods.Click(EvaluationProperty.Default.WorklistPulldown);
                pulldown = ApplicationManager.ApplicationWindow.Get<ComboBox>(EvaluationProperty.Default.WorklistPulldown);
            }

            var worklist = pulldown.Items[0];
            HelperMethods.Click(worklist);
        }

        /// <summary>
        /// Opens the page setup window
        /// </summary>
        public static void PageSetup()
        {
            StatusManager.AddStatus("Opening Page Setup Window");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            HelperMethods.Click(EvaluationProperty.Default.PageSetupButton);
        }

        /// <summary>
        /// Opens the PDF Save window
        /// </summary>
        public static void PDFSave()
        {
            StatusManager.AddStatus("Opening PDF Save Window");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            HelperMethods.Click(EvaluationProperty.Default.SaveToPDFButton);
        }

        /// <summary>
        /// Navigates to the first page via the previous page arrow
        /// </summary>
        public static void PreviousPageArrow()
        {
            StatusManager.AddStatus("Navigating to the first page via Previous Page");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            Button PreviousPageButton = ApplicationManager.ApplicationWindow.Get<Button>(EvaluationProperty.Default.PreviousPageButton);
            while (PreviousPageButton.Enabled)
            {
                HelperMethods.Click(PreviousPageButton);
            }
        }

        /// <summary>
        /// Opens the print window
        /// </summary>
        public static void Print()
        {
            StatusManager.AddStatus("Opening Print Window");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            HelperMethods.Click(EvaluationProperty.Default.PrintButton);
        }

        /// <summary>
        /// Opens the search pane
        /// </summary>
        public static void Search()
        {
            StatusManager.AddStatus("Opening Search Pane");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            HelperMethods.Click(EvaluationProperty.Default.SearchButton);
        }


        /// <summary>
        /// Sets the zoom percentage from the zoom dropdown menu.
        /// Can only zoom to 10, 25, 50 75, 100, 150, 200, and 500.
        /// </summary>
        /// <param name="percentage"> The zoom percentage </param>
        public static void ZoomToPercentage(int percentage)
        {
            StatusManager.AddStatus("Zooming to " + percentage + "%");

            ElementIdentifier checkboxIdentifier;
            switch (percentage)
            {
                case 10:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom10Checkbox;
                    break;
                case 25:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom25Checkbox;
                    break;
                case 50:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom50Checkbox;
                    break;
                case 75:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom75Checkbox;
                    break;
                case 100:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom100Checkbox;
                    break;
                case 150:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom150Checkbox;
                    break;
                case 200:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom200Checkbox;
                    break;
                case 500:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom500Checkbox;
                    break;
                default:
                    throw new WorkflowException("ZoomToPercentage cannot be called with a percentage of " + percentage);
            }

            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            // Open the dropdown
            IUIItem zoomDropdown = ApplicationManager.ApplicationWindow.Get(EvaluationProperty.Default.ZoomDropdown);
            HelperMethods.Click(zoomDropdown);

            // Click the checkbox
            CheckBox zoomCheckbox = zoomDropdown.Get<CheckBox>(checkboxIdentifier);
            HelperMethods.Click(zoomCheckbox);

            // Close the dropdown
            HelperMethods.Click(zoomDropdown);
        }

        /// <summary>
        /// Zooms in using the "Zoom In" button
        /// </summary>
        public static void ZoomIn()
        {
            StatusManager.AddStatus("Zooming in");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            HelperMethods.Click(EvaluationProperty.Default.ZoomInButton);
        }

        /// <summary>
        /// Zooms out using the "Zoom Out" button
        /// </summary>
        public static void ZoomOut()
        {
            StatusManager.AddStatus("Zooming out");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            HelperMethods.Click(EvaluationProperty.Default.ZoomOutButton);
        }
    }
}
