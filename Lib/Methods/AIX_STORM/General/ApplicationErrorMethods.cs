﻿using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Status;

using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    public class ApplicationErrorMethods
    {
        /// <summary>
        /// Restarts the instrument manager if an application error is on screen.
        /// Throws a ApplicationErrorException after restart if an error was found.
        /// If no error was found, no action is taken and no exception is thrown.
        /// </summary>
        public static void RestartOnApplicationError()
        {
            StatusManager.AddStatus("Looking for an Application Error window");

            Window window = null;

            try
            {
                window = ApplicationManager.ApplicationWindow.ModalWindow(WindowName.Default.ApplicationError);
            }
            catch
            {
                // No error window found
                return;
            }

            window.Focus();

            // Record information about the error
            string ErrorMessage = window.Get<Label>(GeneralProperty.Default.ErrorMessage).Text;
            string LogFile = window.Get<Label>(GeneralProperty.Default.LogFile).Text;

            // Close the current instrument managaer
            HelperMethods.Click(GeneralProperty.Default.QuitButton, window);
            HelperMethods.Wait();

            // Start a new instrument manager
            ApplicationManager.LaunchAndLogin(ApplicationManager.InstrumentManager, ApplicationManager.RackManager);

            // Raise an exception with information about the error
            throw new ApplicationErrorException(ErrorMessage + ", details at " + LogFile);
        }
    }
}
