﻿using GSD_RnDAutomation;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;

using System;
using System.Collections.Generic;

using TestStack.White.InputDevices;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.TreeItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.WindowsAPI;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    class GeneralMethods
    {
        /// <summary>
        /// Navigates through the folders to the "C:\Test Automation\Temp" directory
        /// </summary>
        private static void NavigateToTempFolder()
        {
            // Open the "C" Drive
            Tree FolderTree = ApplicationManager.ApplicationWindow.Get<Tree>(GeneralProperty.Default.FolderTree);
            FolderTree.Node("C:\\").Expand();
            HelperMethods.Wait();

            // Open the "Test Automation" folder
            FolderTree.Node("C:\\", "Test Automation").Expand();
            HelperMethods.Wait();

            // Click on the "Temp" folder
            HelperMethods.Click(FolderTree.Node("C:\\", "Test Automation", "Temp"));
        }

        /// <summary>
        /// Navigates through the folders to the "C:\Test Automation\Test Data" directory
        /// </summary>
        private static void NavigateToTestDataFolder()
        {
            // Open the "C" Drive
            Tree FolderTree = ApplicationManager.ApplicationWindow.Get<Tree>(GeneralProperty.Default.FolderTree);
            FolderTree.Node("C:\\").Expand();
            HelperMethods.Wait();

            // Open the "Test Automation" folder
            FolderTree.Node("C:\\", "Test Automation").Expand();
            HelperMethods.Wait();

            // Click on the "Test Data" folder
            HelperMethods.Click(FolderTree.Node("C:\\", "Test Automation", "Test Data"));
        }

        /// <summary>
        /// Navigates a file selection dialog to load a file from the "C:\Test Automation\Temp" directory.
        /// </summary>
        /// <param name="filename">The name of the file in "C:\Test Automation\Temp" with the extension</param>
        internal static void OpenFile(string filename, string folder = "Temp")
        {
            try
            {
                if (folder == "Test Data")
                {
                    // Go to the Test Data folder
                    NavigateToTestDataFolder();
                }
                else if (folder == "Temp")
                {
                    // Go to the Temp folder
                    NavigateToTempFolder();
                }
                else
                {
                    // Report the unknown folder name
                    throw new WorkflowException("Unknown folder \"" + folder + "\"");
                }

                // Search for a file with the proper filename
                ListBox fileList = ApplicationManager.ApplicationWindow.Get<ListBox>(GeneralProperty.Default.FileList);
                ListItems files = fileList.Items;
                foreach (ListItem file in files)
                {
                    // If the name matches
                    if (file.Text.ToLower() == filename.ToLower())
                    {
                        // Select the file
                        HelperMethods.Click(file);

                        // Press Open
                        HelperMethods.Click(GeneralProperty.Default.OpenButton);

                        return;
                    }
                }

                // Report file not found
                throw new WorkflowException("Failed to find file with name \"" + filename + "\" in folder \"" + folder + "\"");
            }
            catch (Exception e)
            {
                // If something goes wrong, press Cancel
                HelperMethods.Click(CommonProperty.Default.CancelButton);
                throw new WorkflowException("Failed to open file", e);
            }
        }

        /// <summary>
        /// Navigates a file selection dialog to save a file to the "C:\Test Automation\Temp" directory. 
        /// </summary>
        /// <param name="filename">The name of the file, or null for default value</param>
        internal static void SaveFile(string filename = null)
        {
            try
            {
                // Go to the Temp folder
                NavigateToTempFolder();

                // If we're using a custom filename instead of the default value
                if (filename != null)
                {
                    // Click into the filename text box
                    HelperMethods.Click(GeneralProperty.Default.FileNameTextBox);

                    // Clear the existing text with CTRL+A and DELETE
                    Keyboard keyboard = new Keyboard();
                    keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                    HelperMethods.Wait();
                    keyboard.Enter("a");
                    HelperMethods.Wait();
                    keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                    HelperMethods.Wait();
                    keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                    HelperMethods.Wait();

                    // Enter the filename
                    keyboard.Enter(filename);
                }

                // Press Save
                HelperMethods.Click(GeneralProperty.Default.SaveButton);

                // Check for the "Are you sure you want to overwrite this file?" window
                List<Window> windows = ApplicationManager.ApplicationWindow.ModalWindows();
                foreach (Window window in windows)
                {
                    // If we find it
                    if (window.Name == WindowName.Default.ConfirmedOverwrite)
                    {
                        // Press Yes
                        HelperMethods.Click(CommonProperty.Default.YesButton_automationID);
                    }
                }

            }
            catch (Exception e)
            {
                // If something goes wrong, press Cancel
                HelperMethods.Click(CommonProperty.Default.CancelButton);
                throw new WorkflowException("Failed to save file", e);
            }
        }
    }
}
