﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;

using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WPFUIItems;
using TestStack.White.WindowsAPI;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    public class SettingMethods
    {
        /// <summary>
        /// Opens the settings to find the machine's serial number.
        /// Throws WorkflowException if the settings fail to open.
        /// </summary>
        /// <returns>The serial number found.</returns>
        internal static string GetSerialNumber()
        {
            OptionMethods.OpenOption(MenuProperty.Default.Settings);

            Window window;
            try
            {
                window = ApplicationManager.ApplicationWindow.ModalWindow(WindowName.Default.SettingsWindow);
            }
            catch (Exception e)
            {
                throw new WorkflowException("Failed to find or open Settings window", e);
            }

            string serialNumber = window.Get<Label>(MenuProperty.Default.SettingsSerialNumberValue).Text;

            HelperMethods.Click(MenuProperty.Default.SettingsCancelButton, window);

            return serialNumber;
        }
    }
}
