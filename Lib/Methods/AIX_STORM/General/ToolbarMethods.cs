﻿using GSD_RnDAutomation.Common.Methods;

using TestStack.White.UIItems;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    class ToolbarMethods
    {
        internal static ToolTip GetWorklistStateTooltip()
        {
            return HelperMethods.GetTooltip(GeneralProperty.Default.WorklistState);
        }
        internal static ToolTip GetWorklistWasteBottle()
        {
            return HelperMethods.GetTooltip(GeneralProperty.Default.WasteBottle);
        }
        internal static ToolTip GetWorklistWashBottle()
        {
            return HelperMethods.GetTooltip(GeneralProperty.Default.WashBottle1);
        }
    }
}