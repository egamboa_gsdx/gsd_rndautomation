﻿using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Status;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WPFUIItems;
using TestStack.White.WindowsAPI;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    public class HomeMethods
    {
        /// <summary>
        /// Resumes paused worklist. Runs test for 3 seconds before continuing
        /// </summary>
        public static void Continue()
        {
            StatusManager.AddStatus("Continuing worklist");

            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                HelperMethods.Click(HomeProperty.Default.AIXPauseContinueButton);
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                HelperMethods.Click(HomeProperty.Default.StormPauseContinueButton);
            }

            // Allow software to run test for 3 seconds
            Thread.Sleep(3000);
        }

        /// <summary>
        /// AIX only.
        /// Modifies the expiration date for a test.
        /// </summary>
        /// <param name="date">The new expiration date. Must be in the format "MM/DD/YYYY".</param>
        public static void ModifyExpirationDate(string date)
        {
            // AIX only
            if (ApplicationManager.InstrumentManager != InstrumentManager.AIX)
            {
                throw new SoftwareException("HomeMethods.ModifyExpirationDate() cannot be run in non-AIX software. Be sure to use WorklistMethods.ModifyExpirationDate() on Storm.");
            }

            StatusManager.AddStatus("Modifying Expiration Date");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                HelperMethods.Click(HomeProperty.Default.AIXExpirationDate);
                HelperMethods.Click(HomeProperty.Default.ExpirationDateTextbox);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                HelperMethods.Click(HomeProperty.Default.AIXTppaExpirationDate);
                HelperMethods.Click(HomeProperty.Default.ExpirationDateTextbox);
            }


            Keyboard keyboard = new Keyboard();
            keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
            HelperMethods.Wait();
            keyboard.Enter("a");
            HelperMethods.Wait();
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
            HelperMethods.Wait();
            keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
            HelperMethods.Wait();
            keyboard.Enter(date);
            HelperMethods.Wait();

            HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
        }

        /// <summary>
        /// AIX only.
        /// Modifies the lot number for the worklist.
        /// </summary>
        /// <param name="lotNumber">The new lot number to enter.</param>
        public static void ModifyLotNumber(int lotNumber)
        {
            // AIX only
            if (ApplicationManager.InstrumentManager != InstrumentManager.AIX)
            {
                throw new SoftwareException("HomeMethods.ModifyLotNumber() cannot be run in non-AIX software. Be sure to use WorklistMethods.ModifyLotNumber() on Storm.");
            }

            StatusManager.AddStatus("Modifying Lot Number");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);


            if (ApplicationManager.RackManager == RackManager.classic)
            {
                HelperMethods.Click(HomeProperty.Default.AIXLotNumber);
                HelperMethods.Click(HomeProperty.Default.LotNumberTextbox);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                HelperMethods.Click(HomeProperty.Default.AIXTppaLotNumber);
                HelperMethods.Click(HomeProperty.Default.LotNumberTextbox);
            }

            Keyboard keyboard = new Keyboard();
            keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
            HelperMethods.Wait();
            keyboard.Enter("a");
            HelperMethods.Wait();
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
            HelperMethods.Wait();
            keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
            HelperMethods.Wait();
            keyboard.Enter(lotNumber.ToString());
            HelperMethods.Wait();

            HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
        }

        /// <summary>
        /// Pauses the worklist. Waits for the pause action to complete.
        /// </summary>
        public static void Pause()
        {
            StatusManager.AddStatus("Pausing worklist");

            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                HelperMethods.Click(HomeProperty.Default.AIXPauseContinueButton);
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                HelperMethods.Click(HomeProperty.Default.StormPauseContinueButton);
            }

            // Wait up to 30 seconds for the pause to complete
            for (int i = 0; i < 30; i++)
            {
                Thread.Sleep(1000);
                // A bug on AIX doesn't update the current action when complete, check the pause/continue button instead
                if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    Button PauseContinueButton = ApplicationManager.ApplicationWindow.Get<Button>(HomeProperty.Default.AIXPauseContinueButton);
                    if (PauseContinueButton.Name != "Pause")
                    {
                        return;
                    }
                }
                else
                {
                    Label CurrentAction = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.CurrentAction);
                    if (CurrentAction.Text != "Current Action: Instrument is pausing...")
                    {
                        return;
                    }
                }
            }
            throw new WorkflowException("Pause failed to complete within 30 seconds");
        }

        /// <summary>
        /// Start the worklist from a specific position. Runs test for 3 seconds before continuing
        /// </summary>
        /// <param name="position"> Default "First". Enter "Last" to start from last position </param>
        public static void SpecialStartSpecificPosition(string position = "First")
        {
            StatusManager.AddStatus("Starting worklist with Special Start at Specific Position");

            List<Window> windows;

            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            HelperMethods.Click(HomeProperty.Default.SpecialStartButton);

            windows = ApplicationManager.TryGetWindows();
            if (windows.Any(x => x.Name == "Expiration Date" || x.Name == "Expired Test"))
            {
                HelperMethods.Click(HomeProperty.Default.NoButton);
                FixExpirationDate();
                SpecialStartSpecificPosition(position);
                return;
            }

            ListBox options = ApplicationManager.ApplicationWindow.Get<ListBox>(HomeProperty.Default.SpecialStartList);
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                var specificPos = options.Get(HomeProperty.Default.StormStartAtSpecificPosition);
                HelperMethods.Click(specificPos);
                HelperMethods.Wait();
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                var specificPos = options.Get(HomeProperty.Default.AIXStartAtSpecificPosition);
                HelperMethods.Click(specificPos);
                HelperMethods.Wait();
            }

            HelperMethods.Click(CommonProperty.Default.StartButton);

            try
            {
                if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                {
                    ListBox actions = ApplicationManager.ApplicationWindow.Get<ListBox>(HomeProperty.Default.StormSpecificPositionList);
                    if (position == "First")
                    {
                        if (actions.ScrollBars.CanScroll)
                            actions.ScrollBars.Vertical.SetToMinimum();
                        HelperMethods.Click(actions.Items.First());
                    }
                    else if (position == "Last")
                    {
                        if (actions.ScrollBars.CanScroll)
                            actions.ScrollBars.Vertical.SetToMaximum();
                        HelperMethods.Click(actions.Items.Last());
                    }
                }
                else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    ListBox actions = ApplicationManager.ApplicationWindow.Get<ListBox>(HomeProperty.Default.AIXSpecificPositionList);
                    if (position == "First")
                    {
                        if (actions.ScrollBars.CanScroll)
                            actions.ScrollBars.Vertical.SetToMinimum();
                        HelperMethods.Click(actions.Items.First());
                    }
                    else if (position == "Last")
                    {
                        if (actions.ScrollBars.CanScroll)
                            actions.ScrollBars.Vertical.SetToMaximum();
                        HelperMethods.Click(actions.Items.Last());
                    }
                }
                HelperMethods.Wait();

                HelperMethods.Click(CommonProperty.Default.StartButton);
            }
            catch (Exception e)
            {
                HelperMethods.Click(CommonProperty.Default.CancelButton_textValue);
                HelperMethods.Click(CommonProperty.Default.CancelButton_textValue);
                throw new WorkflowException("No specific positions available", e);
            }

            if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                windows = ApplicationManager.TryGetWindows();
                if (windows.Any(x => x.Name == "Expiration date"))
                {
                    HelperMethods.Click(HomeProperty.Default.NoButton);
                    FixExpirationDate();
                    HelperMethods.Click(HomeProperty.Default.SpecialStartButton);
                    HelperMethods.Click(HomeProperty.Default.AIXStartAtSpecificPosition);
                    HelperMethods.Click(CommonProperty.Default.StartButton);
                }

                if (windows.Any(x => x.Name == "Lot Number"))
                {
                    HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
                    FixLotNumber();
                    SpecialStartSpecificPosition(position);
                    return;
                }

                // Control Scheme - Do you wish to continue?
                HelperMethods.Click(CommonProperty.Default.YesButton_textValue);
                // Check MTP - Have all wells been properly excluded?
                HelperMethods.Click(CommonProperty.Default.YesButton_textValue);
                // Load MTPs
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
                // Load MTPs - Make sure covers have been removed
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
                // Reagent Wizard - Please connect Wash Bottle 1
                HelperMethods.Click(HomeProperty.Default.NextButton);
                // Reagent Wizard - Place antigen suspension
                HelperMethods.Click(HomeProperty.Default.NextButton);
                // Reagent Wizard - Place decon
                HelperMethods.Click(HomeProperty.Default.NextButton);
            }
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
            }

            if (ApplicationManager.RackManager == RackManager.bolt) //bolt
            {
                // Load MTPs
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
                
            }

                // Check to see if there is a worklist name conflict warning
                windows = ApplicationManager.TryGetWindows();
            if (windows.Any(x => x.Name == "Run Worklist"))
            {
                HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
            }

            //Allow software to run test for 3 seconds
            StatusManager.AddStatus("Running test for 3 seconds");
            Thread.Sleep(3000);
        }

        /// <summary>
        /// Performs a reread of the current worklist. (Called "Recapture well images" on AIX).  
        /// This function takes many extra precautions due to its use in the server stress test where more issues occur.
        /// Throws WorkflowException if the worklist is currently running or the reread option is not available (i.e. the worklist has not been run).
        /// </summary>
        public static void Reread()
        {
            StatusManager.AddStatus("Re-reading the wells on the worklist");

            List<Window> windows;

            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            if (!ApplicationManager.ApplicationWindow.Get(HomeProperty.Default.SpecialStartButton).Enabled)
            {
                throw new WorkflowException("Cannot start worklist while worklist is currently running.");
            }

            HelperMethods.Click(HomeProperty.Default.SpecialStartButton);

            windows = ApplicationManager.TryGetWindows();
            if (windows.Any(x => x.Name == "Expiration Date"))
            {
                HelperMethods.Click(HomeProperty.Default.NoButton);
                FixExpirationDate();
                SpecialStartSpecificPosition();
                return;
            }

            ListBox options;
            try
            {
                options = ApplicationManager.ApplicationWindow.Get<ListBox>(HomeProperty.Default.SpecialStartList);
            }
            catch (Exception e)
            {
                throw new WorkflowException("Failed to open Special Start dialog", e);
            }

            try
            {
                if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                {
                    var specificPos = options.Get(HomeProperty.Default.StormReread);
                    HelperMethods.Click(specificPos);
                    HelperMethods.Wait();
                }
                else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    var specificPos = options.Get(HomeProperty.Default.AIXReread);
                    HelperMethods.Click(specificPos);
                    HelperMethods.Wait();
                }
            }
            catch (Exception e)
            {
                HelperMethods.Click(CommonProperty.Default.CancelButton_textValue);
                throw new WorkflowException("Reread option not found. The worklist must be run for option to appear.", e);
            }

            HelperMethods.Click(CommonProperty.Default.StartButton);

            // AIX asks to rename the worklist
            windows = ApplicationManager.TryGetWindows();
            if (windows.Any(x => x.Name == "Run Worklist"))
            {
                HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
            }
        }

        /// <summary>
        /// Waits until the worklist is finished, specifically when the "Special Start" button is enabled (allowing a new run to start).
        /// Throws WorkflowException if the worklist takes too long to finish.
        /// </summary>
        /// <param name="maximumWaitTime">The maximum amount of time (in minutes) to wait before throwing a WorkflowException (default 10 minutes).</param>
        /// <param name="sleepTime">The amount of time to wait between completion checks (in milliseconds).</param>
        public static void WaitForWorklistToFinish(int maximumWaitTime = 10, int sleepTime = 200)
        {
            StatusManager.AddStatus("Waiting up to " + maximumWaitTime + " minutes for worklist to finish");
            int checks = maximumWaitTime * 60 * 1000 / sleepTime; // Number of iterations = (total time) / (wait time) 

            while (checks > 0)
            {

                try
                {
                    CloseWorklistFinishDialog();
                    break;
                }
                catch (WorkflowException)
                {
                    Thread.Sleep(sleepTime);
                }
                checks--;



            }

            // Wait three seconds to let things finish
            Thread.Sleep(3000);
        }
        public static void WaitForWorklistToFinishView(int maximumWaitTime = 10, int sleepTime = 200)
        {
            StatusManager.AddStatus("Waiting up to " + maximumWaitTime + " minutes for worklist to finish");
            int checks = maximumWaitTime * 60 * 1000 / sleepTime; // Number of iterations = (total time) / (wait time) 

            while (checks > 0)
            {

                try
                {
                    CloseWorklistFinishDialogViewResults();
                    break;
                }
                catch (WorkflowException)
                {
                    Thread.Sleep(sleepTime);
                }
                checks--;




            }

            // Wait three seconds to let things finish
            Thread.Sleep(3000);
        }
    
         

        /// <summary>
        /// Closes the dialog that appears when the worklist finishes.
        /// </summary>
        public static void CloseWorklistFinishDialog()
        {
            StatusManager.AddStatus("Closing worklist finished dialog");
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                try
                {
                    List<Window> windows = ApplicationManager.TryGetWindows(2);
                    foreach (Window window in windows)
                    {
                        if (window.Name == WindowName.Default.StormWorklistRunFinished)
                        {
                            HelperMethods.Click(CommonProperty.Default.OkButton_textValue, window);

                            // Wait a bit to let things finish
                            Thread.Sleep(1000);
                            return;
                        }
                    }
                }
                catch
                {

                }
                throw new WorkflowException("Worklist finish dialog not found.");
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                try
                {
                    List<Window> windows = ApplicationManager.TryGetWindows(2);
                    foreach (Window window in windows)
                    {
                        if (window.Name == WindowName.Default.AIXWorklistRunFinished)
                        {
                            HelperMethods.Click(CommonProperty.Default.OkButton_textValue, window);

                            // Wait a bit to let things finish
                            Thread.Sleep(1000);
                            return;
                        }
                    }
                }
                catch
                {

                }
                throw new WorkflowException("Worklist finish dialog not found.");
            }
        }


        public static void CloseWorklistFinishDialogViewResults()
        {
            StatusManager.AddStatus("Closing worklist finished dialog");
           if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                try
                {
                    List<Window> windows = ApplicationManager.TryGetWindows(2);
                    foreach (Window window in windows)
                    {
                        if (window.Name == WindowName.Default.AIXWorklistRunFinished)
                        {
                            HelperMethods.Click(HomeProperty.Default.ViewResults, window);

                            // Wait a bit to let things finish
                            Thread.Sleep(1000);
                            return;
                        }
                    }
                }
                catch
                {

                }
                throw new WorkflowException("Worklist finish dialog not found.");
            }
        }
        /// <summary>
        /// Start the worklist from a stopped position. Runs test for 3 seconds before continuing
        /// </summary>
        public static void SpecialStartStoppedPosition()
        {
            StatusManager.AddStatus("Pausing worklist");

            List<Window> windows;

            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            HelperMethods.Click(HomeProperty.Default.SpecialStartButton);

            try
            {
                HelperMethods.Click(HomeProperty.Default.StoppedPositionButton);
                HelperMethods.Click(CommonProperty.Default.StartButton);
            }
            catch (Exception e)
            {
                HelperMethods.Click(CommonProperty.Default.CancelButton_textValue);
                throw new WorkflowException("No previously stopped worklist", e);
            }

            // Storm expiration date can not be changed after worklist is stopped
            if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                windows = ApplicationManager.TryGetWindows();
                if (windows.Any(x => x.Name == "Expiration date"))
                {
                    HelperMethods.Click(HomeProperty.Default.NoButton);
                    FixExpirationDate();
                    HelperMethods.Click(HomeProperty.Default.SpecialStartButton);
                    HelperMethods.Click(HomeProperty.Default.StoppedPositionButton);
                    HelperMethods.Click(CommonProperty.Default.StartButton);
                }

                if (windows.Any(x => x.Name == "Lot Number"))
                {
                    HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
                    FixLotNumber();
                    SpecialStartStoppedPosition();
                    return;
                }

                // Load MTPs
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
                // Load MTPs - Make sure covers have been removed
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
                // Reagent Wizard - Please connect Wash Bottle 1
                HelperMethods.Click(HomeProperty.Default.NextButton);
                // Reagent Wizard - Place antigen suspension
                HelperMethods.Click(HomeProperty.Default.NextButton);
                // Reagent Wizard - Place decon
                HelperMethods.Click(HomeProperty.Default.NextButton);
            }

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
            }

            if (ApplicationManager.RackManager == RackManager.bolt) //bolt
            {
                // Load MTPs
                HelperMethods.Click(HomeProperty.Default.ContinueButton);

            }
            //Allow software to run test for 3 seconds
            Thread.Sleep(3000);

            //Check if worklist is already done
            windows = ApplicationManager.TryGetWindows();
            if (windows.Any(x => x.Name == "Worklist Finished"))
            {
                HelperMethods.Click(CommonProperty.Default.OkButton);
            }
        }

        /// <summary>
        /// Start the worklist. Runs test for 3 seconds before continuing
        /// </summary>
        public static void Start()
        {
            StatusManager.AddStatus("Starting worklist");

            List<Window> windows;

            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            HelperMethods.Click(CommonProperty.Default.StartButton);
            //GUIvm.Click(CommonProperty.Default.YesButton_textValue);

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                windows = ApplicationManager.TryGetWindows();
                // If an expiration date window appears, change the expiration date of the test
                if (windows.Any(x => x.Name == "Expiration Date" || x.Name == "Lot Number"))
                {
                    HelperMethods.Click(HomeProperty.Default.NoButton);
                    FixExpirationDate();
                    MenuMethods.NavigateTo(MenuProperty.Default.Home);

                    HelperMethods.Click(CommonProperty.Default.StartButton);
                }

                HelperMethods.Click(HomeProperty.Default.ContinueButton);
                if (ApplicationManager.TryGetWindows().Any(x => x.Name == "Warning"))
                {
                    HelperMethods.Click(CommonProperty.Default.Ok);

                }
            }
            if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                windows = ApplicationManager.TryGetWindows();
                if (windows.Any(x => x.Name == "Expiration Date" || x.Name == "Lot Number"))
                {
                    HelperMethods.Click(HomeProperty.Default.NoButton);
                    FixExpirationDate();
                    HelperMethods.Click(CommonProperty.Default.StartButton);
                }

                // Control Scheme - Do you wish to continue?
                HelperMethods.Click(CommonProperty.Default.YesButton_textValue);
                HelperMethods.Wait();
                // Check MTP - Have all wells been properly excluded
                HelperMethods.Click(CommonProperty.Default.YesButton_textValue);
                HelperMethods.Wait();
                // Load MTPs
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
                // Load MTPs - Make sure covers have been removed
                HelperMethods.Click(HomeProperty.Default.ContinueButton);
                // Reagent Wizard - Please connect Wash Bottle 1
                HelperMethods.Click(HomeProperty.Default.NextButton);
                // Reagent Wizard - Place antigen suspension
                HelperMethods.Click(HomeProperty.Default.NextButton);
                // Reagent Wizard - Place decon
                HelperMethods.Click(HomeProperty.Default.NextButton);
                
                if (ApplicationManager.RackManager == RackManager.classic) 
                {
                    // Reagent Wizard - Finished
                    HelperMethods.Click(HomeProperty.Default.ContinueButton);
                }
                else if (ApplicationManager.RackManager == RackManager.SIR)
                {
                    HelperMethods.Click(HomeProperty.Default.NextButton);
                    HelperMethods.Click(HomeProperty.Default.ContinueButton);
                }
            }

            //Allow software to run test for 3 seconds
            Thread.Sleep(3000);

            //Check if worklist is already done
            windows = ApplicationManager.TryGetWindows();
            if (windows.Count > 1)
            {
                HelperMethods.Click(CommonProperty.Default.OkButton);
            }
        }

        /// <summary>
        /// Stop the worklist
        /// </summary>
        public static void Stop()
        {
            StatusManager.AddStatus("Stopping  worklist");

            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            try
            {
                HelperMethods.Click(HomeProperty.Default.StopButton);
                HelperMethods.Click(CommonProperty.Default.YesButton_textValue);
            }
            catch (Exception e)
            {
                throw new WorkflowException("Worklist already stopped", e);
            }
        }

        /// <summary>
        /// Changes the expiration date of all tests to "12/31/2019".
        /// </summary>
        private static void FixExpirationDate()
        {
            string newExpirationDate = "12/31/2050";
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                WorklistMethods.ModifyAllExpirationDates(newExpirationDate);
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                HomeMethods.ModifyExpirationDate(newExpirationDate);
            }
        }

        /// <summary>
        /// Changes the lot number of all tests to "123456".
        /// </summary>
        private static void FixLotNumber()
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                WorklistMethods.ModifyAllLotNumbers(123456);
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                HomeMethods.ModifyLotNumber(123456);
            }
        }
    }
}
