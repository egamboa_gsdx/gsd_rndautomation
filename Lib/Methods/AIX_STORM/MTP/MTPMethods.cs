﻿using System;
using System.Threading;

using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WPFUIItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.WindowsAPI;

using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Status;



namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    public class MTPMethods
    {
        internal static IUIItem GetMTP(int mtpIndex)
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                switch (mtpIndex)
                {
                    case 1:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.Plate1);
                    case 2:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.Plate2);
                    case 3:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.PredilutionPlate);
                    default:
                        throw new ArgumentException("GetMTP() can only find MTP 1, 2, and 3 on Storm");
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                switch (mtpIndex)
                {
                    case 1:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.Plate1);
                    case 2:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.Plate2);
                    case 3:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.Plate3);
                    case 4:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.Plate4);
                    default:
                        throw new ArgumentException("GetMTP() can only find MTP 1, 2, 3, and 4 on AIX");
                }
            }
            throw new SoftwareException("Unexpected software version");
        }

        internal static Label GetHole(int mtpIndex, int holeIndex)
        {

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                if (holeIndex < 1 || holeIndex > 96)
                {
                    throw new ArgumentException("holeIndex must be between 1 and 96 on Storm");
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                if (holeIndex < 1 || holeIndex > 48)
                {
                    throw new ArgumentException("holeIndex must be between 1 and 48 on AIX");
                }
            }
            return GetMTP(mtpIndex).GetMultiple<Label>(MTPProperty.Default.Hole)[holeIndex - 1];
        }


        internal static IUIItem GetExcludeButton(int mtpIndex)
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                if (ApplicationManager.RackManager == RackManager.bolt)
                {
                    return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.ExcludeStripsButtonBolt);
                }
                    switch (mtpIndex)
                {
                    case 1:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.ExcludeStripsPlate1Button);
                    case 2:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.ExcludeStripsPlate2Button);
                    case 3:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.ExcludeStripsPredilutionPlateButton);
                    default:
                        throw new ArgumentException("GetExcludeButton() can only find exclude strips buttons 1, 2, and 3");
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                switch (mtpIndex)
                {
                    case 1:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.ExcludeWellsPlate1Button);
                    case 2:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.ExcludeWellsPlate2Button);
                    case 3:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.ExcludeWellsPlate3Button);
                    case 4:
                        return ApplicationManager.ApplicationWindow.Get(MTPProperty.Default.ExcludeWellsPlate4Button);
                    default:
                        throw new ArgumentException("GetExcludeButton() can only find exclude wells buttons 1, 2, 3, and 4");
                }
            }
            throw new SoftwareException("Unexpected software version");
        }

        /// <summary>
        /// Storm only.
        /// Excludes a specific number of strips for a MTP.
        /// </summary>
        /// <param name="mtpIndex">The MTP index of the plate to exclude wells from.</param>
        /// <param name="excludeStrips">The number of strips to exlude.</param>
        public static void ExcludeStrips(int mtpIndex, int excludeStrips)
        {
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("ExcludeStrips() cannot be called on non-Storm software. Use ExcludeWells() for AIX.");
            }

            if (excludeStrips < 0 || excludeStrips > 12)
            {
                throw new ArgumentException("excludeStrips must be between 0 and 12");
            }

            StatusManager.AddStatus("Excluding strips");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            HelperMethods.Click(GetExcludeButton(mtpIndex));

            var windows = ApplicationManager.TryGetWindows();
            foreach (var window in windows)
            {
                if (window.Name == WindowName.Default.ExcludeStrips)
                {
                    Label numberOfExcludedStripsLabel = window.Get<Label>(ExcludeProperty.Default.NumberOfStripsExcluded);
                    int excludedStrips = int.Parse(numberOfExcludedStripsLabel.Text);
                    int difference = excludedStrips - excludeStrips;
                    while (difference != 0)
                    {
                        if (difference > 0)
                        {
                            HelperMethods.Click(ExcludeProperty.Default.ExcludeLessStripsButton, window);
                            difference--;
                        }
                        else if (difference < 0)
                        {
                            HelperMethods.Click(ExcludeProperty.Default.ExcludeMoreStripsButton, window);
                            difference++;
                        }
                    }
                    HelperMethods.Click(CommonProperty.Default.OkButton_automationID, window);
                    return;
                }
            }
            throw new WorkflowException("Failed to find exclude strips dialog window");
        }

        /// <summary>
        ///  AIX only 
        ///  Grabs the Control Scheme bombocox and returns the selcted text
        /// </summary>
        public static string GetControlSchemeDropDownBox()
        {
            StatusManager.AddStatus("Getting ControlSchemeDropDownList");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);
            ComboBox ControlSchemeDropDownList = null;
            if (ApplicationManager.InstrumentManager != InstrumentManager.AIX)
            {
                throw new SoftwareException("This control scheme cannot be called on non-AIX software.");
            }
            else if (ApplicationManager.RackManager == RackManager.classic)
            {
                ControlSchemeDropDownList = ApplicationManager.ApplicationWindow.Get<ComboBox>(MTPProperty.Default.ControlSchemeDropDownList.ToSearchCriteria());

            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                ControlSchemeDropDownList = ApplicationManager.ApplicationWindow.Get<ComboBox>(MTPProperty.Default.ControlSchemeDropDownListTppa.ToSearchCriteria());

            }
            return ControlSchemeDropDownList.SelectedItemText;

        }

        /// <summary>
        /// BY: ERNEST GAMBOA
        ///  AIX only 
        ///  Sets the Control Scheme combobox
        ///  <param name="controlScheme"/>Name of control scheme to select</param>
        /// </summary>
        public static void SetControlSchemeDropDownBox(int index)
        {
            StatusManager.AddStatus("Setting Control Scheme in MTP Tab");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            if (ApplicationManager.InstrumentManager != InstrumentManager.AIX)
            {
                throw new SoftwareException("This control scheme cannot be called on non-AIX software.");
            }
            ComboBox ControlSchemeDropDownList = ApplicationManager.ApplicationWindow.Get<ComboBox>(MTPProperty.Default.ControlSchemeDropDownList.ToSearchCriteria());

            ControlSchemeDropDownList.Select(index);
        }

        /// <summary>
        ///  AIX only 
        ///  Checks the text on the Control Scheme bombocox on the MTP Tab
        /// </summary>
        /// 
        public static void MTPSelectControlScheme(int controlScheme)
        {
            ComboBox ControlSchemeDropDownList = ApplicationManager.ApplicationWindow.Get<ComboBox>(MTPProperty.Default.ControlSchemeDropDownList.ToSearchCriteria());
            HelperMethods.Click(ControlSchemeDropDownList);
            HelperMethods.Wait();

            HelperMethods.Click(ControlSchemeDropDownList.Items[(int)controlScheme]);
            HelperMethods.Wait();
        }

        /// <summary>
        /// AIX only.
        /// Excludes a specific number of wells for a MTP.
        /// </summary>
        /// <param name="mtpIndex">The MTP index of the plate to exclude wells from.</param>
        /// <param name="excludeWells">The number of wells to exlude.</param>
        public static void ExcludeWells(int mtpIndex, int excludeWells)
        {
            if (ApplicationManager.InstrumentManager != InstrumentManager.AIX)
            {
                throw new SoftwareException("ExcludeWells() cannot be called on non-AIX software. Use ExcludeStrips() for Storm.");
            }

            if (excludeWells < 0 || excludeWells > 47)
            {
                throw new ArgumentException("excludeWells must be between 0 and 47");
            }

            StatusManager.AddStatus("Excluding wells");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            HelperMethods.Click(GetExcludeButton(mtpIndex));

            var windows = ApplicationManager.TryGetWindows();
            foreach (var window in windows)
            {
                if (window.Name == WindowName.Default.ExcludeWells)
                {
                    IUIItem plate = window.Get(ExcludeProperty.Default.Plate);
                    IUIItem hole = plate.GetMultiple(ExcludeProperty.Default.Hole)[excludeWells];
                    HelperMethods.Click(hole);
                    HelperMethods.Click(CommonProperty.Default.OkButton_textValue, window);
                    return;
                }
            }
            throw new WorkflowException("Failed to find exclude wells dialog window");
        }

        internal static MTPWellTooltip GetWellTooltip(int mtpIndex, int holeIndex)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            HelperMethods.Hover(GetHole(mtpIndex, holeIndex));
            Thread.Sleep(1500);

            var windows = ApplicationManager.Application.GetWindows();
            foreach (Window window in windows)
            {
                if (window.Name == WindowName.Default.Tooltip)
                {
                    return new MTPWellTooltip(window);
                }
            }
            throw new WorkflowException("The tooltip for the well could not be found.");
        }
    }
}
