﻿using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    internal class MTPWellTooltip
    {
        private Window tooltip;

        public MTPWellTooltip(Window tooltipWindow)
        {
            tooltip = tooltipWindow;
        }

        public string MTPIndex
        {
            get { return tooltip.GetMultiple<Label>(MTPProperty.Default.MTPIndexValue)[MTPProperty.Default.MTPIndexValue.Index].Text; }
        }

        public string HoleIndex
        {
            get { return tooltip.GetMultiple<Label>(MTPProperty.Default.HoleIndexValue)[MTPProperty.Default.HoleIndexValue.Index].Text; }
        }

        public string Type
        {
            get
            {
                if (HasType)
                {
                    return tooltip.GetMultiple<Label>(MTPProperty.Default.TypeValue)[MTPProperty.Default.TypeValue.Index].Text;
                }
                else
                {
                    return "";
                }
            }
        }

        public bool HasType
        {
            get { return tooltip.GetMultiple<Label>(MTPProperty.Default.TypeValue).Length > MTPProperty.Default.TypeValue.Index; }
        }

        public string Name
        {
            get
            {
                if (HasType)
                {
                    return tooltip.GetMultiple<Label>(MTPProperty.Default.NameValue)[MTPProperty.Default.NameValue.Index].Text;
                }
                else
                {
                    return "";
                }
            }
        }

        public bool HasName
        {
            get { return tooltip.GetMultiple<Label>(MTPProperty.Default.NameValue).Length > MTPProperty.Default.NameValue.Index; }
        }

        public string Test
        {
            get { return tooltip.GetMultiple<Label>(MTPProperty.Default.TestValue)[MTPProperty.Default.TestValue.Index].Text; }
        }
    }
}
