﻿using GSD_RnDAutomation.Common.Methods;
using System.Linq;
using TestStack.White.UIItems.ListBoxItems;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    public class MenuMethods
    {
        /// <summary>
        /// Navigates to a menu tab. Does nothing if called for the current tab.
        /// </summary>
        /// <param name="menuItem"></param>
        public static void NavigateTo(int menuItem)
        {
            ListBox menu = ApplicationManager.ApplicationWindow.Get<ListBox>(MenuProperty.Default.MenuButtonList);
            ListItem button;

            // An update to AIX removed the blank button between Tools and Evaluation.
            // to account for that, MenuItem.Evaluation and greater now index from the back
            if (menuItem > 5)
            {
                int negativeIndex = menuItem - 9; // e.g. -1 means last item
                button = menu.Item(menu.Items.Count + negativeIndex);
            }
            else
            {
                button = menu.Item(menuItem);
            }

            if (button.IsSelected)
            {
                return; // The correct menu is already selected
            }

            // Find the clickable point manually
            double height = button.Bounds.Bottom - button.Bounds.Top;
            System.Windows.Point clickablePoint = button.Bounds.TopLeft;
            clickablePoint.X += height / 2;
            clickablePoint.Y += height / 2;

            ApplicationManager.ApplicationWindow.Mouse.LeftClick(clickablePoint);
            ApplicationManager.Application.WaitWhileBusy();
            HelperMethods.Wait();

            if (menuItem == MenuProperty.Default.Evaluation)
            {
                // If report is being generated in evaluation wait
                while (ApplicationManager.ApplicationWindow.Items.Any(x => x.Name == "Generating Report..." && x.Visible)) { }
            }
        }
    }
}
