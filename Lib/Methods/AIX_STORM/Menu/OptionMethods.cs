﻿using TestStack.White.UIItems.ListBoxItems;
using GSD_RnDAutomation.Common.Methods;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    public class OptionMethods
    {
        /// <summary>
        /// Clicks on an option to open its window.
        /// </summary>
        /// <param name="optionItem"></param>
        public static void OpenOption(int optionItem)
        {
            ListBox options = ApplicationManager.ApplicationWindow.Get<ListBox>(MenuProperty.Default.OptionsButtonList);
            ListItem button = options.Item(optionItem);

            // Find the clickable point manually
            double height = button.Bounds.Bottom - button.Bounds.Top;
            System.Windows.Point clickablePoint = button.Bounds.TopLeft;
            clickablePoint.X += height / 2;
            clickablePoint.Y += height / 2;

            ApplicationManager.ApplicationWindow.Mouse.LeftClick(clickablePoint);
            ApplicationManager.Application.WaitWhileBusy();
            HelperMethods.Wait();
        }

        /// <summary>
        /// Returns True when the option is enabled.
        /// </summary>
        /// <param name="optionItem"></param>
        /// <returns></returns>
        public static bool OptionEnabled(int optionItem)
        {
            ListBox options = ApplicationManager.ApplicationWindow.Get<ListBox>(MenuProperty.Default.OptionsButtonList);
            return options.Item(optionItem).Enabled;
        }
    }
}
