﻿using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    internal class RackHoleTooltip
    {
        private Window tooltip;

        public RackHoleTooltip(Window tooltipWindow)
        {
            tooltip = tooltipWindow;
        }

        public string Rack
        {
            get { return tooltip.GetMultiple<Label>(RacksProperty.Default.RackValue)[RacksProperty.Default.RackValue.Index].Text; }
        }

        public string RackIndex
        {
            get { return tooltip.GetMultiple<Label>(RacksProperty.Default.RackIndexValue)[RacksProperty.Default.RackIndexValue.Index].Text; }
        }

        public string HoleIndex
        {
            get { return tooltip.GetMultiple<Label>(RacksProperty.Default.HoleIndexValue)[RacksProperty.Default.HoleIndexValue.Index].Text; }
        }

        public string Type
        {
            get { return tooltip.GetMultiple<Label>(RacksProperty.Default.TypeValue)[RacksProperty.Default.TypeValue.Index].Text; }
        }

        public bool HasType
        {
            get
            {
                if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    return tooltip.GetMultiple<Label>(RacksProperty.Default.TypeValue).Length - 3 > RacksProperty.Default.TypeValue.Index;
                }
                else // Storm
                {
                    return tooltip.GetMultiple<Label>(RacksProperty.Default.TypeValue).Length > RacksProperty.Default.TypeValue.Index;
                }
            }
        }

        public string Name
        {
            get { return tooltip.GetMultiple<Label>(RacksProperty.Default.NameValue)[RacksProperty.Default.NameValue.Index].Text; }
        }

        public bool HasName
        {
            get
            {
                if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    return tooltip.GetMultiple<Label>(RacksProperty.Default.NameValue).Length - 3 > RacksProperty.Default.NameValue.Index;
                }
                else // Storm
                {
                    return tooltip.GetMultiple<Label>(RacksProperty.Default.NameValue).Length > RacksProperty.Default.NameValue.Index;
                }
            }
        }


        public string Test
        {
            get { return tooltip.GetMultiple<Label>(RacksProperty.Default.TestValue)[RacksProperty.Default.TestValue.Index].Text; }
        }
        public bool HasTest
        {
            get
            {
                if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    return tooltip.GetMultiple<Label>(RacksProperty.Default.TestValue).Length - 3 > RacksProperty.Default.TestValue.Index;
                }
                else // Storm
                {
                    return tooltip.GetMultiple<Label>(RacksProperty.Default.TestValue).Length > RacksProperty.Default.TestValue.Index;
                }
            }
        }

        public string Volume
        {
            get { return tooltip.GetMultiple<Label>(RacksProperty.Default.VolumeValue)[RacksProperty.Default.VolumeValue.Index].Text; }
        }
        public bool HasVolume
        {
            get
            {
                if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    return tooltip.GetMultiple<Label>(RacksProperty.Default.VolumeValue).Length > RacksProperty.Default.VolumeValue.Index;
                }
                else // Storm
                {
                    return tooltip.GetMultiple<Label>(RacksProperty.Default.VolumeValue).Length > RacksProperty.Default.VolumeValue.Index;
                }
            }
        }
    }
}
