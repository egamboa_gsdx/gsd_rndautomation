﻿using System;
using System.Threading;

using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Status;

using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WPFUIItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.WindowsAPI;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    public class RacksMethods
    {
        internal static IUIItem GetRack(int rackIndex)
        {
            IUIItem[] Racks;

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                {
                    switch (rackIndex)
                    {
                        case 1:
                            return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack1);
                        case 2:
                            return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack2);
                        case 3:
                            return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack3);
                        default:
                            throw new ArgumentException("GetRack() can only find racks 1, 2, and 3");
                    }
                }
                else
                {
                    Racks = ApplicationManager.ApplicationWindow.GetMultiple(RacksProperty.Default.AIXSampleRack);

                    switch (rackIndex)
                    {
                        case 1:
                            return Racks[3];
                        case 2:
                            return Racks[2];
                        case 3:
                            return Racks[0];
                        default:
                            throw new ArgumentException("GetRack() can only find racks 1, 2, and 3");
                    }
                }
            }
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                
                 return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack);
                   
                
            }
            else
            {
                Racks = ApplicationManager.ApplicationWindow.GetMultiple(RacksProperty.Default.AIXSampleRack);
                switch (rackIndex)
                {
                    case 1:
                        return Racks[0];
                    case 2:
                        return Racks[1];
                    case 3:
                        return Racks[2];
                    case 4:
                        return Racks[3];
                    case 5:
                        return Racks[4];
                    case 6:
                        return Racks[5];
                    case 7:
                        return Racks[6];
                    case 8:
                        return Racks[7];
                    case 9:
                        return Racks[8];
                    case 10:
                        return Racks[9];
                    case 11:
                        return Racks[10];
                    case 12:
                        return Racks[11];
                    default:
                        throw new ArgumentException("GetRack() can only find racks 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, and 11 ");
                }
            }
            //throw new SoftwareException("Unexpected software version");
        }

        internal static IUIItem GetReagentRack(int rackIndex = 0)
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                if (ApplicationManager.RackManager == RackManager.bolt)
                {

                    return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormReagentRack);


                }
                else if (ApplicationManager.RackManager == RackManager.SIR)
                {
                    switch (rackIndex)
                    {
                        case 1:
                            return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.SIRReagentRack);
                        case 2:
                            return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.SIRReagentRack2);
                        default:
                            return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.SIRReagentRack);

                    }
                    
                }
                else
                {
                    switch (rackIndex)
                    {
                        case 1:
                            return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormReagentRack1);
                        case 2:
                            return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormReagentRack2);
                        default:
                            return ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormReagentRack);
                    }
                }






            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX && ApplicationManager.RackManager == RackManager.classic)
            {
                IUIItem[] Racks = ApplicationManager.ApplicationWindow.GetMultiple(RacksProperty.Default.AIXSampleRack);
                return Racks[1];
            }
            else
            {
                IUIItem[] Racks = ApplicationManager.ApplicationWindow.GetMultiple(RacksProperty.Default.AIXSampleRack);
                return Racks[12];
            }
            throw new SoftwareException("Unexpected software version");
        }

        internal static Label GetReagentHole(int reagentHoleIndex, int rackIndex = 0)
        {
            Label[] ReagentHoles = GetReagentRack(rackIndex).GetMultiple<Label>(RacksProperty.Default.SampleHole);
            return ReagentHoles[reagentHoleIndex - 1];
        }

        internal static Label GetSampleHole(int rackIndex, int sampleHoleIndex)
        {
            Label[] SampleHoles = GetRack(rackIndex).GetMultiple<Label>(RacksProperty.Default.SampleHole);
            return SampleHoles[sampleHoleIndex - 1];
        }

        /// <summary>
        /// Storm only.
        /// Splits a reagent bottle.
        /// </summary>
        /// <param name="reagentBottleHole">The hole index of the reagent bottle.</param>
        public static void SplitReagentBottle(int reagentBottleHole, int rackIndex = 0)
        {
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("SplitReagentBottle() cannot be called on non-Storm software.");
            }

            if (reagentBottleHole < 1 || reagentBottleHole > 16)
            {
                throw new WorkflowException("Cannot split reagent bottle with hole index " + reagentBottleHole);
            }

            StatusManager.AddStatus("Splitting reagent bottle");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            Label reagentBottle = GetReagentHole(reagentBottleHole, rackIndex);
            reagentBottle.RightClick();
            HelperMethods.Wait();

            TestStack.White.UIItems.MenuItems.Menu menu = ApplicationManager.ApplicationWindow.PopupMenu(RacksProperty.Default.SplitBottle);

            if (!menu.Enabled)
            {
                throw new WorkflowException("Original reagent bottle cannot be split more than once, use the split bottle to split more.");
            }
            HelperMethods.Click(menu);
        }

        /// <summary>
        /// Removes a split reagent bottle.
        /// </summary>
        /// <param name="reagentBottleHole">The hole of the split reagent bottle to remove</param>
        public static void RemoveSplitReagentBottle(int reagentBottleHole, int rackIndex = 0)
        {
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("RemoveSplitReagentBottle() cannot be called on non-Storm software.");
            }

            if (reagentBottleHole < 1 || reagentBottleHole > 16)
            {
                throw new WorkflowException("Cannot remove split reagent bottle with hole index " + reagentBottleHole);
            }

            StatusManager.AddStatus("Removing Split reagent bottle");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            Label reagentBottle = GetReagentHole(reagentBottleHole, rackIndex);
            reagentBottle.RightClick();
            HelperMethods.Wait();

            TestStack.White.UIItems.MenuItems.Menu menu = ApplicationManager.ApplicationWindow.PopupMenu(RacksProperty.Default.RemoveSplitBottle);

            if (!menu.Enabled)
            {
                throw new WorkflowException("Split reagent bottle could not be removed.");
            }
            HelperMethods.Click(menu);
        }

        /// <summary>
        /// Sets the dead volume for a reagent bottle.
        /// </summary>
        /// <param name="reagentBottleHole">The hole index of the reagent bottle.</param>
        /// <param name="deadVolume">The new dead volume to enter.</param>
        public static void SetReagentBottleDeadVolume(int reagentBottleHole, int deadVolume, int rackIndex = 0)
        {
            if (reagentBottleHole < 1 || reagentBottleHole > 16)
            {
                throw new WorkflowException("Cannot set dead volume of reagent bottle with hole index " + reagentBottleHole);
            }

            StatusManager.AddStatus("Setting dead volume for reagent");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            Label reagentBottle = GetReagentHole(reagentBottleHole, rackIndex);
            reagentBottle.RightClick();
            HelperMethods.Wait();

            //TestStack.White.UIItems.MenuItems.Menu menu = ApplicationManager.ApplicationWindow.PopupMenu(RacksProperty.Default.ChangeDeadVolume);

            
            HelperMethods.Click(RacksProperty.Default.ChangeDeadVolume2);
            ApplicationManager.Application.WaitWhileBusy();
            HelperMethods.Wait();

            var windows = ApplicationManager.TryGetWindows();
            ApplicationManager.Application.WaitWhileBusy();
            HelperMethods.Wait();
            ApplicationManager.Application.WaitWhileBusy();
            HelperMethods.Wait();
            bool check = true;

            CheckBox chckbox = ApplicationManager.ApplicationWindow.Get<CheckBox>(RacksProperty.Default.EnableOverride);
            if ((!chckbox.Checked && check) || (chckbox.Checked && !check))
            {
                HelperMethods.Click(RacksProperty.Default.EnableOverride);

                HelperMethods.Click(RacksProperty.Default.DeadVolumeTextBox);


                Keyboard keyboard = new Keyboard();
                keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.Enter("a");
                keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                keyboard.Enter(deadVolume.ToString());

                HelperMethods.Click(CommonProperty.Default.OkButton_automationID);
            }
            else
            {
                HelperMethods.Click(RacksProperty.Default.DeadVolumeTextBox);


                Keyboard keyboard = new Keyboard();
                keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.Enter("a");
                keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                keyboard.Enter(deadVolume.ToString());

                HelperMethods.Click(CommonProperty.Default.OkButton_automationID);
            }
            // foreach (var window in windows)
            //{

            //if (window.Name == "BUFFER_ANA_GSD" || window.Name == "Conjugate_ANA_GSD" || window.Name == "SUBSTRATE_ANA_GSD" || window.Name == "STOP_ANA_GSD")

            //{
            

                   
               // }
               //return;
           // }
            //throw new WorkflowException("Failed to open Set Dead Volume window");
        }

        /// <summary>
        /// Storm only.
        /// Moves a reagent bottle to a new hole.
        /// </summary>
        /// <param name="intialHoleIndex">The hole index containing the reagent bottle to move.</param>
        /// <param name="newHoleIndex">The hole index where the bottle will be placed. Must be currently empty.</param>
        public static void MoveReagentBottle(int intialHoleIndex, int newHoleIndex, int rackIndex = 0)
        {
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("MoveReagentBottle() cannot be called on non-Storm software.");
            }

            if (intialHoleIndex < 1 || intialHoleIndex > 16)
            {
                throw new WorkflowException("Cannot move reagent bottle from hole index " + intialHoleIndex);
            }

            if (newHoleIndex < 1 || newHoleIndex > 16)
            {
                throw new WorkflowException("Cannot move reagent bottle to hole index " + newHoleIndex);
            }

            StatusManager.AddStatus("Moving reagent bottle");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            Label intialHole = GetReagentHole(intialHoleIndex, rackIndex);
            Label newHole = GetReagentHole(newHoleIndex, rackIndex);

            if (newHole.Text != "")
            {
                throw new WorkflowException("Cannot move reagent bottle to non-empty hole");
            }

            AttachedMouse mouse = ApplicationManager.ApplicationWindow.Mouse;
            HelperMethods.Wait();
            HelperMethods.Hover(intialHole);
            HelperMethods.Wait();
            HelperMethods.Wait();
            mouse.DragAndDrop(intialHole, newHole);
            HelperMethods.Wait();
            mouse.DragAndDrop(intialHole, newHole);
            HelperMethods.Wait();
        }

        /// <summary>
        /// Storm only.
        /// Moves a reagent bottle to a new hole.
        /// </summary>
        /// <param name="intialHoleIndex">The hole index containing the reagent bottle to move.</param>
        /// <param name="newHoleIndex">The hole index where the bottle will be placed. Must be currently empty.</param>
        public static void MoveControlTube(int initalRackIndex, int intialHoleIndex, int newRackIndex, int newHoleIndex)
        {
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("MoveControlTube() cannot be called on non-Storm software.");
            }

            StatusManager.AddStatus("Moving reagent bottle");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            Label intialHole = GetSampleHole(initalRackIndex, intialHoleIndex);
            Label newHole = GetSampleHole(newRackIndex, newHoleIndex);

            if (newHole.Text != "")
            {
                throw new WorkflowException("Cannot move control tube to non-empty hole");
            }

            AttachedMouse mouse = ApplicationManager.ApplicationWindow.Mouse;
            mouse.DragAndDrop(intialHole, newHole);
            HelperMethods.Wait();
        }

        internal static RackHoleTooltip GetSampleTooltip(int rackIndex, int sampleIndex)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            HelperMethods.Hover(GetSampleHole(rackIndex, sampleIndex));
            Thread.Sleep(1500);

            var windows = ApplicationManager.Application.GetWindows();
            foreach (Window window in windows)
            {
                if (window.Name == RacksProperty.Default.TooltipWindowName)
                {
                    return new RackHoleTooltip(window);
                }
            }
            throw new WorkflowException("The tooltip for the sample could not be found.");
        }

        internal static RackHoleTooltip GetReagentTooltip(int reagentHoleIndex)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            HelperMethods.Hover(GetReagentHole(reagentHoleIndex));
            Thread.Sleep(1500);

            var windows = ApplicationManager.Application.GetWindows();
            foreach (Window window in windows)
            {
                if (window.Name == RacksProperty.Default.TooltipWindowName)
                {
                    return new RackHoleTooltip(window);
                }
            }
            throw new WorkflowException("The tooltip for the reagent bottle could not be found.");
        }

        /// <summary>
        /// Returns the IUIItem for the wash bottle at the specified index.
        /// </summary>
        /// <param name="index">The index of the wash bottle, base 1</param>
        /// <returns></returns>
        internal static IUIItem GetWashBottle(int index)
        {
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("GetWashBottle() cannot be called on non-Storm software.");
            }

            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            IUIItem racksTab;
            if (ApplicationManager.RackManager == RackManager.SIR)
                racksTab = ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSlideInRacksTab);
            else if (ApplicationManager.RackManager == RackManager.bolt)
                racksTab = ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormRacksTabBolt); 
            else
                racksTab = ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormRacksTab); ;
            return racksTab.GetMultiple(RacksProperty.Default.StormWashBottle)[index - 1];
        }

        internal static IUIItem GetWashBottleTooltip(int index)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);
            return HelperMethods.GetTooltip(GetWashBottle(index));
        }
    }
}
