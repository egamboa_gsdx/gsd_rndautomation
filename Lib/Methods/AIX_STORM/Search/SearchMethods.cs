﻿using System.Threading;

using TestStack.White;
using TestStack.White.WindowsAPI;
using TestStack.White.UIItems;
using TestStack.White.InputDevices;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WPFUIItems;

using GSD_RnDAutomation.Common;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Status;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    public class SearchMethods
    {
        /// <summary>
        /// Performs a search on the search tab.
        /// </summary>
        /// <param name="worklist">The "Worklist" search value, or null to ignore</param>
        /// <param name="operatorName">The "Operator" search value, or null to ignore</param>
        /// <param name="instrument">The "Instrument" search value, or null to ignore</param>
        /// <param name="sampleID">The "Sample ID" search value, or null to ignore</param>
        /// <param name="test">The "Test" search value, or null to ignore</param>
        /// <param name="fromDate">The "From Date" value, or null to ignore</param>
        /// <param name="toDate">The "To Date" value, or null to ignore</param>
        public static void Search(
                string worklist = null,
                string operatorName = null,
                string instrument = null,
                string sampleID = null,
                string test = null,
                string fromDate = null,
                string toDate = null
            )
        {
            StatusManager.AddStatus("Searching...");
            MenuMethods.NavigateTo(MenuProperty.Default.Search);

            // Set all the search values
            SetWorklist(worklist);
            SetOperator(operatorName);
            SetInstrument(instrument);
            SetSampleID(sampleID);
            SetTest(test);
            SetFromDate(fromDate);
            SetToDate(toDate);

            // Press Search
            HelperMethods.Click(SearchProperty.Default.SearchButton);
            HelperMethods.Wait();

            // Wait 1 second for the search to finish collecting results
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Button cancelButton = ApplicationManager.ApplicationWindow.Get<Button>(SearchProperty.Default.CancelButton);
                while (!cancelButton.IsOffScreen)
                {
                    // Wait 1 seconds
                    Thread.Sleep(1000);

                    // Press the cancel button
                    try
                    {
                        HelperMethods.Click(cancelButton);
                        return;
                    }
                    catch // catch in case we press the button as it leaves the screen
                    {
                        return;
                    }
                }
            }
        }
        
    

        /// <summary>
        /// Toggles a checkbox to match the specified state
        /// </summary>
        /// <param name="checkboxIdentifier">The ElementIdentifier for the CheckBox</param>
        /// <param name="state">True for checked, False for unchecked</param>
        internal static void ToggleCheckbox(ElementIdentifier checkboxIdentifier, bool state)
        {
            CheckBox checkbox = ApplicationManager.ApplicationWindow.Get<CheckBox>(checkboxIdentifier);

            // If the checkbox state does not match the required state
            if (checkbox.Checked != state)
            {
                // Click the checkbox to toggle the state
                HelperMethods.Click(checkbox);
            }
        }

        internal static void SetWorklist(string worklist)
        {
            ToggleCheckbox(SearchProperty.Default.WorklistCheckBox, worklist != null);

            if (worklist != null)
            {
                // Click into the textbox
                IUIItem searchTabContainer = ApplicationManager.ApplicationWindow.Get(SearchProperty.Default.SearchTabContainer);
                if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                {
                    IUIItem worklistTextBox = searchTabContainer.Get(SearchProperty.Default.WorklistTextBox);
                    HelperMethods.Click(worklistTextBox);
                }
                else
                {
                    IUIItem AixWorklistTextBox = searchTabContainer.Get(SearchProperty.Default.AixWorklistTextBox);
                    HelperMethods.Click(AixWorklistTextBox);
                }
                AttachedKeyboard keyboard = ApplicationManager.ApplicationWindow.Keyboard;

                // CTRL + A, and DELETE to remove existing text
                keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.Enter("a");
                keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);

                // Enter the worklist
                keyboard.Enter(worklist);
            }
        }

        internal static void SetOperator(string operatorName)
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm) {
                ToggleCheckbox(SearchProperty.Default.OperatorCheckBox, operatorName != null);

                if (operatorName != null)
                {
                    // Open the dropdown
                    HelperMethods.Click(SearchProperty.Default.OperatorDropdown);

                    // Get the dropdown window
                    Window dropdown = ApplicationManager.ApplicationWindow.ModalWindow(SearchProperty.Default.DropdownWindowName);

                    try
                    {
                        // Find and click on the operator with the matching name
                        HelperMethods.Click(new ElementIdentifier(textValue: operatorName, className: SearchProperty.Default.DropdownItemClassName));
                    }
                    catch (AutomationException e)
                    {
                        // If we couldn't find the operator, throw a WorkflowException
                        throw new WorkflowException("Failed to find operator \"" + operatorName + "\" in dropdown", e);
                    }
                }
            }
        }

        internal static void SetInstrument(string instrument)
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                ToggleCheckbox(SearchProperty.Default.InstrumentCheckBox, instrument != null);

                if (instrument != null)
                {
                    // Open the dropdown
                    HelperMethods.Click(SearchProperty.Default.InstrumentDropdown);

                    // Get the dropdown window
                    Window dropdown = ApplicationManager.ApplicationWindow.ModalWindow(SearchProperty.Default.DropdownWindowName);

                    try
                    {
                        // Find and click on the instrument with the matching name
                        HelperMethods.Click(new ElementIdentifier(textValue: instrument, className: SearchProperty.Default.DropdownItemClassName));
                    }
                    catch (AutomationException e)
                    {
                        // If we couldn't find the instrument, throw a WorkflowException
                        throw new WorkflowException("Failed to find instrument \"" + instrument + "\" in dropdown", e);
                    }
                }
            }
        }

        internal static void SetSampleID(string sampleID)
        {
            ToggleCheckbox(SearchProperty.Default.SampleIdCheckBox, sampleID != null);

            if (sampleID != null)
            {
                // Click into the textbox
                HelperMethods.Click(SearchProperty.Default.SampleIdTextBox);

                AttachedKeyboard keyboard = ApplicationManager.ApplicationWindow.Keyboard;

                // CTRL + A, and DELETE to remove existing text
                keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.Enter("a");
                keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);

                // Enter the sample ID
                keyboard.Enter(sampleID);
            }
        }

        internal static void SetTest(string test)
        {
            ToggleCheckbox(SearchProperty.Default.TestCheckBox, test != null);

            if (test != null)
            {
                // Open the dropdown
                HelperMethods.Click(SearchProperty.Default.TestDropdown);

                // Get the dropdown window
                Window dropdown = ApplicationManager.ApplicationWindow.ModalWindow(SearchProperty.Default.DropdownWindowName);

                try
                {
                    // Find and click on the test with the matching name
                    HelperMethods.Click(new ElementIdentifier(textValue: test, className: SearchProperty.Default.DropdownItemClassName));
                }
                catch (AutomationException e)
                {
                    // If we couldn't find the test, throw a WorkflowException
                    throw new WorkflowException("Failed to find test \"" + test + "\" in dropdown", e);
                }
            }
        }

        internal static void SetFromDate(string fromDate)
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                ToggleCheckbox(SearchProperty.Default.FromDateCheckBox, fromDate != null);
            }
            else
            {
                ToggleCheckbox(SearchProperty.Default.AixFromDateCheckBox, fromDate != null);
            }

            if (fromDate != null)
            {
                // Click into the textbox
                HelperMethods.Click(SearchProperty.Default.FromDateDatePicker);

                AttachedKeyboard keyboard = ApplicationManager.ApplicationWindow.Keyboard;

                // CTRL + A, and DELETE to remove existing text
                keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.Enter("a");
                keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);

                // Enter the date
                keyboard.Enter(fromDate);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.RETURN);
            }
        }

        internal static void SetToDate(string toDate)
        {
            ToggleCheckbox(SearchProperty.Default.ToDateCheckBox, toDate != null);

            if (toDate != null)
            {
                // Click into the textbox
                HelperMethods.Click(SearchProperty.Default.ToDateDatePicker);

                AttachedKeyboard keyboard = ApplicationManager.ApplicationWindow.Keyboard;

                // CTRL + A, and DELETE to remove existing text
                keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.Enter("a");
                keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);

                // Enter the date
                keyboard.Enter(toDate);
            }
        }
    }
}
