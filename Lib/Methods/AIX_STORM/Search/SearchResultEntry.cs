﻿using System;
using TestStack.White.UIItems;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    /// <summary>
    /// ListViewRow wrapper for the search result table on the Search tab
    /// </summary>
    internal class SearchResultEntry
    {
        private ListViewRow row;

        public SearchResultEntry(ListViewRow row)
        {
            this.row = row;
        }

        public string Worklist
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchResultWorklistIndex].Text;
            }
        }

        public string Operator
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchResultOperatorIndex].Text;
            }
        }

        public DateTime DateTime
        {
            get
            {
                return DateTime.Parse(row.Cells[SearchProperty.Default.SearchResultDateTimeIndex].Text);
            }
        }

        public string PatientID
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchResultPatientIDIndex].Text;
            }
        }

        public string SampleID
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchResultSampleIDIndex].Text;
            }
        }
        public string AIXSampleID
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchAIXSampleIdIndex].Text;
            }
        }
        public string Mtp
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchAIXMtpIndex].Text;
            }
        }
        public string MtpPosition
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchAIXMtpPositionIndex].Text;
            }
        }
        public string Racklocation
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchAIXRackLocationIndex].Text;
            }
        }
        public string User
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchAIXUserIndex].Text;
            }
        }

        public string Time
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchAIXTimeIndex].Text;
            }
        }
        public string Result
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchAIXResultIndex].Text;
            }
        }
        public string Overridden
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchAIXOverriddenIndex].Text;
            }
        }
        public string Instrument
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchResultInstrumentIndex].Text;
            }
        }

        public string Test
        {
            get
            {
                return row.Cells[SearchProperty.Default.SearchResultTestIndex].Text;
            }
        }




    }
}
