﻿using GSD_RnDAutomation.Common.Methods;

using TestStack.White.UIItems.ListBoxItems;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    /// <summary>
    /// Methods for the Control Scheme dialog on AIX
    /// </summary>
    public class ControlSchemeMethods
    {
        public static void SelectControlScheme(int controlScheme)
        {
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                ComboBox ControlSchemeDropdown = ApplicationManager.ApplicationWindow.Get<ComboBox>(WorklistProperty.Default.ControlSchemeDropdown.ToSearchCriteria());
                HelperMethods.Click(ControlSchemeDropdown);
                HelperMethods.Wait();

                HelperMethods.Click(ControlSchemeDropdown.Items[controlScheme]);
                HelperMethods.Wait();

                HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                ComboBox ControlSchemeDropdown = ApplicationManager.ApplicationWindow.Get<ComboBox>(WorklistProperty.Default.TPPAcs.ToSearchCriteria());
                HelperMethods.Click(ControlSchemeDropdown);
                HelperMethods.Wait();

                HelperMethods.Click(ControlSchemeDropdown.Items[controlScheme]);
                HelperMethods.Wait();

                HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
            }
        }
    }
}
