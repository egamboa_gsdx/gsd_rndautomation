﻿using System;
using System.Linq;
using System.Threading;

using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.ListViewItems;
using TestStack.White.UIItems.TreeItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WPFUIItems;
using TestStack.White.WindowsAPI;

using GSD_RnDAutomation.Common;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Status;

namespace GSD_RnDAutomation.Methods.AIX_STORM
{
    /// <summary>
    /// Methods for the Worklist tab
    /// </summary>
    public class WorklistMethods
    {
        /// <summary>
        /// Adds a name for the worklist
        /// </summary>
        /// <param name="name"></param>
        public static void AddWorklistName(string name)
        {
            StatusManager.AddStatus("Add worklist name");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                HelperMethods.Click(WorklistProperty.Default.StormWorklistName);
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                HelperMethods.Click(WorklistProperty.Default.AIXWorklistName);
            }

            AttachedKeyboard keyboard = ApplicationManager.ApplicationWindow.Keyboard;
            keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
            keyboard.Enter("a");
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
            keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
            keyboard.Enter(name);
            HelperMethods.Wait();
        }

        /// <summary>
        /// Storm only.
        /// Adds the "ANA_GSD" test to the worklist.
        /// </summary>
        /// <param name="numberOfTests">The number of tests to add.</param>
        public static void AddTest(int numberOfTests = 1, string testName = "ANA_GSD")//ANA-12IgG-Test ANA_GSD Anti_treponema_4_IgG
        {
            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("AddTest() cannot be run in non-Storm software.");
            }

            StatusManager.AddStatus("Adding " + numberOfTests + " Test(s)");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            while (numberOfTests > 0)
            {
                try
                {
                    HelperMethods.Hover(WorklistProperty.Default.AddTestButton);
                    HelperMethods.Wait();
                    HelperMethods.Click(WorklistProperty.Default.AddTestButton);
                    HelperMethods.Wait();
                    HelperMethods.Wait();
                    HelperMethods.Wait();
                }
                catch (AutomationException e)
                {
                    // This error can occur when no tests have been added on the server.
                    // Make sure the to restore the server with SmokeTest/TestData/StormBackup.tar
                    throw new WorkflowException("No tests are available", e);
                }

                try
                {
                    HelperMethods.Wait();
                    HelperMethods.Hover(new ElementIdentifier(textValue: testName));
                    HelperMethods.Wait();
                    HelperMethods.Click(new ElementIdentifier(textValue: testName));
                    ApplicationManager.Application.WaitWhileBusy();
                }
                catch (AutomationException e)
                {
                    // there is an error where the test cannot be found when its clearly on screen.
                    // This can appear when running test cases in alphabetical order BY C# SCRIPT NAME.
                    // Running tests cases in alphabetical order by test case name (found inside the scripts) results in no errors.
                    throw new WorkflowException(testName + " test not found", e);
                }

                Window window = null;
                foreach (Window modal in ApplicationManager.ApplicationWindow.ModalWindows())
                {
                    if (modal.Name == WindowName.Default.ErrorWindow)
                    {
                        window = modal;
                    }
                }
                if (window != null)
                {
                    HelperMethods.Click(CommonProperty.Default.OkButton_automationID, window);
                    throw new WorkflowException("An error occured while adding a test");
                }

                numberOfTests--;
            }
        }

        /// <summary>
        /// Assigns a sample to a test using the checkbox.
        /// </summary>
        /// <param name="sampleIndex">One-based index of sample</param>
        /// <param name="testIndex">One-based index of test</param>
        public static void AssignSample(int sampleIndex = 1, int testIndex = 1)
        {
            StatusManager.AddStatus("Assign Sample...");

            HighlightSamples(sampleIndex);
            ClickSampleTest(sampleIndex, testIndex);
        }
        /// <summary>
        /// METHOD NAME: AddSamples
        /// BY:ERNEST GAMBOA
        /// =============================================
        /// Add samples depending on parameter passed in
        ///  0 - add samples manually
        ///  1 - add samples using auto increment
        ///  2 - add samples by import
        /// =============================================
        /// Control Scheme options:
        ///  0 - Controls on all plates
        ///  1 - No controls
        ///  2 - Controls on first plate only
        /// * Note: default parameters are for
        ///        auto increment with no controls
        /// =============================================
        /// </summary>
        /// <param name="choice">variable to choose how to add samples; default 1 (Auto Increment)</param>
        /// <param name="numberOfSamples">number of samples to be added; default 5</param>
        /// <param name="controlScheme">select which control scheme to assign; default 1 (No controls)(</param>
        /// <param name="sampleIDTemplate">sample id name template; default null</param>
        /// <param name="startNumber">Start ID number; default 1</param>
        public static void AddSamples(int choice = 1,
                                      int numberOfSamples = 5,
                                      int controlScheme = 1,
                                      string sampleIDTemplate = null,
                                      int startNumber = 1
                                      )
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
            //Click Add samples button to begin adding sample process
                    if (ApplicationManager.RackManager == RackManager.SIR)
                    {
                        choice = 2;
                        if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                            HelperMethods.Click(WorklistProperty.Default.SirAddSamplesButton);
                        else
                            HelperMethods.Click(WorklistProperty.Default.StormSirAddSamplesButton);
                    }
                    else
                    {
                        HelperMethods.Click(WorklistProperty.Default.AddSamplesButton);
                        HelperMethods.Click(WorklistProperty.Default.AddSamplestoWorklistButton);
                    }

            //AIX ONLY; Select control scheme passed in
            if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                if (ApplicationManager.RackManager == RackManager.classic)
                {
                    switch (controlScheme)
                    {
                        case 0:
                            ControlSchemeMethods.SelectControlScheme(WorklistProperty.Default.ControlsOnAllPlates);
                            break;
                        case 1:
                            ControlSchemeMethods.SelectControlScheme(WorklistProperty.Default.NoControls);
                            break;
                        case 2:
                            ControlSchemeMethods.SelectControlScheme(WorklistProperty.Default.ControlsOnlyOnFirstPlate);
                            break;
                        default:
                            throw new ValidationException("Index out of range...no control scheme selected...");
                    }
                }

                else if (ApplicationManager.RackManager == RackManager.SIR)
                {
                    HelperMethods.Click(WorklistProperty.Default.TPPAcs);
                    switch (controlScheme)
                    {
                        case 0:
                            ControlSchemeMethods.SelectControlScheme(WorklistProperty.Default.ControlsOnAllPlates);
                            break;
                        case 1:
                            ControlSchemeMethods.SelectControlScheme(WorklistProperty.Default.NoControls);
                            break;
                        case 2:
                            ControlSchemeMethods.SelectControlScheme(WorklistProperty.Default.ControlsOnlyOnFirstPlate);
                            break;
                        default:
                            throw new ValidationException("Index out of range...no control scheme selected...");
                    }
                }
            }

            Keyboard keyboard = new Keyboard();
            switch (choice)
            {
                case 0:
                    StatusManager.AddStatus("Adding samples manually...");

                    //Select manual in Add Sample Window
                    var manualButton = ApplicationManager.ApplicationWindow.Get(WorklistProperty.Default.ManualButton);
                    double height = manualButton.Bounds.Bottom - manualButton.Bounds.Top;
                    System.Windows.Point clickablePoint = manualButton.Bounds.TopLeft;
                    clickablePoint.Y += height / 2;
                    AttachedMouse mouse = ApplicationManager.ApplicationWindow.Mouse;
                    mouse.LeftClick(clickablePoint);
                    HelperMethods.Wait();

                    HelperMethods.Click(WorklistProperty.Default.SampleIDTextBox);
                    if (sampleIDTemplate == null)
                    {
                        sampleIDTemplate = "Sample";
                    }
                    // We need the window to get its keyboard
                    keyboard.Enter(sampleIDTemplate);
                    HelperMethods.Click(WorklistProperty.Default.AddButton);

                    // If samples are already added
                    if (ApplicationManager.TryGetWindows().Any(x => x.Name == "Warning"))
                    {
                        HelperMethods.Click(CommonProperty.Default.CancelButton_automationID);
                        HelperMethods.Click(WorklistProperty.Default.CloseButton);
                        New();
                        throw new WorkflowException("Samples could not be added because samples already exist in worklist");
                    }

                    //For some reason we need to grab this window since its not a sub window
                    var manualwindowPlaceSamples = ApplicationManager.Application.GetWindow(WindowName.Default.PlaceSample, InitializeOption.NoCache);
                    HelperMethods.Click(WorklistProperty.Default.DoneButton, manualwindowPlaceSamples);
                    HelperMethods.Click(WorklistProperty.Default.CloseButton);
                    break;
                case 1:
                    StatusManager.AddStatus("Adding samples by Auto Increment...");


                    //selects auto increment in Add Samples Window
                    HelperMethods.Click(WorklistProperty.Default.AutoIncrementButton);

                    //Updates Sample ID Template and/or Start number if provided
                    if (sampleIDTemplate != null)
                    {
                        HelperMethods.Click(WorklistProperty.Default.SampleIDTemplateTextBox);

                        keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                        keyboard.Enter("a");
                        keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                        keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                        keyboard.Enter(sampleIDTemplate);
                    }

                    if (startNumber != 1)
                    {
                        HelperMethods.Click(WorklistProperty.Default.StartNumber);

                        keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                        keyboard.Enter("a");
                        keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                        keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                        keyboard.Enter(Convert.ToString(startNumber));
                    }

                    HelperMethods.Click(WorklistProperty.Default.AddButton);

                    // TODO make this throw a workflow exception instead of creating a new worklist
                    // If samples are already added
                    if (ApplicationManager.TryGetWindows().Any(x => x.Name == "Warning"))
                    {
                        StatusManager.AddStatus("Clearing Loaded Samples");
                        HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
                        HelperMethods.Click(WorklistProperty.Default.CloseButton);
                        New();
                        AddSamples(choice: 1, numberOfSamples: numberOfSamples);
                        return;
                    }

                    //For some reason we need to grab this window since its not a sub window
                    var autowindowPlaceSamples = ApplicationManager.Application.GetWindow(WindowName.Default.PlaceSample, InitializeOption.NoCache);
                    for (int i = 1; i < numberOfSamples; i++)
                    {
                        AttachedKeyboard attachedkeyboard = autowindowPlaceSamples.Keyboard;
                        attachedkeyboard.PressSpecialKey(KeyboardInput.SpecialKeys.SPACE);
                        Thread.Sleep(50);
                    }
                    HelperMethods.Wait();

                    HelperMethods.Click(WorklistProperty.Default.DoneButton, autowindowPlaceSamples);
                    HelperMethods.Click(WorklistProperty.Default.CloseButton);

                    ApplicationManager.ApplicationWindow.WaitWhileBusy();
                    ApplicationManager.ApplicationWindow.Focus();

                    break;
                case 2:
                    StatusManager.AddStatus("Importing samples file...");

                    var importButton = ApplicationManager.ApplicationWindow.Get(WorklistProperty.Default.ImportButton);
                    double iheight = importButton.Bounds.Bottom - importButton.Bounds.Top;
                    System.Windows.Point iclickablePoint = importButton.Bounds.TopLeft;

                    iclickablePoint.Y += iheight / 2;
                    AttachedMouse imouse = ApplicationManager.ApplicationWindow.Mouse;
                    imouse.LeftClick(iclickablePoint);
                    HelperMethods.Wait();


                    HelperMethods.Click(WorklistProperty.Default.AddButton);
                    HelperMethods.Wait();
                    HelperMethods.Wait();

                    try
                    {
                        if (ApplicationManager.RackManager == RackManager.classic)
                        {
                            GeneralMethods.OpenFile("64SamplesInRacks.xlsx", "Test Data");
                            if (controlScheme == 2)
                            {
                                HelperMethods.Click(CommonProperty.Default.Ok); //bypass controls warningcd
                            }
                                
                        }
                            

                        
                        else
                        {
                            if (controlScheme == 1)
                            {
                                HelperMethods.Wait();
                                GeneralMethods.OpenFile("SIR16Samples.xlsx", "Test Data");
                                HelperMethods.Wait();
                            }
                            else if (controlScheme == 3 && ApplicationManager.RackManager == RackManager.SIR)
                            {
                                GeneralMethods.OpenFile("192SamplesInRacksSIR.xlsx", "Test Data");
                            }
                            else if (ApplicationManager.RackManager == RackManager.bolt)
                            {
                                GeneralMethods.OpenFile("64SamplesInRacks.xlsx", "Test Data");
                            }
                            else 
                            {
                                GeneralMethods.OpenFile("SIR184Samples.xlsx", "Test Data");
                            }

                        }
                       


                        //For some reason we need to grab this window since its not a sub window
                        var importSuccessful = ApplicationManager.Application.GetWindow("Import Successfull", InitializeOption.NoCache);
                        HelperMethods.Click(WorklistProperty.Default.DoneButton, importSuccessful);
                        HelperMethods.Click(WorklistProperty.Default.CloseButton);
                        HelperMethods.Wait();
                        if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                        {
                            if (ApplicationManager.RackManager == RackManager.bolt)
                            {
                                if (ApplicationManager.TryGetWindows().Any(x => x.Name == "Warning"))
                                {
                                    HelperMethods.Click(CommonProperty.Default.Ok);

                                }
                            }
                        }
                            

                        if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                        {
                            if (ApplicationManager.RackManager == RackManager.classic)
                            {
                                AssignAllSamplesToTest();
                            }
                            if (ApplicationManager.RackManager == RackManager.SIR)
                            {
                                AssignAllSamplesToTest(0);
                                AssignAllSamplesToTest(5);
                            }
                        }
                        
                        

                    }
                    catch (WorkflowException e)
                    {
                        HelperMethods.Click(WorklistProperty.Default.CloseButton);
                        throw new WorkflowException("No saved sample excel documents to load", e);
                    }
                    break;
                default:
                    throw new ValidationException("Incorrect value passed in...no such case for " + choice + " established...");
            }
        }

        /// <summary>
        /// Returns true when the extra column on the left is present.
        /// </summary>
        /// <returns></returns>
        public static bool IsExtraColumnPresent()
        {
            ListView samplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            ListViewColumns headerItems = samplesGrid.Header.Columns;

            // This is the "Sample ID" column if the extra column is not present
            ListViewColumn sampleIDHeader = samplesGrid.Header.Columns[1];
            return sampleIDHeader.Text != WorklistProperty.Default.SamplesGridSampleIDColumnText;
        }

        /// <summary>
        /// Returns the index of a sample cell adjusted for possible extra column on left.
        /// </summary>
        /// <param name="normalIndex">Zero-based index of the cell if the extra column is not present.</param>
        /// <returns></returns>
        public static int GetShiftedSampleIndex(int normalIndex)
        {
            return IsExtraColumnPresent() ? normalIndex + 1 : normalIndex;
        }

        /// <summary>
        /// Drags the first row of the samples list to the last and swaps the two
        /// </summary>
        public static void DragAndDropSamples()
        {
            StatusManager.AddStatus("Drag and Drop Samples...");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);

            // Some machines will have an extra row of checkboxes on the farthest left side
            int sampleNamePosition = GetShiftedSampleIndex(1);

            ListViewCell Drag = SamplesGrid.Rows[0].Cells[sampleNamePosition];
            ListViewCell Drop = SamplesGrid.Rows[SamplesGrid.Rows.Count - 1].Cells[sampleNamePosition];

            AttachedMouse mouse = ApplicationManager.ApplicationWindow.Mouse;
            mouse.DragAndDrop(Drag, Drop);
        }

        /// <summary>
        /// Drag and drops a test over another test to change their order.
        /// </summary>
        /// <param name="testIndexDrag">The test to drag</param>
        /// <param name="testIndexDrop">The test to drop the dragged test on</param>
        public static void DragAndDropTest(int testIndexDrag, int testIndexDrop)
        {
            StatusManager.AddStatus("Drag and Drop Test...");

            IUIItem item = ApplicationManager.ApplicationWindow.Get(WorklistProperty.Default.TestColumn);

            AttachedMouse mouse = ApplicationManager.ApplicationWindow.Mouse;
            mouse.DragAndDrop(item, GetTestTooltipPoint(testIndexDrag), item, GetTestTooltipPoint(testIndexDrop));
        }
        /// <summary>
        /// METHOD NAME: HighlightSamples
        /// BY: Ernest Gamboa
        /// =========================================
        /// Highlight samples in the sample grid
        /// using a specified key (SHIFT or CTRL),
        /// select all,
        /// or select only a certain sample
        /// =========================================
        /// *Note: Default key is null
        /// =========================================
        /// </summary>
        /// <param name="sample">sample index to highlight</param>
        /// <param name="key">action to use to highlight samples</param>
        public static void HighlightSamples(int sample = 1, string action = null)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
            if (action != null)
            {
                action = action.ToUpper();
            }

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            AttachedKeyboard keyboard = ApplicationManager.ApplicationWindow.Keyboard;
            ListViewRow FirstSample = SamplesGrid.Rows[0];
            int sampleNumberPosition = GetShiftedSampleIndex(0);

            switch (action)
            {
                case "SHIFT":
                    StatusManager.AddStatus("Highlighting samples using " + action + " key");
                    keyboard.LeaveAllKeys();
                    Thread.Sleep(500);
                    keyboard.HoldKey(KeyboardInput.SpecialKeys.SHIFT);
                    Thread.Sleep(500);
                    HelperMethods.Click(SamplesGrid.Rows[sample - 1].Cells[sampleNumberPosition]);
                    Thread.Sleep(500);
                    keyboard.LeaveKey(KeyboardInput.SpecialKeys.SHIFT);
                    Thread.Sleep(500);
                    break;
                case "CTRL":
                    StatusManager.AddStatus("Highlighting samples using " + action + " key");
                    keyboard.LeaveAllKeys();
                    Thread.Sleep(500);
                    keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                    Thread.Sleep(500);
                    HelperMethods.Click(SamplesGrid.Rows[sample - 1].Cells[sampleNumberPosition]);
                    Thread.Sleep(500);
                    keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                    Thread.Sleep(500);
                    break;
                case "ALL":
                    //some applications may not have extra column for checkbxes
                    if (IsExtraColumnPresent())
                    {
                        // Click on the first sample's name
                        HelperMethods.Click(FirstSample.Cells[2]);

                        HighlightSamples(SamplesGrid.Rows.Count, "SHIFT");
                    }
                    else
                    {
                        // Click on the first sample's name
                        HelperMethods.Click(FirstSample.Cells[1]);
                        HelperMethods.Wait();

                        // Press Ctrl+A
                        keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                        HelperMethods.Wait();
                        keyboard.Enter("a");
                        HelperMethods.Wait();
                        keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                        HelperMethods.Wait();
                    }
                    break;
                case null:
                    StatusManager.AddStatus("Selecting sample " + sample);

                    ListViewRow Sample = SamplesGrid.Rows[sample - 1];

                    // Click on the sample's name
                    HelperMethods.Click(Sample.Cells[sampleNumberPosition]);
                    break;
                default:
                    throw new WorkflowException("Unknown key (" + action + ") passed in...unable to highlight samples...");
            }
        }

        /// <summary>
        /// Multiply currently selected samples by a specified multiplication factor
        /// </summary>
        /// <param name="factor"></param>
        public static void MultiplySamples(int factor)
        {
            StatusManager.AddStatus("Multiply Samples by " + factor);
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            try
            {
                HelperMethods.Hover(WorklistProperty.Default.MultiplySamplesButton);
                HelperMethods.Click(WorklistProperty.Default.MultiplySamplesButton);

                switch (factor)
                {
                    case 2:
                        HelperMethods.Click(WorklistProperty.Default.Multiply2Button);
                        break;
                    case 4:
                        HelperMethods.Click(WorklistProperty.Default.Multiply4Button);
                        break;
                    case 8:
                        HelperMethods.Click(WorklistProperty.Default.Multiply8Button);
                        break;
                    default:
                        HelperMethods.Click(WorklistProperty.Default.MultiplyCustomButton);
                        HelperMethods.Click(WorklistProperty.Default.TextBox);
                        Keyboard keyboard = new Keyboard();
                        keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                        keyboard.Enter("a");
                        keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                        keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                        HelperMethods.Wait();
                        keyboard.Enter(factor.ToString());
                        HelperMethods.Wait();
                        HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
                        break;
                }
            }
            catch (Exception e)
            {
                throw new WorkflowException("No samples selected to multiply", e);
            }
        }

        /// <summary>
        /// Wipes current tests and samples
        /// </summary>
        public static void New()
        {
                StatusManager.AddStatus("New worklist");

                // If a sub window is open close it
                // Sometimes a nameless invisible window will open 
                if (ApplicationManager.TryGetWindows().Where(x => x.Name.Length > 0).Count() > 1)
                {
                    try
                    {
                        HelperMethods.Click(CommonProperty.Default.CancelButton_automationID);
                        HelperMethods.Click(HomeProperty.Default.CloseButton);
                        HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
                        MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
                    }
                    catch
                    {
                        // the sub window has been closed
                    }
                }
                else
                {
                    MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
                }

                MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

                try
                {
                    Button NewButton = ApplicationManager.ApplicationWindow.Get<Button>(WorklistProperty.Default.NewButton);

                    // If the new button is disabled, try stopping the worklist
                    if (!NewButton.Enabled)
                    {
                        HomeMethods.Stop();
                        MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
                    }

                    HelperMethods.Click(WorklistProperty.Default.NewButton);
                    HelperMethods.Click(CommonProperty.Default.YesButton_textValue);
                    HelperMethods.Wait();
                }
                catch (Exception e)
                {
                    throw new WorkflowException("Cannot press \"NEW\" without samples added", e);
                }
            
        }

        internal static string removeSamplesAlertMissingMessage = "Remove Samples Alert did not appear!";

        /// <summary>
        /// Presses the remove button.
        /// On Storm, it also clears the remove samples warning.
        /// Throws WorkflowException if the button is disabled.
        /// Throws WorkflowException if the sample was multiplied.
        /// Throws ValidationException on Storm if the warning does not appear.
        /// </summary>
        internal static void RemoveSamples()
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);


            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Button RemoveSamplesButton = ApplicationManager.ApplicationWindow.Get<Button>(WorklistProperty.Default.RemoveSamplesButton);
                if (!RemoveSamplesButton.Enabled)
                {
                    throw new WorkflowException("Remove samples button is disabled.");
                }
                HelperMethods.Hover(RemoveSamplesButton);
                HelperMethods.Click(RemoveSamplesButton);
                HelperMethods.Wait();

            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                Button AIXRemoveSamplesButton = ApplicationManager.ApplicationWindow.Get<Button>(WorklistProperty.Default.AIXRemoveSamplesButton);
                if (!AIXRemoveSamplesButton.Enabled)
                {
                    throw new WorkflowException("Remove samples button is disabled.");
                }
                HelperMethods.Hover(AIXRemoveSamplesButton);
                HelperMethods.Click(AIXRemoveSamplesButton);
                HelperMethods.Wait();

            }


            /*Button RemoveSamplesButton = ApplicationManager.ApplicationWindow.Get<Button>(WorklistProperty.Default.RemoveSamplesButton);
            if (!RemoveSamplesButton.Enabled)
            {
                throw new WorkflowException("Remove samples button is disabled.");
            }
            HelperMethods.Hover(RemoveSamplesButton);
            HelperMethods.Click(RemoveSamplesButton);
            HelperMethods.Wait(); */

            foreach (Window window in ApplicationManager.ApplicationWindow.ModalWindows())
            {
                if (window.Name == WindowName.Default.RemoveMultipliedSampleAlert)
                {
                    HelperMethods.Click(CommonProperty.Default.OkButton_automationID, window);
                    throw new WorkflowException("Cannot remove multiplied sample");
                }
                else if (window.Name == WindowName.Default.RemoveSampleAlert)
                {
                    HelperMethods.Click(CommonProperty.Default.YesButton_automationID, window);
                    return; // If we find the alert, exit here
                }
            }
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                // If the alert was not found, throw an exception
                throw new WorkflowException(removeSamplesAlertMissingMessage);
            }
        }

        /// <summary>
        /// Removes all samples from worklist
        /// </summary>
        public static void RemoveAllSamples()
        {
            StatusManager.AddStatus("Remove all samples");

            HighlightSamples(action: "ALL");
            Thread.Sleep(3000);

            RemoveSamples();
        }

        /// <summary>
        /// Removes the currently selected samples.
        /// </summary>
        public static void RemoveCurrentSamples(bool errorOnFail = true)
        {
            StatusManager.AddStatus("Remove current samples");

            try
            {
                RemoveSamples();
            }
            catch
            {
                if (errorOnFail)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Removes one ANA_GSD test
        /// </summary>
        public static void RemoveTest(string testName = "ANA_GSD")//ANA_GSD
        {
            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("RemoveTest() cannot be run in non-Storm software.");
            }

            StatusManager.AddStatus("Remove test");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            HelperMethods.Click(WorklistProperty.Default.RemoveTestButton);
            HelperMethods.Click(new ElementIdentifier(textValue: testName));
        }

        private static System.Windows.Point GetTestTooltipPoint(int testIndex = 1)
        {
            StatusManager.AddStatus("Opening test property window");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            // Find the cell underneath test name
            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);

            // If there are no samples in worklist add a sample so that test can be accessed
            if (SamplesGrid.Rows.Count == 0)
            {
                WorklistMethods.AddSamples(choice: GeneralProperty.Default.ManualAdd, sampleIDTemplate: "0");
            }

            ListViewRow SampleRow = SamplesGrid.Rows[0];
            var header = ApplicationManager.ApplicationWindow.Get(WorklistProperty.Default.TestColumn);
            System.Windows.Point clickablePoint;
            double width, height;

            // AIX cannot find the cell underneath the test
            if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                UIItem AIXSample = new UIItem(SampleRow.AutomationElement, SampleRow.ActionListener);
                RadioButton[] SampleTestRadioButtons = AIXSample.GetMultiple<RadioButton>(WorklistProperty.Default.AIXSamplesGridTestCheckbox);
                RadioButton SampleTestRadioButton = SampleTestRadioButtons[testIndex];

                // Get test name location
                width = SampleTestRadioButton.Bounds.Right - SampleTestRadioButton.Bounds.Left;
                height = header.Bounds.Bottom - header.Bounds.Top;
                clickablePoint = SampleTestRadioButton.Bounds.TopLeft;
            }
            else
            {
                int SampleCellIndex = GetShiftedSampleIndex(3 + testIndex - 1);
                ListViewCell SampleTest = SampleRow.Cells[SampleCellIndex];

                // Get test name location
                width = SampleTest.Bounds.Right - SampleTest.Bounds.Left;
                height = header.Bounds.Bottom - header.Bounds.Top;
                clickablePoint = SampleTest.Bounds.TopLeft;
            }

            clickablePoint.X += width / 2;
            clickablePoint.Y -= height / 1.5;

            return clickablePoint;
        }

        /// <summary>
        /// Hovers over test to produce tooltip
        /// </summary>
        /// <param name="testIndex">One-based index of the test to use.</param>
        internal static void ShowTestTooltip(int testIndex = 1)
        {
            StatusManager.AddStatus("Viewing test tooltip");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            AttachedMouse mouse = ApplicationManager.ApplicationWindow.Mouse;
            mouse.Location = GetTestTooltipPoint(testIndex);
            Thread.Sleep(1500);
        }

        /// <summary>
        /// Clicks on a test to open the Test Properties window
        /// </summary>
        /// <param name="testIndex">One-based index of the test to use.</param>
        internal static void OpenTestPropertiesWindow(int testIndex = 1)
        {
            AttachedMouse mouse = ApplicationManager.ApplicationWindow.Mouse;
            mouse.LeftClick(GetTestTooltipPoint(testIndex));
            HelperMethods.Wait();
        }

        /// <summary>
        /// Right-clicks on a test to assign all samples to the test
        /// </summary>
        /// <param name="testIndex">One-based index of the test to use.</param>
        public static void AssignAllSamplesToTest(int testIndex = 1)
        {
            AttachedMouse mouse = ApplicationManager.ApplicationWindow.Mouse;
            mouse.Location = GetTestTooltipPoint(testIndex);
            mouse.RightClick();
            HelperMethods.Wait();
        }

        /// <summary>
        /// Clicks on a sample's test checkbox to assign it to a test.
        /// </summary>
        /// <param name="sampleIndex">One-based index of sample</param>
        /// <param name="testIndex">One-based index of test</param>
        private static void ClickSampleTest(int sampleIndex, int testIndex = 1)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            ListViewRow SampleRow = SamplesGrid.Rows[sampleIndex - 1];
            UIItem Sample = new UIItem(SampleRow.AutomationElement, SampleRow.ActionListener);

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                // Some machines will have an extra row of checkboxes on the farthest left side
                testIndex = GetShiftedSampleIndex(testIndex);

                CheckBox[] SampleTestCheckboxes = Sample.GetMultiple<CheckBox>(WorklistProperty.Default.StormSamplesGridTestCheckbox);
                CheckBox SampleTestCheckbox = SampleTestCheckboxes[testIndex - 1];

                // Deselect the test first, this ensure multiple sample selections work properly
                //if (SampleTestCheckbox.IsSelected)
                //{
                //    SampleTestCheckbox.Click();
                //}
                // Assign the sample(s) to the test
                HelperMethods.Click(SampleTestCheckbox);
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                RadioButton[] SampleTestRadioButtons = Sample.GetMultiple<RadioButton>(WorklistProperty.Default.AIXSamplesGridTestCheckbox);
                RadioButton SampleTestRadioButton = SampleTestRadioButtons[testIndex - 1];

                HelperMethods.Click(SampleTestRadioButton);
                //// Note: if selecting multiple selection
                //// if the sample being clicked is already selected for the test but samples others are not, nothing will happen
            }
        }

        /// <summary>
        /// Storm only.
        /// Modifies the lot number for a test.
        /// </summary>
        /// <param name="lotNumber">The new lot number to enter.</param>
        /// <param name="testIndex">The one-based index of the test to modify (Default: first test).</param>
        public static void ModifyLotNumber(int lotNumber, int testIndex = 1)
        {
            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("WorklistMethods.ModifyLotNumber() cannot be run in non-Storm software. Be sure to use HomeMethods.ModifyLotNumber() on AIX.");
            }

            StatusManager.AddStatus("Modifying Lot Number");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            OpenTestPropertiesWindow(testIndex);
            HelperMethods.Click(WorklistProperty.Default.LotNumberTextbox);

            Keyboard keyboard = new Keyboard();
            keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
            HelperMethods.Wait();
            keyboard.Enter("a");
            HelperMethods.Wait();
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
            HelperMethods.Wait();
            keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
            HelperMethods.Wait();
            keyboard.Enter(lotNumber.ToString());
            HelperMethods.Wait();

            HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
        }

        /// <summary>
        /// Storm only.
        /// Modifies the expiration date for a test.
        /// </summary>
        /// <param name="date">The new expiration date. Must be in the format "MM/DD/YYYY".</param>
        /// <param name="testIndex">The one-based index of the test to modify (Default: first test).</param>
        public static void ModifyExpirationDate(string date, int testIndex = 1)
        {
            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("WorklistMethods.ModifyExpirationDate() cannot be run in non-Storm software. Be sure to use HomeMethods.ModifyExpirationDate() on AIX.");
            }

            StatusManager.AddStatus("Modifying Expiration Date");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            OpenTestPropertiesWindow(testIndex);
            HelperMethods.Click(WorklistProperty.Default.ExpirationDateTextbox);

            Keyboard keyboard = new Keyboard();
            keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
            HelperMethods.Wait();
            keyboard.Enter("a");
            HelperMethods.Wait();
            keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
            HelperMethods.Wait();
            keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
            HelperMethods.Wait();
            keyboard.Enter(date);
            HelperMethods.Wait();

            HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
        }

        internal static void ModifyAllLotNumbers(int lotNumber)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            int headerColumnCount = SamplesGrid.Header.Columns.Count;
            int nonTestColumnCount = GetShiftedSampleIndex(3);
            int testCount = headerColumnCount - nonTestColumnCount;

            for (int test = 1; test <= testCount; test++)
            {
                ModifyLotNumber(lotNumber, test);
            }
        }

        internal static void ModifyAllExpirationDates(string date)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            int headerColumnCount = SamplesGrid.Header.Columns.Count;
            int nonTestColumnCount = GetShiftedSampleIndex(3);
            int testCount = headerColumnCount - nonTestColumnCount;

            for (int test = 1; test <= testCount; test++)
            {
                ModifyExpirationDate(date, test);
            }
        }

        /// <summary>
        /// Storm only.
        /// Modifies a runtime variable for a test.
        /// </summary>
        /// <param name="variableName">The name of the variable.</param>
        /// <param name="value">The value to enter.</param>
        /// <param name="testIndex">The index of the test to use.</param>
        public static void ModifyRuntimeVariable(string variableName, double value, int testIndex = 1)
        {
            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("WorklistMethods.ModifyRuntimeVariable() cannot be run in non-Storm software.");
            }

            StatusManager.AddStatus("Modifying Runtime Variable");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            OpenTestPropertiesWindow(testIndex);

            ListView RuntimeVariablesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.RuntimeVariablesGrid);
            foreach (var row in RuntimeVariablesGrid.Rows)
            {
                if (row.Cells[0].Text == variableName)
                {
                    TextBox valueBox = row.Get<TextBox>();
                    HelperMethods.Click(valueBox);

                    Keyboard keyboard = new Keyboard();
                    keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                    HelperMethods.Wait();
                    keyboard.Enter("a");
                    HelperMethods.Wait();
                    keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                    HelperMethods.Wait();
                    keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                    HelperMethods.Wait();
                    keyboard.Enter(value.ToString());
                    HelperMethods.Wait();

                    HelperMethods.Click(CommonProperty.Default.OkButton_textValue);

                    return;
                }
            }

            HelperMethods.Click(CommonProperty.Default.CancelButton_automationID);

            throw new WorkflowException("Could not find runtime variable with name " + variableName);
        }

        /// <summary>
        /// Loads a worklist from the "C:\Test Automation\" folder
        /// Parameter passed in provides sub level folder location
        /// </summary>
        /// <param name="filename">The filename without the file extension</param>
        /// <param name="foldername">The folder name found with C:\Test Automation\</param>
        public static void LoadWorklist(string filename = null, string foldername = null)
        {
            StatusManager.AddStatus("Loading worklist...");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            HelperMethods.Click(WorklistProperty.Default.LoadWorklistButton);

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                GeneralMethods.OpenFile(filename + ".TBSTORMWORKLIST", foldername);
            }
            else
            {
                GeneralMethods.OpenFile(filename + ".AIX1000WORKLIST", foldername);

            }

            // Worklist Loaded popup, WAIT FOR POPUP
            
             Window popup = ApplicationManager.ApplicationWindow.ModalWindow(WindowName.Default.WorklistLoaded);
            HelperMethods.WaitForSetupAction();
             HelperMethods.Click(CommonProperty.Default.OkButton_textValue, popup);
            
            
        }

        /// <summary>
        /// Saves a worklist to the "C:\Test Automation\Temp" folder
        /// </summary>
        /// <param name="filename">The filename without the file extension, or null for default (worklist name)</param>
        public static void SaveWorklist(string filename = null)
        {
            StatusManager.AddStatus("Saving worklist...");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            HelperMethods.Click(WorklistProperty.Default.SaveWorklistButton);

            GeneralMethods.SaveFile(filename);
        }
    }
}
