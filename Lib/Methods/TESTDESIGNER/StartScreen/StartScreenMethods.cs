﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Status;
using System.Threading;
using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WPFUIItems;
using TestStack.White.WindowsAPI;
using GSD_RnDAutomation.Properties;
using GSD_RnDAutomation.Common;
using TestStack.White;

namespace GSD_RnDAutomation.Lib.Methods.TESTDESIGNER.StartScreen
{
    class StartScreenMethods
    {


        /// <summary>
        /// Create a new test for Tempest Test Designer.
        /// </summary>
        public static void CreateNewTest(string testName = null,
                                         string testAuthor = null,
                                         int revisions = 0,
                                         string stripFormat = null,
                                         string readerFormat = null)
        {
            StatusManager.AddStatus("Creating a new test");
            AttachedKeyboard keyboard = ApplicationManager.ApplicationWindow.Keyboard;

            if (testName != null)
            {
                // Click into the textbox
                HelperMethods.Click(StartScreenProperty.Default.TestNameTextBox);
                // CTRL + A, and DELETE to remove existing text
                keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.Enter("a");
                keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                // Enter the sample ID
                keyboard.Enter(testName);
            }
            if (testAuthor != null)
            {
                // Click into the textbox
                HelperMethods.Click(StartScreenProperty.Default.TestAuthorTextBox);
                // CTRL + A, and DELETE to remove existing text
                keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.Enter("a");
                keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                // Enter the sample ID
                keyboard.Enter(testAuthor);
            }
            if (revisions != 0)
            {
                // Click into the textbox
                HelperMethods.Click(StartScreenProperty.Default.RevisionTextBox);
                // CTRL + A, and DELETE to remove existing text
                keyboard.HoldKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.Enter("a");
                keyboard.LeaveKey(KeyboardInput.SpecialKeys.CONTROL);
                keyboard.PressSpecialKey(KeyboardInput.SpecialKeys.DELETE);
                // Enter the sample ID
                keyboard.Enter(revisions.ToString());
            }
            if (stripFormat != null)
            {
                // Open the dropdown
                ComboBox cmb = ApplicationManager.ApplicationWindow.Get<ComboBox>(StartScreenProperty.Default.StripFormatDropDown.ToSearchCriteria());
                HelperMethods.Click(cmb);
                HelperMethods.Wait();

                // Get the dropdown window
                // Window dropdown = ApplicationManager.ApplicationWindow.ModalWindow(StartScreenProperty.Default.StartScreenDropdownWindowName);               

                try
                {
                // Find and click on the instrument with the matching name
                HelperMethods.Click(cmb.Items[1]);
                }
                catch (AutomationException e)
                {
                // If we couldn't find the instrument, throw a WorkflowException
                throw new WorkflowException("Failed to find Strip Format \"" + stripFormat + "\" in dropdown", e);
                }
            }
            if (readerFormat != null)
            {
                // Open the dropdown
                HelperMethods.Click(StartScreenProperty.Default.ReaderFormatDropDown);

                // Get the dropdown window
                Window dropdown = ApplicationManager.ApplicationWindow.ModalWindow(StartScreenProperty.Default.StartScreenDropdownWindowName);

                try
                {
                    // Find and click on the instrument with the matching name
                    HelperMethods.Click(new ElementIdentifier(textValue: readerFormat, className: StartScreenProperty.Default.StartScreenDropdownWindowName));
                }
                catch (AutomationException e)
                {
                    // If we couldn't find the instrument, throw a WorkflowException
                    throw new WorkflowException("Failed to find Reader Format \"" + readerFormat + "\" in dropdown", e);
                }
            }
        }   
    }
}
