﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Status;

namespace GSD_RnDAutomation.Methods.TORRENT
{
    public class TempestAnalysisMethods
    {
        /// <summary>
        /// Navigate to Analysis Tab in Main view
        /// </summary>
        /// e.g.
        public static void NavigateToAnalysis()
        {
            StatusManager.AddStatus("Navigating to Analysis Tab View...");
            AnalysisTabBtn?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.TextToBePresentInElement(
                ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.HeaderTitleBy), "Analysis"));
        }

        /// <summary>
        /// Select test given by parameter passed in.
        /// </summary>
        /// <param name="testName">Name of Test to be selected from list of tests</param>
        /// e.g.
        public static void SelectTest(string testName)
        {
            StatusManager.AddStatus(string.Format("Selecting test...{0}", testName));

            SelectTestEntry?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(SelectionPopupFindBy.SelectionTitleBy));

            var selectedItem = TempestSelectionViewMethods.SelectItem(testName);
        }

        /// <summary>
        /// Select lot number given by parameter from available lot numbers.
        /// if the lot number exists, returns the Element
        /// </summary>
        /// <param name="lotNumber">Lot Number to be selected</param>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement SelectLotNumber(string lotNumber)
        {
            StatusManager.AddStatus("Selecting Lot Number...");
            var lotNumbers = LotNumbersList;

            foreach(var lot in lotNumbers)
            {
                if (lot.Text == lotNumber)
                {
                    lot.Click();
                    return lot;
                }
            }
            return null;
        }

        /// <summary>
        /// Clicks the radio button to show all data for the selected lots.
        /// </summary>
        /// e.g.
        public static void ShowAllData()
        {
            StatusManager.AddStatus("Show All Data for Selected Lot Number...");
            AllDataRadioBtn?.Click();
        }

        /// <summary>
        /// Clicks the radio button to show data from a specified date range for the given lots
        /// </summary>
        /// <param name="fromDate">From date to search from</param>
        /// <param name="toDate">To Date to search to</param>
        /// e.g.
        public static void ShowDataBetweenRange(string fromDate, string toDate)
        {
            StatusManager.AddStatus("Show data between a set range...");
            SelectedDateRangeRadioBtn?.Click();

            if (!string.IsNullOrEmpty(fromDate))
            {
                StatusManager.AddStatus("Enabling 'From Date'...");
                FromDateSwitch?.Click();

                // TODO set from date
            }

            if (!string.IsNullOrEmpty(toDate))
            {
                StatusManager.AddStatus("Enabling 'To Date'...");
                ToDateSwitch?.Click();
                // TODO  set to date
            }
        }

        /// <summary>
        /// Generate a report once conditions are met. i.e. the button is enabled
        /// </summary>
        /// e.g.
        public static void GenerateReport()
        {
            StatusManager.AddStatus("Generating Test Analysis Report...");
            GenerateReportBtn?.Click();
        }

        /// <summary>
        /// Gets the Analysis Tab Button on the Main View
        /// </summary>
        /// <returns>Appium Element associasted with Analysis Tab View button</returns>
        /// e.g.
        public static AppiumElement AnalysisTabBtn
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.AnalysisTabBy); }
        }

        /// <summary>
        /// Gets Levey-Jennings element on Analysis Tab View
        /// </summary>
        /// <returns>Appium Element Associated with Levey-Jennings button</returns>
        /// e.g.
        public static AppiumElement LeveyJenningsAnalysisBtn
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.LeveyJenningsAnalysisButtonBy); }
        }

        /// <summary>
        /// Gets the list of test from Analysis tab view
        /// </summary>
        /// <returns>Appium Element Associated with Test list</returns>
        /// e.g.
        public static AppiumElement SelectTestEntry
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.SelectTestTextFieldBy); }
        }

        /// <summary>
        /// Get List of Lot numbers
        /// </summary>
        /// <returns>Read Only Collection Appium Elements associated with Lot Numbers</returns>
        /// e.g.
        public static ReadOnlyCollection<AppiumElement> LotNumbersList
        {
            get { return ApplicationManager.Driver?.FindElements(TempestAnalysisFindBy.LotNumberListBy); }
        }

        /// <summary>
        /// Get 'Show all data for selected lots' radio button
        /// </summary>
        /// <returns>appium element associasted with 'Show all data for selected lots'</returns>
        /// e.g.
        public static AppiumElement AllDataRadioBtn
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.AllDataRadioButtonBy); }
        }

        /// <summary>
        /// Get 'Show data for selected lots between' radio button
        /// </summary>
        /// <returns>Appium element for radio button</returns>
        /// e.g.
        public static AppiumElement SelectedDateRangeRadioBtn
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.SelectedDateRangeRadioButtonBy); }
        }

        /// <summary>
        /// Get switch to toggle From Date
        /// </summary>
        /// <returns>Appium Element associated with From Date switch</returns>
        /// e.g.
        public static AppiumElement FromDateSwitch
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.ToggleFromDateSwitchBy); }
        }

        /// <summary>
        /// Get Date picker for From Date:
        /// </summary>
        /// <returns>Appium Element associated with Date Picker for From Date:</returns>
        /// e.g.
        public static AppiumElement FromDatePicker
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.FromDateDatePickerBy); }
        }

        /// <summary>
        /// Get toggle switch for 'To' date:
        /// </summary>
        /// <returns>Appium Element associated with toggle switch for To date</returns>
        /// e.g.
        public static AppiumElement ToDateSwitch
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.ToggleToDateSwitchBy); }
        }

        /// <summary>
        /// Get date picker for To Date
        /// </summary>
        /// <returns>Appium Element associated with To date date picker</returns>
        /// e.g.
        public static AppiumElement ToDatePicker
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.ToDateDatePickerBy); }
        }

        /// <summary>
        /// Get Button to Generate Report
        /// </summary>
        /// <returns>Appium Element associated with Generate Report Button</returns>
        /// e.g.
        public static AppiumElement GenerateReportBtn
        {
            get { return ApplicationManager.Driver?.FindElement(TempestAnalysisFindBy.GenerateReportBy); }
        }
    }
}
