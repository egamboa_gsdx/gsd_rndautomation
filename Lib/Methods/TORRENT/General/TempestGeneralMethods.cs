﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Status;

namespace GSD_RnDAutomation.Methods.TORRENT
{
    public class TempestGeneralMethods
    {
        /// <summary>
        /// Get the current username from Main View Title bar
        /// Opens Current User Popup View
        /// </summary>
        /// e.g.
        public static void ViewUser()
        {
            StatusManager.AddStatus("Opening User Info...");
            UserTitle?.Click();
        }

        /// <summary>
        /// Gets the current date and time on main title bar
        /// </summary>
        /// <returns>String for current date and time appium element</returns>
        /// e.g.
        public static string CurrentDateAndTime()
        {
            StatusManager.AddStatus("Getting current date & time...");

            return CurrentDateTime?.Text;
        }

        /// <summary>
        /// Open and close kebab menu that contains about, help, and shutdown
        /// </summary>
        /// e.g.
        public static void OpenCloseKebabMenu()
        {
            StatusManager.AddStatus("Interacting with Kebab Menu...");
            KebabMenu?.Click();
        }
        
        /// <summary>
        /// Gets the Name of current View on Main Title Bar
        /// </summary>
        /// e.g.
        public static string MainHeaderTitle
        {
            get { return ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.HeaderTitleBy).Text; }
        }

        /// <summary>
        /// Get the Current logged in User label in the Main Title bar
        /// </summary>
        /// e.g.
        public static AppiumElement UserTitle
        {
            get { return ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.UserName); }
        }

        /// <summary>
        /// Gets current Date & Time Label in Main title
        /// </summary>
        /// e.g.
        public static AppiumElement CurrentDateTime
        {
            get { return ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.CurrentDateTimeBy); }
        }

        /// <summary>
        /// Get Kebab Menu Vertical Ellipsis
        /// </summary>
        /// e.g.
        public static AppiumElement KebabMenu
        {
            get { return ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.KebabMenuBy); }
        }
    }
}
