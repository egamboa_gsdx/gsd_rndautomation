﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT
{
    public class TempestHomeMethods
    {
        
        /// <summary>
        /// Naviaget to Home Tab View
        /// </summary>
        /// e.g.
        public static void NavigateToHome()
        {
            StatusManager.AddStatus("Navigating to Home Tab...");
            HomeTabButton?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.TextToBePresentInElement(
                ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.HeaderTitleBy), "Home"));
        }

        /// <summary>
        /// Open Status Log
        /// </summary>
        /// e.g.
        public static void OpenStatusLog()
        {
            StatusManager.AddStatus("Opening Status log...");
            AppiumElement openStatusLog = ApplicationManager.Driver?.FindElement(TempestHomeFindBy.StatusOpenBtnBy);
            openStatusLog?.Click();
        }

        /// <summary>
        /// Close Status log
        /// </summary>
        /// e.g.
        public static void CloseStatusLog()
        {
            StatusManager.AddStatus("Closing Status log...");
            AppiumElement closeStatusLog = ApplicationManager.Driver?.FindElement(TempestHomeFindBy.StatusCloseBtnBy);
            closeStatusLog?.Click();
        }

        /// <summary>
        /// Get Home Tab button on Main View Side bar -e.g.
        /// </summary>
        /// e.g.
        public static AppiumElement HomeTabButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestHomeFindBy.HomeTabBtnBy); }
        }

        /// <summary>
        /// Get Label for Available Sample Positions
        /// </summary>
        /// e.g.
        public static string AvailableSamplePosition
        {
            get { return ApplicationManager.Driver?.FindElement(TempestHomeFindBy.AvailableSamplePositionsBy).Text; }
        }

        /// <summary>
        /// Get Label for Available Strips Positions
        /// </summary>
        /// e.g.
        public static string AvailableStripPosition
        {
            get { return ApplicationManager.Driver?.FindElement(TempestHomeFindBy.AvailableStripPositionsBy).Text; }
        }
    }
}
