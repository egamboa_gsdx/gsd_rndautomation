﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Properties;

namespace GSD_RnDAutomation.Methods.TORRENT
{
    public class TempestLoadingMethods
    {
        /// <summary>
        /// Navigate to Loading Tab in the Main View
        /// </summary>
        /// e.g.
        public static void NavigateToLoading()
        {
            StatusManager.AddStatus("Navigating to Loading Tab...");
            AppiumElement loadingTabBtn = ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.LoadingTabBtnBy);
            loadingTabBtn?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.TextToBePresentInElement(
                ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.HeaderTitleBy), "Loading"));
        }

        /// <summary>
        /// Add samples to specified sample position
        /// </summary>
        /// <param name="samplePos">Index of sample position</param>
        /// <exception cref="WorkflowException">Appium Element is not found</exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// e.g.
        public static void AddSample(string samplename, int samplePos)
        {
            NavigateToLoading();
            StatusManager.AddStatus("Adding sample...");
            try
            {
                switch (samplePos)
                {
                    case 1:
                        AddSamplePositionOne?.Click();
                        break;
                    case 2:
                        AddSamplePositionTwo?.Click();
                        break;
                    case 3:
                        AddSamplePositionThree?.Click();
                        break;
                    case 4:
                        AddSamplePositionFour?.Click();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(String.Format("Invalid Sample Position.(Sample Position {0})", samplePos));
                }

                ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(SelectionPopupFindBy.SelectionTitleBy));
                var selectionTitle = TempestSelectionViewMethods.SelectionViewTitle;
                if (selectionTitle == null || !selectionTitle.Text.Equals("Add Sample"))
                {
                    throw new WorkflowException("Add Sample Selection Popup did not show.");
                }

                var itemSelected = TempestSelectionViewMethods.SelectItem("Manually Enter Sample ID");

                if (itemSelected == null)
                {
                    throw new WorkflowException("Item could not be selected");
                }

                ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(AddSampleManuallyPopupFindBy.AddSampleManuallyTitleBy));

                var manuallyAddSamplesEntry = TempestAddSampleMethods.ManualEntryAddSampleEntry;
                var manuallyAddSampleBtn = TempestAddSampleMethods.ManualEntryAddSampleOkButton;

                if (manuallyAddSamplesEntry == null || manuallyAddSampleBtn == null)
                {
                    throw new WorkflowException("Manual Entry Add Sample popup did not show.");
                }

                manuallyAddSamplesEntry?.SendKeys(samplename);

                if (!manuallyAddSampleBtn.Enabled)
                {
                    throw new WorkflowException("Cannot add Sample manually. 'Ok' Button not enabled.");
                }
                manuallyAddSampleBtn?.Click();

                ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(PlaceSampleTubePopupFindBy.PlaceSampleTubeTitleBy));

                HelperMethods.Refocus(ApplicationManager.ApplicationWindow);
                var fakeInstTray = ApplicationManager.ApplicationWindow.Get(FakeInstrumentProperty.Default.SampleOneTrayOpenClosed);

                if (!fakeInstTray.Name.Equals("Tray is open"))
                {
                    throw new WorkflowException("Tray did not open when loading sample.");
                }

                HelperMethods.Click(FakeInstrumentProperty.Default.AddSampleOneButton);

                ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(PlaceSampleTubePopupFindBy.PlaceSampleTubeTitleBy));

                TempestAddSampleMethods.PlaceSampleLoadSampleTubeButton?.Click();

                ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(ManuallySpecificTestPopupFindBy.ManuallySpecificTestTitleBy));

                var cancelSpecifyTest = ApplicationManager.Driver?.FindElement(ManuallySpecificTestPopupFindBy.CancelAddTestBtnBy);
                if (cancelSpecifyTest == null)
                {
                    throw new WorkflowException("Could not find element to cancel adding manually specify test");
                }

                cancelSpecifyTest.Click();
            }
            catch
            {
                throw new WorkflowException(string.Format("Could not perform adding sample to Sample Position {0}.", samplePos));
            }
            
        }

        public static void RemoveSample(int index)
        {
            StatusManager.AddStatus("Removing sample...");
            OpenCloseSampleTray(index);
            HelperMethods.Refocus(ApplicationManager.ApplicationWindow);
            var fakeInstTray = ApplicationManager.ApplicationWindow.Get(FakeInstrumentProperty.Default.SampleOneTrayOpenClosed);

            if (!fakeInstTray.Name.Equals("Tray is open"))
            {
                OpenCloseSampleTray(index);
            }

            switch (index)
            {
                case 1:
                    HelperMethods.Click(FakeInstrumentProperty.Default.AddSampleOneButton);
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            OpenCloseSampleTray(index);
        }

        /// <summary>
        /// Opens or closes tray of specified sample position
        /// </summary>
        /// <param name="samplePos">index of sample position</param>
        /// <exception cref="WorkflowException">Element cannot be found</exception>
        /// e.g.
        public static void OpenCloseSampleTray(int samplePos)
        {
            StatusManager.AddStatus("Opening or Closing Sample Tray...Depends on who you ask");
            try
            {
                AppiumElement openCloseBtn1 = ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionOneOpenCloseTrayBtnBy);
                AppiumElement openCloseBtn2 = ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionTwoOpenCloseTrayBtnBy);
                AppiumElement openCloseBtn3 = ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionThreeOpenCloseTrayBtnBy);
                AppiumElement openCloseBtn4 = ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionFourOpenCloseTrayBtnBy);

                switch (samplePos)
                {
                    case 0:
                        openCloseBtn1?.Click();
                        openCloseBtn2?.Click();
                        openCloseBtn3?.Click();
                        openCloseBtn4?.Click();
                        break;
                    case 1:
                        openCloseBtn1?.Click();
                        break;
                    case 2:
                        openCloseBtn2?.Click();
                        break;
                    case 3:
                        openCloseBtn3?.Click();
                        break;
                    case 4:
                        openCloseBtn4?.Click();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("Invalid Sample Position.");
                }
            }
            catch
            {
                throw new WorkflowException("Could not find Button to open or close sample tray.");
            }
        }

        /// <summary>
        /// Open and close strips tray
        /// </summary>
        /// <param name="index">index of strips tray position</param>
        /// <exception cref="WorkflowException">Appium Element not found</exception>
        /// e.g.
        public static void OpenCloseStripsTrays(int index)
        {
            StatusManager.AddStatus("Opening or closing Strips Tray...");
            try
            {
                AppiumElement stripsTrayOpenClose = ApplicationManager.Driver?.FindElement(By.XPath(string.Format(
                            TempestLoadingFindBy.StripPositionOpenCloseTrayString, index)));
                stripsTrayOpenClose?.Click();
            }
            catch
            {
                throw new WorkflowException("Unable to get Strips tray position.");
            }
        }

        public static AppiumElement LoadingTabBtn
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.LoadingTabBtnBy); }
        }

        public static AppiumElement AvailableSamplePosition
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.AvailableSamplePosition); }
        }

        public static AppiumElement AddSamplePositionOne
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionOneAddSampleBtnBy); }
        }

        public static AppiumElement AddSamplePositionTwo
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionTwoAddSampleBtnBy); }
        }

        public static AppiumElement AddSamplePositionThree
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionThreeAddSampleBtnBy); }
        }

        public static AppiumElement AddSamplePositionFour
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionFourAddSampleBtnBy); }
        }

        public static AppiumElement SampleOneName
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionOneSampleNameBy); }
        }

        public static AppiumElement SampleTwoName
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionTwoSampleNameBy); }
        }

        public static AppiumElement SampleThreeName
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionThreeSampleNameBy); }
        }

        public static AppiumElement SampleFourName
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLoadingFindBy.SamplePositionFourSampleNameBy); }
        }
    }
}
