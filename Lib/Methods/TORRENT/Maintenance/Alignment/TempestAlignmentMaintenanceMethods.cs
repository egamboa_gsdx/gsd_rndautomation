﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using GSD_RnDAutomation.TORRENT.Elements.AlignmentMaintenance;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT.Maintenance
{
    public class TempestAlignmentMaintenanceMethods
    {
        /// <summary>
        /// Get Align Instrument Frame on Alignment Maintenance View
        /// </summary>
        /// <returns></returns>
        public static AppiumElement GetAlignInstrumentFrame()
        {
            return ApplicationManager.Driver?.FindElement(TempestAlignmentMaintenanceFindBy.AlignInstrumentFrameBy);
        }

        /// <summary>
        /// Get Align Reader Frame on Alignment Maintenance View
        /// </summary>
        /// <returns></returns>
        public static AppiumElement GetAlignReaderFrame()
        {
            return ApplicationManager.Driver?.FindElement(TempestAlignmentMaintenanceFindBy.AlignReaderFrameBy);
        }
    }
}
