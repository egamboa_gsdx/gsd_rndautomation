﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using GSD_RnDAutomation.TORRENT.Elements.CalibrationMaintenance;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT.Maintenance
{
    public class TempestCalibrationMaintenanceMethods
    {
        /// <summary>
        /// Get Calibrate Absorbance Reader Frame in Calibrate Maintenance View
        /// </summary>
        /// <returns></returns>
        public static AppiumElement GetCalibrateAbsorbaneReaderFrame()
        {
            return ApplicationManager.Driver?.FindElement(TempestCalibrationMaintenanceFindBy.CalibrateAbsorbanceReaderFrameBy);
        }

        /// <summary>
        /// Get Cailbreate Luminescense Reader Frame in Calibrate Maintenance View
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetCalibrateLuminescenseReaderFrame()
        {
            return ApplicationManager.Driver?.FindElement(TempestCalibrationMaintenanceFindBy.CalibrateLuminescenseReaderFrameBy);
        }
    }
}
