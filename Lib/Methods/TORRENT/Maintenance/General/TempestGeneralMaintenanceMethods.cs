﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using GSD_RnDAutomation.TORRENT.Elements.GeneralMaintenance;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT.Maintenance
{
    public class TempestGeneralMaintenanceMethods
    {
        /// <summary>
        /// Open Prime Wash Manifold popup
        /// </summary>
        public static void OpenPrimeWashManifoldPopup()
        {
            StatusManager.AddStatus("Performing Prime Wash Manifold procedure...");
            AppiumElement primeWashFrame = GetPrimeWashManifoldFrame();
            primeWashFrame?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(PrimeWashManifoldPopupFindBy.PrimeWashManifoldTitleBy));
        }

        /// <summary>
        /// Open Shake Strips popup view
        /// </summary>
        /// e.g.
        public static void OpenShakeStripsPopup()
        {
            StatusManager.AddStatus("Performing Shake Strips procedure...");
            AppiumElement shakeStripsFrame = GetShakeStripsFrame();
            shakeStripsFrame?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(ShakeStripsPopupFindBy.ShakeStripsTitleBy));
        }

        /// <summary>
        /// Closes Shake Strips Popup view
        /// </summary>
        /// e.g.
        public static void CloseShakeStripsPopup()
        {
            StatusManager.AddStatus("Closing Shake Strips popup...");
            AppiumElement closeShakeStrips = GetShakeStripsPopupCloseButton();
            closeShakeStrips?.Click();
        }

        // Prime Wash Manifolds Maintenance Elements e.g.
        #region
        /// <summary>
        /// Get Prime Wash Manifold Frame in General Maintenance View
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetPrimeWashManifoldFrame()
        {
            return ApplicationManager.Driver?.FindElement(TempestGeneralMaintenanceFindBy.PrimeWashManifoldFrameBy);
        }

        
        #endregion

        // Shake Strips Maintenance Elements e.g.
        #region
        /// <summary>
        /// Shake Strips Frame in General Maintenance View
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetShakeStripsFrame()
        {
            return ApplicationManager.Driver?.FindElement(TempestGeneralMaintenanceFindBy.ShakeStripsFrameBy);
        }

        /// <summary>
        /// Get Shake strips Popup Title
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public  static AppiumElement GetShakesStripPopupTitle()
        {
            return ApplicationManager.Driver?.FindElement(ShakeStripsPopupFindBy.ShakeStripsTitleBy);
        }

        /// <summary>
        /// Get Shake Strips turn off all button in popup view
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetShakesStripPopupTurnOffAllShakersButton()
        {
            return ApplicationManager.Driver?.FindElement(ShakeStripsPopupFindBy.TurnOffAllShakersButtonBy);
        }

        /// <summary>
        /// Get Close button for Shake Strips popup view
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetShakeStripsPopupCloseButton()
        {
            return ApplicationManager.Driver?.FindElement(ShakeStripsPopupFindBy.ShakeStripCloseButtonBy);
        }

        /// <summary>
        /// Gets Shakes Strip toggle switch for specified index
        /// </summary>
        /// <param name="index">shake strips slot #</param>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetShakeStripsToggleSwitch(int index)
        {
            return ApplicationManager.Driver?.FindElement(By.XPath(string.Format(ShakeStripsPopupFindBy.ShakeStripSwitchString, index)));
        }

        /// <summary>
        /// Get Slider for the specified shake strips index
        /// </summary>
        /// <param name="index">strips slot number</param>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetShakeStripsRPMSlider(int index)
        {
            return ApplicationManager.Driver?.FindElement(By.XPath(string.Format(ShakeStripsPopupFindBy.ShakeStripRPMSliderString, index)));
        }
        #endregion
    }
}
