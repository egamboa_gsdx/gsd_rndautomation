﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.GeneralMaintenance;
using GSD_RnDAutomation.TORRENT.Elements.AlignmentMaintenance;
using GSD_RnDAutomation.TORRENT.Elements.CalibrationMaintenance;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT
{
    public class TempestMaintenanceMethods
    {
        /// <summary>
        /// Navigate to Maintenance Tab View
        /// </summary>
        /// e.g.
        public static void NavigateToMaintenance()
        {
            StatusManager.AddStatus("Navigating to Maintenance Tab...");
            AppiumElement maintenanceTab = GetMaintenanceTabButton();

            maintenanceTab?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.TextToBePresentInElement(
                ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.HeaderTitleBy), "Maintenance"));
        }

        /// <summary>
        /// Navigate to General Maintenance View
        /// </summary>
        /// e.g.
        public static void GeneralMaintenance()
        {
            StatusManager.AddStatus("Navigating to General Maintenance...");
            AppiumElement generalMaintenance = GetGeneralMaintenance();
            generalMaintenance?.Click();
        }

        /// <summary>
        ///  Navigate to Alignment Maintenance View
        /// </summary>
        /// e.g.
        public static void AlignmentMaintenance()
        {
            StatusManager.AddStatus("Navigating to Alignment Maintenance...");
            AppiumElement alignmentMaintenance = GetAlignmentMaintenance();
            alignmentMaintenance?.Click();
        }

        /// <summary>
        /// Navigate to Calibration Maintenance
        /// </summary>
        /// e.g.
        public static void CalibrationMaintenance()
        {
            StatusManager.AddStatus("Navigating to Calibration Maintenance...");
            AppiumElement calibrationMaintenance = GetCalibrationMaintenance();
            calibrationMaintenance?.Click();
        }

        /// <summary>
        /// Gets Maintenance Tab button on Main View Side bar
        /// </summary>
        /// <returns>Button to view Maintenance Tabe</returns>
        /// e.g.
        public static AppiumElement GetMaintenanceTabButton()
        {
            return ApplicationManager.Driver?.FindElement(TempestMaintenanceFindBy.MaintenanceTabBy);
        }

        /// <summary>
        /// Get General Maintenance Label to view General Maintenance options
        /// </summary>
        /// <returns>General Maintenance Label</returns>
        /// e.g.
        public static AppiumElement GetGeneralMaintenance()
        {
            return ApplicationManager.Driver?.FindElement(TempestGeneralMaintenanceFindBy.GeneralMaintenanceBy);
        }

        /// <summary>
        /// Get Alignment Maintenance Label to view Alignment Maintenance options
        /// </summary>
        /// <returns>Alignment Maintenance label</returns>
        /// e.g.
        public static AppiumElement GetAlignmentMaintenance()
        {
            return ApplicationManager.Driver?.FindElement(TempestAlignmentMaintenanceFindBy.AlignmentMaintenanceBy);
        }

        /// <summary>
        /// Get Calibration Maintenance label to view Calibration Maintenance options
        /// </summary>
        /// <returns>Calibration Maintenance Label</returns>
        public static AppiumElement GetCalibrationMaintenance()
        {
            return ApplicationManager.Driver?.FindElement(TempestCalibrationMaintenanceFindBy.CalibrationMaintenanceBy);
        }
    }    
}
