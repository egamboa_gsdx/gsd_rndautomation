﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT.Popups
{
    public class TempestAddSampleMethods
    {
        // Manually Entry Add Samples
        #region
        public static void CancelManualAddSample()
        {
            StatusManager.AddStatus("Cancelling Manually Entry Add Sample...");
            ManualEntryAddSampleCancelButton?.Click();
        }

        public static void OkManualAddSample()
        {
            StatusManager.AddStatus("Adding sample manually...");
            ManualEntryAddSampleOkButton?.Click();
        }
        
        public static AppiumElement ManualEntryAddSampleTitle
        {
            get { return ApplicationManager.Driver?.FindElement(AddSampleManuallyPopupFindBy.AddSampleManuallyTitleBy); }
        }

        public static AppiumElement ManualEntryAddSampleEntry
        {
            get { return ApplicationManager.Driver?.FindElement(AddSampleManuallyPopupFindBy.ManuallyAddSampleEntryBy); }
        }

        public static AppiumElement ManualEntryAddSampleOkButton
        {
            get { return ApplicationManager.Driver?.FindElement(AddSampleManuallyPopupFindBy.ManuallyAddSampleOkBtnBy); }
        }

        public static AppiumElement ManualEntryAddSampleCancelButton
        {
            get { return ApplicationManager.Driver?.FindElement(AddSampleManuallyPopupFindBy.ManuallyAddSampleCancelBtnBy); }
        }
        #endregion

        // Place Sample Tube
        #region
        public static void CancelLoadSample()
        {
            StatusManager.AddStatus("Cancellilng Load Sample...");
            PlaceSampleCancelButton?.Click();
        }

        public static void LoadSample()
        {
            StatusManager.AddStatus("Loading sample...");
            PlaceSampleLoadSampleTubeButton?.Click();
        }
        public static AppiumElement PlaceSampleTitle
        {
            get { return ApplicationManager.Driver?.FindElement(PlaceSampleTubePopupFindBy.PlaceSampleTubeTitleBy); }
        }

        public static AppiumElement PlaceSampleMarkSampleStatSwitch
        {
            get { return ApplicationManager.Driver?.FindElement(PlaceSampleTubePopupFindBy.MarkSampleStatSwitchBy); }
        }

        public static AppiumElement PlaceSampleLoadSampleTubeButton
        {
            get { return ApplicationManager.Driver?.FindElement(PlaceSampleTubePopupFindBy.LoadSampleTubeBtnBy); }
        }

        public static AppiumElement PlaceSampleCancelButton
        {
            get { return ApplicationManager.Driver?.FindElement(PlaceSampleTubePopupFindBy.LoadSampleCancelTubeBtnBy); }
        }
        #endregion
    }
}
