﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT.Popups
{
    public class TempestConfirmationMethods
    {
        public static void ConfirmCancel()
        {
            //ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(ConfirmationPopupFindBy.ConfirmationPopupTitleBy));
            var title = ApplicationManager.Driver?.FindElement(ConfirmationPopupFindBy.ConfirmationPopupTitleBy);
            var cancelBtn = ApplicationManager.Driver?.FindElement(ConfirmationPopupFindBy.ConfirmationCancelBtnBy);
            if (title != null) // Title will change in future
            {
                cancelBtn?.Click();
            }
        }

        public static void ConfirmOk()
        {
            //ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(ConfirmationPopupFindBy.ConfirmationPopupTitleBy));
            var title = ApplicationManager.Driver?.FindElement(ConfirmationPopupFindBy.ConfirmationPopupTitleBy);
            var okBtn = ApplicationManager.Driver?.FindElement(ConfirmationPopupFindBy.ConfirmationOkBtnBy);
            if (title != null) // Title will change in future
            {
                okBtn?.Click();
            }
        }
    }
}
