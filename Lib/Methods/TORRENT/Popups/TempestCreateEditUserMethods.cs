﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT.Popups
{
    public class TempestCreateEditUserMethods
    {
        public static void CreateEditUser(string username, string password, string reenterpassword,
            string email, string phonenum, string userrole = "Basic user", bool isActive = true, int option = 0)
        {
            switch (option)
            {
                case 0:
                    StatusManager.AddStatus("Creating new user...");
                    if (CreateEditUserTitle == null || CreateEditUserTitle.Text != "Add User")
                    {
                        throw new WorkflowException("Add User popup did not show.");
                    }
                    
                    UserNameTextEntry?.SendKeys(username);
                    PasswordTextEntry?.SendKeys(password);
                    ReEnterPasswordTextEntry?.SendKeys(reenterpassword);
                    EmailTextEntry?.SendKeys(email);
                    PhoneNumberTextEntry?.SendKeys(phonenum);

                    // Select User Role
                    UserLevelPicker?.Click();
                    By alertAddTitleBy = By.Id("android:id/alertTitle");
                    By alertAddListView = By.Id("android:id/select_dialog_listview");
                    
                    ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(alertAddTitleBy));
                    var alertTitle = ApplicationManager.Driver?.FindElement(alertAddTitleBy);
                    var itemList = ApplicationManager.Driver?.FindElement(alertAddListView).FindElements(By.XPath("//.././android.widget.TextView"));

                    if (itemList == null)
                    {
                        throw new WorkflowException("Could not find element.");
                    }

                    var addItem = itemList.FirstOrDefault(u => u.Text == userrole);

                    if (addItem == null)
                    {
                        throw new WorkflowException("User Role does not exist.");
                    }

                    addItem.Click();

                    OkButton?.Click();

                    bool? isDisplayed = null;
                    try
                    {
                        ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(DialogPopupFindBy.DialogPopupTitleBy));
                        isDisplayed = ApplicationManager.Driver?.FindElement(DialogPopupFindBy.DialogPopupTitleBy).Displayed;
                    }
                    catch
                    {
                        isDisplayed = false;
                    }
                    finally
                    {
                        if (isDisplayed == true)
                        {
                            ApplicationManager.Driver?.FindElement(DialogPopupFindBy.DialogOkBtnBy).Click();
                            CancelButton?.Click();
                            throw new Exception("Another user exists with the given username. *Note* will change this once more reliable tests are written.");
                        }
                    }
                    break;
                case 1:
                    StatusManager.AddStatus("Editing selected user...");
                    if (CreateEditUserTitle == null || CreateEditUserTitle.Text != "Edit User")
                    {
                        throw new WorkflowException("Edit User popup did not show.");
                    }

                    if (UserNameTextEntry != null && !UserNameTextEntry.Text.Equals(username))
                    {
                        UserNameTextEntry.SendKeys(username);
                    }

                    if (EmailTextEntry != null && !EmailTextEntry.Text.Equals(email))
                    {
                        EmailTextEntry.SendKeys(email);
                    }

                    if (PhoneNumberTextEntry != null && !EmailTextEntry.Text.Equals(phonenum))
                    {
                        PhoneNumberTextEntry.SendKeys(phonenum);
                    }

                    if(UserLevelPicker != null && UserLevelPicker.Text != userrole)
                    {
                        UserLevelPicker?.Click();

                        By alertEditTitleBy = By.Id("android:id/alertTitle");
                        By alertEditListView = By.Id("android:id/select_dialog_listview");

                        ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(alertEditTitleBy));
                        var alertEditTitle = ApplicationManager.Driver?.FindElement(alertEditTitleBy);
                        var itemEditList = ApplicationManager.Driver?.FindElement(alertEditListView).FindElements(By.XPath("//.././android.widget.TextView"));

                        if (itemEditList == null)
                        {
                            throw new WorkflowException("Could not find element.");
                        }

                        var editItem = itemEditList.FirstOrDefault(u => u.Text == userrole);

                        if (editItem == null)
                        {
                            throw new WorkflowException("User Role does not exist.");
                        }

                        editItem.Click();
                    }

                    if (!isActive)
                    {
                        if (ActiveUserSwitch == null || ActiveUserSwitch.Text == "OFF")
                        {
                            throw new WorkflowException("User has already been deactivated or could not be deactivated...someone messed up!");
                        }

                        ActiveUserSwitch?.Click();
                    }
                    OkButton?.Click();
                    break;
                default:
                    throw new ArgumentException();
            }
        }
        public static AppiumElement CreateEditUserTitle
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.CreateEditUserTitleBy); }
        }

        public static AppiumElement UserNameTextEntry
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.UsernameEntryBy); }
        }

        public static AppiumElement PasswordTextEntry
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.PasswordEntryBy); }
        }

        public static AppiumElement ReEnterPasswordTextEntry
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.ReEnteredPasswordEntryBy); }
        }

        public static AppiumElement EmailTextEntry
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.EmailAddressEntryBy); }
        }

        public static AppiumElement PhoneNumberTextEntry
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.PhoneNumberEntryBy); }
        }

        public static AppiumElement UserLevelPicker
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.UserLevelPickerBy); }
        }

        public static AppiumElement ActiveUserSwitch
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.ActiveUserSwitchBy); }
        }

        public static AppiumElement OkButton
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.OkUserButtonBy); }
        }

        public static AppiumElement CancelButton
        {
            get { return ApplicationManager.Driver?.FindElement(CreateEditUserPopupFindBy.CancelUserButtonBy); }
        }
    }
}
