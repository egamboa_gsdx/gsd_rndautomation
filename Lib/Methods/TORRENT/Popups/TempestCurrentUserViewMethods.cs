﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Status;

namespace GSD_RnDAutomation.Methods.TORRENT.Popups
{
    public class TempestCurrentUserViewMethods
    {
        /// <summary>
        /// When the Current User popup view is open, logs out the current user
        /// </summary>
        /// e.g.
        public static void LogoutCurrentUser()
        {
            StatusManager.AddStatus("Logging out Current User...");

            AppiumElement logoutBtn = ApplicationManager.Driver?.FindElement(CurrentUserPopupFindBy.CurrenterUserLogoutBtnBy);
            logoutBtn?.Click();
        }

        /// <summary>
        /// Closes Current User Popup view
        /// </summary>
        /// e.g.
        public static void CloseCurrentUserPopup()
        {
            StatusManager.AddStatus("Closing Current User Popup...");
            AppiumElement userCloseBtn = ApplicationManager.Driver?.FindElement(CurrentUserPopupFindBy.CurrenterUserCloseBtnBy);
            userCloseBtn?.Click();
        }

        /// <summary>
        /// Gets Current Username in Current user popup view
        /// </summary>
        /// <returns>String from Appium Element label for current user</returns>
        /// e.g.
        public static string GetCurrenterUserNamePopup()
        {
            return ApplicationManager.Driver?.FindElement(CurrentUserPopupFindBy.CurrentUserNameBy).Text;
        }

        /// <summary>
        /// Gets Current User Role level in Current User popup view
        /// </summary>
        /// <returns>String from Appium Element label for Current User role</returns>
        /// e.g.
        public static string GetCurrentUserRolePopup()
        {
            return ApplicationManager.Driver?.FindElement(CurrentUserPopupFindBy.CurrentUserRoleBy).Text;
        }

        /// <summary>
        /// Gets Current User Email in Current User popup view
        /// </summary>
        /// <returns>String from Appium Element label for Current User Email</returns>
        public static string GetCurrentUserEmailPopup()
        {
            return ApplicationManager.Driver?.FindElement(CurrentUserPopupFindBy.CurrentUserEmailBy).Text;
        }

        /// <summary>
        /// Get Current User Phone Number in Current user Popup view
        /// </summary>
        /// <returns>string from Appium Element label for Current User Phone Number</returns>
        /// e.g.
        public static string GetCurrentUserPhoneNumPopup()
        {
            return ApplicationManager.Driver?.FindElement(CurrentUserPopupFindBy.CurrentUserPhoneNumberBy).Text;
        }
    }
}
