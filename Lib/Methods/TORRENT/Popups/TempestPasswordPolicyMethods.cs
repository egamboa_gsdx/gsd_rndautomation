﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT.Popups
{
    public class TempestPasswordPolicyMethods
    {
        public static AppiumElement PasswordPolicyTitle
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.PasswordPolicyTitleBy); }
        }

        public static AppiumElement PasswordMinLength
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.PasswordMinLengthEntryBy); }
        }

        public static AppiumElement AllowableSpecialChar
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.AllowableCharEntryBy); }
        }

        public static AppiumElement RequireSpecialChar
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.RequireSpecialCharSwitchBy); }
        }

        public static AppiumElement AllowSpaces
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.AllowSpacesSwitchBy); }
        }

        public static AppiumElement MaxLoginAttempt
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.MaxLoginAttemptEntryBy); }
        }

        public static AppiumElement LockoutDuration
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.LockoutDurationEntryBy); }
        }

        public static AppiumElement ResetPasswordPeriodically
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.ResetPasswordPeriodicallySwitchBy); }
        }

        public static AppiumElement PasswordExpiration
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.PasswordExpirationEntryBy); }
        }

        public static AppiumElement OkButton
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.PasswordPolicyOkButtonBy); }
        }

        public static AppiumElement CancelButton
        {
            get { return ApplicationManager.Driver?.FindElement(PasswordPolicyPopupFindBy.PasswordPolicyCancelButtonBy); }
        }
    }
}
