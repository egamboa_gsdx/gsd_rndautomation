﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using GSD_RnDAutomation.TORRENT.Elements.GeneralMaintenance;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT.Maintenance;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT.Popups
{
    public class TempestPrimeWashManifoldMethods
    {
        public static void IsPrimeWashManifoldPopup()
        {
            try
            {
                AppiumElement popupTitle = GetPrimeWashManifoldPopupTitle();
                if (popupTitle == null)
                {
                    throw new WorkflowException("Prime Wash Manifold Popup did not show. Could not continue.");
                }
            }
            catch
            {
                TempestGeneralMaintenanceMethods.OpenPrimeWashManifoldPopup();
            }
            

        }

        /// <summary>
        /// Increase Priming Cycle
        /// </summary>
        /// <param name="increase">Amount to increase priming cycles</param>
        /// e.g.
        public static void IncreasePrimingCycles(int increase = 1)
        {
            StatusManager.AddStatus("Increasing Priming Cycles...");
            Thread.Sleep(1000);
            IsPrimeWashManifoldPopup();

            AppiumElement increasePrime = GetPrimingCyclesIncrementStepper();
            for (int i = 0; i < increase; i++)
            {
                increasePrime?.Click();
            }
        }

        /// <summary>
        /// Decrease Priming Cycle
        /// </summary>
        /// <param name="decrease">Amount to decrease Priming Cycles</param>
        /// e.g.
        public static void DecreasePrimingCycles(int decrease = 1)
        {
            StatusManager.AddStatus("Decreasing Priming Cycles...");
            Thread.Sleep(1000);
            IsPrimeWashManifoldPopup();

            AppiumElement decreasePrime = GetPrimingCyclesDecrementStepper();
            for (int i = 0; i < decrease; i++)
            {
                decreasePrime?.Click();
            }

        }

        /// <summary>
        /// Check or uncheck wash for prime wash
        /// </summary>
        /// <param name="index">index of wash bottle</param>
        /// e.g.
        public static void CheckUncheckWash(int index = 1)
        {
            StatusManager.AddStatus("Checking/Unchecking Wash for Prime Wash...");
            Thread.Sleep(1000);
            IsPrimeWashManifoldPopup();

            AppiumElement wash = GetPrimeWashBottleCheckbox(index);
            wash?.Click();
        }

        /// <summary>
        /// Close Prime Wash Manifold Popup
        /// </summary>
        /// e.g.
        public static void ClosePrimeWashManifoldPopup()
        {
            StatusManager.AddStatus("Closing Prime Wash Manifold popup...");
            AppiumElement primeWashClose = GetPrimeWashManifoldPopupCloseButton();
            primeWashClose?.Click();
        }

        /// <summary>
        /// Get Prime Wash Popup Title
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetPrimeWashManifoldPopupTitle()
        {
            return ApplicationManager.Driver?.FindElement(PrimeWashManifoldPopupFindBy.PrimeWashManifoldTitleBy);
        }

        /// <summary>
        /// Get Prime Wash Popup Start Button
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetPrimeWashManifoldPopupStartButton()
        {
            return ApplicationManager.Driver?.FindElement(PrimeWashManifoldPopupFindBy.PrimingStartBtnBy);
        }

        /// <summary>
        /// Get Prime Wash Popup Stop Button
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetPrimeWashManifoldPopupStopButton()
        {
            return ApplicationManager.Driver?.FindElement(PrimeWashManifoldPopupFindBy.PrimingStopBtnBy);
        }

        /// <summary>
        /// Get Prime Wash Manifold Popup Close Button
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetPrimeWashManifoldPopupCloseButton()
        {
            return ApplicationManager.Driver?.FindElement(PrimeWashManifoldPopupFindBy.PrimeWashManifoldCloseBtnBy);
        }

        /// <summary>
        /// Text entry for Priming Cycle - note this is read only
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetPrimingCyclesText()
        {
            return ApplicationManager.Driver?.FindElement(PrimeWashManifoldPopupFindBy.PrimingCyclesBy);
        }

        /// <summary>
        /// Decrement Priming Cycle stepper
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetPrimingCyclesDecrementStepper()
        {
            return ApplicationManager.Driver?.FindElement(PrimeWashManifoldPopupFindBy.PrimingCyclesDecrementStepperBy);
        }

        /// <summary>
        /// Increment Priming Cycle stepper
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetPrimingCyclesIncrementStepper()
        {
            return ApplicationManager.Driver?.FindElement(PrimeWashManifoldPopupFindBy.PrimingCyclesIncrementStepperBy);
        }

        /// <summary>
        /// Checkbox to use specified bottle for wash
        /// </summary>
        /// <param name="index">bottle index</param>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GetPrimeWashBottleCheckbox(int index = 1)
        {
            return ApplicationManager.Driver?.FindElement(By.XPath(string.Format(PrimeWashManifoldPopupFindBy.PrimeWashBottleCheckBoxString, index)));
        }
    }
}
