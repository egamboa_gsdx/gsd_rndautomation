﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Status;
using System.Collections.ObjectModel;

namespace GSD_RnDAutomation.Methods.TORRENT.Popups
{
    public class TempestSelectionViewMethods
    {
        /// <summary>
        /// Cancel Selection and close selection popup
        /// </summary>
        /// e.g.
        public static void CancelSelection()
        {
            StatusManager.AddStatus("Cancelling selection...");
            SelectionCancelButton?.Click();
        }

        /// <summary>
        /// Select an item found in the selection view popup
        /// </summary>
        /// <param name="selectedItem">name/item to select in selection list</param>
        /// <returns>Appium element of item selected from list. null if item is not found in the list</returns>
        /// e.g.
        public static AppiumElement SelectItem(string selectedItem)
        {
            StatusManager.AddStatus(string.Format("Selecting {0} from list...", selectedItem));

            if (EnumItemSelectionList != null)
            {
                var itemEnumList = EnumItemSelectionList?.FindElements(By.XPath("//././android.widget.TextView"));
                foreach (var enumItem in itemEnumList)
                {
                    if (enumItem.Text.Equals(selectedItem))
                    {
                        enumItem.Click();
                        return (AppiumElement)enumItem;
                    }
                }
            }
            return null;
        }
        
        /// <summary>
        /// Get Title of Selection popup view
        /// </summary>
        /// <returns>appium element of Selection popup view title</returns>
        /// e.g.
        public static AppiumElement SelectionViewTitle
        {
            get { return ApplicationManager.Driver?.FindElement(SelectionPopupFindBy.SelectionTitleBy); }
        }

        /// <summary>
        /// Get list of Items in Selection View popup
        /// </summary>
        /// <returns>ReadOnlyCollection of Appium Elements from selection list</returns>
        /// e.g.
        public static AppiumElement ItemSelectionList
        {
            get { return ApplicationManager.Driver?.FindElement(SelectionPopupFindBy.SelectionListBy); }
        }

        /// <summary>
        /// Get list of Enumerable Itesm in Selection View popup
        /// </summary>
        /// <returns>ReadOnlyCollection of Appium elements from selection list</returns>
        /// e.g.
        public static AppiumElement EnumItemSelectionList
        {
            get { return ApplicationManager.Driver?.FindElement(SelectionPopupFindBy.SelectionEnumListBy); }
        }

        /// <summary>
        /// Get Cancel button on Selection Popup View
        /// </summary>
        /// <returns>Appium Element for cancel button</returns>
        /// e.g.
        public static AppiumElement SelectionCancelButton
        {
            get { return ApplicationManager.Driver?.FindElement(SelectionPopupFindBy.SelectionCancelBtnBy); }
        }
    }
}
