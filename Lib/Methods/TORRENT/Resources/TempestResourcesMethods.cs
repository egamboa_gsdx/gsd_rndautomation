﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

namespace GSD_RnDAutomation.Methods.TORRENT
{
    public class TempestResourcesMethods
    {
        /// <summary>
        /// Navigate to Resources Tab in Main View
        /// </summary>
        /// e.g.
        public static void NavigateToResources()
        {
            StatusManager.AddStatus("Navigating to Resources Tab...");
            AppiumElement resourcesTabBtn = ApplicationManager.Driver?.FindElement(TempestResourcesFindBy.ResourceTabBy);
            resourcesTabBtn?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.TextToBePresentInElement(
                ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.HeaderTitleBy), "Resources"));
        }

        /// <summary>
        /// Perform Action associated with bottle type
        ///     Waste        - Empty Waste Bottle
        ///     Wash         - Change Wash Fluid
        ///     System Fluid - Fill System Fluid
        /// </summary>
        /// <param name="bottleIndex">Index of bottle to be changed</param>
        /// <exception cref="WorkflowException">Appium Element could not be found</exception>
        /// e.g.
        public static void PerformBottleAction(int bottleIndex = 1)
        {
            StatusManager.AddStatus("Performing Bottle Action...");
            try
            {
                AppiumElement bottleChangeCmd = ApplicationManager.Driver?.FindElement(By.XPath(string.Format(TempestResourcesFindBy.WashWasteBottleChangeButtonString, bottleIndex)));
                bottleChangeCmd?.Click();
            }
            catch
            {
                throw new WorkflowException("Could not find Element...Bottle Index May be out of range.");
            }
        }

        /// <summary>
        /// Get current wash/waste bottle fluid percentage
        /// </summary>
        /// <param name="bottleIndex">Index of bottle position</param>
        /// <returns>fluid percentage</returns>
        /// <exception cref="WorkflowException">Appium Element not found</exception>
        /// e.g.
        public static string GetWashWasteBottleFluidPercentage(int bottleIndex = 1)
        {
            try
            {
                return ApplicationManager.Driver?.FindElement(By.XPath(string.Format(TempestResourcesFindBy.WashWasteBottle1VolumePercentString, bottleIndex))).Text;
            }
            catch
            {
                throw new WorkflowException("Could not find Element...Bottle Index May be out of range.");
            }
        }

        /// <summary>
        /// Get wash/waste bottle name
        /// </summary>
        /// <param name="bottleIndex">position of wash/waste bottle</param>
        /// <returns>Name of washwastebottle</returns>
        /// <exception cref="WorkflowException">Appium Element could not be found</exception>
        /// e.g.
        public static string GetWashWasteBottleName(int bottleIndex = 1)
        {
            try
            {
                return ApplicationManager.Driver?.FindElement(By.XPath(string.Format(TempestResourcesFindBy.WashWasteBottleNameString, bottleIndex))).Text;
            }
            catch
            {
                throw new WorkflowException(string.Format("Could not find bottle name for bottle index {0}.", bottleIndex));
            }
        }

        /// <summary>
        /// Gets internal temperature of instrument
        /// </summary>
        /// <returns>Curent Internal Temp</returns>
        /// <exception cref="WorkflowException"></exception>
        /// e.g.
        public static string GetInternalTemp()
        {
            try
            {
                return ApplicationManager.Driver?.FindElement(TempestResourcesFindBy.InteralTemperatureBy).Text;
            }
            catch
            {
                throw new WorkflowException("Could not find internal temperature...whooops!");
            }
        }

        /// <summary>
        /// Gets Current incubator temperature.
        /// </summary>
        /// <returns>current incubator temperature</returns>
        /// <exception cref="WorkflowException"></exception>
        /// e.g.
        public static string GetIncubatorTemp()
        {
            try
            {
                return ApplicationManager.Driver?.FindElement(TempestResourcesFindBy.IncubatorTemperatureBy).Text;
            }
            catch
            {
                throw new WorkflowException("Could not find incubator temperature...awkward");
            }
        }

        /// <summary>
        /// Gets Current Available Disposable Tips
        /// </summary>
        /// <returns>Current Disposable Tips Available</returns>
        /// <exception cref="WorkflowException"></exception>
        /// e.g.
        public static string GetAvailableDisposableTips()
        {
            try
            {
                return ApplicationManager.Driver?.FindElement(TempestResourcesFindBy.AvailableDisposalbeTipsBy).Text;
            }
            catch
            {
                throw new WorkflowException("Could not find Available Disposable tips...");
            }
        }
    }
}
