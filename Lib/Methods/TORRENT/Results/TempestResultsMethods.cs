﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Status;

namespace GSD_RnDAutomation.Methods.TORRENT
{
    public class TempestResultsMethods
    {
        /// <summary>
        /// Navigate to Results Tab from Main View
        /// </summary>
        /// e.g.
        public static void NavigateToResults()
        {
            StatusManager.AddStatus("Navigating to Results Tab...");
            AppiumElement resultsTabBtn = ApplicationManager.Driver?.FindElement(TempestResultsFindBy.ResultsTabBy);
            resultsTabBtn?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.TextToBePresentInElement(
                ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.HeaderTitleBy), "Results"));
        }

        /// <summary>
        /// Check or uncheck Search on Sample Id
        /// </summary>
        /// <returns>state of checkbox - true = checked; false = unchecked</returns>
        /// e.g.
        public static bool CheckSearchOnSampleID()
        {
            StatusManager.AddStatus("Checking/Unchecking Search On Sample ID...");
            SearchOnSampleIdCheckbox?.Click();

            return bool.Parse(SearchOnSampleIdCheckbox?.GetAttribute("checked"));
        }

        /// <summary>
        /// Check or uncheck Search On Test
        /// </summary>
        /// <returns>state of checkbox - true = checked; false = unchecked</returns>
        /// e.g.
        public static bool CheckSearchOnTest()
        {
            StatusManager.AddStatus("Checking/Unchecking Search on Test...");
            SearchOnTestCheckbox?.Click();

            return bool.Parse(SearchOnTestCheckbox?.GetAttribute("checked"));
        }

        /// <summary>
        /// Check or uncheck Search On Operator
        /// </summary>
        /// <returns>state of checkbox - true = checked; false = unchecked</returns>
        /// e.g.
        public static bool CheckSearchOnOperator()
        {
            StatusManager.AddStatus("Checking/Unchecking Search on Operator...");
            SearchOnOperatorCheckbox?.Click();

            return bool.Parse(SearchOnOperatorCheckbox?.GetAttribute("checked"));
        }

        /// <summary>
        /// check or uncheck search on From Date
        /// </summary>
        /// <returns>state of checkbox - true = checked; false = unchecked</returns>
        /// e.g.
        public static bool CheckSearchFromDate()
        {
            StatusManager.AddStatus("Checking/Unchecking Search on From Date...");
            SearchOnFromDateCheckbox?.Click();

            return bool.Parse(SearchOnFromDateCheckbox?.GetAttribute("checked"));
        }

        /// <summary>
        /// check or uncheck search on To Date
        /// </summary>
        /// <returns>state of checkbox - true = checked; false = unchecked</returns>
        /// e.g.
        public static bool CheckSearchToDate()
        {
            StatusManager.AddStatus("Checking/Unchecking Search on To Date...");
            SearchOnToDateCheckbox?.Click();

            return bool.Parse(SearchOnToDateCheckbox?.GetAttribute("checked"));
        }

        /// <summary>
        /// Print report of selected search result
        /// </summary>
        /// e.g.
        public static void PrintReport()
        {
            StatusManager.AddStatus("Printing Results Report...");
            PrintReportButton?.Click();

            // TODO When items populate results to enable print button
        }

        /// <summary>
        /// Export report of selected search result
        /// </summary>
        /// e.g.
        public static void ExportReport()
        {
            StatusManager.AddStatus("Exporting Results Report...");
            ExportReportButton?.Click();

            // TODO when items populate resutls to enable export button
        }

        /// <summary>
        /// Refresh search result
        /// </summary>
        /// e.g.
        public static void RefreshList()
        {
            StatusManager.AddStatus("Refreshing results list...");
            RefreshButton?.Click();
        }

        /// <summary>
        /// Get checkbox for Search on Sample Id
        /// </summary>
        /// <returns>Appium Element associated with checkbox for Search on Sample Id</returns>
        /// e.g.
        public static AppiumElement SearchOnSampleIdCheckbox
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchSampleIdCheckboxBy); }
        }
        
        /// <summary>
        /// Get text entry for Search on Sample Id
        /// </summary>
        /// <returns>Appium Element associated with Text Entry for Search On Sample Id</returns>
        /// e.g.
        public static AppiumElement SearchOnSampleIDTextEntry
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchSampleIDTextEntryBy); }
        }

        /// <summary>
        /// Get Checkbox for Search on Test
        /// </summary>
        /// <returns>Appium Element associated with checkbox for Search On Test</returns>
        /// e.g.
        public static AppiumElement SearchOnTestCheckbox
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchTestCheckboxBy); }
        }

        /// <summary>
        /// Get picker for Search on Test selection
        /// </summary>
        /// <returns>Appium Element associated with picker for Search on Test</returns>
        /// e.g.
        public static AppiumElement SearchOnTestPicker
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchTestPickerBy); }
        }

        /// <summary>
        /// Get Checkbox for Search on Operator
        /// </summary>
        /// <returns>Appium Element associated with checkbox for Search on Operator</returns>
        /// e.g.
        public static AppiumElement SearchOnOperatorCheckbox
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchOperatorCheckboxBy); }
        }

        /// <summary>
        /// Get picker for Search On Operator
        /// </summary>
        /// <returns>Appium Element associated with picker for Search On Operator</returns>
        /// e.g.
        public static AppiumElement SearchOnOperatorPicker
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchOperatorButtonBy); }
        }

        /// <summary>
        /// Get checkbox for Search on From Date
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement SearchOnFromDateCheckbox
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchFromDateCheckboxBy); }
        }

        /// <summary>
        /// Get datepicker for Search on From Date
        /// </summary>
        /// <returns>Appium Element associated with datepicker for Search on From Date</returns>
        /// e.g.
        public static AppiumElement SearchOnFromDateDatePicker
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchFromDateTextBy); }
        }

        /// <summary>
        /// Get checkbox for Search on To Date
        /// </summary>
        /// <returns>Apium Element associated with checkbox for Search on To Date</returns>
        /// e.g.
        public static AppiumElement SearchOnToDateCheckbox
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchToDateCheckboxBy); }
        }

        /// <summary>
        /// Get datepicker for Search On To Date
        /// </summary>
        /// <returns>Appium Element associated with datepicker for Search on To Date</returns>
        /// e.g.
        public static AppiumElement SearchOnToDateDatePicker
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.SearchToDateTextBy); }
        }

        /// <summary>
        /// Get button to Print Report
        /// </summary>
        /// <returns>Appium Element associated with button for Print Report</returns>
        /// e.g.
        public static AppiumElement PrintReportButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.PrintReportButtonBy); }
        }

        /// <summary>
        /// Get button to Export Report
        /// </summary>
        /// <returns>Appium ELement associated with button for Export Report</returns>
        /// e.g.
        public static AppiumElement ExportReportButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.ExportReportButtonBy); }
        }

        /// <summary>
        /// Get button to Refresh search results
        /// </summary>
        /// <returns>Appium Element associated with button to refresh list</returns>
        /// e.g.
        public static AppiumElement RefreshButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestResultsFindBy.RefreshResultsListBy); }
        }
    }
}
