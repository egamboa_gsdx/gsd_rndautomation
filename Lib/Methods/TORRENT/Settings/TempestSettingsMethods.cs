﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Settings;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT
{
    public class TempestSettingsMethods
    {
        /// <summary>
        /// Navigate to Settings Tab in Main View
        /// </summary>
        /// e.g.
        public static void NavigateToSettings()
        {
            StatusManager.AddStatus("Navigating to Settings Tab...");
            SettingsTabButton?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.TextToBePresentInElement(
                ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.HeaderTitleBy), "Settings")); 
        }

        /// <summary>
        /// View Users Settings in Settings Tab View
        /// </summary>
        /// e.g.
        public static void NavigateToUsersSettings()
        {
            StatusManager.AddStatus("Navigating to Users Settings...");
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Settings"))
            {
                NavigateToSettings();
            }

            UsersSettingsViewButton?.Click();
        }

        /// <summary>
        /// View Tests Settings in Settings Tab View
        /// </summary>
        /// e.g.
        public static void NavigateToTestsSettings()
        {
            StatusManager.AddStatus("Navigating to Tests Settings...");
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Settings"))
            {
                NavigateToSettings();
            }

            TestsSettingsViewButton.Click();
        }

        /// <summary>
        /// View LIS Settings in Settings Tab View
        /// </summary>
        /// e.g.
        public static void NavigateToLISSettings()
        {
            StatusManager.AddStatus("Navigating to LIS Settings...");
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Settings"))
            {
                NavigateToSettings();
            }

            LISSettingsViewButton?.Click();
        }

        /// <summary>
        /// View Maintenance Settings in Settings Tab View
        /// </summary>
        public static void NavigateToMaintenanceSettings()
        {
            StatusManager.AddStatus("Navigating to Maintenance Settings...");
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Settings"))
            {
                NavigateToSettings();
            }

            MaintenanceSettingsViewButton?.Click();
        }

        /// <summary>
        /// View Backup Settings in Settings Tab View
        /// </summary>
        public static void NavigateToBackupSettings()
        {
            StatusManager.AddStatus("Navigating to Backup Settings...");
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Settings"))
            {
                NavigateToSettings();
            }

            BackupSettingsViewButton?.Click();
        }

        /// <summary>
        /// View General Settings in Settings Tab View
        /// </summary>
        public static void NavigateToGeneralSettings()
        {
            StatusManager.AddStatus("Navigating to General Settings...");
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Settings"))
            {
                NavigateToSettings();
            }

            GeneralSettingsViewButton?.Click();
        }

        /// <summary>
        /// View Update Settings in Settings Tab View
        /// </summary>
        public static void NavigateToUpdateSettings()
        {
            StatusManager.AddStatus("Navigating to Update Settings...");
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Settings"))
            {
                NavigateToSettings();
            }

            UpdateSettingsViewButton?.Click();
        }

        /// <summary>
        /// Get Settings Tab button from Main View Side Bar
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement SettingsTabButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestSettingsFindBy.SettingsTabBy); }
        }

        /// <summary>
        /// Get Label/Button for Users Settings in Settings View
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement UsersSettingsViewButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestUserSettingsFindBy.UserSettingsTabBy); }
        }

        /// <summary>
        /// Get Label/Button for Tests Settings in Settings View
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement TestsSettingsViewButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestTestsSettingsFindBy.TestsSettingTabBy); }
        }

        /// <summary>
        /// Get Label/Button for LIS Settings in Setting Views
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement LISSettingsViewButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestLISSettingsFindBy.LISSettingsTabBy); }
        }

        /// <summary>
        /// Get Label/Button for Maintenance Settings in Settings View
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement MaintenanceSettingsViewButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestMaintenanceSettingsFindBy.MaintenanceSettingsTabBy); }
        }

        /// <summary>
        /// Get Label/Button for Backup Settings in Settings View
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement BackupSettingsViewButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestBackupSettingsFindBy.BackupSettingsTabBy); }
        }

        /// <summary>
        /// Get Label/Button for General Settings in Settings View
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement GeneralSettingsViewButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestGeneralSettingsFindBy.GeneralSettingsTabBy); }
        }

        /// <summary>
        /// Get Label/Button for Update Settings in Settings View
        /// </summary>
        /// <returns></returns>
        /// e.g.
        public static AppiumElement UpdateSettingsViewButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestUpdateSettingsFindBy.UpdateSettingsTabBy); }
        }
    }
}
