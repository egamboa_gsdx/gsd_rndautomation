﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Settings;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Methods.TORRENT.Settings
{
    public class TempestUsersSettingsMethods
    {
        public static string CreateUser(string username, string password, string reenterpassword,
            string email, string phonenum, string userrole = "Basic user", bool isActive = true)
        {
            StatusManager.AddStatus("Adding New User...");
            TempestSettingsMethods.NavigateToUsersSettings();

            // Check to see if any users have been previously added.
            // if so, get count of users that contain username parameter
            // and format new username accordingly by appending index
            int index = 0;
            //if (UsersList != null)
            //{
            //    index = UsersList.Select(u => u.Text.Contains(username)).Count();
                
            //}

            var newUsername =  index == 0 ? username : string.Format("{0}{1}", username, index);

            NewUserButton?.Click();
            

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(CreateEditUserPopupFindBy.CreateEditUserTitleBy));

            TempestCreateEditUserMethods.CreateEditUser(newUsername, password, reenterpassword, email, phonenum, userrole, option: 0);

            return newUsername;            
        }

        public static void EditUser(string username, string newusername, string newemail, string newphonenum, string newuserrole, bool isActive = true)
        {
            StatusManager.AddStatus("Editing Selected User...");
            TempestSettingsMethods.NavigateToUsersSettings();
            var userList = UsersList?.FindElements(By.XPath("//././android.widget.TextView"));
            var user = userList.FirstOrDefault(u => u.Text == username);
            if (user == null)
            {
                throw new WorkflowException("Error 404: User not found.");
            }
            user.Click();
            EditUserButton?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(CreateEditUserPopupFindBy.CreateEditUserTitleBy));

            var role = !string.IsNullOrEmpty(newuserrole) ? newuserrole : "Basic user";
            TempestCreateEditUserMethods.CreateEditUser(newusername, null, null, newemail, newphonenum, role, isActive, 1);
        }

        public static void ViewPasswordPolicy()
        {
            StatusManager.AddStatus("Opening Password Policy...");
            TempestSettingsMethods.NavigateToUsersSettings();

            PasswordPolicyButton?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(PasswordPolicyPopupFindBy.PasswordPolicyTitleBy));
        }

        public static AppiumElement NewUserButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestUserSettingsFindBy.NewUserButtonBy); }
        }

        public static AppiumElement EditUserButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestUserSettingsFindBy.EditUserButtonBy); }
        }

        public static AppiumElement PasswordPolicyButton
        {
            get { return ApplicationManager.Driver?.FindElement(TempestUserSettingsFindBy.PasswordPolicyButtonBy); }
        }

        public static AppiumElement UsersList
        {
            get { return ApplicationManager.Driver?.FindElement(TempestUserSettingsFindBy.UserAccountListViewBy); }
        }


    }
}
