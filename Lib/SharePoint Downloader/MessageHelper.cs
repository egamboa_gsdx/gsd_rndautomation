﻿using System;
using System.Linq;


namespace GSD_RnDAutomation.SharePoint_Downloader
{
    public static class MessageHelper
    {
        public static void Color(string aMessage, MessageType AType)
        {
            switch (AType)
            {
                case MessageType.Error:
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case MessageType.Warning:
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case MessageType.Progress:
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
            }
            Console.WriteLine(aMessage);
            Console.ResetColor();
        }
    }

    public enum MessageType
    {
        Error,
        Warning,
        Progress
    }
}
