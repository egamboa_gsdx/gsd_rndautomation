﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Threading;
using System.Security.Cryptography;
using System.Text;

namespace GSD_RnDAutomation.SharePoint_Downloader
{
    public class SPContext
    {
        public Microsoft.SharePoint.Client.SharePointOnlineCredentials Credentials;
        public Microsoft.SharePoint.Client.ClientContext Context;
        private const int ConnectionRetry = 5;

        public System.Security.SecureString Password { get; set; }
        public string Username { get; set; }
        public string URL { get; set; }
        public string DocLibName { get; set; }
        public string DestinationPath { get; set; }

        private void CreateCreds()
        {
            Credentials = new Microsoft.SharePoint.Client.SharePointOnlineCredentials(Username, Password);
        }

        private void CreateContext()
        {
            CreateCreds();
            Context = new Microsoft.SharePoint.Client.ClientContext(URL);
            Context.Credentials = Credentials;
        }

        private void InitializeStorm()
        {
            Username = "gsdbuildserver@gsdx.us";
            URL = "https://gsdxus.sharepoint.com/sites/InstrumentationandSoftware";
            DocLibName = "Shared Documents/Software Downloads/Nightly Builds/Storm/LaboratoryEdition";
            DestinationPath = @"C:\Builds\Storm";

        }

        private void InitializeAIX()
        {
            Username = "gsdbuildserver@gsdx.us";
            URL = "https://gsdxus.sharepoint.com/sites/InstrumentationandSoftware";
            DocLibName = "Shared Documents/Software Downloads/Nightly Builds/AIX1000";
            DestinationPath = @"C:\Builds\AIX";
        }

        public static byte[] ToByteArray<T>(T obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static T FromByteArray<T>(byte[] data)
        {
            if (data == null)
                return default(T);
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(data))
            {
                object obj = bf.Deserialize(ms);
                return (T)obj;
            }
        }

        /// <summary>
        /// saves password into protected byte array
        /// </summary>
        /// <param name="aSecureString"></param>
        /// <param name="aPath"></param>
        /// <returns></returns>
        public static byte[] SavePasswordString(string aSecureString, string aPath)
        {
            var byteArray = ToByteArray<string>(aSecureString);
            var protectedArray = ProtectedData.Protect(byteArray, null, DataProtectionScope.CurrentUser);
            File.WriteAllBytes(aPath, protectedArray);
            return protectedArray;
        }

        /// <summary>
        /// Parses saved file into byte array
        /// </summary>
        /// <param name="aPath"></param>
        /// <returns></returns>
        public static string FetchPasswordString(string aPath)
        {
            var protectedArray = File.ReadAllBytes(aPath);
            var byteArray = ProtectedData.Unprotect(protectedArray, null, DataProtectionScope.CurrentUser);
            return FromByteArray<string>(byteArray);
        }

        public string GetPassword()
        {
            var pwd = new StringBuilder();
            /*while (true)
            {
                ConsoleKeyInfo i = Console.ReadKey(true);
                if(i.Key == ConsoleKey.Enter)
                {
                    break;
                }else if( i.Key == ConsoleKey.Backspace)
                {
                    if(pwd.Length > 0)
                    {
                        pwd.Remove(pwd.Length - 1, 1);
                    }
                }else if(i.KeyChar != '\u0000')
                {
                    pwd.Append(i.KeyChar);
                    Console.Write("*");
                }
            }*/

            //PasswordForm pform = new PasswordForm();

            var stringpwd = pwd.ToString();
            return stringpwd;
        }

        public void InitClientContextQuery(Software aSoftware, System.Security.SecureString aPass)
        {
            Console.WriteLine("Creating Context and Credentials");
            Password = aPass;
            //Initialize Default Values
            if (aSoftware == Software.Storm)
                InitializeStorm();
            else
                InitializeAIX();
            CreateContext();
        }

        public bool InitConnectionQuery()
        {
            Console.WriteLine("Querying Server");
            for(int i = 0; i < ConnectionRetry; i++)
            {
                try
                {
                    Context.ExecuteQuery();
                    MessageHelper.Color("Connection OK", MessageType.Error);
                    return true;
                }
                catch
                {
                    MessageHelper.Color(String.Format("Connection Failed! Retry #{0}", i + 1), MessageType.Error);
                    Thread.Sleep(1000);
                }
            }
            return false;
        }

        public enum Software
        {
            Storm,
            AIX,
            Torrent
        }
    }
}
