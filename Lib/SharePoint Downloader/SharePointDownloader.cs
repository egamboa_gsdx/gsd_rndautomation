﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml.Linq;

using GSD_RnDAutomation.Common.Methods;

namespace GSD_RnDAutomation.SharePoint_Downloader
{
    public static class SharePointDownloader
    {

        private static bool restart = false;

        public static void SPDownloader(string[] args)
        {

            //create file structure
            var TAFFolder = @"C:\Builds\Test Automation Framework";
            var StormFolder = @"C:\Builds\Storm";
            var AIXFolder = @"C:\Builds\AIX";

            if (!Directory.Exists(TAFFolder))
                Directory.CreateDirectory(TAFFolder);
            if (!Directory.Exists(StormFolder))
                Directory.CreateDirectory(StormFolder);
            if (!Directory.Exists(AIXFolder))
                Directory.CreateDirectory(AIXFolder);

            //Get password, if its missing then ask for a new one from the user
            var SPHelper = new SPContext();
            var securePass = "";
            string DownloadFileString, SoftwareToInstall;

            try
            {
                securePass = SPContext.FetchPasswordString(@"C:\Builds\Test Automation Framework\creds.txt");
            }
            catch
            {
                MessageHelper.Color("Password will be stored", MessageType.Warning);
                Console.WriteLine("Enter Password");
                securePass = SPHelper.GetPassword();

                //Encrypt
                SPContext.SavePasswordString(securePass, @"C:\Builds\Test Automation Framework\creds.txt");
                Console.WriteLine("\n");
            }

            //Convert password to securestring
            var secure = new SecureString();
            foreach(char c in securePass)
            {
                secure.AppendChar(c);
            }

            //Load Options XML, if missing then create Options
            //These options affect which software to download and whether to install them
            if (!File.Exists(@"C:\Builds\settings.xml"))
            {
                Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan");
                CreateOptions();
            }

            XElement settings = XElement.Load(@"C:\Builds\settings.xml", LoadOptions.SetBaseUri | LoadOptions.SetLineInfo);

            //Check main funct entry point arguments for multiple arguments. This allows 'Main' method to run a second
            //time to allow both softwae to be download
            if(args.Count() != 0)
            {
                if("AIX" == (args[0] ?? "Not AIX"))
                {
                    settings.SetElementValue("Software", "AIX");
                }
                else if("Storm" == (args[0] ?? "Not Storm"))
                {
                    settings.SetElementValue("Sfotware", "Storm");
                }
            }

            //Initalize settings dependent variables
            if (settings.Element("Software").Value == "Storm")
            {
                SoftwareToInstall = "Storm";
                SPHelper.InitClientContextQuery(SPContext.Software.Storm, secure);
                DownloadFileString = "StormLaboratoryInstaller";
            }
            else
            {
                SoftwareToInstall = "AIX";
                SPHelper.InitClientContextQuery(SPContext.Software.AIX, secure);
                DownloadFileString = "AIX1000SoftwareSuiteInstaller";
            }

            //Create context, create creds and connect
            if (!SPHelper.InitConnectionQuery())
            {
                //Login Failed
                MessageHelper.Color("Login Request Failed!", MessageType.Error);
                Console.WriteLine("Press any key to qui...");
                Console.ReadKey();
                //Delete Password since it is presumably bad
                var filePassInfo = new FileInfo(@"C:\Builds\Test Automation Framework\creds.txt");
                filePassInfo.Delete();
                securePass = null;
                return;
            }

            //Grab list of Software
            var ListOfDownloads = SPHelper.Context.Web.GetFolderByServerRelativeUrl(SPHelper.DocLibName).Files;
            SPHelper.Context.Load(ListOfDownloads);
            SPHelper.Context.ExecuteQuery();

            if(ListOfDownloads[0].Name == null)
            {
                MessageHelper.Color("List of files NOT initialized!", MessageType.Error);
                Console.WriteLine("Press any key to quit...");
                Console.ReadKey();
                return;
            }

            //Iterate over list of files and find which ones are named the correspoding software string (DownloadFileString)
            var fileCurrent = ListOfDownloads[0];
            foreach(var file in ListOfDownloads)
            {
                if (file.Name.Contains(DownloadFileString))
                {
                    if (file.Name.EndsWith(".zip"))
                    {
                        fileCurrent = file;
                        break;
                    }
                }
            }

            //Remove Old Build Files
            MessageHelper.Color("Build Found:" + fileCurrent.Name, MessageType.Progress);
            Console.WriteLine("Removing Old Build...");

            var di = new DirectoryInfo(SPHelper.DestinationPath);
            //if the most recent version of the software does not exist
            if(di.EnumerateFiles(fileCurrent.Name).Count() == 0)
            {
                foreach(FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach(DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete();
                }

                //Use background worker to get progress and notify user of downloading the initial build
                MessageHelper.Color("Downloading Build...", MessageType.Warning);
                var backgroundWorker = new BackgroundWorker();
                backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
                backgroundWorker.WorkerReportsProgress = true;
                backgroundWorker.WorkerSupportsCancellation = true;

                //Setup download build
                Microsoft.SharePoint.Client.FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(SPHelper.Context, fileCurrent.ServerRelativeUrl);
                SPHelper.Context.Load(ListOfDownloads.GetByUrl(fileCurrent.ServerRelativeUrl), file => file.Length);
                SPHelper.Context.ExecuteQuery();
                var fileName = Path.Combine(SPHelper.DestinationPath, fileCurrent.Name);

                //Download build with progress update
                using (var fileStream = File.Create(fileName))
                {
                    int data, left;
                    int previousPercent = 0;
                    long test = 0;
                    long total = ListOfDownloads.GetByUrl(fileCurrent.ServerRelativeUrl).Length;
                    Console.CursorVisible = false;
                    Console.Write(@"|....................|");
                    Console.SetCursorPosition(1, Console.CursorTop);

                    while((data = fileInfo.Stream.ReadByte()) != -1)
                    {
                        fileStream.WriteByte((byte)data);
                        test++;
                        if(test %10000 == 0)
                        {
                            var percentProgress = (int)(test * 100 / total);
                            if(percentProgress != previousPercent)
                            {
                                left = Console.CursorLeft;
                                previousPercent = percentProgress;
                                Console.SetCursorPosition(22, Console.CursorTop);
                                Console.Write(percentProgress + @"%");
                                Console.SetCursorPosition(left, Console.CursorTop);
                                backgroundWorker.ReportProgress(percentProgress);
                            }
                        }
                    }

                    left = Console.CursorLeft;
                    Console.SetCursorPosition(22, Console.CursorTop);
                    Console.Write(100 + @"%");
                    Console.SetCursorPosition(left, Console.CursorTop);
                    Console.Write("*");
                }

                Console.CursorVisible = true;
                Console.SetCursorPosition(0, Console.CursorTop + 1);

                //Unzip Build
                Console.WriteLine("Unzipping Build");
                ZipFile.ExtractToDirectory(SPHelper.DestinationPath + @"\" + fileCurrent.Name, SPHelper.DestinationPath);

                if (Boolean.Parse(settings.Element("Auto_Install").Value))
                {
                    //Create Reg creation thread (this ensures that the reg key is set since the taf process
                    //was initially programmed to resest the computer which
                    //could result in this process being incomplete
                    Thread t = new Thread(CreateRegKey);
                    t.Start();

                    //Start TAF
                    try
                    {
                        if(Boolean.Parse(settings.Element("Both_Software").Value) && args.Count() == 0)
                        {
                            InstallMethods.Install(SoftwareToInstall, false);
                            restart = true;
                        }
                        else
                        {
                            InstallMethods.Install(SoftwareToInstall, true);
                        }
                    }
                    catch
                    {
                        MessageHelper.Color("Test Automation Framework process failed to run!", MessageType.Error);
                        MessageHelper.Color("Make sure exe is in the correct location.", MessageType.Warning);
                        Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan");
                        Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationFlag", false);
                        return;
                    }
                    while (t.IsAlive)
                    {
                        //Do Nothing
                    }
                }
            }
            else
            {
                Console.WriteLine("Newest " + SoftwareToInstall + " Build Already Installed!");
                if (restart)
                {
                    Process.Start("shutdown", "/r /t 0");
                }
            }

            //Re-enter main with different arg to downlad build if both softwares need to be downloaded
            if(Boolean.Parse(settings.Element("Both_Software").Value) && settings.Element("Software").Value == "Storm" && args.Count() == 0)
            {
                MessageHelper.Color(@"Both Software Enabled! If undesired, change Both_Software to 'false' in xml.", MessageType.Warning);
                SPDownloader(new string[] { "AIX" });
            }else if( Boolean.Parse(settings.Element("Both_Software").Value) && settings.Element("Software").Value == "AIX" && args.Count() == 0)
            {
                MessageHelper.Color(@"Both Software Enabled! If undesired, change Both_Software to 'false' in xml.", MessageType.Warning);
                SPDownloader(new string[] { "Storm" });
            }

            if (Boolean.Parse(settings.Element("Test_Both_Software").Value))
            {
                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationApp", "Both");
            }
            else
            {
                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationApp", settings.Element("Software").Value);
            }

            MessageHelper.Color("Done!", MessageType.Warning);

            //For dev purposes, will be removed when full end to end automation is complete
            if(args.Count() > 0)
            {
                Console.WriteLine("Press Enter to Quit...");
                Console.ReadLine();
                return;
            }
        }

        private static void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage % 5 == 0)
                Console.Write("*");
        }

        //Create the options XML from user input
        private static void CreateOptions()
        {
            char bothSoftware, software, install, runBoth;
            bothSoftware = software = install = runBoth = ' ';
            var RegexYN = new Regex("[y||n||Y||N]");
            var RegexSA = new Regex("[S||A||s||a]");

            Console.WriteLine(@"Creating XML config. This will be stored in: C:\Builds\settings.xml");
            MessageHelper.Color("After first run config can only be changed manually", MessageType.Warning);
            XElement settings = new XElement("Settings");

            while (!RegexYN.IsMatch(bothSoftware.ToString()))
            {
                Console.WriteLine(@"Download both software? Y/N: ");
                bothSoftware = Console.ReadKey().KeyChar;
                Console.WriteLine("\n");
            }
            if(bothSoftware == 'n' || bothSoftware == 'N')
            {
                settings.SetElementValue("Both_Software", false);
                while (!RegexYN.IsMatch(install.ToString()))
                {
                    Console.WriteLine(@"Install software? Y/N: ");
                    install = Console.ReadKey().KeyChar;
                    Console.WriteLine("\n");
                }
                while (!RegexSA.IsMatch(software.ToString()))
                {
                    Console.WriteLine(@"Storm or AIX? S/A: ");
                    software = Console.ReadKey().KeyChar;
                    Console.WriteLine("\n");
                }

                if (install == 'n' || install == 'N')
                    settings.SetElementValue("Auto_Install", false);
                else
                    settings.SetElementValue("Auto_Install", true);

                if (software == 's' || software == 'S')
                    settings.SetElementValue("Software", "Storm");
                else
                    settings.SetElementValue("Software", "AIX");

                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true)
                    .SetValue("AutomationApp", settings.Element("Software").Value);
                settings.SetElementValue("Test_Both_Software", false);
            }
            else
            {
                MessageHelper.Color(@"Both Software Enabled! If undesired, change Both_Software to 'false in xml.", MessageType.Warning);
                while (!RegexYN.IsMatch(install.ToString()))
                {
                    Console.WriteLine(@"Install software? Y/N: ");
                    install = Console.ReadKey().KeyChar;
                    Console.WriteLine("\n");
                }
                if (install == 'y' || install == 'Y')
                    settings.SetElementValue("Auto_Install", true);
                else
                    settings.SetElementValue("Auto_Install", false);
                software = 'S';
                settings.SetElementValue("Both_Software", true);
                settings.SetElementValue("Software", "Storm");

                while (!RegexYN.IsMatch(runBoth.ToString()))
                {
                    Console.WriteLine(@"Test both software? Y/N: ");
                    runBoth = Console.ReadKey().KeyChar;
                    Console.WriteLine("\n");
                }
                if(runBoth == 'y' || runBoth == 'Y')
                {
                    Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationApp", "Both");
                    settings.SetElementValue("Test_Both_Software", true);
                }
                else
                {
                    Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true)
                        .SetValue("AutomationApp", settings.Element("Software").Value);
                    settings.SetElementValue("Test_Both_Software", false);
                }
            }
            settings.Save(@"C:\Builds\settings.xml");
        }

        //creates and sets the automation flag registry key
        static void CreateRegKey()
        {
            try
            {
                Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan");
                Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Gold Standard Diagnostics\AutomationPlan", true).SetValue("AutomationFlag", true);
            }
            catch
            {
                MessageHelper.Color("Something went wrong writing the registry key flag!", MessageType.Error);
                MessageHelper.Color("Check privileges and try again", MessageType.Error);
                return;
            }

            MessageHelper.Color("Registry key set!", MessageType.Progress);
        }
    }
}
