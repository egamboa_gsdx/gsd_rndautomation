﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Management;
using System.Linq;

using Microsoft.Diagnostics.Tracing.Parsers;
using Microsoft.Diagnostics.Tracing.Session;


namespace GSD_RnDAutomation.StressTest
{
    public sealed class NetworkPerformanceReporter : IDisposable
    {
        private DateTime m_EtwStartTime;
        private TraceEventSession m_EtwSession;

        private string processName;

        private readonly Counters m_Counters = new Counters();

        private class Counters
        {
            public long Received;
            public long Sent;
        }

        private NetworkPerformanceReporter(string process)
        {
            processName = process;
        }

        public static NetworkPerformanceReporter Create(string process)
        {
            var networkPerforamncePreseneter = new NetworkPerformanceReporter(process);
            networkPerforamncePreseneter.Initialise();
            return networkPerforamncePreseneter;
        }

        private void Initialise()
        {
            //Not that the ETW class blocks processing messages, so should be run on a different thread if
            // you want the application to remain respsonive
            Task.Run(() => StartEtwSession());
        }

        private void StartEtwSession()
        {
            try
            {
                var processId = Array.Find(Process.GetProcessesByName(processName), x => x.ProcessName == processName).Id;
                ResetCounters();

                using (m_EtwSession = new TraceEventSession(KernelTraceEventParser.KernelSessionName))
                {
                    m_EtwSession.EnableKernelProvider(KernelTraceEventParser.Keywords.NetworkTCPIP);
                    m_EtwSession.Source.Kernel.TcpIpRecv += data =>
                    {
                        if (data.ProcessID == processId)
                        {
                            lock (m_Counters)
                            {
                                m_Counters.Sent += data.size;
                            }
                        }
                    };

                    m_EtwSession.Source.Process();
                }
            }
            catch (Exception e)
            {
                if(!(System.IO.Directory.Exists(System.IO.Directory.GetCurrentDirectory() + @"\Log")))
                {
                    System.IO.Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory() + @"\Log");
                }
                System.IO.File.WriteAllText(System.IO.Directory.GetCurrentDirectory()
                                            + @"\Log\Error_" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")
                                            + ".txt", e.Message + e.StackTrace);
                ResetCounters();
            }
        }

        public NetworkPerformanceData GetNetworkPerformanceData()
        {
            var timeDifferenceInSeconds = (DateTime.Now - m_EtwStartTime).TotalSeconds;

            NetworkPerformanceData networkData;

            lock (m_Counters)
            {
                networkData = new NetworkPerformanceData
                {
                    BytesReceived = Convert.ToInt64(m_Counters.Received / timeDifferenceInSeconds),
                    BytesSent = Convert.ToInt64(m_Counters.Sent / timeDifferenceInSeconds)
                };
            }

            //Reset the counters to get fresh reading for next time this is called
            ResetCounters();

            return networkData;
        }

        private void ResetCounters()
        {
            lock (m_Counters)
            {
                m_Counters.Sent = 0;
                m_Counters.Received = 0;
            }
            m_EtwStartTime = DateTime.Now;
        }

        public void Dispose()
        {
            m_EtwSession?.Dispose();
        }

        public sealed class NetworkPerformanceData
        {
            public long BytesReceived { get; set; }
            public long BytesSent { get; set; }
        }
    }
}
