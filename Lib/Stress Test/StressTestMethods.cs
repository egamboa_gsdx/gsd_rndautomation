﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Automation;

using GSD_RnDAutomation;
using GSD_RnDAutomation.Common;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;

using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;

namespace GSD_RnDAutomation.StressTest
{
    public enum StressTestType
    {
        undefined,
        Download,
        Upload,
        UploadAndDownload
        
    }

    public class StressTestMethods
    {
        private static ElementIdentifier OKButton = CommonProperty.Default.OkButton_textValue;

        public bool ErrorFlag = false;
        /// <summary>
        /// Constructor: takes in the process name from the GetProcessString() function
        /// </summary>
        /// <param name="processName"></param>
        public StressTestMethods()
        {}

        public void CreateWorklist()
        {
            CheckAppErrors();

            HelperMethods.WaitForSetupAction();

            //For Storm, add the "ANA_GSD" test
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                try
                {
                    WorklistMethods.AddTest(1, "ANA_GSD");
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("ANA_GSD Test Nt Found!", "Error");
                }
            }

            int sampleCount = (ApplicationManager.InstrumentManager == InstrumentManager.Storm) ? 92 : 192;
            
            WorklistMethods.AddSamples(numberOfSamples: sampleCount);

            if(ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                try
                {
                    WorklistMethods.AssignAllSamplesToTest();
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("Failed to Assign Samples to Test!", "Error!");
                }
            }

            //name worklist
            WorklistMethods.AddWorklistName("StressTestAutomation");

            //For AIX, add lot number

          //  if(ApplicationManager.InstrumentManager == InstrumentManager.AIX)
          //  {
         //       HomeMethods.ModifyLotNumber(123456);
          //  }

            //Start Worklist
            HomeMethods.SpecialStartSpecificPosition("Last");

            //Make sure there are no errors
            CheckAppErrors();

            HomeMethods.WaitForWorklistToFinish();
            ApplicationManager.ApplicationWindow.Focus();
        }

        /// <summary>
        /// Performs a special start re-read
        /// </summary>
        /// <returns> returns true if reread was successful; returns false if worklist is still running or an error occured</returns>
        public bool SpecialStartReRead()
        {
            try
            {
                HelperMethods.Wait();
                HomeMethods.CloseWorklistFinishDialog();
            }
            catch
            {
                //worklist has not finished running
            }

            //Attempt to start a reread
            try
            {
                HelperMethods.Wait();
                HomeMethods.Reread();
                return true;
            }
            catch(WorkflowException)
            {
                return false; // Worklist is still running
            }
            catch
            {
                //An unknonw error occured
                return false;
            }
        }

        public bool Evaluation()
        {
            try
            {
                CheckAppErrors();
                HelperMethods.Wait();

                //Check to see if the current report is still loading
                IUIItem loadingWorklistMessage = null;
                if(ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                {
                    loadingWorklistMessage = ApplicationManager.ApplicationWindow.Get(EvaluationProperty.Default.StormLoadingWorklistMessage);
                } else if(ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    loadingWorklistMessage = ApplicationManager.ApplicationWindow.Get(EvaluationProperty.Default.AIXLoadingWorklistMessage);
                }

                if (loadingWorklistMessage == null || loadingWorklistMessage.Visible)
                    return false;

                ComboBox ComboWorklists = ApplicationManager.ApplicationWindow.Get<ComboBox>(EvaluationProperty.Default.WorklistPulldown);
                if (ComboWorklists.Enabled)
                {
                    //Open the worklist dropdown
                    ComboWorklists.Expand();

                    //if the click collpases the drop down control then re click it
                    if(ComboWorklists.ExpandCollapseState == ExpandCollapseState.Collapsed)
                    {
                        ComboWorklists.Expand();
                    }

                    Thread.Sleep(800);

                    //if its not at the maximum scroll bar position go down. If it is then go up.
                    var keyboard = ApplicationManager.ApplicationWindow.Keyboard;
                    if (!ComboWorklists.ScrollBars.Vertical.IsNotMinimum)
                    {
                        keyboard.PressSpecialKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.DOWN);
                        HelperMethods.Wait();
                        keyboard.PressSpecialKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.RETURN);
                    }
                    else
                    {
                        keyboard.PressSpecialKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.UP);
                        HelperMethods.Wait();
                        keyboard.PressSpecialKey(TestStack.White.WindowsAPI.KeyboardInput.SpecialKeys.RETURN);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Checks for windows with the name "Application Error" or "Server Communication Lost"
        /// Sets an error flag if error occurs
        /// </summary>
        private void CheckAppErrors()
        {
            try
            {
                List<Window> windows = ApplicationManager.TryGetWindows(3);

                if (windows.Any(x => x.AutomationElement.Current.Name == "Application Error"))
                {
                    //sets error flag to inform parent form.
                    ErrorFlag = true;
                }
                else if (windows.Any(x => x.AutomationElement.Current.Name == "Server Communication Lost"))
                {
                    //sets error flag to inform parent form.
                    ErrorFlag = true;
                }
                else
                {
                    //all clear
                    return;
                }
            }
            catch
            {
                // Failed to get windows
            }
        }

        public void ClickEvalButton()
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);
        }
    }
}
