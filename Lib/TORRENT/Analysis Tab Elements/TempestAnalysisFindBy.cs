﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements
{
    public static class TempestAnalysisFindBy
    {
        /// <summary>
        /// Analysis Tab View button...use to navigate to Analysis Tab
        /// </summary>
        public static readonly By AnalysisTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labNavigationIconAnalysis\"]");

        /// <summary>
        /// Button to Select Levey-Jennings Analysis
        /// </summary>
        public static readonly By LeveyJenningsAnalysisButtonBy = By.XPath("//android.widget.TextView[@content-desc=\"labLevey - Jennings\"]");

        public static readonly By LJControlListBy = By.XPath("//android.view.ViewGroup[@content-desc=\"stackLJControlList\"]/*");

        /// <summary>
        /// Textfield to Select Test to Analyze...opens Select Test Popup
        /// </summary>
        public static readonly By SelectTestTextFieldBy = By.XPath("//android.widget.EditText[@content-desc=\"entSelectedTest\"]");

        /// <summary>
        /// List of Lot Numbers
        /// </summary>
        public static readonly By LotNumberListBy = By.XPath("//android.support.v7.widget.RecyclerView[@content-desc=\"listLotNumbers\"]/*");
        
        /// <summary>
        /// Radio button to select to show All Data for a selected lot
        /// </summary>
        public static readonly By AllDataRadioButtonBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radShowAllData\"]");

        /// <summary>
        /// Radio Button to select Date Range for seleted lot
        /// </summary>
        public static readonly By SelectedDateRangeRadioButtonBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radShowDataSelectedDate\"]");

        /// <summary>
        /// Toggle switch to enable From Date
        /// </summary>
        public static readonly By ToggleFromDateSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiIncludeFromDate\"]");

        /// <summary>
        /// Textfield to enter From Date...shows DatePicker Popup
        /// </summary>
        public static readonly By FromDateDatePickerBy = By.XPath("//android.widget.EditText[@content-desc=\"pckFromDate\"]");

        /// <summary>
        /// Toglge switch to enable To Date
        /// </summary>
        public static readonly By ToggleToDateSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiIncludeToDate\"]");

        /// <summary>
        /// Textfield to enter To Date...shows DatePicker Popup
        /// </summary>
        public static readonly By ToDateDatePickerBy = By.XPath("//android.widget.EditText[@content-desc=\"pckToDate\"]");

        /// <summary>
        /// Generate Report Button
        /// </summary>
        public static readonly By GenerateReportBy = By.XPath("//android.widget.Button[@content-desc=\"butGenerateReport\"]");
    }
}
