﻿
namespace GSD_RnDAutomation.Lib.TORRENT
{
    public static class AndroidDeviceProperty
    {
        /// <summary>
        /// Device Name found in settings under about section. The default is set to a device found on my computer
        /// </summary>
        public static string DeviceName { get; set; } = "AOSP on IA Emulator";
        /// <summary>
        /// Udid for device found by using command 'adb device' in command prompt/terminal
        /// </summary>
        public static string UdId { get; set; } = "emulator-5554";
        /// <summary>
        /// AndroidOS version found on device. Tempest is developed using Android 9
        /// </summary>
        public static string PlatformVersion { get; set; } = "9";
        /// <summary>
        /// OS for device. Tempest is an Android apk
        /// </summary>
        public static string PlatformName { get; set; } = "Android";
    }
}
