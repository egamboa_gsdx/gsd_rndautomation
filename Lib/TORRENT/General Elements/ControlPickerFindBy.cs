﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class ControlPickerFindBy
    {
        /// <summary>
        /// Picker Popup Title
        /// </summary>
        public static readonly By PickerControlTitleBy = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView");

        /// <summary>
        /// List view of selectable Items in picker popup
        /// </summary>
        public static readonly By PickerControlListBy = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView");

        /// <summary>
        /// Cancel button for picker popup
        /// </summary>
        public static readonly By PickerCancelButtonBy = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button");

    }
}
