﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements
{
    /// <summary>
    /// General Elements found within Tempest Instrument Manager UI. Most items are found in Title Bar
    /// </summary>
    public static class TempestGeneralFindBy
    {
        /// <summary>
        /// Find Element for Header Title. Will be used to identify which tab is currently selected.
        /// </summary>
        public static readonly By HeaderTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labSelectedItemTitle\"]");

        /// <summary>
        /// On the main window title bar, this finds the element for identifying currently logged in user
        /// </summary>
        public static readonly By UserName = By.XPath("//android.widget.TextView[@content-desc=\"labLoggedInUser\"]");

        /// <summary>
        /// Instrument Serial Number found on Main Title Bar
        /// </summary>
        public static readonly By InstrumentSerialNumber = By.XPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup/" +
                "android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/" +
                "android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout[1]/" +
                "android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]");

        /// <summary>
        /// Current Date and Time in title bar
        /// </summary>
        public static readonly By CurrentDateTimeBy = By.XPath("//android.widget.TextView[@content-desc=\"labCurrentDateTime\"]");

        /// <summary>
        /// Kebab menu button to show About, Help, Shutdown options
        /// </summary>
        public static readonly By KebabMenuBy = By.XPath("//android.widget.Button[@content-desc=\"butKebabMenu\"]");

        /// <summary>
        /// About found in Kebab Menu
        /// </summary>
        public static readonly By KebabAboutLabelBy = By.XPath("//android.widget.TextView[@content-desc=\"labKebabItemAbout\"]");

        /// <summary>
        /// Help found in Kebab Menu
        /// </summary>
        public static readonly By KebabHelpLabelBy = By.XPath("//android.widget.TextView[@content-desc=\"labKebabItemHelp\"]");

        /// <summary>
        /// Shutdown found in Kebab Menu
        /// </summary>
        public static readonly By KebabShutdownLabelBy = By.XPath("//android.widget.TextView[@content-desc=\"labKebabItemShutdown\"]");

        /// <summary>
        /// Title Bar Status Indicator for Instrument Status
        /// </summary>
        public static readonly By StatusLightBy = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/" +
            "android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView[3]");
    }
}
