﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements
{
    public static class TempestHomeFindBy
    {
        /// <summary>
        /// Home Tab Button -- Use to navigate to Home Tab View
        /// </summary>
        public static readonly By HomeTabBtnBy = By.XPath("//android.widget.TextView[@content-desc=\"labNavigationIconHome\"]");

        /// <summary>
        /// Status open button -- Opens Status List
        /// </summary>
        public static readonly By StatusOpenBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butShowStatusLog\"]");
        
        /// <summary>
        /// Status close button -- Closes Status List
        /// </summary>
        public static readonly By StatusCloseBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butHideStatusLog\"]");

        /// <summary>
        /// Available Sample Positions Text
        /// </summary>
        public static readonly By AvailableSamplePositionsBy = By.XPath("//android.widget.TextView[@content-desc=\"labAvailableSamplePositions\"]");

        /// <summary>
        /// Available Strip Positions Text
        /// </summary>
        public static readonly By AvailableStripPositionsBy = By.XPath("//android.widget.TextView[@content-desc=\"labAvailableStripPositions\"]");
    }
}
