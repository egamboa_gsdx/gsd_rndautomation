﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements
{
    public static class TempestLoadingFindBy
    {
        /// <summary>
        /// Loading Tab Button -- Use to navigate to Loading Tab View
        /// </summary>
        public static readonly By LoadingTabBtnBy = By.XPath("//android.widget.TextView[@content-desc=\"labNavigationIconLoading\"]");

        /// <summary>
        /// Available Sample Positions i.e. Sample Positions: 4/4 Available
        /// </summary>
        public static readonly By AvailableSamplePosition = By.XPath("//android.widget.TextView[@content-desc=\"labAvailableSamplePositions\"]");

        /// <summary>
        /// Available Strip Positions i.e. Strip Positions: 12/12 Available
        /// </summary>
        public static readonly By AvailableStripPosition = By.XPath("//android.widget.TextView[@content-desc=\"labAvailableStripPositions\"]");

        /// <summary>
        /// Sample Position FindBy Elements
        /// </summary>
        #region
        /// <summary>
        /// Sample Position One
        /// </summary>
        #region
        /// <summary>
        /// Button to open and close sample position One
        /// </summary>
        public static  readonly By SamplePositionOneOpenCloseTrayBtnBy = By.XPath("//android.widget.TextView[@content-desc=\"labSamplesTrayOpenClose1\"]");

        /// <summary>
        /// Sample Position One Add Sample Button
        /// </summary>
        public static readonly By SamplePositionOneAddSampleBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butAddSample1\"]");

        /// <summary>
        /// Sample Position One Sample Name
        /// </summary>
        public static readonly By SamplePositionOneSampleNameBy = By.XPath("//android.widget.TextView[@content-desc=\"labSampleID1\"]");
        #endregion

        /// <summary>
        /// Sample Position Two
        /// </summary>
        #region
        ///<summary>
        ///Sample Position Two Open and close tray button
        /// </summary>
        public static readonly By SamplePositionTwoOpenCloseTrayBtnBy = By.XPath("//android.widget.TextView[@content-desc=\"labSamplesTrayOpenClose2\"]");

        /// <summary>
        /// Sample Position Two Add Sample Button
        /// </summary>
        public static readonly By SamplePositionTwoAddSampleBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butAddSample2\"]");

        /// <summary>
        /// Sample Position Two Sample Name
        /// </summary>
        public static readonly By SamplePositionTwoSampleNameBy = By.XPath("//android.widget.TextView[@content-desc=\"labSampleID2\"]");
        #endregion

        /// <summary>
        /// Sample Position Three
        /// </summary>
        #region
        /// <summary>
        /// Sample Position Three Open and Close Tray Button
        /// </summary>
        public static readonly By SamplePositionThreeOpenCloseTrayBtnBy = By.XPath("//android.widget.TextView[@content-desc=\"labSamplesTrayOpenClose3\"]");

        /// <summary>
        /// Sample Position Three Add Sample Button
        /// </summary>
        public static readonly By SamplePositionThreeAddSampleBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butAddSample3\"]");

        /// <summary>
        /// Sample Position Three Sample Name
        /// </summary>
        public static readonly By SamplePositionThreeSampleNameBy = By.XPath("//android.widget.TextView[@content-desc=\"labSampleID3\"]");
        #endregion

        /// <summary>
        /// Sample Position Four
        /// </summary>
        #region
        /// <summary>
        /// Sample Position Four Open and Close Tray Button
        /// </summary>
        public static readonly By SamplePositionFourOpenCloseTrayBtnBy = By.XPath("//android.widget.TextView[@content-desc=\"labSamplesTrayOpenClose4\"]");

        /// <summary>
        /// Sample Position Four Add Sample Button
        /// </summary>
        public static readonly By SamplePositionFourAddSampleBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butAddSample4\"]");

        /// <summary>
        /// Sample Position Four Sample Name
        /// </summary>
        public static readonly By SamplePositionFourSampleNameBy = By.XPath("//android.widget.TextView[@content-desc=\"labSampleID4\"]");
        #endregion
        #endregion

        /// <summary>
        /// String format to get Open/Close Strips tray element
        /// </summary>
        public static readonly string StripPositionOpenCloseTrayString = "//android.widget.TextView[@content-desc=\"labStripsTrayOpenClose{0}\"]";
    }
}
