﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements
{
    public static class TempestLoginFindBy
    {
        /// <summary>
        /// Header for Login Popup View
        /// </summary>
        public static readonly By LoginTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labLoginTitle\"]");
        
        /// <summary>
        /// When logging in, this finds the element to enter the password for login.
        /// </summary>
        public static readonly By PasswordEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entPassword\"]");

        /// <summary>
        /// Enter button to login
        /// </summary>
        public static readonly By enterPasswordBtnBy = By.XPath("//android.widget.Button[@content-desc=\"btnLogin\"]");

        /// <summary>
        /// Pick user dropdown box
        /// </summary>
        public static readonly By PickUserBy = By.XPath("//android.widget.Button[@content-desc=\"pckUsers\"]");
    }
}
