﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.AlignmentMaintenance
{
    public static class TempestAlignmentMaintenanceFindBy
    {
        /// <summary>
        /// Alignment Maintenance
        /// </summary>
        public static readonly By AlignmentMaintenanceBy = By.XPath("//android.widget.TextView[@content-desc=\"labAlignmentMaintenance\"]");

        /// <summary>
        /// Aligh Instrument
        /// </summary>
        public static readonly By AlignInstrumentFrameBy = By.XPath("//android.view.ViewGroup[@content-desc=\"fraAlign instrument\"]");

        /// <summary>
        /// Align Reader
        /// </summary>
        public static readonly By AlignReaderFrameBy = By.XPath("//android.view.ViewGroup[@content-desc=\"fraAlign reader\"]");
    }
}
