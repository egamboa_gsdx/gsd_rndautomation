﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.CalibrationMaintenance
{
    public static class TempestCalibrationMaintenanceFindBy
    {
        /// <summary>
        /// Calibration Maintenance
        /// </summary>
        public static readonly By CalibrationMaintenanceBy = By.XPath("//android.widget.TextView[@content-desc=\"labCalibrationMaintenance\"]");

        /// <summary>
        /// Calibrate Absorbance Reader
        /// </summary>
        public static readonly By CalibrateAbsorbanceReaderFrameBy = By.XPath("//android.view.ViewGroup[@content-desc=\"fraCalibrate absorbance reader\"]");

        /// <summary>
        /// Calibrate Luminescense
        /// </summary>
        public static readonly By CalibrateLuminescenseReaderFrameBy = By.XPath("//android.view.ViewGroup[@content-desc=\"fraCalibrate luminescense reader\"]");
    }
}
