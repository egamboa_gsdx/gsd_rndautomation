﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.GeneralMaintenance
{
    public static  class TempestGeneralMaintenanceFindBy
    {
        /// <summary>
        /// General Maintenance
        /// </summary>
        public static readonly By GeneralMaintenanceBy = By.XPath("//android.widget.TextView[@content-desc=\"labGeneralMaintenance\"]");

        /// <summary>
        /// Home Instrument
        /// </summary>
        public static readonly By HomeInstrumentFrameBy = By.XPath("//android.view.ViewGroup[@content-desc=\"fraHome Instrument\"]");

        /// <summary>
        /// Prime Wash Manifold
        /// </summary>
        public static readonly By PrimeWashManifoldFrameBy = By.XPath("//android.view.ViewGroup[@content-desc=\"fraPrime Wash Manifold\"]");

        /// <summary>
        /// Shake Strips
        /// </summary>
        public static readonly By ShakeStripsFrameBy = By.XPath("//android.view.ViewGroup[@content-desc=\"fraShake strips\"]");
    }
}
