﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements
{
    public static class TempestMaintenanceFindBy
    {
        /// <summary>
        /// Maintenance Tab View Button...use to navigate to Maintenance Tab
        /// </summary>
        public static readonly By MaintenanceTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labNavigationIconMaintenance\"]");
    }
}
