﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class AddSampleManuallyPopupFindBy
    {
        /// <summary>
        /// Add Sample Manually Title
        /// </summary>
        public static readonly By AddSampleManuallyTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labAddSampleManuallyTitle\"]");

        /// <summary>
        /// Add Sample Manually Text Entry
        /// </summary>
        public static readonly By ManuallyAddSampleEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entManuallyAddSampleEnterSampleID\"]");

        /// <summary>
        /// Manually Add Sample Ok Button
        /// </summary>
        public static readonly By ManuallyAddSampleOkBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butManuallyAddSampleOk\"]");

        /// <summary>
        /// Manually Add Sample Cancel Button
        /// </summary>
        public static readonly By ManuallyAddSampleCancelBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butManuallyAddSampleCancel\"]");
    }
}
