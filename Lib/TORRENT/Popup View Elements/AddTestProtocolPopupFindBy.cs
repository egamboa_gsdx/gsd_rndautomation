﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class AddTestProtocolPopupFindBy
    {
        /// <summary>
        /// Add Test Protocol Title
        /// </summary>
        public static readonly By AddTestProtocolTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labAddTestProtocolPopupTitle\"]");

        /// <summary>
        /// Add Test Protocol Message
        /// </summary>
        public static readonly By AddTestProtocolMessageBy = By.XPath("//android.widget.TextView[@content-desc=\"labAddTestProtocolMessage\"]");

        /// <summary>
        /// Cancel Add Test Protocol Button
        /// </summary>
        public static readonly By CancelAddTestProtocolBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butCancel\"]");
    }
}
