﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class ConfigureFileSharingPopupFindBy
    {
        /// <summary>
        /// Configure File Share Popup Title
        /// </summary>
        public static readonly By ConfigureFileSharingTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labConfigureFileShareTitle\"]");

        /// <summary>
        /// File Share Name Text Entry
        /// </summary>
        public static readonly By ShareNameEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entShareName\"]");

        /// <summary>
        /// File Share Uri Text Entry
        /// </summary>
        public static readonly By ShareUriEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entShareUri\"]");

        /// <summary>
        /// File Share Domain Name Text Entry
        /// </summary>
        public static readonly By DomainEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entDomain\"]");

        /// <summary>
        /// Username Text Entry
        /// </summary>
        public static readonly By UsernameEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entUserName\"]");

        /// <summary>
        /// Password Text Entry
        /// </summary>
        public static readonly By PasswordEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entPassword\"]");

        /// <summary>
        /// Add File Share Connection Button
        /// </summary>
        public static readonly By AddFileShareConBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butAddFileShareConnection\"]");

        /// <summary>
        /// If no file share is connected, this message is displayed
        /// </summary>
        public static readonly By NoFileShareConnectionMessageBy = By.XPath("//android.widget.TextView[@content-desc=\"labNoFileShareCon\"]");

        /// <summary>
        /// Close File Share Popup
        /// </summary>
        public static readonly By CloseFileShareBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butClose\"]");
    }
}
