﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class ConfigurePrinterPopupFindBy
    {
        /// <summary>
        /// Configure Printer Popup Title
        /// </summary>
        public static readonly By ConfigurePrinterTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labConfigureNetworkPrinterTitle\"]");

        /// <summary>
        /// List of Available Printer Names
        /// </summary>
        public static readonly By PrinterNameListBy = By.XPath("//android.support.v7.widget.RecyclerView[@content-desc=\"listPrinterNames\"]");

        /// <summary>
        /// List of Available Printer Uris
        /// </summary>
        public static readonly By PrinterUriListBy = By.XPath("//android.support.v7.widget.RecyclerView[@content-desc=\"listPrinterUri\"]");

        /// <summary>
        /// Add Printer Button
        /// </summary>
        public static readonly By AddPrinterBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butAddPrinter\"]");

        /// <summary>
        /// Cancel Configure Printer Button
        /// </summary>
        public static readonly By CancelAddPrinterBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butCancel\"]");
    }
}
