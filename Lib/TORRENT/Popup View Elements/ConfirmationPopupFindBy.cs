﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class ConfirmationPopupFindBy
    {
        /// <summary>
        /// Confirmation Popup Title
        /// </summary>
        public static readonly By ConfirmationPopupTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labConfirmationTitle\"]");

        /// <summary>
        /// Confirmation Popup Message
        /// </summary>
        public static readonly By ConfirmationPopupMessageBy = By.XPath("//android.widget.TextView[@content-desc=\"labConfirmationMessage\"]");

        /// <summary>
        /// Confirmation Popup Ok Button - Button Text varies on confirmation view
        /// </summary>
        public static readonly By ConfirmationOkBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butConfirmationOk\"]");

        /// <summary>
        /// Confirmation Popup Cancel Button - Button Text varies on confirmation view
        /// </summary>
        public static readonly By ConfirmationCancelBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butConfirmationCancel\"]");
    }
}
