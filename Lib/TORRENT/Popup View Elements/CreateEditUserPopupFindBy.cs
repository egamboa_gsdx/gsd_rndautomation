﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class CreateEditUserPopupFindBy
    {
        /// <summary>
        /// Header title of Add/Edit User Popup
        /// </summary>
        public static readonly By CreateEditUserTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labCreateEditUserTitle\"]");

        /// <summary>
        /// Username Text Entry
        /// </summary>
        public static readonly By UsernameEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entUsername\"]");

        /// <summary>
        /// Password Text Entry
        /// </summary>
        public static readonly By PasswordEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entPassword\"]");

        /// <summary>
        /// ReEntered Password Text Entry
        /// </summary>
        public static readonly By ReEnteredPasswordEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entReEnteredPassword\"]");

        /// <summary>
        /// Email Address Text Entry
        /// </summary>
        public static readonly By EmailAddressEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entEmailAddress\"]");

        /// <summary>
        /// Phone Number Text Entry
        /// </summary>
        public static readonly By PhoneNumberEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entPhoneNumber\"]");

        /// <summary>
        /// User Level Picker
        /// </summary>
        public static readonly By UserLevelPickerBy = By.XPath("//android.view.ViewGroup[@content-desc=\"pckUserLevel_Container\"]");

        /// <summary>
        /// Toggle Active User
        /// </summary>
        public static readonly By ActiveUserSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiIsActive\"]");

        /// <summary>
        /// Ok Button
        /// </summary>
        public static readonly By OkUserButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butCreateEditUserOk\"]");

        /// <summary>
        /// Cancel Button
        /// </summary>
        public static readonly By CancelUserButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butCreateEditUserCancel\"]");

    }
}
