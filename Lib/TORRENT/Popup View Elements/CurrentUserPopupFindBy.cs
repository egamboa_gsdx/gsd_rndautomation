﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class CurrentUserPopupFindBy
    {
        /// <summary>
        /// Current User Popup Title
        /// </summary>
        public static readonly By CurrentUserTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labCurrentUserTitle\"]");

        /// <summary>
        /// Current User logged in UserName
        /// </summary>
        public static readonly By CurrentUserNameBy = By.XPath("//android.widget.TextView[@content-desc=\"labCurrentUserName\"]");

        /// <summary>
        /// Current User logged in UserRole
        /// </summary>
        public static readonly By CurrentUserRoleBy = By.XPath("//android.widget.TextView[@content-desc=\"labCurrentUserRole\"]");

        /// <summary>
        /// Currenter user logged in Email Address
        /// </summary>
        public static readonly By CurrentUserEmailBy = By.XPath("//android.widget.TextView[@content-desc=\"labCurrentUserEmailAddress\"]");

        /// <summary>
        /// Current user logged in phone number
        /// </summary>
        public static readonly By CurrentUserPhoneNumberBy = By.XPath("//android.widget.TextView[@content-desc=\"labCurrentUserPhoneNum\"]");

        /// <summary>
        /// Logout of Current User
        /// </summary>
        public static readonly By CurrenterUserLogoutBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butCurrentUserLogout\"]");

        /// <summary>
        /// Close Current User popup
        /// </summary>
        public static readonly By CurrenterUserCloseBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butCurrentUserClose\"]");
    }
}
