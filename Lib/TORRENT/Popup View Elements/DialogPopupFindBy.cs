﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class DialogPopupFindBy
    {
        /// <summary>
        /// Dialog Popup Title
        /// </summary>
        public static readonly By DialogPopupTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labDialogPopupTitle\"]");

        /// <summary>
        /// Dialog Popup Message
        /// </summary>
        public static readonly By DialogPopupMessageBy = By.XPath("//android.widget.TextView[@content-desc=\"labDialogPopupMessage\"]");

        /// <summary>
        /// Dialog Popup Ok Button
        /// </summary>
        public static readonly By DialogOkBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butDialogOk\"]");
    }
}
