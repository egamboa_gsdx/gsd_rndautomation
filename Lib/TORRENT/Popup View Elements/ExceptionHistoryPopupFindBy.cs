﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class ExceptionHistoryPopupFindBy
    {
        /// <summary>
        ///  Exception History Popup Title
        /// </summary>
        public static readonly By ExceptionHistoryTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labExceptionHistoryTitle\"]");

        /// <summary>
        /// Exception History List
        /// </summary>
        public static readonly By ExceptionHistoryListBy = By.XPath("//android.widget.ListView[@content-desc=\"listFatalExceptions\"]");

        /// <summary>
        /// View Selected Exception Button
        /// </summary>
        public static readonly By ViewExceptionBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butView\"]");

        /// <summary>
        /// Save Selected Exception Button
        /// </summary>
        public static readonly By SaveExceptionBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butSave\"]");

        /// <summary>
        /// Delete Selected Exception Button
        /// </summary>
        public static readonly By DeleteExceptionBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butDelete\"]");

        /// <summary>
        /// Close Exception History Popup
        /// </summary>
        public static readonly By CloseExceptionHistoryBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butClose\"]");
    }
}
