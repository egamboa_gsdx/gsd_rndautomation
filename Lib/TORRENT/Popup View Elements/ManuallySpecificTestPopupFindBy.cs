﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class ManuallySpecificTestPopupFindBy
    {
        /// <summary>
        /// Manually Specific Test Title
        /// </summary>
        public static readonly By ManuallySpecificTestTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labManuallySpecificTest\"]");

        /// <summary>
        /// Manually Specific Test Message
        /// </summary>
        public static readonly By ManuallySpecificTestMessageBy = By.XPath("//android.widget.TextView[@content-desc=\"labManuallySpecificTestMessage\"]");

        /// <summary>
        /// Cancel Manually Specific Test
        /// </summary>
        public static readonly By CancelAddTestBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butCancelAddTestStrip\"]");
    }
}
