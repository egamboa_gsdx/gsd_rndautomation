﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class OpenFileDialogPopupFindBy
    {
        /// <summary>
        /// Open File Dialog Popup Title
        /// </summary>
        public static readonly By OpenFileDialogTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labOpenFileDialog\"]");

        /// <summary>
        /// Up File Tree Button
        /// </summary>
        public static readonly By UpFileTreeBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butUpFileTree\"]");

        /// <summary>
        /// File name Text Entry
        /// </summary>
        public static readonly By FileNameEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entSelectFile\"]");

        /// <summary>
        /// Files and Folders List
        /// </summary>
        public static readonly By FilesFoldersListBy = By.XPath("//android.support.v7.widget.RecyclerView[@content-desc=\"listFilesFolders\"]");

        /// <summary>
        /// Open File Dialog Ok Button
        /// </summary>
        public static readonly By OpenFileOkBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butOpenFileOk\"]");

        /// <summary>
        /// Refresh File/Folder List Button
        /// </summary>
        public static readonly By RefreshFileBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butOpoenFileRefresh\"]");

        /// <summary>
        /// Cancel Open File Dialog Popup
        /// </summary>
        public static readonly By CancelOpenFileBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butOpenFileCancel\"]");
    }
}
