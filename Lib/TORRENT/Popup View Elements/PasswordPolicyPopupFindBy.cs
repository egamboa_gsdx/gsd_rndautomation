﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class PasswordPolicyPopupFindBy
    {
        /// <summary>
        /// Password Policy Popup Title
        /// </summary>
        public static readonly By PasswordPolicyTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labCreateEditUserTitle\"]");

        /// <summary>
        /// Password Min Length Text Entry
        /// </summary>
        public static readonly By PasswordMinLengthEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entMinPasswordLength\"]");

        /// <summary>
        /// Allowable Character Text Entry
        /// </summary>
        public static readonly By AllowableCharEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entAllowableSpecialChars\"]");

        /// <summary>
        /// Toggle Require One or More Special Characters
        /// </summary>
        public static readonly By RequireSpecialCharSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiRequireOneOrMoreSpecialChars\"]");

        /// <summary>
        /// Toggle Require One or More Numerical Digit
        /// </summary>
        public static readonly By RequireNumericalDigitSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiRequireOneOrMoreNumericalDigits\"]");

        /// <summary>
        /// Toggle Allow Spaces
        /// </summary>
        public static readonly By AllowSpacesSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiAllowSpaces\"]");

        /// <summary>
        /// Max Login Attempts
        /// </summary>
        public static readonly By MaxLoginAttemptEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entMaxLoginAttempts\"]");

        /// <summary>
        /// Lockout duration (minutes)
        /// </summary>
        public static readonly By LockoutDurationEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entLockoutDuration\"]");

        /// <summary>
        /// Toggle Password Reset Periodcally
        /// </summary>
        public static readonly By ResetPasswordPeriodicallySwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiResetPasswordPeriodically\"]");

        /// <summary>
        /// Password Expiration Text Entry
        /// </summary>
        public static readonly By PasswordExpirationEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entPasswordExpiration\"]");

        /// <summary>
        /// Ok Button
        /// </summary>
        public static readonly By PasswordPolicyOkButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butPasswordPolicyOk\"]");

        /// <summary>
        /// Cancel Button
        /// </summary>
        public static readonly By PasswordPolicyCancelButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butPasswordPolicyCancel\"]");
    }
}
