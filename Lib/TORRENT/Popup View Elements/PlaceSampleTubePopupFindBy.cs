﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class PlaceSampleTubePopupFindBy
    {
        /// <summary>
        /// Place Sample Tube Popup Title
        /// </summary>
        public static readonly By PlaceSampleTubeTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labPlaceSampleTubeTitle\"]");

        /// <summary>
        /// Toggle Sample as Stat
        /// </summary>
        public static readonly By MarkSampleStatSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiMarkSampleStat\"]");

        /// <summary>
        /// Load Sample Button
        /// </summary>
        public static readonly By LoadSampleTubeBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butLoadSampleTube\"]");

        /// <summary>
        /// Cancel Load Sample Button
        /// </summary>
        public static readonly By LoadSampleCancelTubeBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butCancelLoadSampleTube\"]");
    }
}
