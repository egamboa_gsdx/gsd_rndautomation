﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class PrimeWashManifoldPopupFindBy
    {
        /// <summary>
        /// Prime Wash Manifold popup Title
        /// </summary>
        /// e.g.
        public static readonly By PrimeWashManifoldTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labPrimeWashManifoldTitle\"]");

        /// <summary>
        /// Start Priming Button
        /// </summary>
        /// e.g.
        public static readonly By PrimingStartBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butPrimingStart\"]");

        /// <summary>
        /// Stop Priming Button
        /// </summary>
        /// e.g.
        public static readonly By PrimingStopBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butPrimingStop\"]");

        /// <summary>
        /// Close Prime Wash Manifold Popup
        /// </summary>
        /// e.g.
        public static readonly By PrimeWashManifoldCloseBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butPrimingClose\"]");

        /// <summary>
        /// Text Entry for Priming Cycle -- is read only
        /// </summary>
        /// e.g.
        public static readonly By PrimingCyclesBy = By.XPath("//android.widget.EditText[@content-desc=\"entPrimingCycles \"]");

        /// <summary>
        /// Decrement Stepper button to decrease Priming Cycles
        /// </summary>
        /// e.g.
        public static readonly By PrimingCyclesDecrementStepperBy = By.XPath("//android.widget.LinearLayout[@content-desc=\"stePrimingCycles\"]/android.widget.Button[@content-desc=\"−\"]");

        /// <summary>
        /// Increment Stepper button to increase Priming Cycles
        /// </summary>
        /// e.g.
        public static readonly By PrimingCyclesIncrementStepperBy = By.XPath("//android.widget.LinearLayout[@content-desc=\"stePrimingCycles\"]/android.widget.Button[@content-desc=\"+\"]");

        /// <summary>
        /// string format to get checkbox for associated wash bottle
        /// </summary>
        /// e.g.
        public static readonly string PrimeWashBottleCheckBoxString = "//android.widget.CheckBox[@content-desc=\"cheWashBottle{0}\"]";
    }
}
