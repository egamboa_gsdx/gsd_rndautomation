﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class ScanSamplePopupFindBy
    {
        /// <summary>
        /// Scan Add Sample Title
        /// </summary>
        public static readonly By ScanSampleTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labScanAddSampleTitle\"]");

        /// <summary>
        /// Manually Enter Sample Button
        /// </summary>
        public static readonly By ManuallyEnterSampleBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butManuallyEnter\"]");

        /// <summary>
        /// Cancel Scan Sample
        /// </summary>
        public static readonly By CancelScanSampleBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butCancelScanSample\"]");
    }
}
