﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class SelectionPopupFindBy
    {
        /// <summary>
        /// Selection Popup Title
        /// </summary>
        public static readonly By SelectionTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labSelectionHeader\"]");

        /// <summary>
        /// List of Selection Items
        /// </summary>
        public static readonly By SelectionListBy = By.XPath("//android.view.ViewGroup[@content-desc=\"listSelection\"]");

        /// <summary>
        /// List of Selection Enum Items
        /// </summary>
        public static readonly By SelectionEnumListBy = By.XPath("//android.view.ViewGroup[@content-desc=\"listEnumSelection\"]");

        /// <summary>
        /// Cancel Selection Button
        /// </summary>
        public static readonly By SelectionCancelBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butCancelSelection\"]");
    }
}
