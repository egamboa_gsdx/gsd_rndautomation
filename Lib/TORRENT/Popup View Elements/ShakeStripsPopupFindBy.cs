﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class ShakeStripsPopupFindBy
    {
        /// <summary>
        /// Shake Strips Popup Title
        /// </summary>
        public static readonly By ShakeStripsTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labShakeStripsTitle\"]");

        /// <summary>
        /// List of Available Strips
        /// </summary>
        public static readonly By ShakeStripsListBy = By.XPath("//android.widget.ListView[@content-desc=\"listStrips\"]");

        /// <summary>
        /// Button to turn off all shakers
        /// </summary>
        public static readonly By TurnOffAllShakersButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butTurnOffAllShakers\"]");

        /// <summary>
        /// Close Shake Strip Popup View
        /// </summary>
        public static readonly By ShakeStripCloseButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butShakeStripsClose\"]");

        /// <summary>
        /// string format to grab specific toggle switch if index is known
        /// </summary>
        public static readonly string ShakeStripSwitchString = "//android.widget.Switch[@content-desc=\"swiShakeStrip{0}\"]";

        /// <summary>
        /// string format to grab specific RPM slider
        /// </summary>
        public static readonly string ShakeStripRPMSliderString = "//android.widget.SeekBar[@content-desc=\"sliRPMStrip{0}\"]";
    }
}
