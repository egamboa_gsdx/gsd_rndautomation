﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Popups
{
    public static class TextInputPopupFindBy
    {
        /// <summary>
        /// Text Input Popup title
        /// </summary>
        public static readonly By TextInputTitleBy = By.XPath("//android.widget.TextView[@content-desc=\"labTextInputTitle\"]");

        /// <summary>
        /// Text Input text entry
        /// </summary>
        public static readonly By TextInputEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entTextInput\"]");

        /// <summary>
        /// Text Input Ok Button
        /// </summary>
        public static readonly By TextInputOkBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butTextInputOk\"]");

        /// <summary>
        /// Text Input Cancel Button
        /// </summary>
        public static readonly By TextInputCancelBtnBy = By.XPath("//android.widget.Button[@content-desc=\"butTextInputCancel\"]");
    }
}
