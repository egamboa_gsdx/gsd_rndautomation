﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements
{
    public static class TempestResourcesFindBy
    {
        /// <summary>
        /// Resource Tab button - use to navigate to Resources Tab View
        /// </summary>
        public static readonly By ResourceTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labNavigationIconResources\"]");

        /// <summary>
        /// Display Available disposable tips i.e. Disposable Tips: 96/96 Available
        /// </summary>
        public static readonly By AvailableDisposalbeTipsBy = By.XPath("//android.widget.TextView[@content-desc=\"labDisposableTipsAvailable\"]");

        /// <summary>
        /// Internal Temperature Reading
        /// </summary>
        public static readonly By InteralTemperatureBy = By.XPath("//android.widget.TextView[@content-desc=\"labInternalTemp\"]");

        /// <summary>
        /// Incubator Temperature Reading
        /// </summary>
        public static readonly By IncubatorTemperatureBy = By.XPath("//android.widget.TextView[@content-desc=\"labIncubatorTemp\"]");

        /// <summary>
        /// string format to get WashWasteBottle percentage
        /// </summary>
        public static readonly string WashWasteBottle1VolumePercentString = "//android.widget.TextView[@content-desc=\"labFluidPercentageWaste{0}\"]";

        /// <summary>
        /// string format to get WashWasteBottle name
        /// </summary>
        public static readonly string WashWasteBottleNameString = "//android.widget.TextView[@content-desc=\"labBottleName{0}\"]";

        /// <summary>
        /// string format to get WashWasteBottle Change Button
        /// </summary>
        public static readonly string WashWasteBottleChangeButtonString = "//android.widget.Button[@content-desc=\"butChangeBottle{0}\"]";
    }
}
