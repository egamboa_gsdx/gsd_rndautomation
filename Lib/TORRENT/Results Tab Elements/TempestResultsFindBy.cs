﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements
{
    public static class TempestResultsFindBy
    {
        /// <summary>
        /// Results Tab View button...use to navigate to Results Tab
        /// </summary>
        public static readonly By ResultsTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labNavigationIconResults\"]");

        /// <summary>
        /// Checkbox to search by Sample ID
        /// </summary>
        public static readonly By SearchSampleIdCheckboxBy = By.XPath("//android.widget.CheckBox[@content-desc=\"cheSearchOnSampleId\"]");

        /// <summary>
        /// Textfield to search by Sample ID
        /// </summary>
        public static readonly By SearchSampleIDTextEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entSearchOnSampleId\"]");

        /// <summary>
        /// Checkbox to search by Test
        /// </summary>
        public static readonly By SearchTestCheckboxBy = By.XPath("//android.widget.CheckBox[@content-desc=\"cheSearchOnTest\"]");

        /// <summary>
        /// Button to open up Select Test Popup view
        /// </summary>
        public static readonly By SearchTestPickerBy = By.XPath("//android.widget.Button[@content-desc=\"pckSearchOnTest\"]");

        /// <summary>
        /// Checkbox to search by operator
        /// </summary>
        public static readonly By SearchOperatorCheckboxBy = By.XPath("//android.widget.CheckBox[@content-desc=\"cheSearchOnOperator\"]");

        /// <summary>
        /// Button to open Select Operator Popup view
        /// </summary>
        public static readonly By SearchOperatorButtonBy = By.XPath("//android.widget.Button[@content-desc=\"pckSearchOnOperator\"]");

        /// <summary>
        /// Checkbox to search From Date
        /// </summary>
        public static readonly By SearchFromDateCheckboxBy = By.XPath("//android.widget.CheckBox[@content-desc=\"cheSearchOnFromDate\"]");

        /// <summary>
        /// Textfield to enter From Date...opens a DatePicker popup
        /// </summary>
        public static readonly By SearchFromDateTextBy = By.XPath("//android.widget.EditText[@content-desc=\"datSearchOnFromDate\"]");

        /// <summary>
        /// Checkbox to search To Date
        /// </summary>
        public static readonly By SearchToDateCheckboxBy = By.XPath("//android.widget.CheckBox[@content-desc=\"cheSearchOnToDate\"]");

        /// <summary>
        /// Textfield to enter To Date...opens a Datepicker popup
        /// </summary>
        public static readonly By SearchToDateTextBy = By.XPath("//android.widget.EditText[@content-desc=\"datSearchOnToDate\"]");

        /// <summary>
        /// Button to refresh Search Results
        /// </summary>
        public static readonly By RefreshResultsListBy = By.XPath("//android.widget.TextView[@content-desc=\"labRefresh\"]");

        /// <summary>
        /// Print Report Button
        /// </summary>
        public static readonly By PrintReportButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butPrintReport\"]");

        /// <summary>
        /// Export Report Button
        /// </summary>
        public static readonly By ExportReportButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butExportReprot\"]");
    }
}
