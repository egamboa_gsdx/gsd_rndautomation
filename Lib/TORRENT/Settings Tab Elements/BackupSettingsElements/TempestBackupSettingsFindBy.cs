﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Settings
{
    public static class TempestBackupSettingsFindBy
    {
        /// <summary>
        /// Navigate to Backup Settings
        /// </summary>
        public static readonly By BackupSettingsTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labBackupSettings\"]");

        /// <summary>
        /// Button to create Backup
        /// </summary>
        public static readonly By CreateBackupButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butCreateBackup\"]");

        /// <summary>
        /// button to restore backup
        /// </summary>
        public static readonly By RestoreBackupButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butRestoreBackup\"]");

        /// <summary>
        /// Toggle Scheduled Backup Enabled
        /// </summary>
        public static readonly By ToggleScheduledBackupEnabledBy = By.XPath("//android.widget.Switch[@content-desc=\"swiRunScheduledBackup\"]");

        /// <summary>
        /// Time picker for backup time
        /// </summary>
        public static readonly By BackupTimePickerBy = By.XPath("//android.widget.EditText[@content-desc=\"pckBackupTime\"]");

        /// <summary>
        /// Daily Backup Elements
        /// </summary>
        #region
        /// <summary>
        /// Daily Backup Radio Button
        /// </summary>
        public static readonly By DailyBackupRadBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radDailyBackup\"]");

        /// <summary>
        /// Toggle Daily Scheduled Backups on Sunday
        /// </summary>
        public static readonly By DayOfWeekSundayBY = By.XPath("//android.widget.Switch[@content-desc=\"swiDayOfWeekSunday\"]");

        /// <summary>
        /// Toggle Daily Scheduled Backups on Monday
        /// </summary>
        public static readonly By DayOfWeekMondayBy = By.XPath("//android.widget.Switch[@content-desc=\"swiDayOfWeekMonday\"]");

        /// <summary>
        /// Toggle Daily Scheduled Backups on Tuesday
        /// </summary>
        public static readonly By DayOfWeekTuesdayBy = By.XPath("//android.widget.Switch[@content-desc=\"swiDayOfWeekTuesday\"]");

        /// <summary>
        /// Toggle Daily Scheduled Backups on Wednesday
        /// </summary>
        public static readonly By DayOfWeekWednesdayBy = By.XPath("//android.widget.Switch[@content-desc=\"swiDayOfWeekWednesday\"]");

        /// <summary>
        /// Toggle Daily Scheduled Backups on Thursday
        /// </summary>
        public static readonly By DayOfWeekThursdayBy = By.XPath("//android.widget.Switch[@content-desc=\"swiDayOfWeekThursday\"]");

        /// <summary>
        /// Toggle Daily Scheduled Backups on Friday
        /// </summary>
        public static readonly By DayOfWeekFridayBy = By.XPath("//android.widget.Switch[@content-desc=\"swiDayOfWeekFriday\"]");

        /// <summary>
        /// Toggle Daily Scheduled Backups on Saturday
        /// </summary>
        public static readonly By DayOfWeekSaturdayBy = By.XPath("//android.widget.Switch[@content-desc=\"swiDayOfWeekSaturday\"]");

        /// <summary>
        /// Select all days of week for daily scheduled backup
        /// </summary>
        public static readonly By SelectDayOfWeekButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butSelectAll\"]");
        #endregion

        ///<summary>
        ///Weekly Backup Elements
        /// </summary>
        #region
        ///<summary>
        ///Weekly Backup
        /// </summary>
        public static readonly By WeeklyBackupRadBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radWeeklyBackup\"]");

        public static readonly By BackupEveryWeekOnBy = By.XPath("//android.widget.EditText[@content-desc=\"entBackupEveryWeekOn\"]");
        #endregion

        ///<summary>
        ///Monthly Backup Elements
        /// </summary>
        #region
        ///<summary>
        ///Monthly Backup Radio Button
        /// </summary>
        public static readonly By MonthlyBackupRadBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radMonthlyBackup\"]");

        /// <summary>
        /// Radio Button To choose day of Month Backup
        /// </summary>
        public static readonly By DayOfMonthBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radBackuPDayofMonth\"]" +
            "/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ImageView");

        /// <summary>
        /// Radio button to choose Weekday of Month
        /// </summary>
        public static readonly By WeekdayOfMonthBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radBackupWeekdayOfMonth\"]" +
            "/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView");

        /// <summary>
        /// Picker to choose Weeknumber of the month
        /// </summary>
        public static readonly By MonthlyWeekNumberBackupPickerBy = By.XPath("//android.widget.EditText[@content-desc=\"entMonthlyWeekNum\"]");

        /// <summary>
        /// Picker to choose weekday of the month
        /// </summary>
        public static readonly By MonthlyWeekDayBackupPickerBy = By.XPath("//android.widget.EditText[@content-desc=\"entMonthlyWeekDay\"]");
        #endregion

        /// <summary>
        /// Text entry for backup file path
        /// </summary>
        public static readonly By BackupPathTextEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entScheduledBackupPath\"]");

        /// <summary>
        /// Change back up file path
        /// </summary>
        public static readonly By ChangeBackupPathButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butChangeBackupPath\"]");

        /// <summary>
        /// Persist Scheduled Backup Settings
        /// </summary>
        public static readonly By PersistBackupSettingsBy = By.XPath("//android.widget.Button[@content-desc=\"butPersistBackupSettings\"]");
    }
}
