﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Settings
{
    public static class TempestGeneralSettingsFindBy
    {
        /// <summary>
        /// View General Settings
        /// </summary>
        public static readonly By GeneralSettingsTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labGeneralSettings\"]");

        /// <summary>
        /// Select Language
        /// </summary>
        public static readonly By SelectLanguagePickerBy = By.XPath("//android.widget.EditText[@content-desc=\"entSelectedLanguage\"]");

        /// <summary>
        /// Change keyboard settings - opens keyboard settings on device
        /// </summary>
        public static readonly By ChangeKeyboardSettingsBy = By.XPath("//android.widget.Button[@content-desc=\"butChangeKeyboardSettings\"]");

        /// <summary>
        /// Set Date & Time
        /// </summary>
        public static readonly By SetDateTimeBy = By.XPath("//android.widget.Button[@content-desc=\"butSetDateTimeSettings\"]");

        /// <summary>
        /// Change logo
        /// </summary>
        public static readonly By SelectLogoBy = By.XPath("//android.widget.Button[@content-desc=\"butSelectLogo\"]");

        /// <summary>
        /// Address line 1
        /// </summary>
        public static readonly By AddressEntry1By = By.XPath("//android.widget.EditText[@content-desc=\"entAddressField1\"]");

        /// <summary>
        /// Address line 2
        /// </summary>
        public static readonly By AddressEntry2By = By.XPath("//android.widget.EditText[@content-desc=\"entAddressField2\"]");

        /// <summary>
        /// Address line 3
        /// </summary>
        public static readonly By AddressEntry3By = By.XPath("//android.widget.EditText[@content-desc=\"entAddressField3\"]");

        /// <summary>
        /// Address line 4
        /// </summary>
        public static readonly By AddressEntry4By = By.XPath("//android.widget.EditText[@content-desc=\"entAddressField4\"]");

        /// <summary>
        /// Address line 5
        /// </summary>
        public static readonly By AddressEntry5By = By.XPath("//android.widget.EditText[@content-desc=\"entAddressField5\"]");

        /// <summary>
        /// Configure Network Printer
        /// </summary>
        public static readonly By ConfigureNetworkPrinterBy = By.XPath("//android.widget.Button[@content-desc=\"butConfigureNetworkPrinter\"]");

        /// <summary>
        /// Configure File Sharing
        /// </summary>
        public static readonly By ConfigureFileSharingBy = By.XPath("//android.widget.Button[@content-desc=\"butConfigureFileSharing\"]");

        /// <summary>
        /// Browse Software Error history
        /// </summary>
        public static readonly By BrowseErrorHistoryBy = By.XPath("//android.widget.Button[@content-desc=\"butBrowseErrorHistory\"]");

        /// <summary>
        /// Main Log Export
        /// </summary>
        public static readonly By MainLogExportBy = By.XPath("//android.widget.Button[@content-desc=\"butMainLogExport\"]");

        /// <summary>
        /// Export index 1 discovered instrument logs - subject to change
        /// </summary>
        public static readonly By ExportInstrumentLogBy = By.XPath("//android.widget.Button[@content-desc=\"butInstr1ExportLogs\"]");

        /// <summary>
        /// Change index 1 discovered instrument's serial number
        /// </summary>
        public static readonly By ChangeInstrumentSerialNumberBy = By.XPath("//android.widget.Button[@content-desc=\"butChangeInstr1SerialNum\"]");
    }
}
