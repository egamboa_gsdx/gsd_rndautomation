﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Settings
{
    public static class TempestLISSettingsFindBy
    {
        /// <summary>
        /// LIS Settings Tab View
        /// </summary>
        public static readonly By LISSettingsTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labLISSettings\"]");

        /// <summary>
        /// Toggle switch to Enable LIS
        /// </summary>
        public static readonly By ToggleEnableLISSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiLisEnabled\"]");

        /// <summary>
        /// Button to Select LIS Protocol
        /// </summary>
        public static readonly By LISProtocolButtonBy = By.XPath("//android.widget.Button[@content-desc=\"pckLISProtocol\"]");

        /// <summary>
        /// Sliding bar to set LIS Timeout - cannot be less than 30 seconds or greater than 120 seconds
        /// binded to Timeout textfield
        /// </summary>
        public static readonly By LISTimeoutSliderBy = By.XPath("//android.widget.SeekBar[@content-desc=\"sliLISTimeout\"]");

        /// <summary>
        /// Textfield to enter LIS Timeout - binded to Seek bar
        /// </summary>
        public static readonly By LISTimeoutSecondsTextFieldBy = By.XPath("//android.widget.EditText[@content-desc=\"entLISTimeout\"]");

        /// RS-232 connection settings elements
        #region
        /// <summary>
        /// Radio button to select RS-232 LIS connection
        /// </summary>
        public static readonly By UseRS232ConnectionRadioButtonBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radRS232Connection\"]");

        /// <summary>
        /// Button to Select a Baud Rate - displays currently selected Baud Rate - opens Select Baud Rate Popup View
        /// </summary>
        public static readonly By BaudRatePickerBy = By.XPath("//android.widget.Button[@content-desc=\"pckBaudRates\"]");

        /// <summary>
        /// Button to select data bits - displays currently selected Data bits - opens Select Data Bits Popup view
        /// </summary>
        public static readonly By DataBitsPickerBy = By.XPath("//android.widget.Button[@content-desc=\"pckDataBits\"]");

        /// <summary>
        /// Button to select Flow Control - displays currently selected Flow control - opens Select Flow Control Popup view
        /// </summary>
        public static readonly By FlowControlPickerBy = By.XPath("//android.widget.Button[@content-desc=\"pckFlowControls\"]");

        /// <summary>
        /// Button to select Parity - displays currently selected parity - opens Select Parity Popup view
        /// </summary>
        public static readonly By ParityPickerBy = By.XPath("//android.widget.Button[@content-desc=\"pckParity\"]");

        /// <summary>
        /// Button to select stop bits - displays currently selected stop bit - opens Select Stop Bit Popup view
        /// </summary>
        public static readonly By StopBitsPickerBy = By.XPath("//android.widget.Button[@content-desc=\"pckStopBits\"]");
        #endregion

        /// TCP/IP connection settings elements
        #region
        /// <summary>
        /// Radio button to select TCP/IP LIS Connection
        /// </summary>
        public static readonly By UseTCPIPConnectionRadioButtonBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radTCPIPConnection\"]");

        /// <summary>
        /// Text entry for first part of server address
        /// </summary>
        public static readonly By ServerAddressTextEntryBy1 = By.XPath("//android.widget.EditText[@content-desc=\"entServerAddressPart1\"]");

        /// <summary>
        /// Text entry for second part of server address
        /// </summary>
        public static readonly By ServerAddressTextEntryBy2 = By.XPath("//android.widget.EditText[@content-desc=\"entServerAddressPart2\"]");

        /// <summary>
        /// Text entry for third part of server address
        /// </summary>
        public static readonly By ServerAddressTextEntryBy3 = By.XPath("//android.widget.EditText[@content-desc=\"entServerAddressPart3\"]");

        /// <summary>
        /// Text entry for fourth part of server address
        /// </summary>
        public static readonly By ServerAddressTextEntryBy4 = By.XPath("//android.widget.EditText[@content-desc=\"entServerAddressPart4\"]");

        /// <summary>
        /// Text entry for Server port
        /// </summary>
        public static readonly By ServerPortTextEntryBy = By.XPath("//android.widget.EditText[@content-desc=\"entServerPort\"]");

        #endregion

        /// Lis logging elements
        #region
        /// <summary>
        /// Toggle switch to enable LIS logging
        /// </summary>
        public static readonly By ToggleLISLoggingSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiLisLogging\"]");

        /// <summary>
        /// button to select Log level - displays currently seleted log level - opens Select Log Level popup view
        /// </summary>
        public static readonly By LogLevelButtonBy = By.XPath("//android.widget.Button[@content-desc=\"pckLogLevels\"]");

        /// <summary>
        /// text field to enter date to view lis logs - opens date picker popup
        /// </summary>
        public static readonly By ViewLisLogForDateBy = By.XPath("//android.widget.EditText[@content-desc=\"dateViewLisLogFor\"]");

        /// <summary>
        /// Button To display LIS log
        /// </summary>
        public static readonly By DisplayLogButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butDisplayLisLog\"]");
        #endregion

        /// <summary>
        /// Button to open and close LIS Association Drawer
        /// </summary>
        public static readonly By OpenCloseLISAssociationButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butToggleLisAssociations\"]");

        /// <summary>
        /// Toggle Switch to enable/disable Prompting Users of LIS Association
        /// </summary>
        public static readonly By TogglePromptUserLISAssocSwitchBy = By.XPath("//android.widget.Switch[@content-desc=\"swiShowLisAssociationIssues\"]");

        /// <summary>
        /// List of LIS Associations
        /// </summary>
        public static readonly By LisAssociationListBy = By.XPath("//android.support.v7.widget.RecyclerView[@content-desc=\"listLisAssociations\"]");
    }
}
