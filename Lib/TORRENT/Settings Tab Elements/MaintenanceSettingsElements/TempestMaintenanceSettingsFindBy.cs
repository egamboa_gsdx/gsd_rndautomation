﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Settings
{
    public static class TempestMaintenanceSettingsFindBy
    {
        /// <summary>
        /// Button to view Maintenance Settings
        /// </summary>
        public static readonly By MaintenanceSettingsTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labMaintenanceSettings\"]");

        /// <summary>
        /// Maintenance Day Picker
        /// </summary>
        public static readonly By PickMaintenanceDayBy = By.XPath("//android.widget.Button[@content-desc=\"pckMaintenanceDay\"]");

        /// <summary>
        /// Maintenance Time Picker
        /// </summary>
        public static readonly By MaintenanceTimePickerBy = By.XPath("//android.widget.EditText[@content-desc=\"pckMaintenanceTime\"]");

        /// <summary>
        /// Radio Button to perform maintenance during startup
        /// </summary>
        public static readonly By DuringStartupRadioButtonBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radDuringStartup\"]");

        /// <summary>
        /// Radio Button to perform maintenance during shutdown
        /// </summary>
        public static readonly By DuringShutdownRadioButtonBy = By.XPath("//android.view.ViewGroup[@content-desc=\"radDuringShutdown\"]");
    }
}
