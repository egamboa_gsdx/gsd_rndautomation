﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements
{
    public static class TempestSettingsFindBy
    {
        /// <summary>
        /// Settings Tab View Button...use to navigate to Settings Tab
        /// </summary>
        public static readonly By SettingsTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labNavigationIconSettings\"]");
    }
}
