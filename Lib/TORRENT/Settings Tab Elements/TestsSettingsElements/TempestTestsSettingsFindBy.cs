﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Settings
{
    public static class TempestTestsSettingsFindBy
    {
        /// <summary>
        /// Tests Settings View Tab button
        /// </summary>
        public static readonly By TestsSettingTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labTestsSettings\"]");

        /// <summary>
        /// Add Test Protocol button
        /// </summary>
        public static readonly By AddTestProtocolButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butAddTestProtocol\"]");

        /// <summary>
        /// List View for currently added Test Protocols
        /// </summary>
        public static readonly By TestProtocolsListViewBy = By.XPath("//android.support.v7.widget.RecyclerView[@content-desc=\"listTestProtocols\"]");
    }
}
