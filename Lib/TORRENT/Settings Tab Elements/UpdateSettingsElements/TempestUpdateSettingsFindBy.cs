﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Settings
{
    public static class TempestUpdateSettingsFindBy
    {
        /// <summary>
        /// Update Settings Tab
        /// </summary>
        public static readonly By UpdateSettingsTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labUpdateSettings\"]");
        
        /// <summary>
        /// Browse Apk Files for updates
        /// </summary>
        public static readonly By BrowseApkBy = By.XPath("//android.widget.Button[@content-desc=\"butBrowseApk\"]");

        /// <summary>
        /// Update Apk Files
        /// </summary>
        public static readonly By UpdateApkBy = By.XPath("//android.widget.Button[@content-desc=\"butUpdateApk\"]");

        /// <summary>
        /// Current Software Version
        /// </summary>
        public static readonly By CurrentSoftwareVersionBy = By.XPath("//android.widget.TextView[@content-desc=\"labCurrentSoftwareVersion\"]");

        /// <summary>
        /// Currently Loaded Apk Version
        /// </summary>
        public static readonly By LoadedApkVersionBy = By.XPath("//android.widget.TextView[@content-desc=\"labLoadedApkVersion\"]");
    }
}
