﻿using OpenQA.Selenium;

namespace GSD_RnDAutomation.TORRENT.Elements.Settings
{
    public static class TempestUserSettingsFindBy
    {
        /// <summary>
        /// Element to View User Settings View
        /// </summary>
        public static readonly By UserSettingsTabBy = By.XPath("//android.widget.TextView[@content-desc=\"labUsersSettings\"]");

        /// <summary>
        /// New User Button to add new user
        /// </summary>
        public static readonly By NewUserButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butNewUser\"]");

        /// <summary>
        /// Edit User Button to edit user
        /// </summary>
        public static readonly By EditUserButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butEditUser\"]");

        /// <summary>
        /// Button to open Password Policy Popup
        /// </summary>
        public static readonly By PasswordPolicyButtonBy = By.XPath("//android.widget.Button[@content-desc=\"butPasswordPolicy\"]");

        /// <summary>
        /// List view of User accounts available on system
        /// </summary>
        public static readonly By UserAccountListViewBy = By.XPath("//android.widget.ListView[@content-desc=\"listUsers\"]");
    }
}
