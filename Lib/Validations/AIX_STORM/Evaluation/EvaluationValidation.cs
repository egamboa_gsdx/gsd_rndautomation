﻿using GSD_RnDAutomation.Common;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Status;

using TestStack.White.UIItems;
using TestStack.White.UIItems.WPFUIItems;
using TestStack.White.UIItems.ListBoxItems;

namespace GSD_RnDAutomation.Validations.AIX_STORM
{
    public class EvaluationValidation
    {
        // This variable stores the current screen width of the evaulation report for comparison after zooming.
        // This variable can be set in test cases by calling RecordPageWidth()
        private static double pageWidth = -1;

        /// <summary>
        /// Validates the report is on the first page.
        /// This is verified by checking if the "First Page" button is enabled.
        /// </summary>
        public static void ValidateOnFirstPage()
        {
            StatusManager.AddStatus("Validating On First Page");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            Button FirstPageButton = ApplicationManager.ApplicationWindow.Get<Button>(EvaluationProperty.Default.FirstPageButton);
            if (FirstPageButton.Enabled)
            {
                throw new ValidationException("Report was not on the first page");
            }
        }

        /// <summary>
        /// Validation for checking the worklist reviewed checkbox
        /// Passes only when all of the following are true:
        /// <para> Evaluation > Verify the worklist reviewed checkbox is checked </para>
        /// </summary>
        /// <param name="check"> Indicates whether checkbox should be checked or not </param>
        public static void ValidateCheckWorklistReview(bool check)
        {
            StatusManager.AddStatus("Validating Check Worklist Review");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            CheckBox chckbox = ApplicationManager.ApplicationWindow.Get<CheckBox>(EvaluationProperty.Default.WorklistReviewedCheckbox);
            if ((!chckbox.Checked && check) || (chckbox.Checked && !check))
            {
                throw new ValidationException("Worklist Reviewed checkbox is not in its designated check state");
            }
        }

        /// <summary>
        /// Validation for opening the worklist dropdown list
        /// Passes only when all of the following are true:
        /// <para> Evaluation > Verify an item of the dropdown list is enabled </para>
        /// </summary>
        public static void ValidateDropdownReport()
        {
            StatusManager.AddStatus("Validating Dropdown Report List");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            ComboBox pulldown = ApplicationManager.ApplicationWindow.Get<ComboBox>(EvaluationProperty.Default.WorklistPulldown);
            var worklists = pulldown.Items;

            if (worklists.Count == 0)
            {
                throw new ValidationException("Worklist dropdown list contains no items");
            }
        }

        /// <summary>
        /// Validates the report is on the last page.
        /// This is verified by checking if the "Last Page" button is enabled.
        /// </summary>
        public static void ValidateOnLastPage()
        {
            StatusManager.AddStatus("Validating On Last Page");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            Button LastPageButton = ApplicationManager.ApplicationWindow.Get<Button>(EvaluationProperty.Default.LastPageButton);
            if (LastPageButton.Enabled)
            {
                throw new ValidationException("Report was not on the last page");
            }
        }

        /// <summary>
        /// Validation for opening a worklist report
        /// Passes only when all of the following are true:
        /// <para> Evaluation > Verify the worklist report exists </para>
        /// </summary>
        public static void ValidateOpenReport()
        {
            StatusManager.AddStatus("Validating Open Worklist Report");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            try
            {
                HelperMethods.Click(EvaluationProperty.Default.WorklistReport);
            }
            catch
            {
                throw new ValidationException("Worklist report was not opened");
            }
        }

        /// <summary>
        /// Validation for opening the page setup window
        /// Passes only when all of the following are true:
        /// <para> Evaluation > Verify the page setup window opens </para>
        /// </summary>
        public static void ValidatePageSetup()
        {
            StatusManager.AddStatus("Validating Open Page Setup Window");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            try
            {
                HelperMethods.Click(EvaluationProperty.Default.CloseButton);
            }
            catch
            {
                throw new ValidationException("Page Setup Window was not opened");
            }
        }

        /// <summary>
        /// Validation for opening the pdf save window
        /// Passes only when all of the following are true:
        /// <para> Evaluation > Verify the pdf save window opens </para>
        /// </summary>
        public static void ValidatePDFSave()
        {
            StatusManager.AddStatus("Validating Open PDF Save Window");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            try
            {
                HelperMethods.Click(CommonProperty.Default.CancelButton_textValue);
            }
            catch
            {
                throw new ValidationException("PDF Save Window was not opened");
            }
        }

        /// <summary>
        /// Validation for opening the print window
        /// Passes only when all of the following are true:
        /// <para> Evaluation > Verify the print window opens </para>
        /// </summary>
        public static void ValidatePrint()
        {
            StatusManager.AddStatus("Validating Open Print Window");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            try
            {
                HelperMethods.Click(EvaluationProperty.Default.CloseButton);
            }
            catch
            {
                throw new ValidationException("Print Window was not opened");
            }
        }

        /// <summary>
        /// Validation for opening the search pane
        /// Passes only when all of the following are true:
        /// <para> Evaluation > Verify the search pane opens </para>
        /// </summary>
        public static void ValidateSearch()
        {
            StatusManager.AddStatus("Validating Open Search Pane");
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            try
            {
                HelperMethods.Click(EvaluationProperty.Default.NavigationTextBox);
                HelperMethods.Click(EvaluationProperty.Default.NavigationButton);
            }
            catch
            {
                throw new ValidationException("Search Pane was not opened");
            }
        }

        private static double GetPageWidth()
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            IUIItem page = ApplicationManager.ApplicationWindow.Get(EvaluationProperty.Default.PagePreview);
            IUIItem topMargin = page.GetMultiple(EvaluationProperty.Default.MarginLine)[EvaluationProperty.Default.MarginLine.Index];
            return topMargin.Bounds.Width;
        }

        /// <summary>
        /// Records the current screen width of the evaulation report.
        /// This value will be used later when calling ValidateZoomIn and ValidateZoomOut.
        /// </summary>
        public static void RecordPageWidth()
        {
            StatusManager.AddStatus("Recording the current report zoom size");

            pageWidth = GetPageWidth();
        }

        /// <summary>
        /// Compares the current report screen size to that recorded from the last RecordPageWidth call.
        /// Throws ValidationException when the size decreased since the RecordPageWidth call.
        /// </summary>
        public static void ValidateZoomIn()
        {
            StatusManager.AddStatus("Validating the report zoomed in");

            if (pageWidth < 0)
            {
                throw new WorkflowException("ValidateZoomIn can only be called after RecordPageWidth");
            }

            double newPageWidth = GetPageWidth();

            if (newPageWidth <= pageWidth)
            {
                throw new ValidationException("Expected larger page width, found " + pageWidth + "px going to " + newPageWidth + "px");
            }
        }

        /// <summary>
        /// Compares the current report screen size to that recorded from the last RecordPageWidth call.
        /// Throws ValidationException when the size increased since the RecordPageWidth call.
        /// </summary>
        public static void ValidateZoomOut()
        {
            StatusManager.AddStatus("Validating the report zoomed out");

            if (pageWidth < 0)
            {
                throw new WorkflowException("ValidateZoomOut can only be called after RecordPageWidth");
            }

            double newPageWidth = GetPageWidth();

            if (newPageWidth >= pageWidth)
            {
                throw new ValidationException("Expected smaller page width, found " + pageWidth + "px going to " + newPageWidth + "px");
            }
        }

        /// <summary>
        /// Validates the Zoom In button.
        /// Passes if the zoom percentage found in the zoom dropdown matches the passed value.
        /// Can only validate 10, 25, 50 75, 100, 150, 200, and 500.
        /// </summary>
        /// <param name="percentage"> The percentage of zoom to verify </param>
        public static void ValidateZoomPercentage(int percentage)
        {
            StatusManager.AddStatus("Validating Zoom of " + percentage + "%");

            ElementIdentifier checkboxIdentifier;
            switch (percentage)
            {
                case 10:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom10Checkbox;
                    break;
                case 25:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom25Checkbox;
                    break;
                case 50:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom50Checkbox;
                    break;
                case 75:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom75Checkbox;
                    break;
                case 100:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom100Checkbox;
                    break;
                case 150:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom150Checkbox;
                    break;
                case 200:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom200Checkbox;
                    break;
                case 500:
                    checkboxIdentifier = EvaluationProperty.Default.Zoom500Checkbox;
                    break;
                default:
                    throw new ValidationException("ValidateZoomPercentage cannot be called with a percentage of " + percentage);
            }

            MenuMethods.NavigateTo(MenuProperty.Default.Evaluation);

            // Open the dropdown
            IUIItem zoomDropdown = ApplicationManager.ApplicationWindow.Get(EvaluationProperty.Default.ZoomDropdown);
            HelperMethods.Click(zoomDropdown);

            // Validate the checkbox
            CheckBox zoomCheckbox = zoomDropdown.Get<CheckBox>(checkboxIdentifier);
            bool isChecked = zoomCheckbox.Checked;

            // Close the dropdown
            HelperMethods.Click(zoomDropdown);

            if (!isChecked)
            {
                throw new ValidationException("Zoom dropdown for " + percentage + "% was not checked");
            }
        }
    }
}
