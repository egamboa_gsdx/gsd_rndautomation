﻿using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Status;

using TestStack.White.UIItems;

namespace GSD_RnDAutomation.Validations.AIX_STORM
{
    public class OptionValidation
    {
        /// <summary>
        /// Validates the current user. Expects the user to be "Tech Service".
        /// </summary>
        public static void ValidateCurrentUser()
        {
            ValidateCurrentUser("Tech Service");
        }

        /// <summary>
        /// Validates the current user.
        /// </summary>
        /// <param name="username">The expected username of the current user.</param>
        public static void ValidateCurrentUser(string username)
        {
            StatusManager.AddStatus("Validating Current User");

            OptionMethods.OpenOption(MenuProperty.Default.CurrentUser);

            var windows = ApplicationManager.TryGetWindows();
            foreach (var window in windows)
            {
                if (window.Name.StartsWith(MenuProperty.Default.CurrentUserWindowName)) // Storm uses "Current User", AIX uses "Current User: USERNAME_HERE"
                {
                    Label[] textItems = window.GetMultiple<Label>(MenuProperty.Default.CurrentUserWindowText);
                    for (int i = 0; i < textItems.Length; i++)
                    {
                        if (textItems[i].Text == MenuProperty.Default.CurrentUserUsernameLabelText)
                        {
                            // Verify the username matches its expected value
                            if (textItems[i + 1].Text != username)
                            {
                                HelperMethods.Click(MenuProperty.Default.CurrentUserCloseButton, window);
                                throw new ValidationException("The current user's username does not match its expected value");
                            }
                        }
                        //else if (textItems[i].Text == MenuProperty.Default.CurrentUserRoleLabelText)
                        //{
                        //    // Verify the role matches its expected value
                        //    if (textItems[i + 1].Text != role)
                        //    {
                        //        HelperMethods.Click(MenuProperty.Default.CurrentUserCloseButton, window);
                        //        throw new ValidationException("The current user's role does not match its expected value");
                        //    }
                        //}
                        //else if (textItems[i].Text == MenuProperty.Default.CurrentUserEmailLabelText)
                        //{
                        //    // Verify the email matches its expected value
                        //    if (textItems[i + 1].Text != email)
                        //    {
                        //        HelperMethods.Click(MenuProperty.Default.CurrentUserCloseButton, window);
                        //        throw new ValidationException("The current user's email does not match its expected value");
                        //    }
                        //}
                    }
                    HelperMethods.Click(MenuProperty.Default.CurrentUserCloseButton, window);
                    return;
                }
            }
            throw new WorkflowException("Current User window was not found.");
        }

        /// <summary>
        /// Validates the current user option is disabled (should occur when worklist is running).
        /// </summary>
        public static void ValidateCurrentUserDisabled()
        {
            StatusManager.AddStatus("Validating Current User is Disabled");

            if (OptionMethods.OptionEnabled(MenuProperty.Default.CurrentUser))
            {
                throw new ValidationException("Current User option is enabled when it was expected to be disabled.");
            }
        }

        /// <summary>
        /// Validates the version number found in the About window
        /// </summary>
        public static void ValidateVersionInfo()
        {
            StatusManager.AddStatus("Validating Version Info");

            OptionMethods.OpenOption(MenuProperty.Default.About);

            var windows = ApplicationManager.TryGetWindows();
            foreach (var window in windows)
            {
                if (window.Name == MenuProperty.Default.AboutWindowName)
                {
                    Label versionLabel = window.Get<Label>(MenuProperty.Default.AboutVersionValue);
                    string version = ApplicationManager.GetCurrentSoftwareVersion();
                    if (versionLabel.Text != "Version " + version)
                    {
                        HelperMethods.Click(MenuProperty.Default.AboutCloseButton, window);
                        throw new ValidationException("Version number did not match its expected value.");
                    }
                    HelperMethods.Click(MenuProperty.Default.AboutCloseButton, window);
                    return;
                }
            }
        }
    }
}
