﻿using System;

using TestStack.White.UIItems;

using GSD_RnDAutomation.Common;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Status;

namespace GSD_RnDAutomation.Validations.AIX_STORM
{
    public class ToolbarValidation
    {
        /// <summary>
        /// Validates the current time shown in the top right.
        /// </summary>
        public static void ValidateCurrentTime()
        {
            StatusManager.AddStatus("Validating toolbar clock time");

            string time = "";

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Label clockLabel = ApplicationManager.ApplicationWindow.Get<Label>(GeneralProperty.Default.StormClockText);
                time = clockLabel.Text;
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                Button clockButton = ApplicationManager.ApplicationWindow.Get<Button>(GeneralProperty.Default.AIXClockButton);
                time = clockButton.Text;
            }
            else
            {
                throw new SoftwareException("ValidateCurrentTime() is only set up for AIX and Storm.");
            }

            DateTime clockTime = DateTime.ParseExact(time, "h:mm:ss tt", System.Globalization.CultureInfo.CurrentCulture);
            DateTime currentTime = DateTime.Now;
            TimeSpan timeDifference = clockTime - currentTime;

            if (Math.Abs(timeDifference.TotalSeconds) > 3) // Allow three second of error for lag
            {
                throw new ValidationException("Toolbar's clock time was off by more than three seconds");
            }
        }
        
        /// <summary>N.S
        /// Validates the tooltip from the waste bottle on the toolbar.
        /// Only passes when the waste bottle parameter matches the text in the tooltip.
        /// </summary>
        /// <param name="wastebottle">The expected worklist wastebottle.</param>

        public static void ValidateWasteBottle(string wastebottle)
        {
            StatusManager.AddStatus("Validating toolbar WasteBottle");

            string text = ToolbarMethods.GetWorklistWasteBottle().Text;

            if (text != "Waste Bottle: " + wastebottle)
            {
                throw new ValidationException("Expected waste bottle '" + wastebottle + "', found '" + text + "'");
            }

        }
        /// <summary> N.S
        /// Validates the tooltip from the wash bottle on the toolbar.
        /// Only passes when the wash bottle parameter matches the text in the tooltip.
        /// </summary>
        /// <param name="washbottle">The expected worklist washbottle .</param>
        public static void ValidateWashBottle(string washbottle)
        {
            StatusManager.AddStatus("Validating toolbar Wash Bottle");

            string text = ToolbarMethods.GetWorklistWashBottle().Text;

            if (text != "Wash Bottle 1: " + washbottle)
            {
                throw new ValidationException("Expected wash bottle 1 '" + washbottle + "', found '" + text + "'");
            }

        }
        /// <summary>
        /// Validates the tooltip from the worklist state on the toolbar.
        /// Only passes when the state parameter matches the text in the tooltip.
        /// </summary>
        /// <param name="state">The expected worklist state.</param>
        public static void ValidateWorklistState(string state)
        {
            StatusManager.AddStatus("Validating toolbar worklist state");

            string text = ToolbarMethods.GetWorklistStateTooltip().Text;

            if (text != "Worklist State: " + state)
            {
                throw new ValidationException("Expected worklist state '" + state + "', found '" + text + "'");
            }
        }
    }
}
