﻿using System;
using System.Text.RegularExpressions;

using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;

using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Status;

namespace GSD_RnDAutomation.Validations.AIX_STORM
{
    public class HomeValidation
    {
        public static void ValidateContinue()
        {
            StatusManager.AddStatus("Validating Continue");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Verify the continue appears in the worklist log
            OpenWorklistLog();
            TextBox WorklistLog = ApplicationManager.ApplicationWindow.Get<TextBox>(HomeProperty.Default.WorklistLog);
            CloseWorklistLog();

            if (!WorklistLog.Text.Contains("Instrument is resuming..."))
            {
                throw new ValidationException("Worklist log does not list inital resuming command");
            }
            if (!WorklistLog.Text.Contains("Worklist Run Restarted"))
            {
                throw new ValidationException("Worklist log does not list resuming command as complete");
            }
        }

        /// <summary>
        /// Validation for finish time indicator
        /// Passes only when all of the following are true:
        /// <para> Home > Verify the finish time is correct </para>
        /// </summary>
        public static void ValidateFinishTime()
        {
            StatusManager.AddStatus("Validating Finish Time");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            Label StartTimeLabel = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StartTime);
            Label FinishTimeLabel = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.EndTime);
            DateTime startTime = DateTime.Parse(StartTimeLabel.Text.Substring(12));
            DateTime finishTime = DateTime.Parse(FinishTimeLabel.Text.Substring(13));
            TimeSpan testDuration = finishTime - startTime;

            // Storm's ANA_GSD test should last between 90 and 100 minutes
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                if (testDuration.TotalMinutes < 90)
                {
                    throw new ValidationException("Test's Finish Time was sooner than expected.");
                }
                if (testDuration.TotalMinutes > 100)
                {
                    throw new ValidationException("Test's Finish Time was later than expected.");
                }
            }
            // AIX's test should last between 18 and 28 minutes
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                if (ApplicationManager.RackManager == RackManager.classic)
                {
                    if (testDuration.TotalMinutes < 18)
                    {
                        throw new ValidationException("Test's Finish Time was sooner than expected.");
                    }
                    if (testDuration.TotalMinutes > 28)
                    {
                        throw new ValidationException("Test's Finish Time was later than expected.");
                    }
                }
                else if (ApplicationManager.RackManager == RackManager.SIR)
                {
                    if (testDuration.TotalMinutes < 60)
                    {
                        throw new ValidationException("Test's Finish Time was sooner than expected.");
                    }
                    if (testDuration.TotalMinutes > 90)
                    {
                        throw new ValidationException("Test's Finish Time was later than expected.");
                    }
                }

            }
        }

        /// <summary>
        /// Validation for Fly-out Status Log View
        /// Passes only when all of the following are true:
        /// <para> Home > Fly-out Log can be opened </para>
        /// <para> Home > Fly-out Log contains no text </para>
        /// </summary>
        public static void ValidateFlyoutLog()
        {
            StatusManager.AddStatus("Validating Fly-out Status Log");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Verify worklist log is empty
            try
            {
                OpenWorklistLog();
                TextBox WorklistLog = ApplicationManager.ApplicationWindow.Get<TextBox>(HomeProperty.Default.WorklistLog);
                CloseWorklistLog();

                if (WorklistLog.Text.Length > 0)
                {
                    throw new ValidationException("Worklist flyout log is not empty");
                }
            }
            catch
            {
                throw new ValidationException("Can not open the Worklist flyout log");
            }
        }

        /// <summary>
        /// Validation for Number of Samples Loaded
        /// Passes only when all of the following are true:
        /// <para> Home > Number of samples loaded matches its designated value </para>
        /// </summary>
        /// <param name="samples"></param>
        public static void ValidateLoadedSamples(int samples)
        {
            StatusManager.AddStatus("Validating Number of Samples Loaded");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Ensure the number of samples loaded is correct
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Label numberOfSamples = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormNumberOfSamplesLoaded);
                if (numberOfSamples.Text != samples.ToString())
                {
                    throw new ValidationException("Number of samples - " + numberOfSamples.Text + " - does not match its expected value of: " + samples);
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                Label numberOfSamples = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXNumberOfSamplesLoaded);
                if (numberOfSamples.Text != samples.ToString())
                {
                    throw new ValidationException("Number of samples - " + numberOfSamples.Text + " - does not match its expected value of: " + samples);
                }
            }
        }

        /// <summary>
        /// AIX only.
        /// Validates the lot number for a test.
        /// Passes only when all of the following are true:
        /// <para> Home > Lot number matches expected value. </para>
        /// <para> Worklist > Test properties > Lot number matches expected value. </para>
        /// </summary>
        /// <param name="lotNumber">The expected lot number.</param>
        public static void ValidateLotNumber(int lotNumber)
        {
            // AIX only
            if (ApplicationManager.InstrumentManager != InstrumentManager.AIX)
            {
                throw new SoftwareException("HomeValidation.ValidateLotNumber() cannot be run in non-AIX software. Be sure to use WorklistValidation.ValidateLotNumber() on Storm.");
            }

            StatusManager.AddStatus("Validating Lot Number");

            // Home
            MenuMethods.NavigateTo(MenuProperty.Default.Home);
            Label lotNumberLabel = null;

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                lotNumberLabel = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXLotNumberVal);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                lotNumberLabel = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXTppaLotNumberVal);
            }
            

            if (lotNumberLabel.Text != lotNumber.ToString())
            {
                throw new ValidationException("Lot number on home tab does not match its expected value of: \"" + lotNumber + "\"");
            }

            // Worklist
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.OpenTestPropertiesWindow();
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.OpenTestPropertiesWindow(5);
            }

            
            TextBox lotNumberTextBox = ApplicationManager.ApplicationWindow.Get<TextBox>(WorklistProperty.Default.LotNumberTextbox);

            if (lotNumberTextBox.Text != lotNumber.ToString())
            {
                HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
                throw new ValidationException("Lot number in test properties does not match its expected value of: \"" + lotNumber + "\"");
            }

            HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
        }

        /// <summary>
        /// AIX only.
        /// Validates the expiration date for the worklist.
        /// Passes only when all of the following are true:
        /// <para> Home > Lot number matches expected value. </para>
        /// <para> Worklist > Test properties > Expiration date matches expected value. </para>
        /// </summary>
        /// <param name="date">The expected expiration date. Should be in the format "MM/DD/YYYY".</param>
        /// <param name="testIndex">The one-based test index to verify (Default: first test).</param>
        public static void ValidateExpirationDate(string date, int testIndex = 1)
        {
            // AIX only
            if (ApplicationManager.InstrumentManager != InstrumentManager.AIX)
            {
                throw new SoftwareException("HomeValidation.ValidateExpiration() cannot be run in non-AIX software. Be sure to use WorklistValidation.ValidateExpiration() on Storm.");
            }

            StatusManager.AddStatus("Validating expiration date");
            Label expirationDateLabel = null;
            // Home
            MenuMethods.NavigateTo(MenuProperty.Default.Home);
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                expirationDateLabel = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXExpirationDateVal);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                expirationDateLabel = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXTppaExpirationDateVal);
            }
            

            if (expirationDateLabel.Text != date)
            {
                throw new ValidationException("Expiration date on home tab does not match its expected value of: \"" + date + "\"");
            }

            // Worklist
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.OpenTestPropertiesWindow(testIndex);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.OpenTestPropertiesWindow(5);
            }
            
            TextBox expirationDateTextBox = ApplicationManager.ApplicationWindow.Get<TextBox>(WorklistProperty.Default.ExpirationDateTextbox);
            if (expirationDateTextBox.Text != date)
            {
                HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
                throw new ValidationException("Expiration date in test properties does not match its expected value of \"" + date + "\"");
            }

            HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
        }

        private static string GetWorklistName()
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Label worklistName = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormWorklistName);
                return worklistName.Text;
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                Label worklistName = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXWorklistName);
                return worklistName.Text;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Validation for Worklist Name
        /// Passes only when all of the following are true:
        /// <para> Home > Worklist name matches its designated value </para>
        /// </summary>
        /// <param name="name"></param>
        public static void ValidateWorklistName(string name)
        {
            StatusManager.AddStatus("Validating Worklist Name");

            if (GetWorklistName() != name)
            {
                throw new ValidationException("Worklist name does not match its expected value of: " + name);
            }
        }

        /// <summary>
        /// Validation for auto generated worklist names.
        /// If the worklist name does match the expected format, throw a ValidationException.
        /// If the time in the worklist name is outside the range of 0 to -15 seconds of the current time, throw a ValidationException.
        /// If the serial number does not match the serial number found in the 
        /// </summary>
        public static void ValidateAutoGeneratedWorklistName()
        {
            StatusManager.AddStatus("Validating Auto-Generated Worklist Name");

            Regex pattern = new Regex(@"^(?<year>\d{4})_(?<month>\d\d)_(?<day>\d\d)_(?<hour>\d\d)_(?<minute>\d\d)_(?<second>\d\d)_(?<serial>.*)$");
            int maxDifference = 15;

            DateTime now = DateTime.Now;
            string serialNumber = SettingMethods.GetSerialNumber();

            string worklistName = GetWorklistName();
            Match match = pattern.Match(worklistName);
            if (match.Success)
            {
                int year = int.Parse(match.Groups["year"].Value);
                int month = int.Parse(match.Groups["month"].Value);
                int day = int.Parse(match.Groups["day"].Value);
                int hour = int.Parse(match.Groups["hour"].Value);
                int minute = int.Parse(match.Groups["minute"].Value);
                int second = int.Parse(match.Groups["second"].Value);
                string serial = match.Groups["serial"].Value;

                DateTime worklistTime = new DateTime(year, month, day, hour, minute, second);
                TimeSpan difference = now - worklistTime;

                if (difference.TotalSeconds < 0)
                {
                    throw new ValidationException("Auto generated worklist name has a time in the future");
                }
                if (difference.TotalSeconds >= maxDifference)
                {
                    throw new ValidationException("Auto generated worklist name has a time older than " + maxDifference + " seconds");
                }
                if (serial != serialNumber)
                {
                    throw new ValidationException("Auto generated worklist name has mismatch serial number");
                }
            }
            else
            {
                throw new ValidationException("Auto generated worklist name did not match its expected format");
            }
        }

        /// <summary>
        /// Validates the operator name on the home tab. Expects the operator to be "Tech Service".
        /// </summary>
        public static void ValidateOperator()
        {
            ValidateOperator("Tech Service");
        }

        /// <summary>
        /// Validates the operator name on the home tab.
        /// </summary>
        /// <param name="username">The expected operator name.</param>
        public static void ValidateOperator(string username)
        {
            StatusManager.AddStatus("Validating Operator");

            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Label operatorLabel = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormOperator);
                if (operatorLabel.Text != username)
                {
                    throw new ValidationException("Operator's username does not match its expected value");
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                Label operatorLabel = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXOperator);
                if (operatorLabel.Text != username)
                {
                    throw new ValidationException("Operator's username does not match its expected value");
                }
            }
        }

        /// <summary>
        /// Validates the worklist is currently paused.
        /// Checks the following:
        /// <para> (non-AIX) Home > Current action is paused </para>
        /// <para> Toolbar > Worklist state is paused </para>
        /// </summary>
        public static void ValidatePause()
        {
            StatusManager.AddStatus("Validating Pause worklist");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // AIX doesnt update the current action when the pause finishes
            if (ApplicationManager.InstrumentManager != InstrumentManager.AIX)
            {
                // Verify the current action is paused
                Label CurrentAction = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.CurrentAction);
                if (CurrentAction.Text != "Current Action: Worklist Run Paused")
                {
                    throw new ValidationException("Current action does not show worklist as paused");
                }
            }

            ToolTip ToolTipText = ToolbarMethods.GetWorklistStateTooltip();

            if (ToolTipText.Text != "Worklist State: Paused")
            {
                throw new ValidationException("Toolbar does not show worklist as Paused");
            }
        }

        public static void ValidateSpecialStartSpecificPosition()
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Home);
            StatusManager.AddStatus("Validating Special Start Specific Position");

            ValidateStart();
        }

        public static void ValidateSpecialStartStoppedPosition()
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Home);
            StatusManager.AddStatus("Validating Special Start Stopped Position");

            ValidateStart();
        }

        /// <summary>
        /// Validation for starting a worklist.
        /// Passes only when all of the following are true:
        /// <para> Home > Verify the current action is running a test </para>
        /// <para> Home > Verify the time left has a value </para>
        /// <para> Home > Verify the worklist log shows the start action </para>
        /// <para> Home > Verify the worklist in the toolbar is running </para>
        /// </summary>
        public static void ValidateStart()
        {
            StatusManager.AddStatus("Validating Start");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Verify the current action is listing something
            Label CurrentAction = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.CurrentAction);

            if (!CurrentAction.Text.StartsWith("Current Action: T1") && !CurrentAction.Text.StartsWith("Current Action: Worklist Run Started"))
            {
                throw new ValidationException("Current action not working on test");
            }

            // Verify the time left has a value
            Label TimeLeft = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.TimeLeft);

            if (!TimeLeft.Text.StartsWith("Time Left: "))
            {
                throw new ValidationException("No time left value");
            }

            // Verify the start appears in the worklist log
            OpenWorklistLog();
            TextBox WorklistLog = ApplicationManager.ApplicationWindow.Get<TextBox>(HomeProperty.Default.WorklistLog);
            CloseWorklistLog();

            if (!WorklistLog.Text.Contains("Worklist Run Started"))
            {
                throw new ValidationException("Worklist log does not list worklist has started");
            }

            ToolTip ToolTipText = ToolbarMethods.GetWorklistStateTooltip();

            if (ToolTipText.Text != "Worklist State: Running")
            {
                throw new ValidationException("Toolbar does not show worklist as running");
            }
        }

        /// <summary>
        /// Validation for start time indicator
        /// Passes only when all of the following are true:
        /// <para> Home > Verify the start time is current time </para>
        /// </summary>
        public static void ValidateStartTime()
        {
            StatusManager.AddStatus("Validating Start Time");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            Label startTimeLabel = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StartTime);
            string startTimeString = startTimeLabel.Text.Substring(11).Trim(); // Remove the "Start Time: " before the time
            DateTime startTime = DateTime.ParseExact(startTimeString, "h:mm tt", System.Globalization.CultureInfo.CurrentCulture);
            DateTime currentTime = DateTime.Now;

            TimeSpan timeDifference = startTime - currentTime;
            if (Math.Abs(timeDifference.TotalMinutes) > 1.5) // Allow a minute and a half of error
            {
                throw new ValidationException("Start Time is too far off of the current time (" + timeDifference.TotalSeconds + " seconds off)");
            }
        }

        /// <summary>
        /// Validation for starting a worklist.
        /// Passes only when all of the following are true:
        /// <para> Home > Verify the current action is stopped </para>
        /// <para> Home > Verify the time left is 00:00:00 </para>
        /// <para> Home > Verify the worklist log shows the stop action </para>
        /// <para> Home > Verify the worklist in the toolbar is stopped </para>
        /// </summary>
        public static void ValidateStop()
        {
            StatusManager.AddStatus("Validating Stop");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);
            HelperMethods.Wait();
            // Verify the current action is stopped
            Label CurrentAction = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.CurrentAction);
            HelperMethods.Wait();
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                if (CurrentAction.Text != "Current Action: Worklist Run Stopped")
                {
                    throw new ValidationException("Current action not stopped");
                }

            }

            // Verify the time left has no value
            Label TimeLeft = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.TimeLeft);

            if (TimeLeft.Text != "Time Left: 00:00:00")
            {
                throw new ValidationException("Time left is visible or has a value");
            }

            // Verify the stop appears in the worklist log
            OpenWorklistLog();
            TextBox WorklistLog = ApplicationManager.ApplicationWindow.Get<TextBox>(HomeProperty.Default.WorklistLog);
            CloseWorklistLog();

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                if (!WorklistLog.Text.Contains("Worklist Stopping"))
                {
                    throw new ValidationException("Worklist log does not list worklist stop action");
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                if (!WorklistLog.Text.Contains("Worklist Run Stopping"))
                {
                    throw new ValidationException("Worklist log does not list worklist stop action");
                }
            }

            if (!WorklistLog.Text.Contains("Worklist Run Stopped"))
            {
                throw new ValidationException("Worklist log does not list worklist is stopped");
            }

            ToolTip ToolTipText = ToolbarMethods.GetWorklistStateTooltip();

            if (ToolTipText.Text != "Worklist State: Stopped")
            {
                throw new ValidationException("Toolbar does not show worklist as stopped");
            }
        }

        /// <summary>
        /// Validation for Number of Tests to Run
        /// Passes only when all of the following are true:
        /// <para> Home > Number of tests to run matches its designated value </para>
        /// </summary>
        /// <param name="samples"></param>
        public static void ValidateTestsToRun(int tests)
        {
            StatusManager.AddStatus("Validating Number of Tests to Run");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Ensure the number of tests to run is correct
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Label numberOfTests = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormNumberOfTestsToRun);
                if (numberOfTests.Text != tests.ToString())
                {
                    throw new ValidationException("Number of tests- " + numberOfTests.Text + " -does not match its expected value of: " + tests);
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                Label numberOfTests = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXNumberOfTestsToRun);
                if (numberOfTests.Text != tests.ToString())
                {
                    throw new ValidationException("Number of tests- " + numberOfTests.Text + " -does not match its expected value of: " + tests);
                }
            }
        }

        /// <summary>
        /// Validation for timing details
        /// Passes only when all of the following are true:
        /// <para> Home > Verify time left has a value </para>
        /// <para> Home > Verify start time has a value </para>
        /// <para> Home > Verify finish time has a value </para>
        /// </summary>
        public static void ValidateTimingDetails()
        {
            StatusManager.AddStatus("Validating Timing Details");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Verify the time left, start time, and finish time have values
            Label TimeLeft = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.TimeLeft);
            Label StartTime = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StartTime);
            Label FinishTime = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.EndTime);

            if (!TimeLeft.Text.StartsWith("Time Left: "))
            {
                throw new ValidationException("No time left value");
            }
            if (!StartTime.Text.StartsWith("Start Time: "))
            {
                throw new ValidationException("No start time value");
            }
            if (!FinishTime.Text.StartsWith("Finish Time: "))
            {
                throw new ValidationException("No finish time value");
            }
        }

        /// <summary>
        /// Validation for Wash Bottle 1
        /// Passes only when all of the following are true:
        /// <para> Home > Wash Bottle 1 text matches its defined value </para>
        /// </summary>
        /// <param name="samples"></param>
        public static void ValidateWashBottle1()
        {
            StatusManager.AddStatus("Validating Wash Bottle 1");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Ensure the wash bottle value is correct
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Label washBottle = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormWashBottle1);
                if (washBottle.Text != HomeProperty.Default.StormWashBottleValue.TextValue)
                {
                    throw new ValidationException("Wash Bottle 1- " + washBottle.Text + " -does not match its expected value of: " +
                        HomeProperty.Default.StormWashBottleValue.TextValue);
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                Label washBottle = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXWashBottle1);
                if (washBottle.Text != HomeProperty.Default.AIXWashBottleValue.TextValue)
                {
                    throw new ValidationException("Wash Bottle 1- " + washBottle.Text + " -does not match its expected value of: " +
                        HomeProperty.Default.AIXWashBottleValue.TextValue);
                }
            }
        }
        /// <summary>
        /// By: Ernest Gamboa
        /// Validation for Home Menu Buttons
        /// Passes only when all of the following are true:
        /// <para> Home > Start Disabled + No tests assigned and No Controls</para>
        /// </summary>
        /// <param name="btnName">name of button to test</param>
        public static void ValidateHomeButtons(string btnName)
        {
            StatusManager.AddStatus("Validating '" + btnName + "' button is disabled");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            switch (btnName.ToUpper())
            {
                case "START":
                    Button startBtn = ApplicationManager.ApplicationWindow.Get<Button>(HomeProperty.Default.StartButton);
                    if (startBtn.Enabled)
                    {
                        throw new ValidationException("Start button enabled! Expected Start Button to be disabled...");
                    }
                    break;
                case "SPECIAL START":
                    Button spStartBtn = ApplicationManager.ApplicationWindow.Get<Button>(HomeProperty.Default.SpecialStartButton);
                    if (spStartBtn.Enabled)
                    {
                        throw new ValidationException("Special Start button enabled! Expected Special Start Button to be disabled...");
                    }
                    break;
                default:
                    throw new ValidationException("No Such Button Exists! Testing either 'Start' or 'Special Start' buttons only...");
            }
        }

        // ****************************************************************************************************
        // Helper functions
        // ****************************************************************************************************

        private static void OpenWorklistLog()
        {
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                HelperMethods.Click(HomeProperty.Default.StormOpenWorklistLog);
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                HelperMethods.Click(HomeProperty.Default.AIXOpenWorklistLog);
            }
        }

        private static void CloseWorklistLog()
        {
            HelperMethods.Click(HomeProperty.Default.CloseWorklistLog);
        }
    }
}
