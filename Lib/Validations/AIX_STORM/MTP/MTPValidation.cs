﻿using GSD_RnDAutomation.Common;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Status;

using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WPFUIItems;

namespace GSD_RnDAutomation.Validations.AIX_STORM
{
    public class MTPValidation
    {
        /// <summary>
        /// Validates the name of a well. Works on samples, non-samples, and empty wells (use name="" for empty wells).
        /// </summary>
        /// <param name="mtpIndex">The MTP index to use.</param>
        /// <param name="holeIndex">The hole index to use.</param>
        /// <param name="name">The expected well name.</param>
        public static void ValidateWellName(int mtpIndex, int holeIndex, string name)
        {
            StatusManager.AddStatus("Validating sample well's name");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            Label well = MTPMethods.GetHole(mtpIndex, holeIndex);
            if (well.Text != name)
            {
                throw new ValidationException("The sample well's name did not match its expected value");
            }
        }

        /// <summary>
        /// Validates the name from the tooltip of a sample well.
        /// </summary>
        /// <param name="mtpIndex">The MTP index to use.</param>
        /// <param name="holeIndex">The hole index to use.</param>
        /// <param name="name">The expected sample well name.</param>
        public static void ValidateSampleWellTooltipName(int mtpIndex, int holeIndex, string name)
        {
            StatusManager.AddStatus("Validating sample well's  name from tooltip");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            MTPWellTooltip tooltip = MTPMethods.GetWellTooltip(mtpIndex, holeIndex);
            if (tooltip.Type != "Sample Well")
            {
                throw new ValidationException("The selected well is not a sample well");
            }
            if (tooltip.Name != name)
            {
                throw new ValidationException("The sample well's name did not match its expected value");
            }
        }

        /// <summary>
        /// Validates the name from the tooltip of a well. Use ValidateSampleWellTooltipName() for sample wells.
        /// </summary>
        /// <param name="mtpIndex">The MTP index to use.</param>
        /// <param name="holeIndex">The hole index to use.</param>
        /// <param name="name">The expected well name.</param>
        public static void ValidateWellTooltipName(int mtpIndex, int holeIndex, string name)
        {
            StatusManager.AddStatus("Validating well's name from tooltip");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            MTPWellTooltip tooltip = MTPMethods.GetWellTooltip(mtpIndex, holeIndex);
            if (tooltip.Name != name)
            {
                throw new ValidationException("The sample well's name did not match its expected value");
            }
        }

        /// <summary>
        /// Validates the name from the tooltip of a empty well.
        /// </summary>
        /// <param name="mtpIndex">The MTP index to use.</param>
        /// <param name="holeIndex">The hole index to use.</param>
        public static void ValidateEmptyWellTooltip(int mtpIndex, int holeIndex)
        {
            StatusManager.AddStatus("Validating empty well's tooltip");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            MTPWellTooltip tooltip = MTPMethods.GetWellTooltip(mtpIndex, holeIndex);
            /*if (tooltip.HasType)
            {
                throw new ValidationException("The empty well's tooltip has a type value");
            }*/
            if (tooltip.HasName)
            {
                throw new ValidationException("The empty well's tooltip has a name value");
            }
        }

        /// <summary>
        /// Storm only.
        /// Validates that an entire strip contains only empty wells.
        /// </summary>
        public static void ValidateEmptyStrip(int mtpIndex, int stripIndex)
        {
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("ValidateEmptyStrip() cannot be called on non-Storm software.");
            }

            StatusManager.AddStatus("Validating empty strip");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            int startHoleIndex = (stripIndex - 1) * 8 + 1;
            int endHoleIndex = startHoleIndex + 8;

            IUIItem plate = MTPMethods.GetMTP(mtpIndex);
            for (int holeIndex = startHoleIndex; holeIndex < endHoleIndex; holeIndex++)
            {
                if (plate.GetMultiple<Label>(MTPProperty.Default.Hole)[holeIndex - 1].Text != "")
                {
                    throw new ValidationException("The selected strip contains a non-empty well.");
                }
            }
        }

        /// <summary>
        ///  AIX only 
        ///  Compares the ControlSchemeDropDownList text to the selected Text
        /// </summary>
        /// <param name="controlSchemeText"></param>
        public static void ValidateControlSchemeDropDownList(string controlSchemeText)
        {
            StatusManager.AddStatus("Validating ControlSchemeDropDown List");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            var actualControlSchemeText = MTPMethods.GetControlSchemeDropDownBox();
            if (controlSchemeText != actualControlSchemeText)
            {
                throw new ValidationException("The selected string " + controlSchemeText + " does not match the actual string " + actualControlSchemeText + ".");
            }
        }

        /// <summary>
        /// Validates a strip's label is approximately aligned with the wells.
        /// Designed for "040 Vertical Strip" Test
        /// </summary>
        /// <param name="mtpIndex"></param>
        /// <param name="stripIndex"></param>
        public static void ValidateStripLabelAlignment(int mtpIndex, int stripIndex)
        {
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("ValidateEmptyStrip() cannot be called on non-Storm software.");
            }

            StatusManager.AddStatus("Validating strip label alignment");
            MenuMethods.NavigateTo(MenuProperty.Default.MicrotiterPlates);

            if(ApplicationManager.RackManager == RackManager.classic || ApplicationManager.RackManager == RackManager.SIR)
            {
                int holeIndex = (stripIndex - 1) * 8 + 2;
                IUIItem well = MTPMethods.GetHole(mtpIndex, holeIndex);

                MTPWellTooltip tooltip = MTPMethods.GetWellTooltip(mtpIndex, holeIndex);
                string labelText = tooltip.Test + "\r\n" + tooltip.Name;

                try
                {
                    IUIItem label = ApplicationManager.ApplicationWindow.Get(new ElementIdentifier(textValue: labelText));

                    double leftDiff = System.Math.Abs(label.Bounds.Left - well.Bounds.Left);

                    // Max misalignment = 2 pixels
                    if (leftDiff > 2)
                    {
                        throw new ValidationException("Strip label was misaligned by " + " pixels");
                    }
                }
                catch (AutomationException e)
                {
                    throw new WorkflowException("Failed to find strip label", e);
                }
            }
            else //bOLT STRIPS ARE LAYED VERTICALLY 
            {
                int holeIndex = (stripIndex - 1) * 8 + 2;
                IUIItem well = MTPMethods.GetHole(mtpIndex, holeIndex);

                MTPWellTooltip tooltip = MTPMethods.GetWellTooltip(mtpIndex, holeIndex);
                string labelText = tooltip.Test + "\r\n" + tooltip.Name;

                try
                {
                    IUIItem label = ApplicationManager.ApplicationWindow.Get(new ElementIdentifier(textValue: labelText));

                    double bottomDiff = System.Math.Abs(label.Bounds.Bottom - well.Bounds.Bottom);

                    // Max misalignment = 7 pixels
                    if (bottomDiff > 7)
                    {
                        throw new ValidationException("Strip label was misaligned by " + " pixels");
                    }
                }
                catch (AutomationException e)
                {
                    throw new WorkflowException("Failed to find strip label", e);
                }
            }
            
        }
    }
}
