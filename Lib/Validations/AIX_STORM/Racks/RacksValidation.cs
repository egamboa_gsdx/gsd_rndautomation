﻿using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Status;

using TestStack.White.UIItems;
using TestStack.White.UIItems.WPFUIItems;
using TestStack.White.UIItems.WindowItems;

namespace GSD_RnDAutomation.Validations.AIX_STORM
{
    public class RacksValidation
    {
        /// <summary>
        /// Validates the name of a reagent bottle.
        /// </summary>
        /// <param name="holeIndex">The reagent bottle's hole index, as found from hovering over the bottle in the Racks tab.</param>
        /// <param name="name">The expected name of the reagent bottle.</param>
        public static void ValidateReagentBottleName(int holeIndex, string name, int rackIndex = 0)
        {
            StatusManager.AddStatus("Validating reagent bottle's name");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);
            HelperMethods.Wait();
            Label reagentBottleName = RacksMethods.GetReagentHole(holeIndex, rackIndex);
            if (reagentBottleName.Text != name)
            {
                throw new ValidationException("The reagent bottle's name did not match its expected value");
            }
        }

        /// <summary>
        /// Validates the name of a reagent bottle from its tooltip.
        /// </summary>
        /// <param name="holeIndex">The reagent bottle's hole index, as found from hovering over the bottle in the Racks tab.</param>
        /// <param name="bottleName">The expected name of the reagent bottle.</param>
        public static void ValidateReagentBottleTooltipName(int holeIndex, string bottleName)
        {
            StatusManager.AddStatus("Validating reagent bottle's tooltip's name");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            RackHoleTooltip tooltip = RacksMethods.GetReagentTooltip(holeIndex);
            if (tooltip.Name != bottleName)
            {
                throw new ValidationException("The name in the tooltip for the reagent bottle did not match its expected value");
            }
        }

        /// <summary>
        /// Validates the dead volume of a reagent bottle.
        /// </summary>
        /// <param name="holeIndex">The reagent bottle's hole index, as found from hovering over the bottle in the Racks tab.</param>
        /// <param name="deadVolume">The expected dead volume of the reagent bottle.</param>
        public static void ValidateReagentBottleDeadVolume(int holeIndex, int deadVolume, int rackIndex = 0)
        {
            if (holeIndex < 1 || holeIndex > 16)
            {
                throw new WorkflowException("Cannot set dead volume of reagent bottle with hole index " + holeIndex);
            }

            StatusManager.AddStatus("Validating dead volume for reagent");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            Label reagentBottle = RacksMethods.GetReagentHole(holeIndex, rackIndex);
            reagentBottle.RightClick();
            HelperMethods.Wait();

           // TestStack.White.UIItems.MenuItems.Menu menu = ApplicationManager.ApplicationWindow.PopupMenu(RacksProperty.Default.ChangeDeadVolume);
            //HelperMethods.Click(menu);
            HelperMethods.Click(RacksProperty.Default.ChangeDeadVolume2);
            ApplicationManager.Application.WaitWhileBusy();
            HelperMethods.Wait();

            var windows = ApplicationManager.TryGetWindows();
            foreach (var window in windows)
            {
                if (window.Name == RacksProperty.Default.SetDeadVolumeWindowName || window.Name == "Conjugate_ANA_GSD")
                {
                    TextBox deadVolumeTextBox = window.Get<TextBox>(RacksProperty.Default.DeadVolumeTextBox);

                    if (deadVolumeTextBox.Text != deadVolume.ToString())
                    {
                        HelperMethods.Click(RacksProperty.Default.EnableOverride);
                        HelperMethods.Wait();
                        HelperMethods.Click(CommonProperty.Default.OkButton_automationID, window);
                     

                        throw new ValidationException("The dead volume did not match its expected value");
                    }
                    HelperMethods.Click(RacksProperty.Default.EnableOverride);
                    HelperMethods.Wait();
                    HelperMethods.Click(CommonProperty.Default.OkButton_automationID, window);
                    

                    return;
                }
            }
            throw new WorkflowException("Failed to open Set Dead Volume window");
        }

        /// <summary>
        /// Validates the name of a sample tube. Also works on control tubes.
        /// </summary>
        /// <param name="rackIndex">The sample rack of the sample tube.</param>
        /// <param name="holeIndex">The hole on the sample rack for the sample tube.</param>
        /// <param name="name">The expected name of the sample tube.</param>
        public static void ValidateSampleTubeName(int rackIndex, int holeIndex, string name)
        {
            StatusManager.AddStatus("Validating sample tube name");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            Label controlTube = RacksMethods.GetSampleHole(rackIndex, holeIndex);
            if (controlTube.Text != name)
            {
                throw new ValidationException("Sample tube name did not match its expected value");
            }
        }

        /// <summary>
        /// Validates the name of a control tube from its tooltip.
        /// </summary>
        /// <param name="rackIndex">The sample rack of the sample tube.</param>
        /// <param name="holeIndex">The hole on the sample rack for the sample tube.</param>
        /// <param name="name">The expected name of the control tube.</param>
        public static void ValidateControlTubeTooltipName(int rackIndex, int holeIndex, string name, int reagentRackIndex = 0)
        {
            StatusManager.AddStatus("Validating control tube name from tooltip");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            RackHoleTooltip tooltip;

            //gets reagent rack depending on software (AIX or STORM)
            if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                tooltip = RacksMethods.GetReagentTooltip(holeIndex);
            }
            else
            {
                tooltip = RacksMethods.GetSampleTooltip(rackIndex, holeIndex);
            }
            if (tooltip.Type != "Control")
            {
                throw new ValidationException("Sample tube position does not contain a control");
            }
            if (tooltip.Name != name)
            {
                throw new ValidationException("The name in the tooltip for the control tube did not match its expected value; Expected: " + tooltip.Name + ", Actual: " + name);
            }
        }


        /// <summary>
        /// Validates the name of a sample tube from its tooltip.
        /// </summary>
        /// <param name="rackIndex">The sample rack of the sample tube.</param>
        /// <param name="holeIndex">The hole on the sample rack for the sample tube.</param>
        /// <param name="name">The expected name of the sample tube.</param>
        public static void ValidateSampleTubeTooltipName(int rackIndex, int holeIndex, string name)
        {
            StatusManager.AddStatus("Validating sample tube name from tooltip");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            RackHoleTooltip tooltip = RacksMethods.GetSampleTooltip(rackIndex, holeIndex);
            if (tooltip.Type != "Sample")
            {
                throw new ValidationException("Sample tube position does not contain a sample");
            }

            if (tooltip.Name != name)
            {
                throw new ValidationException("The name in the tooltip for the sample tube did not match its expected value");
            }
        }

        /// <summary>
        /// Validates an empty sample tube from its tooltip.
        /// </summary>
        /// <param name="rackIndex">The sample rack of the sample tube.</param>
        /// <param name="holeIndex">The hole on the sample rack for the sample tube.</param>
        public static void ValidateEmptySampleTubeTooltip(int rackIndex, int holeIndex)
        {
            StatusManager.AddStatus("Validating empty sample tube from tooltip");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            RackHoleTooltip tooltip = RacksMethods.GetSampleTooltip(rackIndex, holeIndex);
            /*if (tooltip.HasType && tooltip.Type != "Nothing")
            {
                throw new ValidationException("Sample tube position is not empty");
            }*/

            if (tooltip.HasName && tooltip.Name != "")
            {
                throw new ValidationException("The name in the tooltip for the sample tube is not blank");
            }
        }

        /// <summary>
        /// Validates an empty reagent tube from its tooltip.
        /// </summary>
        /// <param name="reagentHoleIndex"></param>
        public static void ValidateEmptyReagentTooltip(int reagentHoleIndex)
        {
            StatusManager.AddStatus("Validating empty Reagent from tooltip");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            RackHoleTooltip tooltip = RacksMethods.GetReagentTooltip(reagentHoleIndex);
            
            if (tooltip.HasTest && tooltip.Type != "")
            {
                throw new ValidationException("Reagent tooltip position is not empty");
            }

            if (tooltip.HasName && tooltip.Name != "")
            {
                throw new ValidationException("The name in the tooltip for the reagent is not blank");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index">The index of the wash bottle, base 1</param>
        public static void ValidateWashBottleActive(int index)
        {
            IUIItem bottle = RacksMethods.GetWashBottle(index);
            if (!bottle.Visible)
            {
                throw new ValidationException("Wash bottle " + index + " is not visible");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index">The index of the wash bottle, base 1</param>
        /// <param name="expectedText">The expected tooltip text</param>
        public static void ValidateWashBottleTooltip(int index, string expectedText)
        {
            string fulltext = RacksMethods.GetWashBottleTooltip(index).Get<Label>().Text;
            string[] lines = fulltext.Split(new string[] { "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries);
            string text = lines[0];
            if (text != expectedText)
            {
                throw new ValidationException("Wash bottle " + index + " tooltip does not match its expected value");
            }
        }
        /// <summary>
        /// BY: ERNEST GAMBOA
        /// This validation method validates the volume is viewable 
        /// from the tooltip on the RACKS TAB
        /// </summary>
        /// <param name="rackIndex">Rack index for the tube tooltip</param>
        /// <param name="holeIndex">Hole Index for the tube tooltip</param>
        public static void ValidateReagentVolumeTooltip(int rackIndex, int holeIndex)
        {
            StatusManager.AddStatus("Validating Reagent Volume Requirement from tooltip");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);

            RackHoleTooltip tooltip = RacksMethods.GetReagentTooltip(holeIndex);
            if (tooltip.HasName)
            {
                if (!tooltip.HasVolume)
                {
                    throw new ValidationException("There should at least be a tooltip suggesting 0mL; i.e.(Total Volume Requirements 0mL");
                }

            }
            else
            {
                throw new ValidationException("No Item is placed in Rack Index: " + rackIndex + ", Hole Index: " + holeIndex);
            }
        }
        /// <summary>
        /// BY: ERNEST GAMBOA
        /// This validation method validates the volume requirement changes
        /// from the tooltip when adding sample(s)
        /// </summary>
        /// <param name="rackIndex">Rack index for the tube tooltip</param>
        /// <param name="holeIndex">Hole Index for the tube tooltip</param>
        /// <param name="lastSampleNum">Index of last sample in sample rack</param>
        public static void ValidateAddSampleReagentVolumeChangeTooltip(int rackIndex = 4, int holeIndex = 4, int lastSampleNum = 10)
        {
            StatusManager.AddStatus("Grabbing original volume");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);
            RackHoleTooltip tooltip = RacksMethods.GetReagentTooltip(holeIndex);
            string oldVolume, newVolume;
            int i = lastSampleNum + 1;
            if (tooltip.HasVolume)
            {
                oldVolume = newVolume = tooltip.Volume;
                while (newVolume == oldVolume)
                {
                    //adds a sample to the worklist
                    //WorklistMethods.AddSamplesAutoIncrement(numberOfSamples: 1, startNumber: i, controlScheme: WorklistProperty.Default.ControlsOnlyOnFirstPlate);
                    WorklistMethods.AddSamples(choice: GeneralProperty.Default.AutoIncrementAdd,
                                               numberOfSamples: 1,
                                               startNumber: i,
                                               controlScheme: WorklistProperty.Default.ControlsOnlyOnFirstPlate);
                    //goes back to RACKS tab to grab updated volume requirmetn tooltip
                    StatusManager.AddStatus("Checking if volume has changed after adding sample...");
                    MenuMethods.NavigateTo(MenuProperty.Default.Racks);
                    newVolume = RacksMethods.GetReagentTooltip(holeIndex).Volume;
                    i++;
                    if (i == 15)
                    {
                        throw new ValidationException("Adding samples did not change volume requirements...");
                    }
                }

            }
            else
            {
                throw new ValidationException("No volume! Tooltip should display volume requirements for this reagent");
            }
        }
        /// <summary>
        /// BY: ERNEST GAMBOA
        /// This validation method validates the volume requirement changes
        /// from the tooltip when removing sample(s)
        /// </summary>
        /// <param name="rackIndex">Rack index for the tube tooltip</param>
        /// <param name="holeIndex">Hole Index for the tube tooltip</param>
        /// <param name="lastSampleNum">Index of last sample in sample rack</param>
        public static void ValidateRemoveSampleReagentVolumeCahgenTooltip(int rackIndex = 4, int holeIndex = 4, int lastSampleNum = 10)
        {
            StatusManager.AddStatus("Grabbing original volume");
            MenuMethods.NavigateTo(MenuProperty.Default.Racks);
            RackHoleTooltip tooltip = RacksMethods.GetReagentTooltip(holeIndex);
            string oldVolume, newVolume;
            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            StatusManager.AddStatus("Getting last Sample ID");
            int i = SamplesGrid.Rows.Count;
            StatusManager.AddStatus("Last Sample ID: #" + i);
            if (tooltip.HasVolume)
            {
                oldVolume = newVolume = tooltip.Volume;
                while (newVolume == oldVolume)
                {
                    //removes sample from worklist; highlights last sample and removes
                    WorklistMethods.HighlightSamples(i);
                    WorklistMethods.RemoveCurrentSamples();
                    //goes back to RACKS tab and grabs updated volume requirement tooltip
                    StatusManager.AddStatus("Checking if volume has changed after removing sample...");
                    MenuMethods.NavigateTo(MenuProperty.Default.Racks);
                    newVolume = RacksMethods.GetReagentTooltip(holeIndex).Volume;
                    i--;
                    if (i == 0)
                    {
                        throw new ValidationException("Removing samples did not adjust Total Volume Requirmeent Tooltip");
                    }

                }

            }
            else
            {
                throw new ValidationException("No volume! Tooltip should display volume requirements for this reagent");
            }
        }
    }
}
