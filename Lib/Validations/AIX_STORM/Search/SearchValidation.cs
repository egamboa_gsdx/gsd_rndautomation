﻿using System;

using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Status;

using TestStack.White.UIItems;


namespace GSD_RnDAutomation.Validations.AIX_STORM
{
    public class SearchValidation
    {
        /// <summary>
        /// Checks the values of each row of the search results.
        /// </summary>
        /// <param name="patientID">The expected "Patient ID" value, or null to ignore</param>
        /// <param name="worklist">The expected "Worklist" value, or null to ignore</param>
        /// <param name="operatorName">The expected "Operator" value, or null to ignore</param>
        /// <param name="instrument">The expected "Instrument" value, or null to ignore</param>
        /// <param name="sampleID">The expected "Sample ID" value, or null to ignore</param>
        /// <param name="AIXSampleID">The expexted "Sample ID" value, or null to ignore</param>
        /// <param name="test">The expected "Test" value, or null to ignore</param>
        /// <param name="minimumDate">The lower date limit (inclusive), or null for no lower limit</param>
        /// <param name="maximumDate">The upper date limit (exclusive), or null for no upper limit</param>
        /// <param name="exactMatch">True for exact string matches, False for substring matches</param>
        public static void ValidateAllSearchResults(
                string patientID = null,
                string worklist = null,
                string operatorName = null,
                string instrument = null,
                string sampleID = null,
                string AIXSampleID = null,
                string test = null,
                DateTime? minimumDate = null,
                DateTime? maximumDate = null,
                bool exactMatch = false
            )
        {
            StatusManager.AddStatus("Validating All Search Results");
            MenuMethods.NavigateTo(MenuProperty.Default.Search);

            // Get the search results table
            ListView ResultsDataGrid = ApplicationManager.ApplicationWindow.Get<ListView>(SearchProperty.Default.SearchResultDataGrid);
            ListViewRows rows = ResultsDataGrid.Rows;

            // For each row
            for (int i = 0; i < rows.Count; i++)
            {
                try
                {
                    SearchResultEntry entry = new SearchResultEntry(rows[i]);

                    // Perform validation
                    ValidateSingleSearchResult(
                        entry,
                        patientID,
                        worklist,
                        operatorName,
                        instrument,
                        sampleID,
                        AIXSampleID,
                        test,
                        minimumDate,
                        maximumDate,
                        exactMatch
                    );
                }
                catch (ValidationException e)
                {
                    throw new ValidationException("Issue on row " + (i + 1) + ": " + e.Message);
                }
            }
        }

        /// <summary>
        /// Checks the values of a single row of the search results.
        /// </summary>
        /// <param name="patientID">The expected "Patient ID" value, or null to ignore</param>
        /// <param name="worklist">The expected "Worklist" value, or null to ignore</param>
        /// <param name="operatorName">The expected "Operator" value, or null to ignore</param>
        /// <param name="instrument">The expected "Instrument" value, or null to ignore</param>
        /// <param name="sampleID">The expected "Sample ID" value, or null to ignore</param>
        /// <param name="AIXSampleID">The expected "Sample ID" value or null to ignore</param>
        /// <param name="test">The expected "Test" value, or null to ignore</param>
        /// <param name="minimumDate">The lower date limit (inclusive), or null for no lower limit</param>
        /// <param name="maximumDate">The upper date limit (exclusive), or null for no upper limit</param>
        /// <param name="exactMatch">True for exact string matches, False for substring matches</param>
        internal static void ValidateSingleSearchResult(
                SearchResultEntry entry,
                string patientID = null,
                string worklist = null,
                string operatorName = null,
                string instrument = null,
                string sampleID = null,
                string AIXSampleID = null,
                string test = null,
                DateTime? minimumDate = null,
                DateTime? maximumDate = null,
                bool exactMatch = false
            )
        {
            // Validate the string properties
            ValidateProperty(entry.PatientID, patientID, "Patient Id", exactMatch);
            ValidateProperty(entry.Worklist, worklist, "Worklist", exactMatch);
            ValidateProperty(entry.Operator, operatorName, "Operator", exactMatch);
            ValidateProperty(entry.Instrument, instrument, "Instrument", exactMatch);
            ValidateProperty(entry.SampleID, sampleID, "Sample ID", exactMatch);
            ValidateProperty(entry.AIXSampleID, AIXSampleID, "SampleID", exactMatch);
            ValidateProperty(entry.Test, test, "Test", exactMatch);

            // Validate the Date/Time property
            if (minimumDate != null)
            {
                if (minimumDate.Value.Date > entry.DateTime.Date)
                {
                    throw new ValidationException("The datetime " + entry.DateTime + " is before the minimum date " + minimumDate.Value.Date);
                }
            }
            if (maximumDate != null)
            {
                if (maximumDate.Value.Date <= entry.DateTime.Date)
                {
                    throw new ValidationException("The datetime " + entry.DateTime + " is on or after the maximum date " + maximumDate.Value.Date);
                }
            }
        }

        private static void ValidateProperty(object result1, string result2, string v, bool exactMatch)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Compares an actual value to an expected value and throws a ValidationException for mismatches.
        /// </summary>
        /// <param name="actualValue">The actual value found from the GUI</param>
        /// <param name="expectedValue">The expected value from the test case, or null to automatically pass</param>
        /// <param name="propertyName">The name of the property being checked, used to format ValidationException</param>
        /// <param name="exactMatch">True for exact string match, False for substring match</param>
        internal static void ValidateProperty(string actualValue, string expectedValue, string propertyName, bool exactMatch)
        {
            if (expectedValue != null)
            {
                if (exactMatch) // Exact match
                {
                    if (actualValue != expectedValue)
                    {
                        throw new ValidationException("Expected " + propertyName + " \"" + expectedValue + "\" but found \"" + actualValue + "\"");
                    }
                }
                else // Substring match
                {
                    // Check proper substring or full match
                    if (!actualValue.Contains(expectedValue))
                    {
                        throw new ValidationException("Expected " + propertyName + " \"" + expectedValue + "\", not found in \"" + actualValue + "\"");
                    }
                }
            }
        }
    }
}
