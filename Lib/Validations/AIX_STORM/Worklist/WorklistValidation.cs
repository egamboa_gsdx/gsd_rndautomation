﻿using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Status;

using System.Collections.Generic;
using System.Net.NetworkInformation;

using TestStack.White.UIItems;
using TestStack.White.UIItems.WPFUIItems;
using System.Windows.Automation;
using TestStack.White.AutomationElementSearch;

namespace GSD_RnDAutomation.Validations.AIX_STORM
{
    public class WorklistValidation
    {
        /// <summary>
        /// Storm only.
        /// Validates the addition of the ANA_GSD test.
        /// Passes only when all of the following are true:
        /// <para> Worklist > A column was added for the new test </para>
        /// <para> Home > "Number of Tests to Run" was incremented </para>
        /// <para> Home > The test appears on the timing diagram </para>
        /// </summary>
        public static void ValidateAddTest()
        {
            StatusManager.AddStatus("Validating Add Test");

            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("ValidateAddTest() cannot be run in non-Storm software.");
            }

            // Validate the Worklist tab
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);

            // Some machines will have an extra row of checkboxes on the farthest left side
            int expectedNumberOfColumns = WorklistMethods.GetShiftedSampleIndex(4);

            // Verify a column was added for the new test
            if (SamplesGrid.Header.Columns.Count != expectedNumberOfColumns)
            {
                throw new ValidationException("Unexpected number of columns in the samples grid");
            }

            // Verify the new column name matches its expected value
            // TODO cannot find the named header column


            // Validate the Home tab
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Verify the test incremented the "Number of Tests to run" field
            Label TestsToRun = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormNumberOfTestsToRun);
            if (TestsToRun.Text != "1")
            {
                throw new ValidationException("Number of tests to run is not 1");
            }

            // Verify the test was added to the timing diagram
            IUIItem TimingDiagram = ApplicationManager.ApplicationWindow.Get(HomeProperty.Default.TimingDiagram);
            Label[] TimingDiagramTests = TimingDiagram.GetMultiple<Label>(HomeProperty.Default.TimingDiagramTest);

            if (TimingDiagramTests.Length != 1)
            {
                throw new ValidationException("No tests found in timing diagram");
            }

            if (TimingDiagramTests[0].Text != "T1: ANA_GSD")
            {
                throw new ValidationException("ANA_GSD not found in timing diagram");
            }
        }

        /// <summary>
        /// Validates adding a test results in some error. Specifically, this passes when AddTest throws a WorkflowException.
        /// Parameters are passed directly to AddTest.
        /// </summary>
        /// <param name="numberOfTests"></param>
        /// <param name="testName"></param>
        public static void ValidateCannotAddTest(int numberOfTests = 1, string testName = "ANA_GSD")
        {
            try
            {
                WorklistMethods.AddTest(numberOfTests, testName);
                throw new ValidationException("Was unexpectedly able to add a test");
            }
            catch (WorkflowException)
            {

            }
        }

        /// <summary>
        /// Validates all samples were assigned to a test.
        /// Passes only when all of the following are true:
        /// <para> Worklist > Every sample's checkbox is checked for the test </para>
        /// </summary>
        /// <param name="testIndex">One-based index of the test</param>
        public static void ValidateAssignAllSamples(int testIndex = 1)
        {
            StatusManager.AddStatus("Validating Assign All Samples");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            ListViewRows SampleRows = SamplesGrid.Rows;

            // Some machines will have an extra row of checkboxes on the farthest left side
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                testIndex = WorklistMethods.GetShiftedSampleIndex(testIndex);
            }

            // Ensure each sample is assigned to the test
            for (int i = 0; i < SampleRows.Count; i++)
            {
                UIItem Sample = new UIItem(SampleRows[i].AutomationElement, SampleRows[i].ActionListener);
                if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                {
                    CheckBox[] SampleTestCheckboxes = Sample.GetMultiple<CheckBox>(WorklistProperty.Default.StormSamplesGridTestCheckbox);
                    CheckBox SampleTestCheckbox = SampleTestCheckboxes[testIndex - 1];

                    if (!SampleTestCheckbox.IsSelected)
                    {
                        throw new ValidationException("Sample " + (i + 1) + " is not assigned to the test");
                    }
                }
                else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    RadioButton[] SampleTestRadioButtons = Sample.GetMultiple<RadioButton>(WorklistProperty.Default.AIXSamplesGridTestCheckbox);
                    RadioButton SampleTestRadioButton = SampleTestRadioButtons[testIndex];

                    if (!SampleTestRadioButton.IsSelected)
                    {
                        throw new ValidationException("Sample " + (i + 1) + " is not assigned to the test");
                    }
                }
            }
        }

        /// <summary>
        /// Validates a sample was assigned to a test.
        /// Passes only when all of the following are true:
        /// <para> Worklist > The sample's checkbox is checked for the test </para>
        /// </summary>
        /// <param name="sampleIndex">One-based index of sample</param>
        /// <param name="testIndex">One-based index of test</param>
        public static void ValidateAssignSample(int sampleIndex = 1, int testIndex = 0)
        {
            StatusManager.AddStatus("Validating Assign Sample");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            ListViewRows SampleRows = SamplesGrid.Rows;

            // Ensure the sample is in the samples grid
            if (sampleIndex > SampleRows.Count)
            {
                throw new ValidationException("Sample " + sampleIndex + " not found in samples grid");
            }

            ListViewRow SampleRow = SampleRows[sampleIndex - 1];
            UIItem Sample = new UIItem(SampleRow.AutomationElement, SampleRow.ActionListener);

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                // Some machines will have an extra row of checkboxes on the farthest left side
                testIndex = WorklistMethods.GetShiftedSampleIndex(testIndex);

                CheckBox[] SampleTestCheckboxes = Sample.GetMultiple<CheckBox>(WorklistProperty.Default.StormSamplesGridTestCheckbox);
                CheckBox SampleTestCheckbox = SampleTestCheckboxes[testIndex - 1];

                if (!SampleTestCheckbox.IsSelected)
                {
                    throw new ValidationException("Sample " + sampleIndex + " is not assigned to the test");
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                RadioButton[] SampleTestRadioButtons = Sample.GetMultiple<RadioButton>(WorklistProperty.Default.AIXSamplesGridTestCheckbox);
                
                if (ApplicationManager.RackManager == RackManager.classic)
                {
                    RadioButton SampleTestRadioButton = SampleTestRadioButtons[testIndex];
                    if (SampleTestRadioButtons[0].IsSelected && testIndex - 1 != 0)
                    {
                        throw new ValidationException("Sample " + sampleIndex + " is not assigned to the test");
                    }
                    else if (!SampleTestRadioButton.IsSelected)
                    {
                        throw new ValidationException("Sample " + sampleIndex + " is not assigned the correct test...");
                    }
                }
                else if (ApplicationManager.RackManager == RackManager.SIR)
                {
                    RadioButton SampleTestRadioButton = SampleTestRadioButtons[testIndex ];
                    if (SampleTestRadioButtons[0].IsSelected && testIndex - 5 != 0)
                    {
                        throw new ValidationException("Sample " + sampleIndex + " is not assigned to the test");
                    }
                    else if (!SampleTestRadioButton.IsSelected)
                    {
                        throw new ValidationException("Sample " + sampleIndex + " is not assigned the correct test...");
                    }
                }
                    
            }
        }

        /// <summary>
        /// Validates adding samples with auto increment.
        /// Passes only when all of the following are true:
        /// <para> Worklist > Verify the correct number of samples were added to the grid </para>
        /// <para> Worklist > Verify the name of each sample matches its expected value </para>
        /// <para> Racks > Samples are added to the correct racks/holes </para>
        /// </summary>
        /// <param name="quantity">The number of samples added with auto increment</param>
        public static void ValidateAddSamplesAutoIncrement(int quantity, string sampleIdTemplate = "Sample #{0}")
        {
            StatusManager.AddStatus("Validating Auto Increment");
            string[] SampleNames = new string[quantity];

            for (int i = 0; i < quantity; i++)
            {
                // SampleNames[i] = "Sample #" + (i + 1);
                SampleNames[i] = string.Format(sampleIdTemplate, (i + 1));
            }
            ValidateSampleNames(SampleNames);
        }

        /// <summary>
        /// Validates a sample is not assigned to a test.
        /// Passes only when all of the following are true:
        /// <para> Worklist > The sample's checkbox is not checked for the test </para>
        /// </summary>
        /// <param name="sampleIndex">One-based index of sample</param>
        /// <param name="testIndex">One-based index of test</param>
        public static void ValidateUnassignSample(int sampleIndex, int testIndex)
        {
            StatusManager.AddStatus("Validating Unassign Sample");

            /* if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
             {
                 throw new SoftwareException("ValidateUnassignSample() cannot be run in non-Storm software.");
             }
            */
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            ListViewRow SampleRow = SamplesGrid.Rows[sampleIndex - 1];
            UIItem Sample = new UIItem(SampleRow.AutomationElement, SampleRow.ActionListener);

            // Some machines will have an extra row of checkboxes on the farthest left side
            testIndex = WorklistMethods.GetShiftedSampleIndex(testIndex);

            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                CheckBox[] SampleTestCheckboxes = Sample.GetMultiple<CheckBox>(WorklistProperty.Default.StormSamplesGridTestCheckbox);
                CheckBox SampleTestCheckbox = SampleTestCheckboxes[testIndex - 1];

                if (SampleTestCheckbox.IsSelected)
                {
                    throw new ValidationException("Sample " + sampleIndex + " is not unassigned for the test");
                }
            }
        }


        /// <summary>
        /// Validates that the specified number of samples have been moved to the bottom
        /// Passes only when all of the following are true:
        /// <para> Worklist > Last sample ids are the smallest numbers</para>
        /// </summary>
        /// <param name="samples"> Number of samples that have been dragged and dropped </param>
        public static void ValidateDragAndDrop(int samples)
        {
            StatusManager.AddStatus("Validating Drag and Drop Samples");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);

            // Some machines will have an extra row of checkboxes on the farthest left side
            int sampleNamePosition = WorklistMethods.GetShiftedSampleIndex(1);

            if (samples > 0)
            {
                int count = 1;
                while (samples > 0)
                {
                    if(ApplicationManager.RackManager == RackManager.classic || ApplicationManager.RackManager == RackManager.SIR || ApplicationManager.RackManager == RackManager.bolt)
                    {
                        if (SamplesGrid.Rows[SamplesGrid.Rows.Count - samples].Cells[sampleNamePosition].Text != "Sample #" + count)
                        {
                            throw new ValidationException("The sample in row " + (SamplesGrid.Rows.Count - samples) + " does not match its expected name");
                        }
                    }
                    else 
                    {
                        if (SamplesGrid.Rows[SamplesGrid.Rows.Count - samples].Cells[sampleNamePosition].Text != count.ToString())
                            throw new ValidationException("The sample in row " + (SamplesGrid.Rows.Count - samples) + " does not match its expected name");
                    }
                    count++;
                    samples--;
                }
            }
        }

        /// <summary>
        /// Validates that a sample is highlighted.
        /// </summary>
        /// <param name="sample">The sample to check.</param>
        public static void ValidateSampleHighlighted(int sample)
        {
            StatusManager.AddStatus("Validating sample is highlighted");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);

            if (WorklistMethods.IsExtraColumnPresent())
            {
                if (!SamplesGrid.Rows[sample - 1].Get<CheckBox>().Checked)
                {
                    throw new ValidationException("Sample " + sample + " was not highlighted as expected");
                }
            }
            else
            {
                if (!SamplesGrid.Rows[sample - 1].IsSelected)
                {
                    throw new ValidationException("Sample " + sample + " was not highlighted as expected");
                }
            }
        }

        /// <summary>
        /// Validates that a sample is not highlighted.
        /// </summary>
        /// <param name="sample">The sample to check.</param>
        public static void ValidateSampleNotHighlighted(int sample)
        {
            StatusManager.AddStatus("Validating sample is not highlighted");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);

            if (WorklistMethods.IsExtraColumnPresent())
            {
                if (SamplesGrid.Rows[sample - 1].Get<CheckBox>().Checked)
                {
                    throw new ValidationException("Unexpected sample was highlighted");
                }
            }
            else
            {
                if (SamplesGrid.Rows[sample - 1].IsSelected)
                {
                    throw new ValidationException("Unexpected sample was highlighted");
                }
            }
        }

        /// <summary>
        /// Validates adding samples via import of a predefined file.
        /// Passes only when all of the following are true:
        /// <para> Worklist > Verify the correct number of samples were added to the grid </para>
        /// <para> Worklist > Verify the name of each sample matches its expected value </para>
        /// <para> Racks > Samples are added to the correct racks/holes </para>
        /// </summary>
        public static void ValidateAddSamplesImport()
        {
            StatusManager.AddStatus("Validating Import Samples");
            if(ApplicationManager.RackManager == RackManager.classic )
            {
                string[] SampleNames = new string[64];
                for (int i = 0; i < 64; i++)
                {
                    SampleNames[i] = (i + 1).ToString();
                }
                ValidateSampleNames(SampleNames);
            }
            else
            {
                string[] SampleNames = new string[16];
                for (int i = 0; i < 16; i++)
                {
                    SampleNames[i] = "Sample #" + (i + 1).ToString();
                }
                ValidateSampleNames(SampleNames);
            }
            
        }

        /// <summary>
        /// Validates adding samples via manual add.
        /// Passes only when all of the following are true:
        /// <para> Worklist > Verify the correct number of samples were added to the grid </para>
        /// <para> Worklist > Verify the name of each sample matches its expected value </para>
        /// <para> Racks > Samples are added to the correct racks/holes </para>
        /// </summary>
        /// <param name="SampleID"> The specified ID of the manually added sample </param>
        public static void ValidateAddSamplesManual(string SampleID)
        {
            StatusManager.AddStatus("Validating Manual Add Samples");
            string[] SampleNames = new string[] { SampleID };
            ValidateSampleNames(SampleNames);
        }

        /// <summary>
        /// Validates multiplied samples from a list sample numbers.
        /// </summary>
        /// <param name="sampleNumbers">Sample numbers from auto increment</param>
        public static void ValidateMultiplySamples(int[] sampleNumbers)
        {
            StatusManager.AddStatus("Validating Multiply samples");

            SortedDictionary<int, int> aixIndices = new SortedDictionary<int, int>();

            string[] sampleNames = new string[sampleNumbers.Length];

            for (int i = 0; i < sampleNumbers.Length; i++)
            {
                if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                {
                    int number = sampleNumbers[i];
                    if (!aixIndices.ContainsKey(number))
                    {
                        sampleNames[i] = "Sample #" + number;
                        aixIndices.Add(number, 1);
                    }
                    else
                    {
                        int aixIndex;
                        aixIndices.TryGetValue(number, out aixIndex);
                        aixIndex++;
                        sampleNames[i] = "Sample #" + number + "_" + aixIndex;
                        aixIndices[number] = aixIndex;
                    }
                }
                else // Storm
                {
                    sampleNames[i] = "Sample #" + sampleNumbers[i];
                }
            }

            ValidateSampleNames(sampleNames, true, false);
        }

        /// <summary>
        /// Validates multiplying currently selected samples by a specified factor
        /// Passes only when all of the following are true:
        /// <para> Worklist > Verify each multiplied sample matches its parent name </para>
        /// </summary>
        /// <param name="factor">The multiplication factor</param>
        public static void ValidateMultiplySamples(int factor)
        {
            StatusManager.AddStatus("Validating Multiply " + factor + " Samples");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
            ListViewRows SamplesList = SamplesGrid.Rows;

            // Verify each multiplied sample matches its parent name
            // Some machines will have an extra row of checkboxes on the farthest left side
            int sampleNamePosition = WorklistMethods.GetShiftedSampleIndex(1);

            for (int i = 0; i < SamplesList.Count;)
            {
                ListViewRow Sample = SamplesList[i];
                if (Sample.IsSelected)
                {
                    string sampleName = Sample.Cells[sampleNamePosition].Text;
                    int amount = i + factor;
                    int aixCount = 2;
                    i++;

                    while (i < amount)
                    {
                        // AIX adds "_#" to the end of each sample copy
                        if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
                        {
                            sampleName = Sample.Cells[sampleNamePosition].Text + "_" + aixCount;
                            aixCount++;
                        }

                        if (SamplesList[i].Cells[sampleNamePosition].Text != sampleName)
                        {
                            throw new ValidationException("The sample in row " + (i + 1) + " does not match its expected name");
                        }
                        i++;
                    }
                }
                i++;
            }
        }

        /// <summary>
        /// Validate the remove samples button does not work
        /// </summary>
        public static void ValidateCannotRemoveSamples()
        {
            StatusManager.AddStatus("Validating cannot remove samples");

            try
            {
                WorklistMethods.RemoveSamples();
                throw new ValidationException("Unexpectatedly removed multiplied sample");
            }
            catch (WorkflowException)
            {

            }
        }

        /// <summary>
        /// Validate the remove samples button displays a confirmation dialog.
        /// 
        /// This method requires samples to be highlighted and will remove said samples.
        /// </summary>
        public static void ValidateRemoveSamplesAlert()
        {
            StatusManager.AddStatus("Validating remove samples alert");

            try
            {
                WorklistMethods.RemoveSamples();
            }
            catch (WorkflowException e)
            {
                // If the exception is for the alert
                if (e.Message == WorklistMethods.removeSamplesAlertMissingMessage)
                {
                    // Throw it as a ValidationException
                    throw new ValidationException(e.Message);
                }
                // Otherwise keep the original WorkflowException
                throw e;
            }
        }

        /// <summary>
        /// Validation for a new worklist.
        /// Passes only when all of the following are true:
        /// <para> Home > Verify the worklist name is blank </para>
        /// <para> Home > Verify the number of samples is zero </para>
        /// <para> Home > Verify the number of tests is zero (Storm only) </para>
        /// </summary>
        public static void ValidateNew()
        {
            StatusManager.AddStatus("Validating New");
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Ensure the worklist name is blank
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Label worklistName = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormWorklistName);
                if (worklistName.Text != "")
                {
                    throw new ValidationException("New worklist name is not blank");
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                Label worklistName = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXWorklistName);
                if (worklistName.Text != "")
                {
                    throw new ValidationException("New worklist name is not blank");
                }
            }

            // Ensure the number of samples is zero
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Label numberOfSamples = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormNumberOfSamplesLoaded);
                if (numberOfSamples.Text != "0")
                {
                    throw new ValidationException("Number of samples is not zero");
                }
            }
            else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX)
            {
                Label numberOfSamples = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.AIXNumberOfSamplesLoaded);
                if (numberOfSamples.Text != "0")
                {
                    throw new ValidationException("Number of samples is not zero");
                }
            }

            // Storm only
            // Ensure the number of tests is zero
            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                Label numberOfTests = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormNumberOfTestsToRun);
                if (numberOfTests.Text != "0")
                {
                    throw new ValidationException("Number of tests is not zero");
                }
            }
        }

        /// <summary>
        /// Validates the New button is disabled (should occur when worklist is running).
        /// </summary>
        public static void ValidateNewDisabledWhileRunningWorklist()
        {
            StatusManager.AddStatus("New Button Disabled During Worklist Run");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
            Button NewButton = ApplicationManager.ApplicationWindow.Get<Button>(WorklistProperty.Default.NewButton);
            if (NewButton.Enabled)
            {
                throw new ValidationException("New Button is enabled when it was expected to be disabled.");
            }
        }

        /// <summary>
        /// Validate removing all samples
        /// </summary>
        public static void ValidateRemoveAllSamples()
        {
            StatusManager.AddStatus("Validating Remove All Samples");

            string[] SampleNames = new string[0];
            ValidateSampleNames(SampleNames);
        }

        /// <summary>
        /// Storm only.
        /// Validates the ANA_GSD test was removed.
        /// Passes only when all of the following are true:
        /// <para> Worklist > The column for the test was removed </para>
        /// <para> Home > "Number of Tests to Run" was decremented </para>
        /// <para> Home > The test is removed from the timing diagram </para>
        /// </summary>
        public static void ValidateRemoveTest()
        {
            StatusManager.AddStatus("Validating Remove Test");

            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("ValidateRemoveTest() cannot be run in non-Storm software.");
            }

            // Validate the Worklist tab
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);

            // Some machines will have an extra row of checkboxes on the farthest left side
            int expectedNumberOfColumns = WorklistMethods.GetShiftedSampleIndex(3);

            // Verify the column for the test was removed
            if (SamplesGrid.Header.Columns.Count != expectedNumberOfColumns)
            {
                throw new ValidationException("Unexpected number of columns in the samples grid");
            }

            // Validate the Home tab
            MenuMethods.NavigateTo(MenuProperty.Default.Home);

            // Verify the "Number of Tests to run" field is zero
            Label TestsToRun = ApplicationManager.ApplicationWindow.Get<Label>(HomeProperty.Default.StormNumberOfTestsToRun);
            if (TestsToRun.Text != "0")
            {
                throw new ValidationException("Number of tests to run is not 0");
            }

            // Verify the test was removed from the timing diagram
            try
            {
                var TimingDiagram = ApplicationManager.ApplicationWindow.Get(HomeProperty.Default.TimingDiagram);
                throw new ValidationException("The timing diagram is still accessible");
            }
            catch
            {
                // do nothing- timing diagram is not available as expected
            }
        }

        /// <summary>
        /// Validates the samples by quantity and name.
        /// Passes only when all of the following are true:
        /// <para> Worklist > Verify the correct number of samples are found in the grid </para>
        /// <para> Worklist > Verify the name of each sample matches its expected value </para>
        /// <para> Racks > Sample names in each hole match their expected value </para>
        /// </summary>
        /// <param name="sampleNames">A list of sample names</param>
        private static void ValidateSampleNames(string[] sampleNames, bool validateWorklist = true, bool validateRacks = true)
        {
            var positionInRack = 64;
            if (validateWorklist)
            {
                // Validate the Worklist tab
                MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

                ListView SamplesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.SamplesGrid);
                ListViewRows SamplesList = SamplesGrid.Rows;

                // Verify the number of rows matches the number of samples added
                if (SamplesList.Count != sampleNames.Length)
                {
                    throw new ValidationException("The number of sample rows does not match the number of expected samples");
                }

                int sampleNamePosition = 0;
                // Some machines will have an extra row of checkboxes on the farthest left side
                if (SamplesList.Count > 0)
                {
                    sampleNamePosition = WorklistMethods.GetShiftedSampleIndex(1);
                }

                // Verify each name matches its expected value
                for (int i = 0; i < sampleNames.Length; i++)
                {
                    ListViewRow Sample = SamplesList[i];
                    if (Sample.Cells[sampleNamePosition].Text != sampleNames[i])
                    {
                        throw new ValidationException("The sample in row " + (i + 1) + " does not match its expected name");
                    }
                }
            }

            if (validateRacks)
            {
                // Validate the Racks tab
                MenuMethods.NavigateTo(MenuProperty.Default.Racks);

                Label[][] Samples = new Label[12][];
                if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                {
                    if (ApplicationManager.RackManager == RackManager.bolt)
                    {
                        Samples[0] = ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack).GetMultiple<Label>(RacksProperty.Default.SampleHole);
                        Samples[1] = ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack).GetMultiple<Label>(RacksProperty.Default.SampleHole);
                        Samples[2] = ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack).GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    }
                    else
                    {
                        Samples[0] = ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack1).GetMultiple<Label>(RacksProperty.Default.SampleHole);
                        Samples[1] = ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack2).GetMultiple<Label>(RacksProperty.Default.SampleHole);
                        Samples[2] = ApplicationManager.ApplicationWindow.Get(RacksProperty.Default.StormSampleRack3).GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    }
                   
                }
                else if (ApplicationManager.InstrumentManager == InstrumentManager.AIX && ApplicationManager.RackManager == RackManager.classic)
                {
                    IUIItem[] Racks = ApplicationManager.ApplicationWindow.GetMultiple(RacksProperty.Default.AIXSampleRack);

                    Samples[0] = Racks[3].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[1] = Racks[2].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    //           Racks[1] is the reagent rack
                    Samples[2] = Racks[0].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                }
                //ApplicationManager.InstrumentManager == InstrumentManager.AIX &&
                if ( ApplicationManager.RackManager == RackManager.SIR) 
                {
                    positionInRack = 16;
                    IUIItem[] Racks = ApplicationManager.ApplicationWindow.GetMultiple(RacksProperty.Default.AIXSampleRack);

                    Samples[0] = Racks[0].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[1] = Racks[1].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[2] = Racks[2].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[3] = Racks[3].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[4] = Racks[4].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[5] = Racks[5].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[6] = Racks[6].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[7] = Racks[7].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[8] = Racks[8].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[9] = Racks[9].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[10] = Racks[10].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    Samples[11] = Racks[11].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                    //reagent rack    Racks[12].GetMultiple<Label>(RacksProperty.Default.SampleHole);
                }

                // Verify each name matches its expected value

                if (ApplicationManager.RackManager != RackManager.bolt)
                {
                    for (int i = 0; i < 192; i++)
                    {
                        int HoleIndex = (i % positionInRack) + 1;
                        int RackIndex = (i / positionInRack) + 1;
                        Label Hole = Samples[RackIndex - 1][HoleIndex - 1];

                        if (Hole.Text != (i < sampleNames.Length ? sampleNames[i] : ""))
                        {
                            // Storm puts controls in racks when a test is added, ignore them
                            if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
                            {
                                // Holes 62 - 64
                                if (HoleIndex >= 62)
                                {
                                    // names are PC_ANA_GSD, CO_ANA_GSD and NC_ANA_GSD
                                    if (Hole.Text.Substring(2) == "_ANA_GSD")
                                    {
                                        continue;
                                    }
                                }
                            }

                            throw new ValidationException("The sample in rack " + RackIndex + " hole " + HoleIndex + " does not match its expected name");
                        }
                    }
                }
                   
                
                
            }
        }

        /// <summary>
        /// Storm only.
        /// Validates the lot number for a test.
        /// Passes only when all of the following are true:
        /// <para> Worklist > Test properties > Lot number matches expected value. </para>
        /// </summary>
        /// <param name="lotNumber">The expected lot number.</param>
        /// <param name="testIndex">The one-based test index to verify (Default: first test).</param>
        public static void ValidateLotNumber(int lotNumber, int testIndex = 1)
        {
            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("WorklistValidation.ValidateLotNumber() cannot be run in non-Storm software. Be sure to use HomeValidation.ValidateLotNumber() on AIX.");
            }

            StatusManager.AddStatus("Validating Lot Number");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            WorklistMethods.OpenTestPropertiesWindow(testIndex);
            TextBox lotNumberTextBox = ApplicationManager.ApplicationWindow.Get<TextBox>(WorklistProperty.Default.LotNumberTextbox);
            if (lotNumberTextBox.Text != lotNumber.ToString())
            {
                HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
                throw new ValidationException("Lot number for test " + testIndex + " does not match its expected value of \"" + lotNumber + "\"");
            }

            HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
        }

        /// <summary>
        /// Storm only.
        /// Validates the expiration date for a test.
        /// Passes only when all of the following are true:
        /// <para> Worklist > Test properties > Expiration date matches expected value. </para>
        /// </summary>
        /// <param name="date">The expected expiration date. Should be in the format "MM/DD/YYYY".</param>
        /// <param name="testIndex">The one-based test index to verify (Default: first test).</param>
        public static void ValidateExpirationDate(string date, int testIndex = 1)
        {
            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("WorklistValidation.ValidateExpiration() cannot be run in non-Storm software. Be sure to use HomeValidation.ValidateExpiration() on AIX.");
            }

            StatusManager.AddStatus("Validating expiration date");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            WorklistMethods.OpenTestPropertiesWindow(testIndex);
            TextBox expirationDateTextBox = ApplicationManager.ApplicationWindow.Get<TextBox>(WorklistProperty.Default.ExpirationDateTextbox);
            if (expirationDateTextBox.Text != date)
            {
                HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
                throw new ValidationException("Expiration date for test " + testIndex + " does not match its expected value of \"" + date + "\"");
            }

            HelperMethods.Click(CommonProperty.Default.OkButton_textValue);
        }

        /// <summary>
        /// Validates the tooltip that appears when hovering over a test.
        /// Compares values to those found in test properties.
        /// </summary>
        /// <param name="testIndex">The one-based test index to verify (Default: first test).</param>
        public static void ValidateTestTooltip(int testIndex = 1)
        {
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);
          
            WorklistMethods.OpenTestPropertiesWindow(testIndex);

            // Get the lot number from the test properties
            TextBox lotNumberTextBox = ApplicationManager.ApplicationWindow.Get<TextBox>(WorklistProperty.Default.LotNumberTextbox);
            string lotNumber = lotNumberTextBox.Text;

            // Get the expiration date from the test properties
            TextBox expirationDateTextBox = ApplicationManager.ApplicationWindow.Get<TextBox>(WorklistProperty.Default.ExpirationDateTextbox);
            string expirationDate = expirationDateTextBox.Text;

            HelperMethods.Click(CommonProperty.Default.OkButton_textValue);

            WorklistMethods.ShowTestTooltip();

            Label[] lines = ApplicationManager.ApplicationWindow.ToolTip.GetMultiple<Label>();

            // Compare
            if (lines[0].Text.Trim() != "Lot Number: " + lotNumber)
            {
                throw new ValidationException("Test tooltip lot number does not match lot number from test properties");
            }
            if (lines[1].Text.Trim() != "Expiration Date: " + expirationDate)
            {
                throw new ValidationException("Test tooltip expiration date does not match expiration date from test properties");
            }
        }

        /// <summary>
        /// Storm only.
        /// Validate a runtime variable for a test.
        /// </summary>
        /// <param name="variableName">The name of the variable.</param>
        /// <param name="value">The expected value.</param>
        /// <param name="testIndex">The index of the test to use.</param>
        public static void ValidateRuntimeVariable(string variableName, double value, int testIndex = 1)
        {
            // Storm only
            if (ApplicationManager.InstrumentManager != InstrumentManager.Storm)
            {
                throw new SoftwareException("WorklistValidation.ValidateRuntimeVariable() cannot be run in non-Storm software.");
            }

            StatusManager.AddStatus("Modifying Runtime Variable");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            WorklistMethods.OpenTestPropertiesWindow(testIndex);

            ListView RuntimeVariablesGrid = ApplicationManager.ApplicationWindow.Get<ListView>(WorklistProperty.Default.RuntimeVariablesGrid);
            foreach (var row in RuntimeVariablesGrid.Rows)
            {
                if (row.Cells[0].Text == variableName)
                {
                    TextBox valueBox = row.Get<TextBox>();
                    if (double.Parse(valueBox.Text) != value)
                    {
                        HelperMethods.Click(CommonProperty.Default.CancelButton_automationID);

                        throw new ValidationException("Runtime variable " + variableName + " has an unexpected value");
                    }

                    HelperMethods.Click(CommonProperty.Default.CancelButton_automationID);

                    return;
                }
            }

            HelperMethods.Click(CommonProperty.Default.CancelButton_automationID);

            throw new WorkflowException("Could not find runtime variable with name " + variableName);
        }

        /// <summary>
        /// by: Ernest Gamboa
        /// Validate if a button is disabled.
        /// </summary>
        /// <param name="buttonName">The name of the worklist button.</param>
        public static void ValidateWorklistButtonDisabled(string buttonName)
        {
            StatusManager.AddStatus("Checking Worklist '" + buttonName.ToUpper() + "' Button disabled while Worklist is running");
            MenuMethods.NavigateTo(MenuProperty.Default.Worklist);

            //Switch case to check if passed in button is enabled. if enabled throw exception
            switch (buttonName.ToUpper())
            {
                case "LOAD":
                    Button loadBtn = ApplicationManager.ApplicationWindow.Get<Button>(WorklistProperty.Default.LoadWorklistButton);
                    if (loadBtn.Enabled)
                    {
                        throw new ValidationException("Worklist Load Button Enabled! Expected Worklist Load Button to be disabled during worklist run...");
                    }
                    break;
                case "SAVE":
                    Button saveBtn = ApplicationManager.ApplicationWindow.Get<Button>(WorklistProperty.Default.SaveWorklistButton);
                    if (saveBtn.Enabled)
                    {
                        throw new ValidationException("Workist Save Button Enabled! Expected Worklist Save Button to be disabled during worklist run...");
                    }
                    break;
                case "NEW":
                    Button newBtn = ApplicationManager.ApplicationWindow.Get<Button>(WorklistProperty.Default.NewButton);
                    if (newBtn.Enabled)
                    {
                        throw new ValidationException("Worklist New Button Enabled! Expected Worklist New Button to be disabled during worklist run...");
                    }
                    break;
                default:
                    throw new ValidationException("No Such '" + buttonName + "' Button Exists! Expecting: 'New', 'Load', 'Save'");
            }
        }
    }
}
