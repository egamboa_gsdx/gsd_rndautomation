﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT
{
    public class TempestGeneralValidations
    {
        /// <summary>
        /// Validate User displayed in Main Title Bar
        /// </summary>
        /// <param name="name">Expected UserName</param>
        /// <exception cref="WorkflowException"></exception>
        /// e.g.
        public static void ValidateUser(string name)
        {
            StatusManager.AddStatus("Validating Currenter User...");
            try
            {
                string user = TempestGeneralMethods.UserTitle?.Text;

                if(user != name)
                {
                    throw new ValidationException(String.Format("Currenter user {0} is not valid. ({1})", user, name));
                }
            }
            catch
            {
                throw new WorkflowException("Could not get current user in title bar...");
            }
        }
        
        /// <summary>
        /// Validate User in Current user Popup
        /// </summary>
        /// <param name="name">Expected UserName</param>
        /// <param name="role">Expected User Role Level</param>
        /// <exception cref="WorkflowException"></exception>
        /// e.g.
        public static void ValidateUserPopup(string name, string role)
        {
            StatusManager.AddStatus("Validating Current User popup...");
            try
            {
                ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementExists(CurrentUserPopupFindBy.CurrentUserTitleBy));
                string user = ApplicationManager.Driver?.FindElement(CurrentUserPopupFindBy.CurrentUserNameBy).Text;
                string userRole = ApplicationManager.Driver?.FindElement(CurrentUserPopupFindBy.CurrentUserRoleBy).Text;
                if(user != name && userRole != role)
                {
                    throw new ValidationException("Current User is not correct. Should Be TechService with User Level: Service user");
                }
            }
            catch 
            {
                
            }
        }

        /// <summary>
        /// Validates the Kebab Menu (Top Right vertical Ellipsis) Is Open
        /// </summary>
        /// <exception cref="WorkflowException"></exception>
        /// e.g.
        public static void KebabMenusIsOpen()
        {
            StatusManager.AddStatus("Validating Kebab Menu is open...");
            try
            {
                var kebabAbout = ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.KebabAboutLabelBy);
                var kebabHelp = ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.KebabHelpLabelBy);
                var kebabShutdown = ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.KebabShutdownLabelBy);

                if (kebabAbout == null || kebabHelp == null || kebabHelp == null)
                {
                    throw new ValidationException("Kebab menu did not open and display properly.");
                }
            }
            catch (Exception ex)
            {
                throw new WorkflowException("Kebab Menu did not open. " + ex.Message);
            }
            
        }

        /// <summary>
        /// Validates Kebab menu (top right vertical ellipsis) is clsoed
        /// </summary>
        /// e.g.
        public static void KebabMenuIsClosed()
        {
            StatusManager.AddStatus("Validating Kebab Menu is closed...");
            try
            {
                AppiumElement kebabAbout = ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.KebabAboutLabelBy);
                AppiumElement kebabHelp = ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.KebabHelpLabelBy);
                AppiumElement kebabShutdown = ApplicationManager.Driver?.FindElement(TempestGeneralFindBy.KebabShutdownLabelBy);

                if (kebabAbout.Displayed || kebabHelp.Displayed || kebabShutdown.Displayed)
                {
                    throw new ValidationException("Kebab menu did not open and display properly.");
                }
            }
            catch
            {
                
            }
        }
    }
}
