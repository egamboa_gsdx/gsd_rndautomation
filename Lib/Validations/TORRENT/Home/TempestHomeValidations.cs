﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT
{
    public class TempestHomeValidations
    {
        /// <summary>
        /// Validate Home Tab is shown on Main View
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void AtHome()
        {
            StatusManager.AddStatus("Verifying on Home Tab...");

            string headerTitle = TempestGeneralMethods.MainHeaderTitle;

            if(headerTitle != "Home")
            {
                throw new ValidationException("Not able to navigate to Home Tab View");
            }
        }

        /// <summary>
        /// Validates Status log is expanded and displayed
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void StatusLogOpened()
        {
            StatusManager.AddStatus("Verifying Status Log is Open...");
            try
            {
                ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(TempestHomeFindBy.StatusCloseBtnBy));
            }
            catch
            {
                throw new ValidationException("Status log did not open.");
            }
        }

        /// <summary>
        /// Validates Status log is collapsed and not shown
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void StatusLogClosed()
        {
            StatusManager.AddStatus("Verifying Status log is closed...");
            try
            {
                ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(TempestHomeFindBy.StatusOpenBtnBy));
            }
            catch
            {
                throw new ValidationException("Status log did not close.");
            }
        }
    }
}
