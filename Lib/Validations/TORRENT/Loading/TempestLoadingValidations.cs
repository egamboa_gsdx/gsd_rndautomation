﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT
{
    public class TempestLoadingValidations
    {
        public static void ValidateAddedSample(string samplename, int index = 1)
        {
            StatusManager.AddStatus("Validating sample has been added...");
            TempestLoadingMethods.NavigateToLoading();

            switch (index)
            {
                case 1:
                    var name = TempestLoadingMethods.SampleOneName?.Text;
                    if (name == null || !name.Equals(samplename))
                    {
                        throw new ValidationException(string.Format("Sample name is incorrect. ({0})", samplename));
                    }
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
