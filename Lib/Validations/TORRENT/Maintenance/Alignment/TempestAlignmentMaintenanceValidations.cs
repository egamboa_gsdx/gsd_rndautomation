﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.GeneralMaintenance;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Maintenance;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT.Maintenance
{
    public class TempestAlignmentMaintenanceValidations
    {
        /// <summary>
        /// Validate Alignment Maintenance View is shown
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateAlignmentMaintenanceView()
        {
            StatusManager.AddStatus("Validating Alignment Maintenance View...");
            var alignInstrument = TempestAlignmentMaintenanceMethods.GetAlignInstrumentFrame();
            var alignReader = TempestAlignmentMaintenanceMethods.GetAlignReaderFrame();
            if (alignInstrument ==null || alignReader == null)
            {
                throw new ValidationException("Alignment Maintenance is not properly shown...");
            }
        }
    }
}
