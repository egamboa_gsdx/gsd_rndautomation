﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.CalibrationMaintenance;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Maintenance;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT.Maintenance
{
    /// <summary>
    /// Validates Calibration maintenance is properly displayed
    /// </summary>
    /// e.g.
    public class TempestCalibrationMaintenanceValidations
    {
        public static void ValidateCalibrationMaintenanceView()
        {
            StatusManager.AddStatus("Validating Calibration Maintenance is shown...");
            var calibrateAbsorbance = TempestCalibrationMaintenanceMethods.GetCalibrateAbsorbaneReaderFrame();
            var calibrateLuminescense = TempestCalibrationMaintenanceMethods.GetCalibrateLuminescenseReaderFrame();

            if (calibrateAbsorbance == null || calibrateLuminescense == null)
            {
                throw new ValidationException("Calibration Maintenance is not shown.");
            }
        }
    }
}
