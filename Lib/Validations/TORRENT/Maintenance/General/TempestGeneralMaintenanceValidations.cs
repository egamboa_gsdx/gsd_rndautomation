﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.GeneralMaintenance;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Maintenance;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Status;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT.Maintenance
{
    public class TempestGeneralMaintenanceValidations
    {
        /// <summary>
        /// Validate Prime Wash Manifold Popup view displays
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidatePrimeWashManifoldPopup()
        {
            StatusManager.AddStatus("Validating Prime Wash Manifold Popup is shown...");
            AppiumElement primeWashTitle = TempestPrimeWashManifoldMethods.GetPrimeWashManifoldPopupTitle();
            if(primeWashTitle == null || !primeWashTitle.Text.Equals("Prime Wash Manifold"))
            {
                throw new ValidationException("Prime Wash Manifold Popup did not show.");
            }
        }

        /// <summary>
        /// Validates Priming Cycle Text Entry has increase/decreased appropriately
        /// </summary>
        /// <param name="expectedPrimingCycles">Priming cycles expected to be shown in Text Entry</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidatePrimingCycle(int expectedPrimingCycles)
        {
            StatusManager.AddStatus("Validating Priming Cycles...");
            AppiumElement currentPrimingCycles = TempestPrimeWashManifoldMethods.GetPrimingCyclesText();
            int actualPrimingCycles = int.Parse(currentPrimingCycles.Text);
            if(actualPrimingCycles != expectedPrimingCycles)
            {
                throw new ValidationException("Priming cycles were not incremented/decremented properly.");
            }
        }

        /// <summary>
        /// Validate Wash Bottle in Prime Wash Manifold Popup are checked/unchecked
        /// </summary>
        /// <param name="isChecked">Test if bottleIndex is checked/unchecked</param>
        /// <param name="bottleIndex">Index of wash bottle</param>
        /// <exception cref="ValidationException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// e.g.
        public static void ValidateWashBottleCheckedUnChecked(bool isChecked, int bottleIndex = 1)
        {
            AppiumElement wash1 = TempestPrimeWashManifoldMethods.GetPrimeWashBottleCheckbox(1);
            AppiumElement wash2 = TempestPrimeWashManifoldMethods.GetPrimeWashBottleCheckbox(2);
            AppiumElement wash3 = TempestPrimeWashManifoldMethods.GetPrimeWashBottleCheckbox(3);

            bool wash1Checked = bool.Parse(wash1.GetAttribute("checked"));
            bool wash2Checked = bool.Parse(wash2.GetAttribute("checked"));
            bool wash3Checked = bool.Parse(wash3.GetAttribute("checked"));

            if (isChecked)
            {
                StatusManager.AddStatus("Validating Prime Wash Wash bottle is checked...");
                switch (bottleIndex)
                {
                    case 1:
                        if (!wash1Checked)
                        {
                            throw new ValidationException("Incorrect wash has been checked.");
                        }
                        break;
                    case 2:
                        if (!wash2Checked)
                        {
                            throw new ValidationException("Incorrect wash has been checked.");
                        }
                        break;
                    case 3:
                        if (!wash3Checked)
                        {
                            throw new ValidationException("Incorrect wash has been checked.");
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else
            {
                StatusManager.AddStatus("Validating Prime Wash Wash bottle is unchecked...");
                switch (bottleIndex)
                {
                    case 1:
                        if (wash1Checked)
                        {
                            throw new ValidationException("Incorrect wash has been unchecked.");
                        }
                        break;
                    case 2:
                        if (wash2Checked)
                        {
                            throw new ValidationException("Incorrect wash has been unchecked.");
                        }
                        break;
                    case 3:
                        if (wash3Checked)
                        {
                            throw new ValidationException("Incorrect wash has been unchecked.");
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
