﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT
{
    public class TempestResourcesValidations
    {
        /// <summary>
        /// Validate Resources Tab is shown on Main View
        /// </summary>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void AtResources()
        {
            StatusManager.AddStatus("Validating Resources Tab is displayed...");

            string headerTitle = TempestGeneralMethods.MainHeaderTitle;

            if (headerTitle != "Resources")
            {
                throw new ValidationException("Resources tab is not properly being viewed.");
            }
        }

        /// <summary>
        /// Validates Available Disposable Tips
        /// </summary>
        /// <param name="expectAvailableTips">Disposable tips expected to be available</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateDisposableTipsAvailable(string expectAvailableTips)
        {
            StatusManager.AddStatus("Validating Current Available Disposable Tips...");

            string currentAvailableTips = TempestResourcesMethods.GetAvailableDisposableTips();

            if (currentAvailableTips != expectAvailableTips)
            {
                throw new ValidationException(string.Format("Current available tips do not match expected available tips. Expected: {0}. Actual {1}.", 
                    expectAvailableTips, currentAvailableTips));
            }
        }

        /// <summary>
        /// Validate Internal Temperature
        /// </summary>
        /// <param name="expectedInternalTemp">Expected Internal Temp</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateInternalTemp(string expectedInternalTemp)
        {
            StatusManager.AddStatus("Validating current internal temperature...");

            string currentInternalTemp = TempestResourcesMethods.GetInternalTemp();

            if (currentInternalTemp != expectedInternalTemp)
            {
                throw new ValidationException(string.Format("Current internal temp does not match expected temp. Expected {0}. Actual {1}.",
                    expectedInternalTemp, currentInternalTemp));
            }
        }

        /// <summary>
        /// Validate Incubator Temperature
        /// </summary>
        /// <param name="expectedIncubatorTemp">Expected Incubator Temperature</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateIncubatorTemp(string expectedIncubatorTemp)
        {
            StatusManager.AddStatus("Validating current incubator temperature...");

            string currentIncubatorTemp = TempestResourcesMethods.GetIncubatorTemp();

            if (currentIncubatorTemp != expectedIncubatorTemp)
            {
                throw new ValidationException(string.Format("Current incubator temp does not match expected temp. Expected {0}. Actual {1}.",
                    expectedIncubatorTemp, currentIncubatorTemp));
            }
        }

        /// <summary>
        /// Validate Expected Fluid WashWaste Bottle Percentage
        /// </summary>
        /// <param name="expectedPercent">Expected Fluid Percentage for specified bottle</param>
        /// <param name="bottleIndex">Index of Bottle in collection</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateWashWasteBottleFluidPercentage(string expectedPercent, int bottleIndex = 1)
        {
            StatusManager.AddStatus("Validating current wash/waste bottle fluid percentage...");

            string currentFluidPercent = TempestResourcesMethods.GetWashWasteBottleFluidPercentage(bottleIndex);

            if (currentFluidPercent != expectedPercent)
            {
                throw new ValidationException(String.Format("Incorrect volume percentage. Expected {0}. Actual {1}",
                    expectedPercent, currentFluidPercent));
            }
        }

        /// <summary>
        /// Validate WashWaste Bottle Name
        /// </summary>
        /// <param name="expectedname">Expected name</param>
        /// <param name="bottleIndex">index of bottle in collection</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateWashWasteBottleName(string expectedname, int bottleIndex = 1)
        {
            StatusManager.AddStatus("Validating wash/waste bottle name...");

            string currentBottleName = TempestResourcesMethods.GetWashWasteBottleName(bottleIndex);

            if (currentBottleName != expectedname)
            {
                throw new ValidationException(string.Format("Incorrect wash/waste bottle name. Expected {0}. Actual {1}.",
                    expectedname, currentBottleName));
            }
        }

        /// <summary>
        /// Validate Change WashWaste Bottle Action opens appropriate popup
        /// </summary>
        /// <param name="bottleIndex">index of bottle in collection</param>
        /// <param name="bottleType">type of bottle performing change action</param>
        /// <exception cref="WorkflowException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// e.g.
        public static void ValidateChangeWashWasteBottleDialog(int bottleIndex = 1, int bottleType = 0)
        {
            switch (bottleType)
            {
                case 0:
                    StatusManager.AddStatus("Validating Select Wash Popup view...");
                    try
                    {
                        AppiumElement selectionViewTitle = ApplicationManager.Driver?.FindElement(SelectionPopupFindBy.SelectionTitleBy);
                        string bottleName = TempestResourcesMethods.GetWashWasteBottleName(bottleIndex);
                        string bottlePos = bottleIndex == 1 ? "Select wash solution for wash bottle 1" : "Select wash solution for wash bottle 2";
                        if (selectionViewTitle.Text != bottlePos)
                        {
                            throw new ValidationException("Incorrect Selection View popup.");
                        }
                    }
                    catch
                    {
                        throw new WorkflowException("Select Wash popup did not show...");
                    }
                    break;
                case 1:
                    StatusManager.AddStatus("Validating Empty Waste Bottle Confirmation Popup view...");
                    try
                    {
                        AppiumElement confirmationTitle = ApplicationManager.Driver?.FindElement(ConfirmationPopupFindBy.ConfirmationPopupTitleBy);
                        AppiumElement confirmationMessage = ApplicationManager.Driver?.FindElement(ConfirmationPopupFindBy.ConfirmationPopupMessageBy);

                        if (confirmationTitle.Text != "Empty Waste Bottle")
                        {
                            throw new ValidationException("Incorrect popup view title. Should be 'Empty Waste Bottle'");
                        }

                        if (confirmationMessage.Text != "Please remove the full waste bottle (left door) and plase back an empty waste bottle.")
                        {
                            throw new ValidationException("Incorrect popup view message.");
                        }
                    }
                    catch
                    {
                        throw new WorkflowException("Empty Waste Bottle Confirmation popup did not show...");
                    }
                    break;
                case 2:
                    StatusManager.AddStatus("Validating Fill System Fluid Confirmation Popup View...");
                    try
                    {
                        AppiumElement confirmationTitle = ApplicationManager.Driver?.FindElement(ConfirmationPopupFindBy.ConfirmationPopupTitleBy);
                        AppiumElement confirmationMessage = ApplicationManager.Driver?.FindElement(ConfirmationPopupFindBy.ConfirmationPopupMessageBy);

                        if (confirmationTitle.Text != "Fill System Fluid")
                        {
                            throw new ValidationException("Incorrect popup view title. Should be 'Fill System Fluid'");
                        }

                        if (confirmationMessage.Text != "Please remove the system fluid bottle (last bottle) and place back a full bottle.\nDo not move the other bottles.")
                        {
                            throw new ValidationException("Incorrect popup view message.");
                        }
                    }
                    catch
                    {
                        throw new WorkflowException("Fill System Fluid Confirmation Popup did not show...");
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Validates disposable trip tray opens or closes
        /// </summary>
        /// <param name="isOpen">state of tray</param>
        /// e.g.
        public static void ValidateDisposableTipsTrayOpenClose(bool isOpen)
        {
            // TODO When Fake Instrument AutomationIDs are available
        }
    }
}
