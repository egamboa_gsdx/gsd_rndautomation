﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT
{
    public class TempestResultsValidations
    {
        /// <summary>
        /// Validate Search on Sample Id
        /// </summary>
        /// <param name="isChecked">state of checkbox for Search on Sample Id</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateSearchOnSampleId(bool isChecked)
        {
            StatusManager.AddStatus("Validating Search On Sample ID ...");
            var searchOnSampleTxt = TempestResultsMethods.SearchOnSampleIDTextEntry;

            bool isEnabled = bool.Parse(searchOnSampleTxt.GetAttribute("enabled"));

            if (!isEnabled && isChecked)
            {
                throw new ValidationException("Search By Sampled ID is not enabled.");
            }

            if(isEnabled && !isChecked)
            {
                throw new ValidationException("Search by Sampled ID should not be enabled when not checked.");
            }
        }

        /// <summary>
        /// Validate Search On Test
        /// </summary>
        /// <param name="isChecked">state of checkbox for Search On Test</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateSearchOnTest(bool isChecked)
        {
            StatusManager.AddStatus("Validating Search on Test...");
            var searchOnTestpck = TempestResultsMethods.SearchOnTestPicker;

            bool isEnabled = bool.Parse(searchOnTestpck.GetAttribute("enabled"));

            if (isEnabled && !isChecked)
            {
                throw new ValidationException("Search by Test should not be enabled when not checked.");
            }

            if (!isEnabled && isChecked)
            {
                throw new ValidationException("Search by test is not enabled when checked.");
            }
        }

        /// <summary>
        /// Validate Search on Operator
        /// </summary>
        /// <param name="isChecked">state of checkbox for Search On Operator</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateSearchOnOperator(bool isChecked)
        {
            StatusManager.AddStatus("Validating Search on Operator...");

            var searchOnOperatorPck = TempestResultsMethods.SearchOnOperatorPicker;

            bool isEnabled = bool.Parse(searchOnOperatorPck.GetAttribute("enabled"));

            if (isEnabled && !isChecked)
            {
                throw new ValidationException("Search by Opeartor should not be enabled when not checked.");
            }

            if (!isEnabled && isChecked)
            {
                throw new ValidationException("Search by Operator is not enabled when checked.");
            }
        }

        /// <summary>
        /// Validate Search on From Date
        /// </summary>
        /// <param name="isChecked">state of checkbox for Search On From Date</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateSearchOnFromDate(bool isChecked)
        {
            StatusManager.AddStatus("Validating Search on From Date...");

            var searchOnFromDateTxt = TempestResultsMethods.SearchOnFromDateDatePicker;

            bool isEnabled = bool.Parse(searchOnFromDateTxt.GetAttribute("enabled"));

            if (isEnabled && !isChecked)
            {
                throw new ValidationException("Search by From Date should not be enabled when not checked.");
            }

            if (!isEnabled && isChecked)
            {
                throw new ValidationException("Search by From Date is not enabled when checked.");
            }
        }

        /// <summary>
        /// Validate Search on To Date
        /// </summary>
        /// <param name="isChecked">state of Search on To Date checkbox</param>
        /// <exception cref="ValidationException"></exception>
        /// e.g.
        public static void ValidateSearchOnToDate(bool isChecked)
        {
            StatusManager.AddStatus("Validating Search on To Date...");

            var searchOnToDateTxt = TempestResultsMethods.SearchOnToDateDatePicker;

            var isEnabled = bool.Parse(searchOnToDateTxt.GetAttribute("enabled"));

            if (isEnabled && !isChecked)
            {
                throw new ValidationException("Search by From Date should not be enabled when not checked.");
            }

            if (!isEnabled && isChecked)
            {
                throw new ValidationException("Search by From Date is not enabled when checked.");
            }
        }
    }
}
