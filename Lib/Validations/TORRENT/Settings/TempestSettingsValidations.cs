﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;

using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT
{
    public class TempestSettingsValidations
    {
        public static void ValidateAtSettings()
        {
            StatusManager.AddStatus("Validating Settings Tab View...");
            var headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Settings"))
            {
                throw new ValidationException("Settings Tab View did not properly show.");
            }
        }
    }
}
