﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GSD_RnDAutomation.Status;
using GSD_RnDAutomation.Common.Exceptions;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Settings;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.TORRENT.Elements.Popups;
using GSD_RnDAutomation.TORRENT.Elements.Settings;

using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Validations.TORRENT
{
    public class TempestUserSettingsValidations
    {
        public static void ValidateUserExists(string testUserName)
        {
            StatusManager.AddStatus("Validating user is added...");
            var userList = TempestUsersSettingsMethods.UsersList?.FindElements(By.XPath("//././android.widget.TextView"));

            if (userList == null)
            {
                throw new ValidationException("No users have been added.");
            }

            var userFound = userList.FirstOrDefault(u => u.Text == testUserName); // xpath subject to change

            if (userFound == null)
            {
                throw new ValidationException("Expected user was not found.");
            }
        }

        public static void ValidateUserInfo(string testUsername, string testEmail, string testPhonenum, string testRole)
        {
            StatusManager.AddStatus("Validating user has been edited...");

            var userList = TempestUsersSettingsMethods.UsersList?.FindElements(By.XPath("//.././android.widget.TextView"));
            var user = userList.FirstOrDefault(u => u.Text == testUsername);
            if (user == null)
            {
                throw new ValidationException("User does not exist...apparently user was not edited.");
            }

            user.Click();
            
            TempestUsersSettingsMethods.EditUserButton?.Click();

            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(CreateEditUserPopupFindBy.CreateEditUserTitleBy));

            if (!TempestCreateEditUserMethods.UserNameTextEntry.Text.Equals(testUsername))
            {
                throw new ValidationException("Expected Username does not match Edit User view.");
            }

            if (!TempestCreateEditUserMethods.EmailTextEntry.Text.Equals(testEmail))
            {
                throw new ValidationException("Expected User email does not match Edit User View.");
            }

            if (!TempestCreateEditUserMethods.PhoneNumberTextEntry.Text.Equals(testPhonenum))
            {
                throw new ValidationException("Expected User phone number does not match Edit User View.");
            }

            var role = TempestCreateEditUserMethods.UserLevelPicker.FindElement(By.XPath("//././android.widget.Button"));

            if (!role.Text.Equals(testRole))
            {
                throw new ValidationException("Expected User Level does not match Edit User View.");
            }

            TempestCreateEditUserMethods.CancelButton?.Click();
        }

        public static void ValidatePasswordPolicy(string minLength = "8", string allowableChar = " ", string maxAttempt = "5",
            string reqSpecialChar = "OFF", string reqDigit = "OFF", string allowSpaces = "OFF", string lockoutDuration = "5", string passExp = "90")
        {
            StatusManager.AddStatus("Validating Password Policy...");


        }
    }
}
