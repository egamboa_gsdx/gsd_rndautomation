﻿using Microsoft.Win32;
using System;
using System.Linq;
using System.Windows.Forms;

using GSD_RnDAutomation.SharePoint_Downloader;
using GSD_RnDAutomation.Common.Testing;

namespace GSD_RnDAutomation
{
	public static class Program
	{
        private static string RegKeyLocation = @"SOFTWARE\Gold Standard Diagnostics\AutomationPlan";
        private static string RegKeyRunFlag = "AutomationFlag";
        private static string RegKeyApp = "AutomationApp";
        private static TestSuite test;

        [STAThread]
		public static void Main(string[] args)
		{
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Count() >= 1)
            {
                if (args[0] == "RegKey")
                {
                    RegistryKey RegKey = Registry.LocalMachine.OpenSubKey(RegKeyLocation, true);
                    string software = RegKey.GetValue(RegKeyApp).ToString();
                    string runFlag = RegKey.GetValue(RegKeyRunFlag).ToString();

                    if (software == "Both" && runFlag == "True")
                    {
                        RegKey.SetValue(RegKeyRunFlag, "False");
                        Application.Run(new Test_GUI("Both"));
                    }
                    else if ((software == "Storm" || software == "AIX") && runFlag == "True")
                    {
                        RegKey.SetValue(RegKeyRunFlag, "False");
                        Application.Run(new Test_GUI(software));
                    }
                    else
                    {
                        Application.Run(new Test_GUI());
                    }
                }
                else if(args.Contains("download"))
                {
                    //Application.Run(new StatusGUI(ref test));
                    Application.Run(new SharePointDownloaderGUI());
                    //SharePointDownloader.SPDownloader(args);
                } 
                else
                {
                    Application.Run(new Test_GUI());
                }
            }
            else
            {
                Application.Run(new Test_GUI());
            }
        }
	}
}
