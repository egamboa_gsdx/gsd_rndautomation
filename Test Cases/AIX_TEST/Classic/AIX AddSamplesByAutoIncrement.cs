﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIX_AddSamplesByAutoIncrement : AIXClassicTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Add Samples by Auto Increment"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            //WorklistMethods.AddSamplesAutoIncrement(10, "PatientID#{0}");
            WorklistMethods.AddSamples();


            // >>>>> Validation <<<<<
            WorklistValidation.ValidateAddSamplesAutoIncrement(5, "Sample #{0}");
            // this is a test to show nerds how to code and git
            // changes in master to show shit
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
