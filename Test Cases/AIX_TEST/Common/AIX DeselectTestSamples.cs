﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIX_DeselectTestSamples : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Deselect Test Samples"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples(numberOfSamples: 10);

            WorklistMethods.AssignSample(1, 1);
            WorklistMethods.AssignSample(3, 1);
            WorklistMethods.AssignSample(6, 1);

            ////////////////////////////////////

            WorklistMethods.AssignSample(1, 2);
            WorklistMethods.AssignSample(3, 2);
            WorklistMethods.AssignSample(6, 2);

            // >>>>> Validation <<<<<

            WorklistValidation.ValidateUnassignSample(1, 1);
            WorklistValidation.ValidateUnassignSample(3, 1);
            WorklistValidation.ValidateUnassignSample(6, 1);

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
