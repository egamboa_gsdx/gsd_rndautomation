﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXControlsOnAllPlates : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Controls on all plates"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            if (ApplicationManager.RackManager == RackManager.classic) 
            {
                WorklistMethods.AddSamples(choice: 1,
                                           numberOfSamples: 184,
                                           controlScheme: 0); 
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.AddSamples(choice: 2, controlScheme: 0);
                

            }

            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                MTPValidation.ValidateWellTooltipName(1, 1, "RPR NRC");
                MTPValidation.ValidateWellTooltipName(1, 2, "RPR RC");

                MTPValidation.ValidateWellTooltipName(2, 1, "RPR NRC");
                MTPValidation.ValidateWellTooltipName(2, 2, "RPR RC");

                MTPValidation.ValidateWellTooltipName(3, 1, "RPR NRC");
                MTPValidation.ValidateWellTooltipName(3, 2, "RPR RC");

                MTPValidation.ValidateWellTooltipName(4, 1, "RPR NRC");
                MTPValidation.ValidateWellTooltipName(4, 2, "RPR RC");

                MTPValidation.ValidateControlSchemeDropDownList("Controls on all Plates");

            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                MTPValidation.ValidateWellTooltipName(1, 1, "TPPA NRC");
                MTPValidation.ValidateWellTooltipName(1, 2, "TPPA RC");

                MTPValidation.ValidateWellTooltipName(2, 1, "TPPA NRC");
                MTPValidation.ValidateWellTooltipName(2, 2, "TPPA RC");

                MTPValidation.ValidateWellTooltipName(3, 1, "TPPA NRC");
                MTPValidation.ValidateWellTooltipName(3, 2, "TPPA RC");

                MTPValidation.ValidateWellTooltipName(4, 1, "TPPA NRC");
                MTPValidation.ValidateWellTooltipName(4, 2, "TPPA RC");

                MTPValidation.ValidateControlSchemeDropDownList("Controls on all Plates");
            }


            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
