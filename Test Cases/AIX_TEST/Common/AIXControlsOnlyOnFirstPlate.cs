﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXControlsOnlyOnFirstPlate : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Controls only on first plate"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.AddSamples(numberOfSamples: 184,
                                       controlScheme: 2);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.AddSamples(choice: 2, controlScheme: 2);
                

            }


            // >>>>> Validation <<<<<



            if (ApplicationManager.RackManager == RackManager.classic)
            {  MTPValidation.ValidateWellTooltipName(1, 1, "RPR NRC");
                MTPValidation.ValidateWellTooltipName(1, 2, "RPR RC");
            
                MTPValidation.ValidateWellTooltipName(2, 1, "Sample #47");
                MTPValidation.ValidateWellTooltipName(2, 2, "Sample #48");

                MTPValidation.ValidateWellTooltipName(3, 1, "Sample #95");
                MTPValidation.ValidateWellTooltipName(3, 2, "Sample #96");

                MTPValidation.ValidateWellTooltipName(4, 1, "Sample #143");
                MTPValidation.ValidateWellTooltipName(4, 2, "Sample #144");
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            { MTPValidation.ValidateWellTooltipName(1, 1, "TPPA NRC");
                MTPValidation.ValidateWellTooltipName(1, 2, "TPPA RC");
            
                MTPValidation.ValidateWellTooltipName(2, 1, "Sample #47");
                MTPValidation.ValidateWellTooltipName(2, 2, "Sample #48");

                MTPValidation.ValidateWellTooltipName(3, 1, "Sample #95");
                MTPValidation.ValidateWellTooltipName(3, 2, "Sample #96");

                MTPValidation.ValidateWellTooltipName(4, 1, "Sample #143");
                MTPValidation.ValidateWellTooltipName(4, 2, "Sample #144");

            }
            MTPValidation.ValidateControlSchemeDropDownList("Controls only on first Plate");

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
