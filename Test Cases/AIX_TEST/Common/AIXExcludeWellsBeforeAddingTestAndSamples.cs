﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXExcludeWellsBeforeAddingTestAndSamples : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "MTP > Exclude Wells before Test File and Samples have been added"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            MTPMethods.ExcludeWells(1, 7);
            WorklistMethods.AddSamples(numberOfSamples: 10);

            // >>>>> Validation <<<<<

            MTPValidation.ValidateWellName(1, 1, "");
            MTPValidation.ValidateWellName(1, 7, "");
            MTPValidation.ValidateWellName(1, 8, "Sample #1");

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
