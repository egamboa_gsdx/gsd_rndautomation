﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXHighlightSamplesShift : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Highlight Samples (Shift Key)"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples(numberOfSamples: 10);
            WorklistMethods.HighlightSamples(3);

            WorklistMethods.HighlightSamples(6, "SHIFT");

            // >>>>> Validation <<<<<

            WorklistValidation.ValidateSampleHighlighted(3);
            WorklistValidation.ValidateSampleHighlighted(4);
            WorklistValidation.ValidateSampleHighlighted(5);
            WorklistValidation.ValidateSampleHighlighted(6);
            WorklistValidation.ValidateSampleNotHighlighted(1);
            WorklistValidation.ValidateSampleNotHighlighted(2);
            WorklistValidation.ValidateSampleNotHighlighted(7);

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
