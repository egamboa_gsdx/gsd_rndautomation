﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXLoadWorklist : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Load Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//


            // >>>>> Commands <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.LoadWorklist("10_samples_10_assingedScreenAction", "Test Data");
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.LoadWorklist("184_Samples_AllAssignedTPPAAction", "Test Data");
               
            }


            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistValidation.ValidateAssignSample(1, 1);
                WorklistValidation.ValidateAssignSample(2, 1);
            }else if (ApplicationManager.RackManager == RackManager.SIR) 
            {
                
                WorklistValidation.ValidateAssignSample(1, 5);
                WorklistValidation.ValidateAssignSample(100, 5);
                WorklistValidation.ValidateAssignSample(184, 5);
            }

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
