﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;
using GSD_RnDAutomation.Properties;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXLoadWorklistReplacesCurrentWorklist : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Load Worklist Replaces Current Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            WorklistMethods.AddWorklistName("Current Worklist");
            WorklistMethods.AddSamples();

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.LoadWorklist("10_samples_10_assingedScreenAction", "Test Data");
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.LoadWorklist("16_Samples_All_Tppa", "Test Data");
            }

            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                HomeValidation.ValidateWorklistName("10_samples_10_assingedScreenAction");
                WorklistValidation.ValidateAssignSample(10, 1);
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                HomeValidation.ValidateWorklistName("16_Samples_All_Tppa");
                WorklistValidation.ValidateAssignSample(16, 5);
            }

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
