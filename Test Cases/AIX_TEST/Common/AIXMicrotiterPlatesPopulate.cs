﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;


/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXMicrotiterPlatesPopulate : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "MTP > Sample Wells Populate After Adding Samples"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            WorklistMethods.AddSamples(numberOfSamples: 16);


            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                MTPValidation.ValidateWellName(1, 1, "Sample #1");
                MTPValidation.ValidateWellName(1, 2, "Sample #2");
                MTPValidation.ValidateWellName(1, 10, "Sample #10");
            }else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                MTPValidation.ValidateWellName(1, 1, "Sample #1");
                MTPValidation.ValidateWellName(1, 2, "Sample #2");
                MTPValidation.ValidateWellName(1, 10, "Sample #10");
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
