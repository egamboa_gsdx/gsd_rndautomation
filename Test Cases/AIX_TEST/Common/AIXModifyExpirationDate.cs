﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXModifyExpirationDate : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Modify Expiration Date and Save"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            WorklistMethods.AddSamples();
            HelperMethods.Wait();
            HomeMethods.ModifyExpirationDate("10/31/2099");

            // >>>>> Validation <<<<<
            HelperMethods.Wait();
            HomeValidation.ValidateExpirationDate("10/31/2099");

            // >>>>> Commands <<<<<
            HelperMethods.Wait();
            HomeMethods.ModifyExpirationDate("1/1/2099");

            // >>>>> Validation <<<<<
            HelperMethods.Wait();
            HomeValidation.ValidateExpirationDate("1/1/2099");



            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
