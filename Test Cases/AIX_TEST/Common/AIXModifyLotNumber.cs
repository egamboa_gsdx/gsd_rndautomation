﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXModifyLotNumber : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Modify Lot Number and Save"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            WorklistMethods.AddSamples();
            HelperMethods.Wait();
            HomeMethods.ModifyLotNumber(987654);

            // >>>>> Validation <<<<<
            HelperMethods.Wait();
            HomeValidation.ValidateLotNumber(987654);

            // >>>>> Commands <<<<<
            HelperMethods.Wait();
            HomeMethods.ModifyLotNumber(123456);

            // >>>>> Validation <<<<<
            HelperMethods.Wait();
            HomeValidation.ValidateLotNumber(123456);

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
