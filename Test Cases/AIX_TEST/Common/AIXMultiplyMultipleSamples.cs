﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXMultiplyMultipleSamples : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Multiply multiple samples"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples(numberOfSamples: 10);

            WorklistMethods.HighlightSamples(1);
            WorklistMethods.HighlightSamples(3, "SHIFT");
            WorklistMethods.MultiplySamples(4);

            // >>>>> Validation <<<<<

            if(ApplicationManager.RackManager == RackManager.classic)
                WorklistValidation.ValidateMultiplySamples(new int[] {1,1,1,1,
                                                                      2,2,2,2,
                                                                      3,3,3,3,
                                                                      4,5,6,7,8,9,10});
            else
                WorklistValidation.ValidateMultiplySamples(new int[] {1,1,1,1,
                                                                      2,2,2,2,
                                                                      3,3,3,3,
                                                                      4,5,6,7,8,9,10,
                                                                      11,12,13,14,15,16});

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
