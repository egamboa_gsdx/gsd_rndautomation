﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXNewWorklist : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > New Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.AddSamples(numberOfSamples: 10);
            }
            if (ApplicationManager.RackManager == RackManager.SIR) 
            {
                WorklistMethods.AddSamples();
            }
            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistValidation.ValidateAddSamplesAutoIncrement(10);
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistValidation.ValidateAddSamplesAutoIncrement(16);
            }
            // >>>>> Commands <<<<<
            WorklistMethods.New();

            // >>>>> Validation <<<<<
            WorklistValidation.ValidateNew();



            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
