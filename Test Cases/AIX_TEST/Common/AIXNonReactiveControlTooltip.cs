﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;


/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXNonReactiveControlTooltip : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Non Reactive Control Tooltip"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples(controlScheme: 2);


            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                RacksValidation.ValidateReagentBottleTooltipName(1, "RPR NRC");
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                RacksValidation.ValidateReagentBottleTooltipName(3, "TPPA NRC");
            }



            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
