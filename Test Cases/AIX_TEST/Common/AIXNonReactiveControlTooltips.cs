﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXNonReactiveControlTooltips : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Non Reactive Control Tooltip"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            if (MTPMethods.GetControlSchemeDropDownBox() == "No Controls")
            {
                MTPMethods.SetControlSchemeDropDownBox(WorklistProperty.Default.ControlsOnlyOnFirstPlate);
            }

            // >>>>> Validation <<<<<

            RacksValidation.ValidateControlTubeTooltipName(4, 1, "Non Reactive Control");
            //RacksValidation.ValidateReagentVolumeTooltip(4, 6);
            RacksValidation.ValidateReagentVolumeTooltip(4, 1);


            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
