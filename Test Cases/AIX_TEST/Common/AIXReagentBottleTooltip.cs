﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;


/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXReagentBottleTooltip : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Reagent Bottle Tooltip"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.AddSamples(numberOfSamples: 10);
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.AddSamples(numberOfSamples: 10, controlScheme: 2);
            }


            
            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                RacksValidation.ValidateReagentBottleTooltipName(4, "RPR Antigen");
                RacksValidation.ValidateReagentBottleTooltipName(5, "Decon");
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                RacksValidation.ValidateReagentBottleTooltipName(6, "TPPA Antigen");
                RacksValidation.ValidateReagentBottleTooltipName(7, "Decon");
            }
            


            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
