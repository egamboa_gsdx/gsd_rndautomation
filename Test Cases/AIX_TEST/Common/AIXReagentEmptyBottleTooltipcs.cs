﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;
using GSD_RnDAutomation.Properties;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXReagentEmptyBottleTooltipcs : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Reagent Empty Bottle Tooltip"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.AddSamples(numberOfSamples: 10);
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.AddSamples(numberOfSamples: 10, controlScheme: 1);
            }
            HelperMethods.Click(WorklistProperty.Default.NewButton);
            HelperMethods.Click(WorklistProperty.Default.YesButton);

            // >>>>> Validation <<<<<
            RacksValidation.ValidateEmptyReagentTooltip(6);
            RacksValidation.ValidateEmptyReagentTooltip(7);


            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
