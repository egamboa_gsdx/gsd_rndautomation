﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXReagentsAfterNewWorklist : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Reagents After New Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            // Add samples to ensure the new worklist button works
            WorklistMethods.AddSamples();


            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                RacksValidation.ValidateReagentBottleName(4, "RPR Antigen");
                RacksValidation.ValidateReagentBottleName(5, "Decon");
                WorklistMethods.New();
                RacksValidation.ValidateReagentBottleName(4, "");
                RacksValidation.ValidateReagentBottleName(5, "");
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                RacksValidation.ValidateReagentBottleName(6, "TPPA Antigen");
                RacksValidation.ValidateReagentBottleName(7, "Decon");
                WorklistMethods.New();
                RacksValidation.ValidateReagentBottleName(6, "");
                RacksValidation.ValidateReagentBottleName(7, "");
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
