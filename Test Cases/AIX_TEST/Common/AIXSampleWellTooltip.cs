﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXSampleWellTooltip : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "MTP > Sample Well Tooltip"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples(numberOfSamples: 10);

            // >>>>> Validation <<<<<

            MTPValidation.ValidateSampleWellTooltipName(1, 1, "Sample #1");
            MTPValidation.ValidateSampleWellTooltipName(1, 10, "Sample #10");
            if (ApplicationManager.RackManager == RackManager.classic)
                MTPValidation.ValidateEmptyWellTooltip(1, 11);
            else
                MTPValidation.ValidateEmptyWellTooltip(1, 17);

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
