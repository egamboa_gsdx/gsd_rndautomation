﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXSaveWorklist : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Save Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples(numberOfSamples: 16);
            WorklistMethods.AddWorklistName("AIXSaveWorklist");

            WorklistMethods.SaveWorklist();
            WorklistMethods.New();

            WorklistMethods.LoadWorklist("AIXSaveWorklist", "Temp");

            // >>>>> Validation <<<<<

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistValidation.ValidateAddSamplesAutoIncrement(16);
                WorklistValidation.ValidateAssignSample(1, 1);
                WorklistValidation.ValidateAssignSample(4, 1);
                WorklistValidation.ValidateAssignSample(8, 1);
                WorklistValidation.ValidateAssignSample(16, 1);
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistValidation.ValidateAssignSample(1, 5);
                WorklistValidation.ValidateAssignSample(4, 5);
                WorklistValidation.ValidateAssignSample(8, 5);
                WorklistValidation.ValidateAssignSample(16, 5);
            }





            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
