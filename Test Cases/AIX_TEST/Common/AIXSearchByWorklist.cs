﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;
using GSD_RnDAutomation.Properties;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXSearchByWorklist : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "SEARCH > Search by Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            WorklistMethods.AddSamples(numberOfSamples: 10);
            WorklistMethods.AddWorklistName("Searchable Worklist Run");
            HomeMethods.SpecialStartSpecificPosition("Last");
            HomeMethods.WaitForWorklistToFinish();

            SearchMethods.Search(worklist: "Searchable Worklist Run");

            // >>>>> Validation <<<<<

            SearchValidation.ValidateAllSearchResults(worklist: "Searchable Worklist Run");


            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
