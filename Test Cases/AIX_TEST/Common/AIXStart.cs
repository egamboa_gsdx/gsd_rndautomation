﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXStart : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "HOME > START Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples();
            HomeMethods.Start();

            // >>>>> Validation <<<<<

            HomeValidation.ValidateStart();

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
