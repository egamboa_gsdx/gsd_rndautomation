﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.AIX_TEST
{
    class AIXWorklistReportZoomInOut : AIXTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "EVALUATION > Zoom in/out"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            EvaluationMethods.ZoomToPercentage(75);
            EvaluationValidation.RecordPageWidth();
            EvaluationMethods.ZoomIn();
            EvaluationMethods.ZoomIn();

            // >>>>> Validation <<<<<

            EvaluationValidation.ValidateZoomIn();

            // >>>>> Commands <<<<<

            EvaluationMethods.ZoomToPercentage(150);
            EvaluationValidation.RecordPageWidth();
            EvaluationMethods.ZoomOut();
            EvaluationMethods.ZoomOut();

            // >>>>> Validation <<<<<

            EvaluationValidation.ValidateZoomOut();

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
