﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormAddSamplesByImport : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Add Samples by Import"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            if (ApplicationManager.RackManager == RackManager.classic || ApplicationManager.RackManager == RackManager.bolt)
            {
                WorklistMethods.AddSamples(choice: GeneralProperty.Default.ImportAdd);
            }
            if(ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.AddSamples();
            }
            // >>>>> Validation <<<<<

            WorklistValidation.ValidateAddSamplesImport();

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
