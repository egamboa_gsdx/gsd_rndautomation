﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormAddTestWithAllRacksFull : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Add Test while all Rack positions are filled with Samples"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.AddSamples(numberOfSamples: 192);
            }
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                WorklistMethods.AddSamples(numberOfSamples: 96);
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                WorklistMethods.AddSamples(choice: 2, controlScheme:3);
            }

            // >>>>> Validation <<<<<

            WorklistValidation.ValidateCannotAddTest();

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
