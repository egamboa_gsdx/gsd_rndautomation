﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormAddTestWithOneRackFull : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Add Test while Rack #1 is filled with Samples"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples(numberOfSamples: 64);
            WorklistMethods.AddTest(1, "ANA_GSD");

            // >>>>> Validation <<<<<

            WorklistValidation.ValidateAddTest();
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                RacksValidation.ValidateSampleTubeName(1, 62, "Sample #62");
                RacksValidation.ValidateSampleTubeName(2, 62, "PC_ANA_GSD");
            }
            else
            {
                RacksValidation.ValidateSampleTubeName(1, 1, "Sample #1");
                RacksValidation.ValidateSampleTubeName(1, 16, "Sample #16");
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
