﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormChangeDeadVolume : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Change Dead Volume"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");
            if (ApplicationManager.RackManager == RackManager.classic)
                RacksMethods.SetReagentBottleDeadVolume(2, 30);
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksMethods.SetReagentBottleDeadVolume(2, 30, 3);
            }
            else
                RacksMethods.SetReagentBottleDeadVolume(1, 30, 2);

            // >>>>> Validation <<<<<

            if (ApplicationManager.RackManager == RackManager.classic)
                RacksValidation.ValidateReagentBottleDeadVolume(2, 30);
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksValidation.ValidateReagentBottleDeadVolume(2, 30, 3);
            }
            else
                RacksValidation.ValidateReagentBottleDeadVolume(1, 30, 2);



            if (ApplicationManager.RackManager == RackManager.classic)
                RacksMethods.SetReagentBottleDeadVolume(2, 50);
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksMethods.SetReagentBottleDeadVolume(2, 50, 3);
            }
            else
                RacksMethods.SetReagentBottleDeadVolume(1, 50, 2);

            // >>>>> Validation <<<<<

            if (ApplicationManager.RackManager == RackManager.classic)
                RacksValidation.ValidateReagentBottleDeadVolume(2, 50);
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksValidation.ValidateReagentBottleDeadVolume(2, 50, 3);
            }
            else
                RacksValidation.ValidateReagentBottleDeadVolume(1, 50, 2);

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
