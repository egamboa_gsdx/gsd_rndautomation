﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormChangeRuntimeVariablePC_Min : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Change Runtime Variables - PC_Min"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");
            WorklistMethods.ModifyRuntimeVariable("PC_Min", 0.592 );

            // >>>>> Validation <<<<<

            WorklistValidation.ValidateRuntimeVariable("PC_Min", 0.592);

            // >>>>> Commands <<<<<

            WorklistMethods.ModifyRuntimeVariable("PC_Min", 0.600);

            // >>>>> Validation <<<<<

            WorklistValidation.ValidateRuntimeVariable("PC_Min", 0.600);

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
