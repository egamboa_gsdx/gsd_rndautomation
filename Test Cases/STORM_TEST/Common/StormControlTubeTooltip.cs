﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormControlTubeTooltip : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Control Tube Tooltip"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");

            // >>>>> Validation <<<<<

            if (ApplicationManager.RackManager == RackManager.classic)
                RacksValidation.ValidateControlTubeTooltipName(1, 64, "NC_ANA_GSD");
            else if (ApplicationManager.RackManager == RackManager.bolt)
                RacksValidation.ValidateControlTubeTooltipName(1, 96, "NC_ANA_GSD");
            else
                RacksValidation.ValidateControlTubeTooltipName(12, 16, "NC_ANA_GSD");

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
