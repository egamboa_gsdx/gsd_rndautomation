﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormDeselectTestSamples : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Deselect Test Samples"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples();
            WorklistMethods.AddTest(1, "ANA_GSD");
            WorklistMethods.AssignSample(1, 1); // Assign
            WorklistMethods.AssignSample(3, 1); // Assign
            WorklistMethods.AssignSample(1, 1); // Unassign
            WorklistMethods.AssignSample(3, 1); // Unassign

            // >>>>> Validation <<<<<

            WorklistValidation.ValidateUnassignSample(1, 1);
            WorklistValidation.ValidateUnassignSample(3, 1);

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
