﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormExcludeStripsAfterAddingTestAndSamples : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "MTP > Exclude Strips after Test File and Samples have been added"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");
            WorklistMethods.AddSamples();
            WorklistMethods.AssignAllSamplesToTest();
            MTPMethods.ExcludeStrips(1, 3);

            // >>>>> Validation <<<<<

            MTPValidation.ValidateEmptyStrip(1, 1);
            MTPValidation.ValidateEmptyStrip(1, 2);
            MTPValidation.ValidateEmptyStrip(1, 3);
            MTPValidation.ValidateWellName(1, 29, "Sample #1");

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
