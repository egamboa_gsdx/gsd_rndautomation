﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormExcludeStripsRetainsGroup : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "MTP > Exclude Strips does not break well grouping"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            WorklistMethods.AddTest(1, "ANA_GSD");
            WorklistMethods.AddSamples(numberOfSamples: 55);
            WorklistMethods.AssignAllSamplesToTest();
            MTPMethods.ExcludeStrips(1, 5);


            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                MTPValidation.ValidateEmptyStrip(1, 1);
                MTPValidation.ValidateEmptyStrip(1, 6);
                MTPValidation.ValidateEmptyStrip(1, 12);

                MTPValidation.ValidateWellName(2, 1, "Blank");
                MTPValidation.ValidateWellName(2, 5, "Sample #1");
                MTPValidation.ValidateWellName(2, 46, "Sample #42");
            }
            if (ApplicationManager.RackManager == RackManager.SIR) 
            {
                MTPValidation.ValidateEmptyStrip(1, 1);
                MTPValidation.ValidateEmptyStrip(1, 5);

                MTPValidation.ValidateWellName(1, 41, "Blank");
                MTPValidation.ValidateWellName(1, 60, "Sample #16");
            }
            if (ApplicationManager.RackManager == RackManager.bolt)
            {
                HelperMethods.Click(CommonProperty.Default.Ok);
                
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
