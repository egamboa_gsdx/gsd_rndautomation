﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormLoadWorklist : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Load Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            if (ApplicationManager.RackManager == RackManager.classic )
            {
                WorklistMethods.LoadWorklist("20_samples_10_assigned", "Test Data");
            }
            if (ApplicationManager.RackManager == RackManager.SIR) 
            {
                WorklistMethods.LoadWorklist("16_samples_8_assigned", "Test Data");
            }
            if (ApplicationManager.RackManager == RackManager.bolt)
            {
                WorklistMethods.LoadWorklist("20_samples_15_assigned", "Test Data");
            }
            

                // >>>>> Validation <<<<<
                if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistValidation.ValidateAddSamplesAutoIncrement(20);

                WorklistValidation.ValidateAddTest();

                WorklistValidation.ValidateAssignSample(1, 1);
                WorklistValidation.ValidateAssignSample(6, 1);
                WorklistValidation.ValidateAssignSample(10, 1);

                WorklistValidation.ValidateUnassignSample(11, 1);
                WorklistValidation.ValidateUnassignSample(16, 1);
                WorklistValidation.ValidateUnassignSample(20, 1);
            }
            else 
            {
                RacksValidation.ValidateSampleTubeName(1, 1, "Sample #1");
                RacksValidation.ValidateSampleTubeName(1, 8, "Sample #8");
                RacksValidation.ValidateSampleTubeName(1, 16, "Sample #16");
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
