﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormLoadWorklistReplacesCurrentWorklist : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Load Worklist Replaces Current Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddWorklistName("Current Worklist");
            //WorklistMethods.AddSamplesImport();
           if (ApplicationManager.InstrumentManager == InstrumentManager.Storm)
            {
                WorklistMethods.AddTest();
            }
            
            WorklistMethods.AddSamples(choice: GeneralProperty.Default.ImportAdd, controlScheme: 2);
            if (ApplicationManager.RackManager == RackManager.classic) 
            {
                WorklistMethods.LoadWorklist("20_samples_10_assigned", "Test Data");
                HomeValidation.ValidateWorklistName("20_samples_10_assigned");

                WorklistValidation.ValidateAddSamplesAutoIncrement(20);

                WorklistValidation.ValidateAssignSample(10, 1);
                WorklistValidation.ValidateUnassignSample(11, 1);

            }
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                WorklistMethods.LoadWorklist("20_samples_15_assigned", "Test Data");
                HomeValidation.ValidateWorklistName("20_samples_15_assigned");

                WorklistValidation.ValidateAddSamplesAutoIncrement(20);

                WorklistValidation.ValidateAssignSample(10, 1);
                WorklistValidation.ValidateUnassignSample(19, 1);

            }
            else 
            {
                WorklistMethods.LoadWorklist("16_samples_8_assigned", "Test Data");
                HomeValidation.ValidateWorklistName("16_samples_8_assigned");
                RacksValidation.ValidateSampleTubeName(1, 1, "Sample #1");
                RacksValidation.ValidateSampleTubeName(1, 8, "Sample #8");
                RacksValidation.ValidateSampleTubeName(1, 16, "Sample #16");
            }
            // >>>>> Validation <<<<<

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
