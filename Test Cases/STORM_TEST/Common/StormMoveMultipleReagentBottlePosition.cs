﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormMoveMultipleReagentBottlePosition : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Move Multiple Reagent Bottle Position"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest();
            if(ApplicationManager.RackManager == RackManager.classic)
            {
                RacksMethods.MoveReagentBottle(1, 5);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(2, 8);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(3, 14);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(4, 16);


                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(5, "Buffer_ANA_GSD");
                HelperMethods.Wait();
                RacksValidation.ValidateReagentBottleName(8, "Conjugate_ANA_GSD");
                HelperMethods.Wait();
                RacksValidation.ValidateReagentBottleName(14, "Substrate_ANA_GSD");
                HelperMethods.Wait();
                RacksValidation.ValidateReagentBottleName(16, "Stop_ANA_GSD");
            }
            else if(ApplicationManager.RackManager == RackManager.SIR)
            {
                RacksMethods.MoveReagentBottle(1, 6, 1);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(2, 7, 1);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(1, 6, 2);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(2, 7, 2);


                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(6, "Buffer_ANA_GSD", 1);
                HelperMethods.Wait();
                RacksValidation.ValidateReagentBottleName(7, "Substrate_ANA_GSD", 1);
                HelperMethods.Wait();
                RacksValidation.ValidateReagentBottleName(6, "Conjugate_ANA_GSD", 2);
                HelperMethods.Wait();
                RacksValidation.ValidateReagentBottleName(7, "Stop_ANA_GSD", 2);
            }
            else
            {
                RacksMethods.MoveReagentBottle(1, 4);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(2, 5);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(3, 6);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(4, 7);


                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(4, "Buffer_ANA_GSD");
                HelperMethods.Wait();
                RacksValidation.ValidateReagentBottleName(5, "Conjugate_ANA_GSD");
                HelperMethods.Wait();
                RacksValidation.ValidateReagentBottleName(6, "Substrate_ANA_GSD");
                HelperMethods.Wait();
                RacksValidation.ValidateReagentBottleName(7, "Stop_ANA_GSD");
            }
        }

            //--------------------//
            //  End of test case  //
            //--------------------//
        
    }
}
