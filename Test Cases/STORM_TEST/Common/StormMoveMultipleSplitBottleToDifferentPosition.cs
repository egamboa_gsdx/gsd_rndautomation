﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormMoveMultipleSplitBottleToDifferentPosition : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Move Multiple Split Bottle to Different Position"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            WorklistMethods.AddTest();

            if(ApplicationManager.RackManager == RackManager.classic)
            {
                // >>>>> Commands <<<<<
                RacksMethods.SplitReagentBottle(1);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(5, 10);
                HelperMethods.Wait();
                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(10, "Buffer_ANA_GSD");

                // >>>>> Commands <<<<<
                RacksMethods.SplitReagentBottle(2);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(5, 9);
                HelperMethods.Wait();
                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(9, "Conjugate_ANA_GSD");

                // >>>>> Commands <<<<<
                RacksMethods.SplitReagentBottle(3);
                HelperMethods.Wait();
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(5, 8);
                HelperMethods.Wait();
                HelperMethods.Wait();
                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(8, "Substrate_ANA_GSD");
            }
            else if (ApplicationManager.RackManager == RackManager.SIR)
            {
                // >>>>> Commands <<<<<
                RacksMethods.SplitReagentBottle(1, 1);
                RacksMethods.MoveReagentBottle(3, 6, 1);
                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(6, "Buffer_ANA_GSD", 1);

                // >>>>> Commands <<<<<
                RacksMethods.SplitReagentBottle(2, 1);
                RacksMethods.MoveReagentBottle(3, 7, 1);
                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(7, "Substrate_ANA_GSD", 1);

                // >>>>> Commands <<<<<
                RacksMethods.SplitReagentBottle(1, 2);
                RacksMethods.MoveReagentBottle(3, 6, 2);
                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(6, "Conjugate_ANA_GSD", 2);
            }
            else
            {
                // >>>>> Commands <<<<<
                RacksMethods.SplitReagentBottle(1);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(5, 7);
                HelperMethods.Wait();
                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(7, "Buffer_ANA_GSD");

                // >>>>> Commands <<<<<
                RacksMethods.SplitReagentBottle(2);
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(5, 9);
                HelperMethods.Wait();
                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(9, "Conjugate_ANA_GSD");

                // >>>>> Commands <<<<<
                RacksMethods.SplitReagentBottle(3);
                HelperMethods.Wait();
                HelperMethods.Wait();
                RacksMethods.MoveReagentBottle(5, 8);
                HelperMethods.Wait();
                HelperMethods.Wait();
                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(8, "Substrate_ANA_GSD");
            }
            

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
