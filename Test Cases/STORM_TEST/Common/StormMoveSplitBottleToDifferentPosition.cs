﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormMoveSplitBottleToDifferentPosition : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Move Split Bottle to Different Position"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest();

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                // >>>>> Commands <<<<<
               
                RacksMethods.SplitReagentBottle(1);
                RacksMethods.MoveReagentBottle(5, 10);

                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(10, "Buffer_ANA_GSD");
            }
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                // >>>>> Commands <<<<<
                
                RacksMethods.SplitReagentBottle(1,1);
                RacksMethods.MoveReagentBottle(5, 7, 1);

                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(7, "Buffer_ANA_GSD",1);
            }
            else
            {
                RacksMethods.SplitReagentBottle(1, 1);
                RacksMethods.MoveReagentBottle(3, 7, 1);

                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(7, "Buffer_ANA_GSD", 1);
            }
        }

            

            //--------------------//
            //  End of test case  //
            //--------------------//
        
    }
}
