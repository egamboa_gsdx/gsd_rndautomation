﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormRackReagentAndControlHoleEmptyAfterRemoveTest : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Rack Reagent and Control Hole Emptied after Removing Test"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");

            // >>>>> Validation <<<<<

            if(ApplicationManager.RackManager == RackManager.classic)
            {
                RacksValidation.ValidateReagentBottleName(1, "Buffer_ANA_GSD");
                RacksValidation.ValidateControlTubeTooltipName(1, 62, "PC_ANA_GSD");
            }
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksValidation.ValidateReagentBottleName(1, "Buffer_ANA_GSD", 1);
                RacksValidation.ValidateControlTubeTooltipName(1, 94, "PC_ANA_GSD");
            }
            


            // >>>>> Commands <<<<<

            WorklistMethods.RemoveTest();

            // >>>>> Validation <<<<<

            if(ApplicationManager.RackManager == RackManager.classic)
            {
                RacksValidation.ValidateReagentBottleName(1, "");
                RacksValidation.ValidateEmptySampleTubeTooltip(1, 62);
            }
            else if (ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksValidation.ValidateReagentBottleName(1, "", 1);
                RacksValidation.ValidateEmptySampleTubeTooltip(1, 94);
            }
            

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
