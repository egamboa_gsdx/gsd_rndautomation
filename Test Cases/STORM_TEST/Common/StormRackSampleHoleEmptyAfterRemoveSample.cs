﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormRackSampleHoleEmptyAfterRemoveSample : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Rack Sample Hole Emptied after Removing Sample"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples();

            // >>>>> Validation <<<<<

            RacksValidation.ValidateSampleTubeName(1, 1, "Sample #1");

            // >>>>> Commands <<<<<

            WorklistMethods.RemoveAllSamples();

            // >>>>> Validation <<<<<

            RacksValidation.ValidateEmptySampleTubeTooltip(1, 1);

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
