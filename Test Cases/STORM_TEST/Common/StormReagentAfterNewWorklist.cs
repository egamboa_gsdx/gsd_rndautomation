﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormReagentAfterNewWorklist : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Reagents After New Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            // Add samples to ensure the new worklist button works
            WorklistMethods.AddSamples();
            WorklistMethods.New();

            WorklistMethods.AddTest(1, "ANA_GSD");

            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                RacksValidation.ValidateReagentBottleName(2, "Conjugate_ANA_GSD");
                RacksValidation.ValidateReagentBottleName(3, "Substrate_ANA_GSD");
            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                RacksValidation.ValidateReagentBottleName(1, "Buffer_ANA_GSD", 13);
                RacksValidation.ValidateReagentBottleName(2, "Substrate_ANA_GSD", 13);
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
