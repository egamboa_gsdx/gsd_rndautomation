﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormReagentBottleTooltip : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Reagent Bottle Tooltip"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");

            // >>>>> Validation <<<<<

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                RacksValidation.ValidateReagentBottleTooltipName(1, "Buffer_ANA_GSD");
                RacksValidation.ValidateReagentBottleTooltipName(4, "Stop_ANA_GSD");
            }
            if (ApplicationManager.RackManager == RackManager.SIR)  
            {
                RacksValidation.ValidateReagentBottleTooltipName(2, "Substrate_ANA_GSD");
                RacksValidation.ValidateReagentBottleTooltipName(1, "Buffer_ANA_GSD");
                
            }

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
