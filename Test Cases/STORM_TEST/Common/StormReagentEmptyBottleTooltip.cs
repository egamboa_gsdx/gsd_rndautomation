﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormReagentEmptyBottleTooltip : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Reagent Empty Bottle Tooltip"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic) 
            {
                RacksValidation.ValidateEmptyReagentTooltip(1);
                RacksValidation.ValidateEmptyReagentTooltip(2);
                RacksValidation.ValidateEmptyReagentTooltip(3);

            }
            if (ApplicationManager.RackManager == RackManager.SIR)
            {
                RacksValidation.ValidateEmptyReagentTooltip(1);
                RacksValidation.ValidateEmptyReagentTooltip(2);
                RacksValidation.ValidateEmptyReagentTooltip(3);
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
