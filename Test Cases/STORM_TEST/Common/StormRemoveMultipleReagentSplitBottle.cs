﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormRemoveMultipleReagentSplitBottle : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Remove Multiple Reagent Split Bottles"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            WorklistMethods.AddTest(1, "ANA_GSD");
            if(ApplicationManager.RackManager == RackManager.classic || ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksMethods.SplitReagentBottle(1);
                RacksMethods.SplitReagentBottle(2);
                RacksMethods.SplitReagentBottle(3);

                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(5, "Buffer_ANA_GSD");
                RacksValidation.ValidateReagentBottleName(6, "Conjugate_ANA_GSD");
                RacksValidation.ValidateReagentBottleName(7, "Substrate_ANA_GSD");

                // >>>>> Commands <<<<<
                RacksMethods.RemoveSplitReagentBottle(5);
                RacksMethods.RemoveSplitReagentBottle(6);
                RacksMethods.RemoveSplitReagentBottle(7);

                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(5, "");
                RacksValidation.ValidateReagentBottleName(6, "");
                RacksValidation.ValidateReagentBottleName(7, "");
            }
        
            
            else
            {
                RacksMethods.SplitReagentBottle(1, 1);
                RacksMethods.SplitReagentBottle(2, 1);
                RacksMethods.SplitReagentBottle(1, 2);

                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(3, "Buffer_ANA_GSD", 1);
                RacksValidation.ValidateReagentBottleName(4, "Substrate_ANA_GSD", 1);
                RacksValidation.ValidateReagentBottleName(3, "Conjugate_ANA_GSD", 2);

                // >>>>> Commands <<<<<
                RacksMethods.RemoveSplitReagentBottle(3, 1);
                RacksMethods.RemoveSplitReagentBottle(4, 1);
                RacksMethods.RemoveSplitReagentBottle(3, 2);

                // >>>>> Validation <<<<<
                RacksValidation.ValidateReagentBottleName(3, "", 1);
                RacksValidation.ValidateReagentBottleName(4, "", 1);
                RacksValidation.ValidateReagentBottleName(3, "", 2);
            }
            

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
