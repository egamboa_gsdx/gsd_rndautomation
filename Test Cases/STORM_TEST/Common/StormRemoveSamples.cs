﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormRemoveSamples : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Remove Samples"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples();
            HelperMethods.Wait();
            WorklistMethods.RemoveAllSamples();

            // >>>>> Validation <<<<<

            WorklistValidation.ValidateRemoveAllSamples();

            // >>>>> Commands <<<<<

            WorklistMethods.New();

            WorklistMethods.AddSamples(numberOfSamples: 10);

            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.HighlightSamples(10);
                WorklistMethods.RemoveCurrentSamples();
            }
            if (ApplicationManager.RackManager == RackManager.SIR) 
            {
                WorklistMethods.HighlightSamples(16);
                WorklistMethods.RemoveCurrentSamples();
            }


            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistValidation.ValidateAddSamplesAutoIncrement(9);
            }
            if (ApplicationManager.RackManager == RackManager.SIR) 
            {
                WorklistValidation.ValidateAddSamplesAutoIncrement(15);
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
