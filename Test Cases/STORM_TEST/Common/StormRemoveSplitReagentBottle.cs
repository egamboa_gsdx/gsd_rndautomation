﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormRemoveSplitReagentBottle : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Remove Split Bottle"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");
            if(ApplicationManager.RackManager == RackManager.classic || ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksMethods.SplitReagentBottle(1);

                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(5, "Buffer_ANA_GSD");

                // >>>>> Commands <<<<<

                RacksMethods.RemoveSplitReagentBottle(5);

                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(5, "");
            }
            else
            {
                RacksMethods.SplitReagentBottle(1, 1);

                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(3, "Buffer_ANA_GSD", 1);

                // >>>>> Commands <<<<<

                RacksMethods.RemoveSplitReagentBottle(3, 1);

                // >>>>> Validation <<<<<

                RacksValidation.ValidateReagentBottleName(3, "", 1);
            }
            

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
