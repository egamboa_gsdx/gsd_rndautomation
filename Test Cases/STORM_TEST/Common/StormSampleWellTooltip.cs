﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormSampleWellTooltip : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "MTP > Sample Well Tooltip"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");
            WorklistMethods.AddSamples(numberOfSamples: 10);
            WorklistMethods.AssignAllSamplesToTest();

            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic) 
            {
                MTPValidation.ValidateWellTooltipName(1, 2, "NC");
                MTPValidation.ValidateSampleWellTooltipName(1, 5, "Sample #1");
                MTPValidation.ValidateSampleWellTooltipName(1, 14, "Sample #10");
                MTPValidation.ValidateEmptyWellTooltip(1, 15);
            }
            if (ApplicationManager.RackManager == RackManager.SIR) 
            {
                MTPValidation.ValidateWellTooltipName(1, 2, "NC");
                MTPValidation.ValidateSampleWellTooltipName(1, 5, "Sample #1");
                MTPValidation.ValidateSampleWellTooltipName(1, 14, "Sample #10");
                MTPValidation.ValidateEmptyWellTooltip(1, 21);
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
