﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormSaveWorklist : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Save Worklist"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddSamples(numberOfSamples: 16);
            WorklistMethods.AddTest(1, "ANA_GSD");
            WorklistMethods.AssignAllSamplesToTest();
            WorklistMethods.AddWorklistName("StromSaveWorklist");

            WorklistMethods.SaveWorklist();

            WorklistMethods.New();

            WorklistMethods.LoadWorklist("StromSaveWorklist", "Temp");

            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic || ApplicationManager.RackManager == RackManager.bolt)
            {
                WorklistValidation.ValidateAddSamplesAutoIncrement(16);
                WorklistValidation.ValidateAddTest();
                WorklistValidation.ValidateAssignAllSamples();
            }
            else 
            {
                WorklistValidation.ValidateAddTest();
                RacksValidation.ValidateSampleTubeName(1,1,"Sample #1");
                RacksValidation.ValidateSampleTubeName(1, 8, "Sample #8");
                RacksValidation.ValidateSampleTubeName(1, 16, "Sample #16");
                RacksValidation.ValidateControlTubeTooltipName(12, 14, "PC_ANA_GSD");
                RacksValidation.ValidateControlTubeTooltipName(12, 15, "CO_ANA_GSD");
                RacksValidation.ValidateControlTubeTooltipName(12, 16, "NC_ANA_GSD");
            }
               
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
