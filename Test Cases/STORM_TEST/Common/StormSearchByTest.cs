﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormSearchByTest : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "SEARCH > Search by Test"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

           // WorklistMethods.AddSamples(numberOfSamples: 10);
           // WorklistMethods.AddTest();
            //WorklistMethods.AssignAllSamplesToTest();
           // HomeMethods.SpecialStartSpecificPosition("Last");
           // HomeMethods.WaitForWorklistToFinish();

            SearchMethods.Search(test: "ANA_GSD");

            // >>>>> Validation <<<<<

            SearchValidation.ValidateAllSearchResults(test: "ANA_GSD");

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
