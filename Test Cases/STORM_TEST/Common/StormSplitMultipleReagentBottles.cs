﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormSplitMultipleReagentBottles : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "RACKS > Split Multiple Reagent Bottles"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");
            if(ApplicationManager.RackManager == RackManager.classic || ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksMethods.SplitReagentBottle(3);
                RacksMethods.SplitReagentBottle(1);
                RacksMethods.SplitReagentBottle(6);
            }
            else
            {
                RacksMethods.SplitReagentBottle(1, 1);
                RacksMethods.SplitReagentBottle(2, 1);
                RacksMethods.SplitReagentBottle(1, 2);

            }
            

            // >>>>> Validation <<<<<

            if(ApplicationManager.RackManager == RackManager.classic || ApplicationManager.RackManager == RackManager.bolt)
            {
                RacksValidation.ValidateReagentBottleName(5, "Substrate_ANA_GSD");
                RacksValidation.ValidateReagentBottleName(6, "Buffer_ANA_GSD");
                RacksValidation.ValidateReagentBottleName(7, "Buffer_ANA_GSD");
            }
            else
            {

                RacksValidation.ValidateReagentBottleName(3, "Buffer_ANA_GSD", 1);
                RacksValidation.ValidateReagentBottleName(4, "Substrate_ANA_GSD", 1);
                RacksValidation.ValidateReagentBottleName(3, "Conjugate_ANA_GSD", 2);
            }
            

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
