﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormStripLabelsAlignWithTest : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "MTP > Strips Labels align with the test"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            if (ApplicationManager.RackManager == RackManager.classic)
            {
                WorklistMethods.AddSamples(numberOfSamples: 24);
            }
            if (ApplicationManager.RackManager == RackManager.SIR) 
            {
                WorklistMethods.AddSamples();

            }
            else
            {
                WorklistMethods.AddSamples(numberOfSamples: 12);
            }
            //WorklistMethods.AddTest(1, "040 Vertical Strip");
            WorklistMethods.AddTest(1, "aaPARVOVIRUS IgM"); //couldnt find test file so added a different one
            WorklistMethods.AssignAllSamplesToTest();
            // >>>>> Validation <<<<<
            if (ApplicationManager.RackManager == RackManager.classic )
            {
                MTPValidation.ValidateStripLabelAlignment(1, 1);
                MTPValidation.ValidateStripLabelAlignment(1, 4);
                MTPValidation.ValidateStripLabelAlignment(1, 12);
                MTPValidation.ValidateStripLabelAlignment(2, 1);
                MTPValidation.ValidateStripLabelAlignment(2, 8);
                MTPValidation.ValidateStripLabelAlignment(2, 12);
            }
            if (ApplicationManager.RackManager == RackManager.SIR) 
            {
                MTPValidation.ValidateStripLabelAlignment(1, 1);
                MTPValidation.ValidateStripLabelAlignment(1, 4);
                MTPValidation.ValidateStripLabelAlignment(1, 12);
                MTPValidation.ValidateStripLabelAlignment(2, 1);
                MTPValidation.ValidateStripLabelAlignment(2, 4);

            }
            else
            {
                MTPValidation.ValidateStripLabelAlignment(1, 1);
                MTPValidation.ValidateStripLabelAlignment(1, 4);
                MTPValidation.ValidateStripLabelAlignment(1, 12);
                
            }
            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
