﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormUnassignAllSamplesFromTest : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "WORKLIST > Unassign ALL Samples from test"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            WorklistMethods.AddTest(1, "ANA_GSD");
            WorklistMethods.AddSamples(numberOfSamples: 10);
            WorklistMethods.AssignAllSamplesToTest(1); // Assign all
            WorklistMethods.AssignAllSamplesToTest(1); // Unassign all

            // >>>>> Validation <<<<<

            WorklistValidation.ValidateUnassignSample(1, 1);
            WorklistValidation.ValidateUnassignSample(2, 1);
            WorklistValidation.ValidateUnassignSample(6, 1);
            WorklistValidation.ValidateUnassignSample(10, 1);

            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
