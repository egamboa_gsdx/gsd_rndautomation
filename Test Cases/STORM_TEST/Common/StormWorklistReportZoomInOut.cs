﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Methods.AIX_STORM;
using GSD_RnDAutomation.Validations.AIX_STORM;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.STORM_TEST
{
    class StormWorklistReportZoomInOut : StormTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "EVALUATION > Zoom in/out"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<

            //EvaluationMethods.ZoomToPercentage(50); for some reason cannot reacognize text value so will rewrite testcase
            EvaluationValidation.RecordPageWidth();
            EvaluationMethods.ZoomIn();
            EvaluationMethods.ZoomIn();

            // >>>>> Validation <<<<<

            EvaluationValidation.ValidateZoomIn();

            // >>>>> Commands <<<<<

            //EvaluationMethods.ZoomToPercentage(100);

            EvaluationValidation.RecordPageWidth();
            EvaluationMethods.ZoomOut();
            EvaluationMethods.ZoomOut();

            // >>>>> Validation <<<<<

            EvaluationValidation.ValidateZoomOut();


            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
