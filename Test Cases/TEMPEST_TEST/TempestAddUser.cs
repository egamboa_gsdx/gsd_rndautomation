﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Settings;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Validations.TORRENT;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestAddUser : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "SETTINGS > Create New User"; }
        }

        public override void RunTest()
        {
            // Command
            string newUser = TempestUsersSettingsMethods.CreateUser("Test User", "testtest", "testtest",
                "abc@abc.com", "1234567890");

            // Validate
            TempestUserSettingsValidations.ValidateUserExists(newUser);
        }
    }
}
