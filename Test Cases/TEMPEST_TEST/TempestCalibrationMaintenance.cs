﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Maintenance;
using GSD_RnDAutomation.Validations.TORRENT;
using GSD_RnDAutomation.Validations.TORRENT.Maintenance;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestCalibrationMaintenance : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "MAINTENANCE > Navigate to Calibration Maintenance"; }
        }

        public override void RunTest()
        {
            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(TempestGeneralFindBy.HeaderTitleBy));
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Maintenance"))
            {
                TempestMaintenanceMethods.NavigateToMaintenance();
            }

            // Command
            TempestMaintenanceMethods.CalibrationMaintenance();

            // Validation
            TempestCalibrationMaintenanceValidations.ValidateCalibrationMaintenanceView();
        }
    }
}
