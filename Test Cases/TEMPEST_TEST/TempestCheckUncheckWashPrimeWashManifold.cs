﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Methods.TORRENT.Maintenance;
using GSD_RnDAutomation.Validations.TORRENT;
using GSD_RnDAutomation.Validations.TORRENT.Maintenance;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestCheckUncheckWashPrimeWashManifold : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "MAINTENANCE > Check/Uncheck Wash for Prime Wash Manifold"; }
        }

        public override void RunTest()
        {
            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(TempestGeneralFindBy.HeaderTitleBy));
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Maintenance"))
            {
                TempestMaintenanceMethods.NavigateToMaintenance();
            }

            TempestMaintenanceMethods.GeneralMaintenance();
            TempestGeneralMaintenanceMethods.OpenPrimeWashManifoldPopup();

            // Validate
            TempestGeneralMaintenanceValidations.ValidatePrimeWashManifoldPopup();

            // Command
            TempestPrimeWashManifoldMethods.CheckUncheckWash(2);

            // Validate
            TempestGeneralMaintenanceValidations.ValidateWashBottleCheckedUnChecked(false, 1);
            TempestGeneralMaintenanceValidations.ValidateWashBottleCheckedUnChecked(true, 2);
            TempestGeneralMaintenanceValidations.ValidateWashBottleCheckedUnChecked(false, 3);

            // Command
            TempestPrimeWashManifoldMethods.CheckUncheckWash(3);

            // Validate
            TempestGeneralMaintenanceValidations.ValidateWashBottleCheckedUnChecked(false, 1);
            TempestGeneralMaintenanceValidations.ValidateWashBottleCheckedUnChecked(false, 2);
            TempestGeneralMaintenanceValidations.ValidateWashBottleCheckedUnChecked(true, 3);

            // Command
            TempestPrimeWashManifoldMethods.CheckUncheckWash(1);

            // Validate
            TempestGeneralMaintenanceValidations.ValidateWashBottleCheckedUnChecked(true, 1);
            TempestGeneralMaintenanceValidations.ValidateWashBottleCheckedUnChecked(false, 2);
            TempestGeneralMaintenanceValidations.ValidateWashBottleCheckedUnChecked(false, 3);



            TempestPrimeWashManifoldMethods.ClosePrimeWashManifoldPopup();
        }
    }
}
