﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Validations.TORRENT;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestCurrentUser : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "GENERAL > Current User"; }
        }

        public override void RunTest()
        {
            // Validate
            TempestGeneralValidations.ValidateUser("TechService");

            // Act
            TempestGeneralMethods.ViewUser();

            // Validate
            TempestGeneralValidations.ValidateUserPopup("TechService", "Service user");

            // Make sure to close user popup
            TempestCurrentUserViewMethods.CloseCurrentUserPopup();
        }
    }
}
