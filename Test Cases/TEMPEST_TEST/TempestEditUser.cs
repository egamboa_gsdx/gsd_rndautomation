﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Settings;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Validations.TORRENT;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestEditUser : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "SETTINGS > Edit User"; }
        }

        public override void RunTest()
        {
            // Command
            // TODO Actually Edit a user
            TempestSettingsMethods.NavigateToUsersSettings();

            // Validation
            TempestUserSettingsValidations.ValidateUserInfo("Test User", "abc@abc.com", "1234567890", "Basic user");
        }
    }
}
