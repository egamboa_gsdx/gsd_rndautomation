﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Methods.TORRENT.Maintenance;
using GSD_RnDAutomation.Validations.TORRENT;
using GSD_RnDAutomation.Validations.TORRENT.Maintenance;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestIncreasePrimingCycle : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "MAINTENANCE > Increase Prime Wash Manifold Priming Cycles"; }
        }

        public override void RunTest()
        {
            // Command            
            ApplicationManager.WaitDriver?.Until(ExpectedConditions.ElementIsVisible(TempestGeneralFindBy.HeaderTitleBy));
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;
            if (!headerTitle.Equals("Maintenance"))
            {
                TempestMaintenanceMethods.NavigateToMaintenance();
            }

            TempestMaintenanceMethods.GeneralMaintenance();
            TempestGeneralMaintenanceMethods.OpenPrimeWashManifoldPopup();

            // Validate
            TempestGeneralMaintenanceValidations.ValidatePrimeWashManifoldPopup();

            // Command
            var currentPrimingCycle = TempestPrimeWashManifoldMethods.GetPrimingCyclesText().Text;
            var currentPrimingCycleint = int.Parse(currentPrimingCycle);
            TempestPrimeWashManifoldMethods.IncreasePrimingCycles(10);

            // Validate
            TempestGeneralMaintenanceValidations.ValidatePrimingCycle(currentPrimingCycleint + 10);

            TempestPrimeWashManifoldMethods.ClosePrimeWashManifoldPopup();
        }
    }
}
