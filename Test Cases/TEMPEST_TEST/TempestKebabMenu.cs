﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Validations.TORRENT;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestKebabMenu : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "GENERAL > Open & Close Kebab Menu"; }
        }

        public override void RunTest()
        {
            TempestGeneralMethods.OpenCloseKebabMenu();

            // Validate
            TempestGeneralValidations.KebabMenusIsOpen();

            TempestGeneralMethods.OpenCloseKebabMenu();

            // Validate
            TempestGeneralValidations.KebabMenuIsClosed();
        }
    }
}
