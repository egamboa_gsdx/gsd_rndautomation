﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Settings;
using GSD_RnDAutomation.Methods.TORRENT.Popups;
using GSD_RnDAutomation.Validations.TORRENT;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestManuallyAddSample : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "LOADING > Add Sample - Manually"; }
        }

        public override void RunTest()
        {
            // Command
            string samplename = "sample1";
            int index = 1;
            TempestLoadingMethods.AddSample(samplename, index);

            // Validation
            TempestLoadingValidations.ValidateAddedSample(samplename, index);

            // Remove sample to continue with other tests
            TempestLoadingMethods.RemoveSample(index);
        }
    }
}
