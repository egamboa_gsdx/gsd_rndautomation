﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Validations.TORRENT;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestOpenCloseStatusLog : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "HOME > Open & Close Status Log"; }
        }

        public override void RunTest()
        {
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;

            if(headerTitle != "Home")
            {
                TempestHomeMethods.NavigateToHome();
            }

            // Act
            TempestHomeMethods.OpenStatusLog();

            // Validate
            TempestHomeValidations.StatusLogOpened();

            // Act
            TempestHomeMethods.CloseStatusLog();

            // Validate
            TempestHomeValidations.StatusLogClosed();
        }
    }
}
