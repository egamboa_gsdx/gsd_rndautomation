﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Validations.TORRENT;

using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestSearchOnSampleId : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "RESULTS > Search on Sample ID"; }
        }

        public override void RunTest()
        {
            string headerTitle = TempestGeneralMethods.MainHeaderTitle;

            if(headerTitle != "Results")
            {
                TempestResultsMethods.NavigateToResults();
            }

            // Command
            var isChecked = TempestResultsMethods.CheckSearchOnSampleID();

            // Validate
            TempestResultsValidations.ValidateSearchOnSampleId(isChecked);

            // Command
            isChecked = TempestResultsMethods.CheckSearchOnSampleID();

            // Validate
            TempestResultsValidations.ValidateSearchOnSampleId(isChecked);
        }
    }
}
