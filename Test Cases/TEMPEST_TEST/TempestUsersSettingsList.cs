﻿using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.TORRENT.Elements;
using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Methods.TORRENT;
using GSD_RnDAutomation.Methods.TORRENT.Settings;
using GSD_RnDAutomation.Validations.TORRENT;
using GSD_RnDAutomation.Validations.TORRENT.Maintenance;

using System.Threading;
using System.Collections;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using GSD_RnDAutomation.Common.Exceptions;
using SeleniumExtras.WaitHelpers;

namespace GSD_RnDAutomation.Test_Cases.TEMPEST_TEST
{
    class TempestUsersSettingsList : TempestTestCase
    {
        public override string TestCaseName
        {
            get { return "SETTINGS > Get Users List in Users Settings"; }
        }

        public override void RunTest()
        {
            // Command
            TempestSettingsMethods.NavigateToUsersSettings();
            var userList = TempestUsersSettingsMethods.UsersList;

            // Validate
            if (userList != null)
                TempestUserSettingsValidations.ValidateUserExists("Test User");
        }
    }
}
