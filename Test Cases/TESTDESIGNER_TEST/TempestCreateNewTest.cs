﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;
using GSD_RnDAutomation.Lib.Methods.TESTDESIGNER.StartScreen;
//using GSD_RnDAutomation.Methods.TESTDESIGNER;
//using GSD_RnDAutomation.Validations.TESTDESIGNER;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.TESTDESIGNER_TEST
{
    class TempestCreateNewTest : TempestTestDesignerTestCase
    {
        public override string TestCaseName
        {
            //------------------//
            //  Test case name  //
            //------------------//
            get { return "STARTSCREEN> Create New Test"; }
        }

        public override void RunTest()
        {
            //--------------------------------//
            //  Start writing test case here  //
            //--------------------------------//

            // >>>>> Commands <<<<<
            StartScreenMethods.CreateNewTest(testName: "HIV", testAuthor: "Nav", revisions: 5, stripFormat: "GSD Fast Strips", readerFormat: "Absorbance");


            // >>>>> Validation <<<<<



            //--------------------//
            //  End of test case  //
            //--------------------//
        }
    }
}
