﻿using GSD_RnDAutomation.Common.Methods;
using GSD_RnDAutomation.Common.Testing;

//using GSD_RnDAutomation.Methods.TEST DESIGNER;
//using GSD_RnDAutomation.Validations.TEST DESIGNER;

/* TEST CASE
 * Enter the name of the test case in the quotation marks below the box labeled "Test case name"
 * Enter commands below the ">>>>> Commands <<<<<" section
 * Enter validation below the ">>>>> Validation <<<<<" section
 * Lists of possible commmands and validation will appear and methods can be autofilled.
 */

namespace GSD_RnDAutomation.Testing.Test_Cases.TESTDESIGNER_TEST
{
    class TempestLaunchStartScreen : TempestTestDesignerTestCase
{
    public override string TestCaseName
    {
        //------------------//
        //  Test case name  //
        //------------------//
        get { return "LAUNCH > Tempest Test Designer Start Screen "; }
    }

    public override void RunTest()
    {
        //--------------------------------//
        //  Start writing test case here  //
        //--------------------------------//

        // >>>>> Commands <<<<<

            

        // >>>>> Validation <<<<<



        //--------------------//
        //  End of test case  //
        //--------------------//
    }
}
}
